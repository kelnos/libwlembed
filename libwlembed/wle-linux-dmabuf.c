/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>

#include "protocol/linux-dmabuf-unstable-v1-protocol.h"
#include "wle-linux-dmabuf.h"
#include "wle-private-types.h"
#include "wle-util.h"
#include "wle-view.h"

struct _WleLinuxDmabuf {
    struct wl_display *remote_display;
    struct zwp_linux_dmabuf_v1 *remote_linux_dmabuf;

    struct wl_global *local_linux_dmabuf;
    struct wl_list local_linux_dmabuf_resources;

    GArray *formats;  // uint32_t
    GArray *modifiers;  // WleModifier

    struct wl_list params;  // WleDmabufParams;
    struct wl_list feedbacks;  // WleDmabufFeedback
};

typedef struct {
    struct wl_display *remote_display;
    struct zwp_linux_buffer_params_v1 *remote_params;
    struct wl_resource *local_params;
    struct wl_list buffers;  // WleBuffer
    struct wl_list link;
} WleDmabufParams;

typedef struct {
    struct zwp_linux_dmabuf_feedback_v1 *remote_feedback;
    struct wl_resource *local_feedback;
    WleView *view;
    struct wl_list link;
} WleDmabufFeedback;

typedef struct {
    uint32_t format;
    uint32_t modifier_hi;
    uint32_t modifier_lo;
} WleModifier;

static void remote_params_created(void *data,
                                  struct zwp_linux_buffer_params_v1 *remote_params,
                                  struct wl_buffer *buffer);
static void remote_params_failed(void *data,
                                 struct zwp_linux_buffer_params_v1 *remote_params);

static void local_params_add(struct wl_client *client,
                             struct wl_resource *params_resource,
                             int fd,
                             uint32_t plane_idx,
                             uint32_t offset,
                             uint32_t stride,
                             uint32_t modifier_hi,
                             uint32_t modifier_lo);
static void local_params_create(struct wl_client *client,
                                struct wl_resource *params_resource,
                                int32_t width,
                                int32_t height,
                                uint32_t format,
                                uint32_t flags);
static void local_params_create_immed(struct wl_client *client,
                                      struct wl_resource *params_resource,
                                      uint32_t buffer_id,
                                      int32_t width,
                                      int32_t height,
                                      uint32_t format,
                                      uint32_t flags);
static void local_params_destroy(struct wl_client *client,
                                 struct wl_resource *params_resource);

static void remote_feedback_done(void *data,
                                 struct zwp_linux_dmabuf_feedback_v1 *remote_feedback);
static void remote_feedback_format_table(void *data,
                                         struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                                         int fd,
                                         uint32_t size);
static void remote_feedback_main_device(void *data,
                                        struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                                        struct wl_array *device);
static void remote_feedback_tranche_done(void *data,
                                         struct zwp_linux_dmabuf_feedback_v1 *remote_feedback);
static void remote_feedback_tranche_target_device(void *data,
                                                  struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                                                  struct wl_array *device);
static void remote_feedback_tranche_formats(void *data,
                                            struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                                            struct wl_array *indices);
static void remote_feedback_tranche_flags(void *data,
                                          struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                                          uint32_t flags);

static void local_feedback_destroy(struct wl_client *client,
                                   struct wl_resource *feedback_resource);

static void remote_dmabuf_format(void *data,
                                 struct zwp_linux_dmabuf_v1 *remote_dmabuf,
                                 uint32_t format);
static void remote_dmabuf_modifier(void *data,
                                   struct zwp_linux_dmabuf_v1 *remote_dmabuf,
                                   uint32_t format,
                                   uint32_t modifier_hi,
                                   uint32_t modifier_lo);

static void local_dmabuf_create_params(struct wl_client *client,
                                       struct wl_resource *dmabuf_resource,
                                       uint32_t params_id);
static void local_dmabuf_get_default_feedback(struct wl_client *client,
                                              struct wl_resource *dmabuf_resource,
                                              uint32_t feedback_id);
static void local_dmabuf_get_surface_feedback(struct wl_client *client,
                                              struct wl_resource *dmabuf_resource,
                                              uint32_t feedback_id,
                                              struct wl_resource *surface_resource);
static void local_dmabuf_destroy(struct wl_client *client,
                                 struct wl_resource *dmabuf_resource);

static const struct zwp_linux_buffer_params_v1_listener remote_params_listener = {
    .created = remote_params_created,
    .failed = remote_params_failed,
};

static const struct zwp_linux_buffer_params_v1_interface local_params_impl = {
    .add = local_params_add,
    .create = local_params_create,
    .create_immed = local_params_create_immed,
    .destroy = local_params_destroy,
};

static const struct zwp_linux_dmabuf_feedback_v1_listener remote_feedback_listener = {
    .done = remote_feedback_done,
    .format_table = remote_feedback_format_table,
    .main_device = remote_feedback_main_device,
    .tranche_done = remote_feedback_tranche_done,
    .tranche_target_device = remote_feedback_tranche_target_device,
    .tranche_formats = remote_feedback_tranche_formats,
    .tranche_flags = remote_feedback_tranche_flags,
};

static const struct zwp_linux_dmabuf_feedback_v1_interface local_feedback_impl = {
    .destroy = local_feedback_destroy,
};

static const struct zwp_linux_dmabuf_v1_listener remote_dmabuf_listener = {
    .format = remote_dmabuf_format,
    .modifier = remote_dmabuf_modifier,
};

static const struct zwp_linux_dmabuf_v1_interface local_dmabuf_impl = {
    .create_params = local_dmabuf_create_params,
    .get_default_feedback = local_dmabuf_get_default_feedback,
    .get_surface_feedback = local_dmabuf_get_surface_feedback,
    .destroy = local_dmabuf_destroy,
};

static void
remote_params_created(void *data, struct zwp_linux_buffer_params_v1 *remote_params, struct wl_buffer *remote_buffer) {
    WleDmabufParams *params = data;

    struct wl_resource *resource = wl_resource_create(wl_resource_get_client(params->local_params),
                                                      &wl_buffer_interface,
                                                      wl_proxy_get_version((struct wl_proxy *)remote_buffer),
                                                      0);
    if (resource == NULL) {
        zwp_linux_buffer_params_v1_send_failed(params->local_params);
    } else {
        WleBuffer *buffer = wle_buffer_new(remote_buffer, resource);
        wl_list_insert(&params->buffers, wle_buffer_get_link(buffer));
        zwp_linux_buffer_params_v1_send_created(params->local_params, resource);
    }
}

static void
remote_params_failed(void *data, struct zwp_linux_buffer_params_v1 *remote_params) {
    WleDmabufParams *params = data;
    zwp_linux_buffer_params_v1_send_failed(params->local_params);
}

static void
local_params_add(struct wl_client *client,
                 struct wl_resource *params_resource,
                 int fd,
                 uint32_t plane_idx,
                 uint32_t offset,
                 uint32_t stride,
                 uint32_t modifier_hi,
                 uint32_t modifier_lo) {
    WleDmabufParams *params = wl_resource_get_user_data(params_resource);
    zwp_linux_buffer_params_v1_add(params->remote_params, fd, plane_idx, offset, stride, modifier_hi, modifier_lo);
    close(fd);
}

static void
local_params_create(struct wl_client *client,
                    struct wl_resource *params_resource,
                    int32_t width,
                    int32_t height,
                    uint32_t format,
                    uint32_t flags) {
    WleDmabufParams *params = wl_resource_get_user_data(params_resource);
    zwp_linux_buffer_params_v1_create(params->remote_params, width, height, format, flags);
}

static void
local_params_create_immed(struct wl_client *client,
                          struct wl_resource *params_resource,
                          uint32_t buffer_id,
                          int32_t width,
                          int32_t height,
                          uint32_t format,
                          uint32_t flags) {
    WleDmabufParams *params = wl_resource_get_user_data(params_resource);

    struct wl_buffer *remote_buffer = zwp_linux_buffer_params_v1_create_immed(params->remote_params,
                                                                              width,
                                                                              height,
                                                                              format,
                                                                              flags);
    if (remote_buffer == NULL) {
        wle_propagate_remote_error(params->remote_display, client, params_resource);
    } else {

        struct wl_resource *resource = wl_resource_create(wl_resource_get_client(params->local_params),
                                                          &wl_buffer_interface,
                                                          wl_proxy_get_version((struct wl_proxy *)remote_buffer),
                                                          buffer_id);
        if (resource == NULL) {
            wl_buffer_destroy(remote_buffer);
            wl_client_post_no_memory(client);
        } else {
            WleBuffer *buffer = wle_buffer_new(remote_buffer, resource);
            wl_list_insert(&params->buffers, wle_buffer_get_link(buffer));
        }
    }
}

static void
local_params_destroy(struct wl_client *client, struct wl_resource *params_resource) {
    wl_resource_destroy(params_resource);
}

static void
local_params_destroy_impl(struct wl_resource *resource) {
    WleDmabufParams *params = wl_resource_get_user_data(resource);

    WleBuffer *buffer, *tmp_buffer;
    wle_buffer_for_each_safe(buffer, tmp_buffer, &params->buffers) {
        wle_buffer_destroy(buffer);
    }

    if (params->remote_params != NULL) {
        zwp_linux_buffer_params_v1_destroy(params->remote_params);
    }

    wl_list_remove(&params->link);
    g_free(params);
}

static void
remote_dmabuf_format(void *data, struct zwp_linux_dmabuf_v1 *remote_dmabuf, uint32_t format) {
    WleLinuxDmabuf *dmabuf = data;

    g_array_append_val(dmabuf->formats, format);

    struct wl_resource *resource;
    wl_resource_for_each(resource, &dmabuf->local_linux_dmabuf_resources) {
        // format event must not be sent as of v4, when get_default_feedback was added instead
        if (wl_resource_get_version(resource) < ZWP_LINUX_DMABUF_V1_GET_DEFAULT_FEEDBACK_SINCE_VERSION) {
            zwp_linux_dmabuf_v1_send_format(resource, format);
        }
    }
}

static void
remote_dmabuf_modifier(void *data,
                       struct zwp_linux_dmabuf_v1 *remote_dmabuf,
                       uint32_t format,
                       uint32_t modifier_hi,
                       uint32_t modifier_lo) {
    WleLinuxDmabuf *dmabuf = data;

    WleModifier modifier = {
        .format = format,
        .modifier_hi = modifier_hi,
        .modifier_lo = modifier_lo,
    };
    g_array_append_val(dmabuf->modifiers, modifier);

    struct wl_resource *resource;
    wl_resource_for_each(resource, &dmabuf->local_linux_dmabuf_resources) {
        // modifier event must not be sent as of v4, when get_default_feedback was added instead
        uint32_t version = wl_resource_get_version(resource);
        if (version >= ZWP_LINUX_DMABUF_V1_MODIFIER_SINCE_VERSION
            && version < ZWP_LINUX_DMABUF_V1_GET_DEFAULT_FEEDBACK_SINCE_VERSION)
        {
            zwp_linux_dmabuf_v1_send_format(resource, format);
        }
    }
}

static void
local_dmabuf_create_params(struct wl_client *client, struct wl_resource *dmabuf_resource, uint32_t params_id) {
    WleLinuxDmabuf *dmabuf = wl_resource_get_user_data(dmabuf_resource);
    struct wl_resource *resource = wl_resource_create(client,
                                                      &zwp_linux_buffer_params_v1_interface,
                                                      wl_resource_get_version(dmabuf_resource),
                                                      params_id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        struct zwp_linux_buffer_params_v1 *remote_params = zwp_linux_dmabuf_v1_create_params(dmabuf->remote_linux_dmabuf);
        if (remote_params == NULL) {
            wl_resource_destroy(resource);
        } else {
            WleDmabufParams *params = g_new0(WleDmabufParams, 1);
            params->remote_display = dmabuf->remote_display;
            params->remote_params = _wle_proxy_set_ours((struct wl_proxy *)remote_params);
            params->local_params = resource;
            wl_list_init(&params->buffers);
            wl_list_insert(&dmabuf->params, &params->link);
            zwp_linux_buffer_params_v1_add_listener(params->remote_params, &remote_params_listener, params);
            wl_resource_set_implementation(resource, &local_params_impl, params, local_params_destroy_impl);
        }
    }
}

static void
remote_feedback_done(void *data, struct zwp_linux_dmabuf_feedback_v1 *remote_feedback) {
    WleDmabufFeedback *feedback = data;
    zwp_linux_dmabuf_feedback_v1_send_done(feedback->local_feedback);
}

static void
remote_feedback_format_table(void *data, struct zwp_linux_dmabuf_feedback_v1 *remote_feedback, int fd, uint32_t size) {
    WleDmabufFeedback *feedback = data;
    zwp_linux_dmabuf_feedback_v1_send_format_table(feedback->local_feedback, fd, size);
    close(fd);
}

static void
remote_feedback_main_device(void *data, struct zwp_linux_dmabuf_feedback_v1 *remote_feedback, struct wl_array *device) {
    WleDmabufFeedback *feedback = data;
    zwp_linux_dmabuf_feedback_v1_send_main_device(feedback->local_feedback, device);
}

static void
remote_feedback_tranche_done(void *data, struct zwp_linux_dmabuf_feedback_v1 *remote_feedback) {
    WleDmabufFeedback *feedback = data;
    zwp_linux_dmabuf_feedback_v1_send_tranche_done(feedback->local_feedback);
}

static void
remote_feedback_tranche_target_device(void *data,
                                      struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                                      struct wl_array *device) {
    WleDmabufFeedback *feedback = data;
    zwp_linux_dmabuf_feedback_v1_send_tranche_target_device(feedback->local_feedback, device);
    ;
}

static void
remote_feedback_tranche_formats(void *data,
                                struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                                struct wl_array *indices) {
    WleDmabufFeedback *feedback = data;
    zwp_linux_dmabuf_feedback_v1_send_tranche_formats(feedback->local_feedback, indices);
}

static void
remote_feedback_tranche_flags(void *data, struct zwp_linux_dmabuf_feedback_v1 *remote_feedback, uint32_t flags) {
    WleDmabufFeedback *feedback = data;
    zwp_linux_dmabuf_feedback_v1_send_tranche_flags(feedback->local_feedback, flags);
}

static void
local_feedback_destroy(struct wl_client *client, struct wl_resource *feedback_resource) {
    wl_resource_destroy(feedback_resource);
}

static void
feedback_view_destroyed(WleView *view, WleDmabufFeedback *feedback) {
    g_signal_handlers_disconnect_by_data(view, feedback);
    wl_resource_destroy(feedback->local_feedback);
}

static void
local_feedback_destroy_impl(struct wl_resource *resource) {
    WleDmabufFeedback *feedback = wl_resource_get_user_data(resource);
    if (feedback->remote_feedback != NULL) {
        zwp_linux_dmabuf_feedback_v1_destroy(feedback->remote_feedback);
    }
    if (feedback->view != NULL) {
        g_signal_handlers_disconnect_by_data(feedback->view, feedback);
    }

    wl_list_remove(&feedback->link);
    g_free(feedback);
}

static void
create_feedback(WleLinuxDmabuf *dmabuf,
                struct wl_resource *dmabuf_resource,
                struct zwp_linux_dmabuf_feedback_v1 *remote_feedback,
                uint32_t feedback_id,
                WleView *view) {
    if (remote_feedback == NULL) {
        wle_propagate_remote_error(dmabuf->remote_display, wl_resource_get_client(dmabuf_resource), dmabuf_resource);
    } else {
        struct wl_resource *resource = wl_resource_create(wl_resource_get_client(dmabuf_resource),
                                                          &zwp_linux_dmabuf_feedback_v1_interface,
                                                          wl_resource_get_version(dmabuf_resource),
                                                          feedback_id);
        if (resource == NULL) {
            zwp_linux_dmabuf_feedback_v1_destroy(remote_feedback);
            wl_client_post_no_memory(wl_resource_get_client(dmabuf_resource));
        } else {
            WleDmabufFeedback *feedback = g_new0(WleDmabufFeedback, 1);
            feedback->remote_feedback = _wle_proxy_set_ours((struct wl_proxy *)remote_feedback);
            feedback->local_feedback = resource;
            feedback->view = view;
            wl_list_insert(&dmabuf->feedbacks, &feedback->link);
            wl_resource_set_implementation(resource, &local_feedback_impl, feedback, local_feedback_destroy_impl);
            zwp_linux_dmabuf_feedback_v1_add_listener(remote_feedback, &remote_feedback_listener, feedback);
            if (view != NULL) {
                g_signal_connect(view, "destroy",
                                 G_CALLBACK(feedback_view_destroyed), feedback);
            }
        }
    }
}

static void
local_dmabuf_get_default_feedback(struct wl_client *client, struct wl_resource *dmabuf_resource, uint32_t feedback_id) {
    WleLinuxDmabuf *dmabuf = wl_resource_get_user_data(dmabuf_resource);
    struct zwp_linux_dmabuf_feedback_v1 *remote_feedback = zwp_linux_dmabuf_v1_get_default_feedback(dmabuf->remote_linux_dmabuf);
    create_feedback(dmabuf, dmabuf_resource, remote_feedback, feedback_id, NULL);
}

static void
local_dmabuf_get_surface_feedback(struct wl_client *client,
                                  struct wl_resource *dmabuf_resource,
                                  uint32_t feedback_id,
                                  struct wl_resource *surface_resource) {
    WleLinuxDmabuf *dmabuf = wl_resource_get_user_data(dmabuf_resource);
    WleView *view = surface_resource != NULL ? WLE_VIEW(wl_resource_get_user_data(surface_resource)) : NULL;

    if (view == NULL) {
        wl_resource_post_error(dmabuf_resource,
                               WL_DISPLAY_ERROR_INVALID_OBJECT,
                               "no surface passed");
    } else {
        struct zwp_linux_dmabuf_feedback_v1 *remote_feedback = zwp_linux_dmabuf_v1_get_surface_feedback(dmabuf->remote_linux_dmabuf,
                                                                                                        wle_view_get_remote_surface(view));
        create_feedback(dmabuf, dmabuf_resource, remote_feedback, feedback_id, view);
    }
}

static void
local_dmabuf_destroy(struct wl_client *client, struct wl_resource *dmabuf_resource) {
    wl_resource_destroy(dmabuf_resource);
}

static void
local_dmabuf_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_dmabuf_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleLinuxDmabuf *dmabuf = data;

    struct wl_resource *resource = wl_resource_create(client,
                                                      &zwp_linux_dmabuf_v1_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource,
                                       &local_dmabuf_impl,
                                       dmabuf,
                                       local_dmabuf_destroy_impl);
        if (dmabuf == NULL) {
            wl_list_init(wl_resource_get_link(resource));
        } else {
            wl_list_insert(&dmabuf->local_linux_dmabuf_resources, wl_resource_get_link(resource));

            if (version < ZWP_LINUX_DMABUF_V1_GET_DEFAULT_FEEDBACK_SINCE_VERSION) {
                for (guint i = 0; i < dmabuf->formats->len; ++i) {
                    uint32_t format = g_array_index(dmabuf->formats, uint32_t, i);
                    zwp_linux_dmabuf_v1_send_format(resource, format);
                }
            }

            if (version >= ZWP_LINUX_DMABUF_V1_MODIFIER_SINCE_VERSION
                && version < ZWP_LINUX_DMABUF_V1_GET_DEFAULT_FEEDBACK_SINCE_VERSION)
            {
                for (guint i = 0; i < dmabuf->formats->len; ++i) {
                    WleModifier *modifier = &g_array_index(dmabuf->modifiers, WleModifier, i);
                    zwp_linux_dmabuf_v1_send_modifier(resource,
                                                      modifier->format,
                                                      modifier->modifier_hi,
                                                      modifier->modifier_lo);
                }
            }
        }
    }
}

WleLinuxDmabuf *
wle_linux_dmabuf_new(struct wl_display *remote_display,
                     struct zwp_linux_dmabuf_v1 *remote_linux_dmabuf,
                     struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_linux_dmabuf != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleLinuxDmabuf *dmabuf = g_new0(WleLinuxDmabuf, 1);
    dmabuf->local_linux_dmabuf = wl_global_create(local_display,
                                                  &zwp_linux_dmabuf_v1_interface,
                                                  wl_proxy_get_version((struct wl_proxy *)remote_linux_dmabuf),
                                                  dmabuf,
                                                  local_dmabuf_bind);
    if (dmabuf->local_linux_dmabuf == NULL) {
        zwp_linux_dmabuf_v1_destroy(remote_linux_dmabuf);
        g_free(dmabuf);
        return NULL;
    } else {
        dmabuf->remote_display = remote_display;
        dmabuf->remote_linux_dmabuf = remote_linux_dmabuf;
        dmabuf->formats = g_array_new(FALSE, TRUE, sizeof(uint32_t));
        dmabuf->modifiers = g_array_new(FALSE, TRUE, sizeof(WleModifier));
        wl_list_init(&dmabuf->local_linux_dmabuf_resources);
        wl_list_init(&dmabuf->params);
        wl_list_init(&dmabuf->feedbacks);
        zwp_linux_dmabuf_v1_add_listener(dmabuf->remote_linux_dmabuf, &remote_dmabuf_listener, dmabuf);
        return dmabuf;
    }
}

void
wle_linux_dmabuf_destroy(WleLinuxDmabuf *dmabuf) {
    if (dmabuf != NULL) {
        WleDmabufFeedback *feedback, *tmp_feedback;
        wl_list_for_each_safe(feedback, tmp_feedback, &dmabuf->feedbacks, link) {
            wl_resource_destroy(feedback->local_feedback);
        }

        WleDmabufParams *params, *tmp_params;
        wl_list_for_each_safe(params, tmp_params, &dmabuf->params, link) {
            wl_resource_destroy(params->local_params);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &dmabuf->local_linux_dmabuf_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(dmabuf->local_linux_dmabuf);

        zwp_linux_dmabuf_v1_destroy(dmabuf->remote_linux_dmabuf);

        g_array_free(dmabuf->formats, TRUE);
        g_array_free(dmabuf->modifiers, TRUE);

        g_free(dmabuf);
    }
}
