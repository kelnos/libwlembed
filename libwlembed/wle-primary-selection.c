/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <unistd.h>

#include "common/wle-debug.h"
#include "protocol/primary-selection-unstable-v1-protocol.h"
#include "wle-primary-selection.h"
#include "wle-seat-private.h"
#include "wle-util.h"

typedef struct _WlePSOffer WlePSOffer;

struct _WlePrimarySelection {
    struct wl_display *remote_display;
    struct zwp_primary_selection_device_manager_v1 *remote_device_manager;

    struct wl_global *local_device_manager;
    struct wl_list local_device_manager_resources;

    struct wl_list devices;  // WlePSDevice
    struct wl_list sources;  // WlePSSource
};

typedef struct _WlePSDevice {
    WleSeat *seat;

    struct zwp_primary_selection_device_v1 *remote_device;
    struct wl_resource *local_device;

    WlePSOffer *cur_selection;
    struct wl_list offers;  // WlePSOffer
    struct wl_list orphan_offer_resources;
} WlePSDevice;

struct _WlePSOffer {
    WlePSDevice *device;

    struct zwp_primary_selection_offer_v1 *remote_offer;
    struct wl_resource *local_offer;

    GPtrArray *mime_types;

    struct wl_list link;
};

typedef struct _WlePSSource {
    struct zwp_primary_selection_source_v1 *remote_source;
    struct wl_resource *local_source;
} WlePSSource;

static void remote_source_send(
    void *data,
    struct zwp_primary_selection_source_v1 *remote_source,
    const char *mime_type,
    int fd);
static void remote_source_cancelled(
    void *data,
    struct zwp_primary_selection_source_v1 *remote_source);

static void local_source_offer(
    struct wl_client *client,
    struct wl_resource *source_resource,
    const char *mime_type);
static void local_source_destroy(
    struct wl_client *client,
    struct wl_resource *source_resource);

static void remote_offer_offer(
    void *data,
    struct zwp_primary_selection_offer_v1 *remote_offer,
    const char *mime_type);

static void local_offer_receive(
    struct wl_client *client,
    struct wl_resource *offer_resource,
    const char *mime_type,
    int fd);
static void local_offer_destroy(
    struct wl_client *client,
    struct wl_resource *offer_resource);

static void remote_device_data_offer(
    void *data,
    struct zwp_primary_selection_device_v1 *remote_device,
    struct zwp_primary_selection_offer_v1 *offer);
static void remote_device_selection(
    void *data,
    struct zwp_primary_selection_device_v1 *remote_device,
    struct zwp_primary_selection_offer_v1 *id /* nullable */);

static void local_device_set_selection(
    struct wl_client *client,
    struct wl_resource *device_resource,
    struct wl_resource *source_resource /* nullable */,
    uint32_t serial);
static void local_device_destroy(
    struct wl_client *client,
    struct wl_resource *device_resource);

static void local_device_manager_create_source(
    struct wl_client *client,
    struct wl_resource *device_manager_resource,
    uint32_t id_id);
static void local_device_manager_get_device(
    struct wl_client *client,
    struct wl_resource *device_manager_resource,
    uint32_t id_id,
    struct wl_resource *seat_resource);
static void local_device_manager_destroy(
    struct wl_client *client,
    struct wl_resource *device_manager_resource);

static const struct zwp_primary_selection_source_v1_listener remote_source_listener = {
    .send = remote_source_send,
    .cancelled = remote_source_cancelled,
};

static const struct zwp_primary_selection_source_v1_interface local_source_impl = {
    .offer = local_source_offer,
    .destroy = local_source_destroy,
};

static const struct zwp_primary_selection_offer_v1_listener remote_offer_listener = {
    .offer = remote_offer_offer,
};

static const struct zwp_primary_selection_offer_v1_interface local_offer_impl = {
    .receive = local_offer_receive,
    .destroy = local_offer_destroy,
};

static const struct zwp_primary_selection_device_v1_listener remote_device_listener = {
    .data_offer = remote_device_data_offer,
    .selection = remote_device_selection,
};

static const struct zwp_primary_selection_device_v1_interface local_device_impl = {
    .set_selection = local_device_set_selection,
    .destroy = local_device_destroy,
};

static const struct zwp_primary_selection_device_manager_v1_interface local_device_manager_impl = {
    .create_source = local_device_manager_create_source,
    .get_device = local_device_manager_get_device,
    .destroy = local_device_manager_destroy,
};

static void
remote_source_send(
    void *data,
    struct zwp_primary_selection_source_v1 *remote_source,
    const char *mime_type,
    int fd) {
    TRACE("entering");
    WlePSSource *source = data;
    zwp_primary_selection_source_v1_send_send(source->local_source, mime_type, fd);
    close(fd);
}

static void
remote_source_cancelled(
    void *data,
    struct zwp_primary_selection_source_v1 *remote_source) {
    TRACE("entering");
    WlePSSource *source = data;
    zwp_primary_selection_source_v1_send_cancelled(source->local_source);
}

static void
local_source_offer(
    struct wl_client *client,
    struct wl_resource *source_resource,
    const char *mime_type) {
    TRACE("entering");
    WlePSSource *source = wl_resource_get_user_data(source_resource);
    zwp_primary_selection_source_v1_offer(source->remote_source, mime_type);
}

static void
local_source_destroy(
    struct wl_client *client,
    struct wl_resource *source_resource) {
    TRACE("entering");
    wl_resource_destroy(source_resource);
}

static void
local_source_destroy_impl(struct wl_resource *source_resource) {
    TRACE("entering");
    WlePSSource *source = wl_resource_get_user_data(source_resource);
    zwp_primary_selection_source_v1_destroy(source->remote_source);
    wl_list_remove(wl_resource_get_link(source_resource));
    g_free(source);
}

static void
remote_offer_offer(
    void *data,
    struct zwp_primary_selection_offer_v1 *remote_offer,
    const char *mime_type) {
    TRACE("entering");
    WlePSOffer *offer = data;
    g_ptr_array_add(offer->mime_types, g_strdup(mime_type));
    zwp_primary_selection_offer_v1_send_offer(offer->local_offer, mime_type);
}

static void
local_offer_receive(
    struct wl_client *client,
    struct wl_resource *offer_resource,
    const char *mime_type,
    int fd) {
    TRACE("entering");
    WlePSOffer *offer = wl_resource_get_user_data(offer_resource);
    if (offer != NULL) {
        zwp_primary_selection_offer_v1_receive(offer->remote_offer, mime_type, fd);
    }
    close(fd);
}

static void
local_offer_destroy(
    struct wl_client *client,
    struct wl_resource *offer_resource) {
    TRACE("entering");
    wl_resource_destroy(offer_resource);
}

static void
local_offer_destroy_impl(struct wl_resource *offer_resource) {
    TRACE("entering");
    WlePSOffer *offer = wl_resource_get_user_data(offer_resource);

    if (offer != NULL) {
        if (offer == offer->device->cur_selection) {
            offer->device->cur_selection = NULL;
        }
        g_ptr_array_free(offer->mime_types, TRUE);

        zwp_primary_selection_offer_v1_destroy(offer->remote_offer);

        wl_list_remove(&offer->link);
        g_free(offer);
    } else {
        // Orphan resource, remove from device's list
        wl_list_remove(wl_resource_get_link(offer_resource));
    }
}

static void
remote_device_data_offer(
    void *data,
    struct zwp_primary_selection_device_v1 *remote_device,
    struct zwp_primary_selection_offer_v1 *remote_offer) {
    TRACE("entering");
    WlePSDevice *device = data;
    if (remote_offer != NULL) {
        struct wl_resource *offer_resource = wl_resource_create(wl_resource_get_client(device->local_device),
                                                                &zwp_primary_selection_offer_v1_interface,
                                                                wl_resource_get_version(device->local_device),
                                                                0);
        if (offer_resource == NULL) {
            wl_client_post_no_memory(wl_resource_get_client(device->local_device));
            zwp_primary_selection_offer_v1_destroy(remote_offer);
        } else {
            WlePSOffer *offer = g_new0(WlePSOffer, 1);
            offer->device = device;
            offer->mime_types = g_ptr_array_new_with_free_func(g_free);
            offer->remote_offer = _wle_proxy_set_ours((struct wl_proxy *)remote_offer);
            offer->local_offer = offer_resource;
            wl_list_insert(&device->offers, &offer->link);
            wl_resource_set_implementation(offer_resource, &local_offer_impl, offer, local_offer_destroy_impl);
            zwp_primary_selection_offer_v1_add_listener(remote_offer, &remote_offer_listener, offer);

            zwp_primary_selection_device_v1_send_data_offer(device->local_device, offer_resource);
        }
    }
}

static void
remote_device_selection(
    void *data,
    struct zwp_primary_selection_device_v1 *remote_device,
    struct zwp_primary_selection_offer_v1 *remote_offer /* nullable */
) {
    TRACE("entering");
    WlePSDevice *device = data;
    WlePSOffer *offer = _wle_proxy_get_user_data((struct wl_proxy *)remote_offer);

    device->cur_selection = offer;
    zwp_primary_selection_device_v1_send_selection(device->local_device, offer != NULL ? offer->local_offer : NULL);
}

static void
local_device_set_selection(
    struct wl_client *client,
    struct wl_resource *device_resource,
    struct wl_resource *source_resource /* nullable */,
    uint32_t serial) {
    TRACE("entering");
    WlePSDevice *device = wl_resource_get_user_data(device_resource);
    WlePSSource *source = source_resource != NULL ? wl_resource_get_user_data(source_resource) : NULL;
    zwp_primary_selection_device_v1_set_selection(device->remote_device,
                                                  source != NULL ? source->remote_source : NULL,
                                                  serial);
}

static void
local_device_destroy(
    struct wl_client *client,
    struct wl_resource *device_resource) {
    wl_resource_destroy(device_resource);
}

static void
local_device_destroy_impl(struct wl_resource *device_resource) {
    TRACE("entering");
    WlePSDevice *device = wl_resource_get_user_data(device_resource);

    if (device->seat != NULL) {
        g_signal_handlers_disconnect_by_data(device->seat, device);
    }

    zwp_primary_selection_device_v1_destroy(device->remote_device);

    struct wl_resource *resource, *tmp_resource;
    wl_resource_for_each_safe(resource, tmp_resource, &device->orphan_offer_resources) {
        wl_resource_destroy(resource);
    }

    WlePSOffer *offer, *tmp_offer;
    wl_list_for_each_safe(offer, tmp_offer, &device->offers, link) {
        wl_resource_destroy(offer->local_offer);
    }

    wl_list_remove(wl_resource_get_link(device_resource));
    g_free(device);
}

static void
seat_focus_changed(WleSeat *seat, WleView *focused_view, uint32_t serial, WlePSDevice *device) {
    TRACE("entering");
    if (focused_view != NULL && wl_resource_get_client(device->local_device) == wle_view_get_client(focused_view))
    {
        DBG("forwarding current selection %p", device->cur_selection);
        if (device->cur_selection != NULL) {
            WlePSOffer *offer = device->cur_selection;

            struct wl_resource *old_local_offer = offer->local_offer;
            offer->local_offer = NULL;
            wl_resource_set_user_data(old_local_offer, NULL);
            wl_list_insert(&device->orphan_offer_resources, wl_resource_get_link(old_local_offer));

            struct wl_resource *local_offer = wl_resource_create(wl_resource_get_client(device->local_device),
                                                                 &zwp_primary_selection_offer_v1_interface,
                                                                 wl_resource_get_version(device->local_device),
                                                                 0);
            if (local_offer == NULL) {
                g_warning("Failed to create new local primary offer resource");
                zwp_primary_selection_device_v1_send_selection(device->local_device, NULL);
            } else {
                wl_resource_set_implementation(local_offer,
                                               &local_offer_impl,
                                               offer,
                                               local_offer_destroy_impl);
                offer->local_offer = local_offer;

                zwp_primary_selection_device_v1_send_data_offer(device->local_device, local_offer);
                for (guint i = 0; i < offer->mime_types->len; ++i) {
                    zwp_primary_selection_offer_v1_send_offer(local_offer, g_ptr_array_index(offer->mime_types, i));
                }
                zwp_primary_selection_device_v1_send_selection(device->local_device, local_offer);
            }
        } else {
            zwp_primary_selection_device_v1_send_selection(device->local_device, NULL);
        }
    }
}

static void
seat_destroyed(WleSeat *seat, WlePSDevice *device) {
    TRACE("entering");
    g_signal_handlers_disconnect_by_data(seat, device);
    device->seat = NULL;
    wl_resource_destroy(device->local_device);
}

static void
local_device_manager_create_source(
    struct wl_client *client,
    struct wl_resource *device_manager_resource,
    uint32_t source_id) {
    WlePrimarySelection *selection = wl_resource_get_user_data(device_manager_resource);

    struct zwp_primary_selection_source_v1 *remote_source = zwp_primary_selection_device_manager_v1_create_source(selection->remote_device_manager);
    if (remote_source == NULL) {
        wle_propagate_remote_error(selection->remote_display, client, device_manager_resource);
    } else {
        struct wl_resource *source_resource = wl_resource_create(client,
                                                                 &zwp_primary_selection_source_v1_interface,
                                                                 wl_resource_get_version(device_manager_resource),
                                                                 source_id);
        if (source_resource == NULL) {
            zwp_primary_selection_source_v1_destroy(remote_source);
            wl_client_post_no_memory(client);
        } else {
            WlePSSource *source = g_new0(WlePSSource, 1);
            source->remote_source = _wle_proxy_set_ours((struct wl_proxy *)remote_source);
            source->local_source = source_resource;
            wl_list_insert(&selection->sources, wl_resource_get_link(source_resource));
            wl_resource_set_implementation(source_resource, &local_source_impl, source, local_source_destroy_impl);
            zwp_primary_selection_source_v1_add_listener(remote_source, &remote_source_listener, source);
        }
    }
}

static void
local_device_manager_get_device(
    struct wl_client *client,
    struct wl_resource *device_manager_resource,
    uint32_t device_id,
    struct wl_resource *seat_resource) {
    WlePrimarySelection *selection = wl_resource_get_user_data(device_manager_resource);
    WleSeat *seat = seat_resource != NULL ? WLE_SEAT(wl_resource_get_user_data(seat_resource)) : NULL;

    if (seat == NULL) {
        wl_resource_post_error(device_manager_resource,
                               WL_DISPLAY_ERROR_INVALID_OBJECT,
                               "null or unknown seat passed");
    } else {
        struct zwp_primary_selection_device_v1 *remote_device = zwp_primary_selection_device_manager_v1_get_device(selection->remote_device_manager,
                                                                                                                   _wle_seat_get_remote_seat(seat));
        if (remote_device == NULL) {
            wle_propagate_remote_error(selection->remote_display, client, device_manager_resource);
        } else {
            struct wl_resource *device_resource = wl_resource_create(client,
                                                                     &zwp_primary_selection_device_v1_interface,
                                                                     wl_resource_get_version(device_manager_resource),
                                                                     device_id);
            if (device_resource == NULL) {
                zwp_primary_selection_device_v1_destroy(remote_device);
                wl_client_post_no_memory(client);
            } else {
                WlePSDevice *device = g_new0(WlePSDevice, 1);
                device->seat = seat;
                g_signal_connect(seat, "focus-changed",
                                 G_CALLBACK(seat_focus_changed), device);
                g_signal_connect(seat, "destroy",
                                 G_CALLBACK(seat_destroyed), device);
                device->remote_device = _wle_proxy_set_ours((struct wl_proxy *)remote_device);
                device->local_device = device_resource;
                wl_list_init(&device->offers);
                wl_list_init(&device->orphan_offer_resources);
                wl_list_insert(&selection->devices, wl_resource_get_link(device_resource));
                wl_resource_set_implementation(device_resource, &local_device_impl, device, local_device_destroy_impl);
                zwp_primary_selection_device_v1_add_listener(remote_device, &remote_device_listener, device);
            }
        }
    }
}

static void
local_device_manager_destroy(
    struct wl_client *client,
    struct wl_resource *device_manager_resource) {
    wl_list_remove(wl_resource_get_link(device_manager_resource));
}

static void
local_device_manager_destroy_impl(struct wl_resource *device_manager_resource) {
    wl_list_remove(wl_resource_get_link(device_manager_resource));
}

static void
local_device_manager_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WlePrimarySelection *selection = data;

    struct wl_resource *local_manager_resource = wl_resource_create(client,
                                                                    &zwp_primary_selection_device_manager_v1_interface,
                                                                    version,
                                                                    id);
    if (local_manager_resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(local_manager_resource,
                                       &local_device_manager_impl,
                                       selection,
                                       local_device_manager_destroy_impl);
        if (selection == NULL) {
            wl_list_init(wl_resource_get_link(local_manager_resource));
        } else {
            wl_list_insert(&selection->local_device_manager_resources, wl_resource_get_link(local_manager_resource));
        }
    }
}

WlePrimarySelection *
wle_primary_selection_new(struct wl_display *remote_display,
                          struct zwp_primary_selection_device_manager_v1 *remote_device_manager,
                          struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_device_manager != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WlePrimarySelection *selection = g_new0(WlePrimarySelection, 1);
    selection->local_device_manager = wl_global_create(local_display,
                                                       &zwp_primary_selection_device_manager_v1_interface,
                                                       wl_proxy_get_version((struct wl_proxy *)remote_device_manager),
                                                       selection,
                                                       local_device_manager_bind);
    if (selection->local_device_manager == NULL) {
        zwp_primary_selection_device_manager_v1_destroy(remote_device_manager);
        g_free(selection);
        return NULL;
    } else {
        selection->remote_display = remote_display;
        selection->remote_device_manager = remote_device_manager;
        wl_list_init(&selection->local_device_manager_resources);
        wl_list_init(&selection->devices);
        wl_list_init(&selection->sources);
        return selection;
    }
}

void
wle_primary_selection_destroy(WlePrimarySelection *selection) {
    if (selection != NULL) {
        struct wl_resource *resource, *tmp_resource;

        wl_resource_for_each_safe(resource, tmp_resource, &selection->sources) {
            wl_resource_destroy(resource);
        }

        wl_resource_for_each_safe(resource, tmp_resource, &selection->devices) {
            wl_resource_destroy(resource);
        }

        wl_resource_for_each_safe(resource, tmp_resource, &selection->local_device_manager_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(selection->local_device_manager);

        zwp_primary_selection_device_manager_v1_destroy(selection->remote_device_manager);

        g_free(selection);
    }
}
