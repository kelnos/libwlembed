/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_FOREIGN_TOPLEVEL_MANAGER_H__
#define __WLE_FOREIGN_TOPLEVEL_MANAGER_H__

#include <glib.h>

#include "protocol/wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"

G_BEGIN_DECLS

typedef struct _WleForeignToplevelManager WleForeignToplevelManager;

WleForeignToplevelManager *wle_foreign_toplevel_manager_new(struct wl_display *remote_display,
                                                            struct zwlr_foreign_toplevel_manager_v1 *remote_foreign_toplevel_manager,
                                                            struct wl_display *local_display);
void wle_foreign_toplevel_manager_destroy(WleForeignToplevelManager *manager);

G_END_DECLS

#endif /* __WLE_FOREIGN_TOPLEVEL_MANAGER_H__ */
