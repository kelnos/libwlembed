/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-client-protocol.h>
#include <wayland-server-protocol.h>

#include "wle-error.h"
#include "wle-subsurface.h"

struct _WleSubsurface {
    WleViewRole parent;

    struct wl_subsurface *remote_subsurface;
    struct wl_resource *local_subsurface;

    WleView *parent_view;
};

static void wle_subsurface_finalize(GObject *object);

static void wle_subsurface_destroy(WleLocalResource *local_resource);

static void local_subsurface_set_position(struct wl_client *client,
                                          struct wl_resource *subsurface_resource,
                                          int32_t x,
                                          int32_t y);
static void local_subsurface_place_above(struct wl_client *client,
                                         struct wl_resource *subsurface_resource,
                                         struct wl_resource *surface_resource);
static void local_subsurface_place_below(struct wl_client *client,
                                         struct wl_resource *subsurface_resource,
                                         struct wl_resource *surface_resource);
static void local_subsurface_set_sync(struct wl_client *client,
                                      struct wl_resource *subsurface_resource);
static void local_subsurface_set_desync(struct wl_client *client,
                                        struct wl_resource *subsurface_resource);
static void local_subsurface_destroy(struct wl_client *client,
                                     struct wl_resource *subsurface_resource);


G_DEFINE_FINAL_TYPE(WleSubsurface, wle_subsurface, WLE_TYPE_VIEW_ROLE)


static const struct wl_subsurface_interface local_subsurface_impl = {
    .set_position = local_subsurface_set_position,
    .place_above = local_subsurface_place_above,
    .place_below = local_subsurface_place_below,
    .set_sync = local_subsurface_set_sync,
    .set_desync = local_subsurface_set_desync,
    .destroy = local_subsurface_destroy,
};

static void
wle_subsurface_class_init(WleSubsurfaceClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_subsurface_finalize;

    WleLocalResourceClass *local_resource_class = WLE_LOCAL_RESOURCE_CLASS(klass);
    local_resource_class->destroy = wle_subsurface_destroy;
}

static void
wle_subsurface_init(WleSubsurface *subsurface) {}

static void
wle_subsurface_finalize(GObject *object) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(object);

    if (subsurface->remote_subsurface != NULL) {
        wl_subsurface_destroy(subsurface->remote_subsurface);
    }

    if (subsurface->local_subsurface != NULL) {
        wl_resource_destroy(subsurface->local_subsurface);
    }

    G_OBJECT_CLASS(wle_subsurface_parent_class)->finalize(object);
}

static void
wle_subsurface_destroy(WleLocalResource *local_resource) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(local_resource);

    if (subsurface->remote_subsurface != NULL) {
        wl_subsurface_destroy(subsurface->remote_subsurface);
        subsurface->remote_subsurface = NULL;
    }

    if (subsurface->local_subsurface != NULL) {
        wl_resource_destroy(subsurface->local_subsurface);
        subsurface->local_subsurface = NULL;
    }

    WLE_LOCAL_RESOURCE_CLASS(wle_subsurface_parent_class)->destroy(local_resource);
}

static void
local_subsurface_set_position(struct wl_client *client,
                              struct wl_resource *subsurface_resource,
                              int32_t x,
                              int32_t y) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(wl_resource_get_user_data(subsurface_resource));
    wl_subsurface_set_position(subsurface->remote_subsurface, x, y);
}

static void
local_subsurface_place_above(struct wl_client *client,
                             struct wl_resource *subsurface_resource,
                             struct wl_resource *surface_resource) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(wl_resource_get_user_data(subsurface_resource));
    WleView *view = subsurface_resource ? WLE_VIEW(wl_resource_get_user_data(surface_resource)) : NULL;

    // TODO: ensure that view is a sibling or parent of subsurface
    if (view == NULL) {
        wl_resource_post_error(subsurface_resource, WL_SUBSURFACE_ERROR_BAD_SURFACE, "surface cannot be null");
    } else if (subsurface_resource == surface_resource) {
        wl_resource_post_error(subsurface_resource, WL_SUBSURFACE_ERROR_BAD_SURFACE, "surface cannot be the same as subsurface");
    } else {
        // TODO: what if one is embedded and one is not?
        wl_subsurface_place_above(subsurface->remote_subsurface, wle_view_get_remote_surface(view));
    }
}

static void
local_subsurface_place_below(struct wl_client *client,
                             struct wl_resource *subsurface_resource,
                             struct wl_resource *surface_resource) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(wl_resource_get_user_data(subsurface_resource));
    WleView *view = subsurface_resource ? WLE_VIEW(wl_resource_get_user_data(surface_resource)) : NULL;

    // TODO: ensure that view is a sibling or parent of subsurface
    if (view == NULL) {
        wl_resource_post_error(subsurface_resource, WL_SUBSURFACE_ERROR_BAD_SURFACE, "surface cannot be null");
    } else if (subsurface_resource == surface_resource) {
        wl_resource_post_error(subsurface_resource, WL_SUBSURFACE_ERROR_BAD_SURFACE, "surface cannot be the same as subsurface");
    } else {
        // TODO: what if one is embedded and one is not?
        wl_subsurface_place_below(subsurface->remote_subsurface, wle_view_get_remote_surface(view));
    }
}

static void
local_subsurface_set_sync(struct wl_client *client, struct wl_resource *subsurface_resource) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(wl_resource_get_user_data(subsurface_resource));
    wl_subsurface_set_sync(subsurface->remote_subsurface);
}

static void
local_subsurface_set_desync(struct wl_client *client, struct wl_resource *subsurface_resource) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(wl_resource_get_user_data(subsurface_resource));
    wl_subsurface_set_desync(subsurface->remote_subsurface);
}

static void
local_subsurface_destroy(struct wl_client *client, struct wl_resource *subsurface_resource) {
    wl_resource_destroy(subsurface_resource);
}

static void
local_subsurface_destroy_impl(struct wl_resource *resource) {
    WleSubsurface *subsurface = WLE_SUBSURFACE(wl_resource_get_user_data(resource));

    if (subsurface->remote_subsurface != NULL) {
        wl_subsurface_destroy(subsurface->remote_subsurface);
        subsurface->remote_subsurface = NULL;
    }

    subsurface->local_subsurface = NULL;
    wle_local_resource_destroyed(WLE_LOCAL_RESOURCE(subsurface));
    g_object_unref(subsurface);
}

WleSubsurface *
_wle_subsurface_new(WleView *view,
                    struct wl_subsurface *remote_subsurface,
                    struct wl_resource *local_subsurface,
                    WleView *parent_view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    g_return_val_if_fail(remote_subsurface != NULL, NULL);
    g_return_val_if_fail(local_subsurface != NULL, NULL);
    g_return_val_if_fail(WLE_IS_VIEW(parent_view), NULL);

    WleSubsurface *subsurface = g_object_new(WLE_TYPE_SUBSURFACE,
                                             "role", WLE_ROLE_SUBSURFACE,
                                             NULL);
    subsurface->remote_subsurface = remote_subsurface;
    subsurface->local_subsurface = local_subsurface;
    subsurface->parent_view = parent_view;

    wl_resource_set_implementation(local_subsurface,
                                   &local_subsurface_impl,
                                   subsurface,
                                   local_subsurface_destroy_impl);

    // local_subsurface takes a reference
    g_object_ref(subsurface);

    return subsurface;
}
