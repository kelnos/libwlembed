/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server-protocol.h>

#include "wle-private-types.h"
#include "wle-util.h"

struct _WleBuffer {
    grefcount ref_count;
    struct wl_buffer *remote_buffer;
    struct wl_resource *local_buffer;
    gboolean forward_release;
    struct wl_list link;
};

struct _WleRegion {
    grefcount ref_count;
    struct wl_region *remote_region;
    struct wl_resource *local_region;
    struct wl_list link;
};


/* WleBuffer */


static void
remote_buffer_release(void *data, struct wl_buffer *remote_buffer) {
    WleBuffer *buffer = data;
    if (buffer->local_buffer != NULL && buffer->forward_release) {
        wl_buffer_send_release(buffer->local_buffer);
    }
    wle_buffer_unref(buffer);
}

static void
local_buffer_destroy(struct wl_client *client, struct wl_resource *buffer_resource) {
    wl_resource_destroy(buffer_resource);
}

static void
local_buffer_destroy_impl(struct wl_resource *resource) {
    WleBuffer *buffer = wl_resource_get_user_data(resource);
    if (buffer->local_buffer != NULL) {
        buffer->local_buffer = NULL;
        wle_buffer_unref(buffer);
    }
}

static const struct wl_buffer_listener remote_buffer_listener = {
    .release = remote_buffer_release,
};

static const struct wl_buffer_interface local_buffer_impl = {
    .destroy = local_buffer_destroy,
};

WleBuffer *
wle_buffer_new(struct wl_buffer *remote_buffer, struct wl_resource *local_buffer) {
    g_return_val_if_fail(remote_buffer != NULL, NULL);
    g_return_val_if_fail(local_buffer != NULL, NULL);

    WleBuffer *buffer = g_slice_new0(WleBuffer);
    buffer->remote_buffer = _wle_proxy_set_ours((struct wl_proxy *)remote_buffer);
    buffer->local_buffer = local_buffer;
    wl_list_init(&buffer->link);

    // Thie initial refcount is owned by local_region, which gets released
    // when the client destroys the region.  Additionally, when we attach
    // remote_buffer to a surface, we need to ref the buffer, as the remote
    // compositor will need the buffer until it calls release() on the
    // buffer listener.
    g_ref_count_init(&buffer->ref_count);

    wl_buffer_add_listener(remote_buffer, &remote_buffer_listener, buffer);
    wl_resource_set_implementation(local_buffer, &local_buffer_impl, buffer, local_buffer_destroy_impl);

    return buffer;
}

struct wl_buffer *
wle_buffer_get_remote_buffer(WleBuffer *buffer) {
    g_return_val_if_fail(buffer != NULL, NULL);
    return buffer->remote_buffer;
}

struct wl_resource *
wle_buffer_get_local_buffer(WleBuffer *buffer) {
    g_return_val_if_fail(buffer != NULL, NULL);
    return buffer->local_buffer;
}

void
wle_buffer_set_forward_release(WleBuffer *buffer, gboolean forward_release) {
    g_return_if_fail(buffer != NULL);
    buffer->forward_release = forward_release;
}

struct wl_list *
wle_buffer_get_link(WleBuffer *buffer) {
    g_return_val_if_fail(buffer != NULL, NULL);
    return &buffer->link;
}

WleBuffer *
wle_buffer_from_link(struct wl_list *link) {
    g_return_val_if_fail(link != NULL, NULL);
    WleBuffer *buffer = wl_container_of(link, buffer, link);
    return buffer;
}

WleBuffer *
wle_buffer_ref(WleBuffer *buffer) {
    g_return_val_if_fail(buffer != NULL, NULL);
    g_ref_count_inc(&buffer->ref_count);
    return buffer;
}

void
wle_buffer_unref(WleBuffer *buffer) {
    if (buffer != NULL) {
        g_return_if_fail(!g_ref_count_compare(&buffer->ref_count, 0));

        if (g_ref_count_dec(&buffer->ref_count)) {
            wle_buffer_destroy(buffer);
        }
    }
}

// We need an extra destroy function as a remote wl_surface can also own a
// reference, but we can't force it to send the wl_buffer.release() event
// in order to get it to drop it.
void
wle_buffer_destroy(WleBuffer *buffer) {
    g_return_if_fail(buffer != NULL);
    if (buffer->remote_buffer != NULL) {
        wl_buffer_destroy(buffer->remote_buffer);
    }
    if (buffer->local_buffer != NULL) {
        struct wl_resource *resource = buffer->local_buffer;
        buffer->local_buffer = NULL;
        wl_resource_destroy(resource);
    }
    wl_list_remove(&buffer->link);
    g_slice_free(WleBuffer, buffer);
}


/* WleRegion */


static void
local_region_add(struct wl_client *client,
                 struct wl_resource *region_resource,
                 int32_t x,
                 int32_t y,
                 int32_t width,
                 int32_t height) {
    WleRegion *region = wl_resource_get_user_data(region_resource);
    wl_region_add(region->remote_region, x, y, width, height);
}

static void
local_region_subtract(struct wl_client *client,
                      struct wl_resource *region_resource,
                      int32_t x,
                      int32_t y,
                      int32_t width,
                      int32_t height) {
    WleRegion *region = wl_resource_get_user_data(region_resource);
    wl_region_subtract(region->remote_region, x, y, width, height);
}

static void
local_region_destroy(struct wl_client *client, struct wl_resource *region_resource) {
    wl_resource_destroy(region_resource);
}

static void
local_region_destroy_impl(struct wl_resource *resource) {
    WleRegion *region = wl_resource_get_user_data(resource);
    if (region->local_region != NULL) {
        region->local_region = NULL;
        wle_region_unref(region);
    }
}

static const struct wl_region_interface local_region_impl = {
    .add = local_region_add,
    .subtract = local_region_subtract,
    .destroy = local_region_destroy,
};

WleRegion *
wle_region_new(struct wl_region *remote_region, struct wl_resource *local_region) {
    g_return_val_if_fail(remote_region != NULL, NULL);
    g_return_val_if_fail(local_region != NULL, NULL);

    WleRegion *region = g_slice_new0(WleRegion);
    region->remote_region = _wle_proxy_set_ours((struct wl_proxy *)remote_region);
    region->local_region = local_region;
    wl_list_init(&region->link);

    // Thie initial refcount is owned by local_region, which gets released
    // when the client destroys the region.
    g_ref_count_init(&region->ref_count);

    wl_resource_set_implementation(local_region, &local_region_impl, region, local_region_destroy_impl);

    return region;
}

struct wl_region *
wle_region_get_remote_region(WleRegion *region) {
    g_return_val_if_fail(region != NULL, NULL);
    return region->remote_region;
}

struct wl_resource *
wle_region_get_local_region(WleRegion *region) {
    g_return_val_if_fail(region != NULL, NULL);
    return region->local_region;
}

struct wl_list *
wle_region_get_link(WleRegion *region) {
    g_return_val_if_fail(region != NULL, NULL);
    return &region->link;
}

WleRegion *
wle_region_from_link(struct wl_list *link) {
    g_return_val_if_fail(link != NULL, NULL);
    WleRegion *region = wl_container_of(link, region, link);
    return region;
}

WleRegion *
wle_region_ref(WleRegion *region) {
    g_return_val_if_fail(region != NULL, NULL);
    g_ref_count_inc(&region->ref_count);
    return region;
}

void
wle_region_unref(WleRegion *region) {
    if (region != NULL) {
        g_return_if_fail(!g_ref_count_compare(&region->ref_count, 0));

        if (g_ref_count_dec(&region->ref_count)) {
            wl_region_destroy(region->remote_region);
            if (region->local_region != NULL) {
                struct wl_resource *resource = region->local_region;
                region->local_region = NULL;
                wl_resource_destroy(resource);
            }
            wl_list_remove(&region->link);
            g_slice_free(WleRegion, region);
        }
    }
}

void
wle_region_destroy_local_region(WleRegion *region) {
    if (region != NULL && region->local_region != NULL) {
        wl_resource_destroy(region->local_region);
    }
}
