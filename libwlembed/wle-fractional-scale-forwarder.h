/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_FRACTIONAL_SCALE_FOWARDER_H__
#define __WLE_FRACTIONAL_SCALE_FOWARDER_H__

#include <glib.h>

#include "protocol/fractional-scale-v1-client-protocol.h"

G_BEGIN_DECLS

typedef struct _WleFractionalScaleForwarder WleFractionalScaleForwarder;

WleFractionalScaleForwarder *wle_fractional_scale_forwarder_create(struct wl_display *remote_display,
                                                                   struct wp_fractional_scale_manager_v1 *remote_manager,
                                                                   struct wl_display *local_display);
void wle_fractional_scale_forwarder_destroy(WleFractionalScaleForwarder *forwarder);

G_END_DECLS

#endif
