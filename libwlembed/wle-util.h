/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_UTIL_H__
#define __WLE_UTIL_H__

#include <glib.h>
#include <wayland-client-protocol.h>
#include <wayland-server-core.h>

G_BEGIN_DECLS

void wle_propagate_remote_error(struct wl_display *remote_display,
                                struct wl_client *client,
                                struct wl_resource *resource);

void *_wle_proxy_set_ours(struct wl_proxy *proxy);
void *_wle_proxy_get_user_data(struct wl_proxy *proxy);

void *_wle_registry_bind(struct wl_registry *registry,
                         uint32_t name,
                         const struct wl_interface *interface,
                         uint32_t version);

int _wle_shm_share_data(void *data,
                        gsize len);

static inline void *
_wle_assert_nonnull(void *ptr) {
    if (ptr == NULL) {
        g_error("Out of memory!");
        abort();
    }
    return ptr;
}

G_END_DECLS

#endif /* __WLE_UTIL_H__ */
