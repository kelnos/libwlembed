/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <glib-unix.h>
#include <gtk/gtk.h>
#include <libwlembed-gtk3/libwlembed-gtk3.h>

#ifdef HAVE_GTK3_LAYER_SHELL
#include <gtk-layer-shell.h>
#endif

typedef struct {
    WleGtkSocket *socket;
    gchar *command;
} EmbedProgramData;

static gboolean
handle_quit_signal(gpointer data) {
    gtk_main_quit();
    return G_SOURCE_REMOVE;
}

static void
embed_something(GtkWidget *button, EmbedProgramData *epdata) {
    g_message("running something");

    const gchar *token = wle_gtk_socket_get_embedding_token(epdata->socket);
    gchar *command = g_strdup_printf("%s --socket-token '%s'", epdata->command, token);

    WleEmbeddedCompositor *ec = wle_gtk_socket_get_embedded_compositor(epdata->socket);
    GError *error = NULL;
    if (!wle_embedded_compositor_spawn_command_line(ec, command, &error)) {
        g_warning("Failed to run something: %s", error->message);
        g_error_free(error);
    } else {
        gtk_widget_set_sensitive(button, FALSE);
    }

    g_free(command);
}

static void
terminate_embed(GtkWidget *button, WleGtkSocket *socket) {
    wle_gtk_socket_destroy_embedded_view(socket);
}

static gboolean
unhide_window(gpointer data) {
    GtkWidget *window = GTK_WIDGET(data);
    gtk_widget_show(window);
    return FALSE;
}

static void
hide_window(GtkWidget *button, GtkWidget *window) {
    gtk_widget_hide(window);
    g_timeout_add(1000, unhide_window, window);
}

static void
socket_plug_added(WleGtkSocket *socket, GtkWidget *button) {
    gtk_widget_set_sensitive(button, TRUE);
}

static void
socket_plug_removed(WleGtkSocket *socket, GError *error, GtkWidget *button) {
    if (error != NULL) {
        g_warning("Embedded application closed unexpectedly: %s", error->message);
    }
    gtk_widget_set_sensitive(button, !gtk_widget_get_sensitive(button));
}

static void
ec_error(WleEmbeddedCompositor *ec, GError *error) {
    g_warning("Error on embedded compositor: %s", error->message);
}

static void
ec_closed(WleEmbeddedCompositor *ec, GError *error) {
    g_message("Embedded compositor was closed (%s)", error != NULL ? error->message : "no error");
    gtk_main_quit();
}

static void
drop_zone_drag_data_received(GtkWidget *widget,
                             GdkDragContext *context,
                             gint x,
                             gint y,
                             GtkSelectionData *data,
                             guint info,
                             guint time,
                             GtkWindow *window) {
    guchar *str = gtk_selection_data_get_text(data);
    GtkWidget *dialog = gtk_dialog_new_with_buttons("Got DnD",
                                                    window,
                                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                                    "Ok",
                                                    GTK_RESPONSE_ACCEPT,
                                                    NULL);
    gchar *text = g_strdup_printf("Received via DnD:\n%s", str);
    GtkWidget *label = gtk_label_new(text);
    gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), label);

    g_free(str);
    g_free(text);

    gtk_widget_show_all(label);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

int
main(int argc, char **argv) {
#ifdef HAVE_GTK3_LAYER_SHELL
    gboolean use_layer_shell_window = FALSE;
#endif

    gdk_set_allowed_backends("wayland");
    gtk_init(&argc, &argv);

    if (argc > 1) {
        if (strcmp(argv[1], "--help") == 0) {
            g_print("Usage: %s %s\n",
                    argv[0],
#ifdef HAVE_GTK3_LAYER_SHELL
                    "[--use-layer-shell-window]"
#else
                    ""
#endif
            );
            exit(0);
        }
#ifdef HAVE_GTK3_LAYER_SHELL
        else if (strcmp(argv[1], "--use-layer-shell-window") == 0)
        {
            use_layer_shell_window = TRUE;
        }
#endif
        else
        {
            g_printerr("Invalid argument: %s\n", argv[1]);
            exit(1);
        }
    }

    GError *error = NULL;
    WleEmbeddedCompositor *ec = wle_gtk_create_embedded_compositor("wlembed-example", &error);

    if (ec == NULL) {
        g_error("Failed to create compositor: %s", error->message);
        g_error_free(error);
        return 1;
    } else {
        g_signal_connect(ec, "error",
                         G_CALLBACK(ec_error), NULL);
        g_signal_connect(ec, "closed",
                         G_CALLBACK(ec_closed), NULL);

        g_unix_signal_add(SIGINT, handle_quit_signal, NULL);
        g_unix_signal_add(SIGTERM, handle_quit_signal, NULL);
        g_unix_signal_add(SIGHUP, handle_quit_signal, NULL);

        GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        g_signal_connect(window, "delete-event",
                         G_CALLBACK(gtk_main_quit), NULL);

#ifdef HAVE_GTK3_LAYER_SHELL
        if (use_layer_shell_window) {
            gtk_layer_init_for_window(GTK_WINDOW(window));
            gtk_layer_set_layer(GTK_WINDOW(window), GTK_LAYER_SHELL_LAYER_TOP);
        }
#endif

        GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 8);
        gtk_container_add(GTK_CONTAINER(window), box);

        GtkWidget *label = gtk_label_new("Click the button below to embed something");
        gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE, 0);

        GtkWidget *socket = wle_gtk_socket_new(ec);
        gtk_widget_set_can_focus(socket, TRUE);
        gtk_box_pack_start(GTK_BOX(box), socket, TRUE, TRUE, 0);

        GtkWidget *embed_button = gtk_button_new_with_label("Click me");
        gtk_box_pack_end(GTK_BOX(box), embed_button, FALSE, FALSE, 0);

        GtkWidget *terminate_button = gtk_button_new_with_label("Terminate embed");
        gtk_widget_set_sensitive(terminate_button, FALSE);
        gtk_box_pack_end(GTK_BOX(box), terminate_button, FALSE, FALSE, 0);
        g_signal_connect(terminate_button, "clicked",
                         G_CALLBACK(terminate_embed), socket);

        GtkWidget *hide_button = gtk_button_new_with_label("Hide for 1s");
        gtk_box_pack_end(GTK_BOX(box), hide_button, FALSE, FALSE, 0);
        g_signal_connect(hide_button, "clicked",
                         G_CALLBACK(hide_window), window);

        GtkWidget *dnd_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 6);
        gtk_box_pack_end(GTK_BOX(box), dnd_box, FALSE, FALSE, 0);

        GtkWidget *drag_zone = gtk_entry_new();
        gtk_entry_set_text(GTK_ENTRY(drag_zone), "Drag me");
        gtk_box_pack_start(GTK_BOX(dnd_box), drag_zone, TRUE, TRUE, 0);

        GtkWidget *drop_zone = gtk_label_new("DnD drop zone");
        gtk_widget_set_size_request(drop_zone, -1, 100);
        gtk_drag_dest_set(drop_zone,
                          GTK_DEST_DEFAULT_ALL,
                          NULL,
                          0,
                          GDK_ACTION_COPY | GDK_ACTION_MOVE);
        gtk_drag_dest_add_text_targets(drop_zone);
        gtk_box_pack_end(GTK_BOX(dnd_box), drop_zone, TRUE, TRUE, 0);
        g_signal_connect(drop_zone, "drag-data-received",
                         G_CALLBACK(drop_zone_drag_data_received), window);

        EmbedProgramData epdata = {
            .socket = WLE_GTK_SOCKET(socket),
            .command = DEFAULT_APP,
        };
        g_signal_connect(embed_button, "clicked",
                         G_CALLBACK(embed_something), &epdata);

        g_signal_connect(socket, "plug-added",
                         G_CALLBACK(socket_plug_added), terminate_button);
        g_signal_connect(socket, "plug-removed",
                         G_CALLBACK(socket_plug_removed), embed_button);
        g_signal_connect(socket, "plug-removed",
                         G_CALLBACK(socket_plug_removed), terminate_button);

        gtk_widget_show_all(window);
        gtk_widget_grab_focus(embed_button);

        gtk_main();

        g_object_unref(ec);

        return 0;
    }
}
