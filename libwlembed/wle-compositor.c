/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-client-protocol.h>
#include <wayland-client.h>
#include <wayland-server.h>

#include "wle-compositor.h"
#include "wle-private-types.h"
#include "wle-subsurface.h"
#include "wle-util.h"
#include "wle-view.h"

struct _WleCompositor {
    GObject parent;

    struct wl_display *remote_display;
    struct wl_compositor *remote_compositor;
    struct wl_subcompositor *remote_subcompositor;

    struct wl_global *local_compositor;
    struct wl_list local_compositor_resources;

    struct wl_global *local_subcompositor;
    struct wl_list local_subcompositor_resources;

    GList *views;  // WleView
    struct wl_list regions;  // WleRegion
    GList *subsurfaces;  // WleSubsurface
};

enum {
    SIG_NEW_VIEW,
    SIG_NEW_SUBSURFACE,

    N_SIGNALS,
};

static void wle_compositor_finalize(GObject *object);

static void local_compositor_create_surface(struct wl_client *client,
                                            struct wl_resource *compositor,
                                            uint32_t id);
static void local_compositor_create_region(struct wl_client *client,
                                           struct wl_resource *compositor,
                                           uint32_t id);

static void local_subcompositor_get_subsurface(struct wl_client *client,
                                               struct wl_resource *subcompositor,
                                               uint32_t id,
                                               struct wl_resource *surface,
                                               struct wl_resource *parent);
static void local_subcompositor_destroy(struct wl_client *client,
                                        struct wl_resource *subcompositor);


G_DEFINE_FINAL_TYPE(WleCompositor, wle_compositor, G_TYPE_OBJECT)


static guint signals[N_SIGNALS] = { 0 };

static struct wl_compositor_interface local_compositor_impl = {
    .create_surface = local_compositor_create_surface,
    .create_region = local_compositor_create_region,
};

static struct wl_subcompositor_interface local_subcompositor_impl = {
    .get_subsurface = local_subcompositor_get_subsurface,
    .destroy = local_subcompositor_destroy,
};

static void
wle_compositor_class_init(WleCompositorClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->finalize = wle_compositor_finalize;

    signals[SIG_NEW_VIEW] = g_signal_new("new-view",
                                         WLE_TYPE_COMPOSITOR,
                                         G_SIGNAL_RUN_LAST,
                                         0,
                                         NULL, NULL,
                                         g_cclosure_marshal_VOID__OBJECT,
                                         G_TYPE_NONE, 1,
                                         WLE_TYPE_VIEW);

    signals[SIG_NEW_SUBSURFACE] = g_signal_new("new-subsurface",
                                               WLE_TYPE_COMPOSITOR,
                                               G_SIGNAL_RUN_LAST,
                                               0,
                                               NULL, NULL,
                                               g_cclosure_marshal_VOID__OBJECT,
                                               G_TYPE_NONE, 1,
                                               WLE_TYPE_SUBSURFACE);
}

static void
wle_compositor_init(WleCompositor *compositor) {
    wl_list_init(&compositor->local_compositor_resources);
    wl_list_init(&compositor->local_subcompositor_resources);
    wl_list_init(&compositor->regions);
}

static void
wle_compositor_finalize(GObject *object) {
    WleCompositor *compositor = WLE_COMPOSITOR(object);

    for (GList *l = compositor->subsurfaces; l != NULL; l = l->next) {
        WleSubsurface *subsurface = WLE_SUBSURFACE(l->data);
        g_signal_handlers_disconnect_by_data(subsurface, compositor);
        wle_local_resource_destroy(WLE_LOCAL_RESOURCE(subsurface));
        g_object_unref(subsurface);
    }
    g_list_free(compositor->subsurfaces);

    WleRegion *region, *tmp_region;
    wle_region_for_each_safe(region, tmp_region, &compositor->regions) {
        wle_region_destroy_local_region(region);
    }

    for (GList *l = compositor->views; l != NULL; l = l->next) {
        WleView *view = WLE_VIEW(l->data);
        g_signal_handlers_disconnect_by_data(view, compositor);
        wle_local_resource_destroy(WLE_LOCAL_RESOURCE(view));
        g_object_unref(view);
    }
    g_list_free(compositor->views);

    struct wl_resource *resource, *tmp_resource;
    wl_resource_for_each_safe(resource, tmp_resource, &compositor->local_subcompositor_resources) {
        wl_resource_destroy(resource);
    }
    if (compositor->local_subcompositor != NULL) {
        wl_global_destroy(compositor->local_subcompositor);
    }

    wl_resource_for_each_safe(resource, tmp_resource, &compositor->local_compositor_resources) {
        wl_resource_destroy(resource);
    }
    if (compositor->local_compositor != NULL) {
        wl_global_destroy(compositor->local_compositor);
    }

    G_OBJECT_CLASS(wle_compositor_parent_class)->finalize(object);
}

static void
view_destroyed(WleView *view, WleCompositor *compositor) {
    compositor->views = g_list_remove(compositor->views, view);
    g_signal_handlers_disconnect_by_data(view, compositor);
    g_object_unref(view);
}

static void
local_compositor_create_surface(struct wl_client *client, struct wl_resource *compositor_resource, uint32_t id) {
    struct wl_resource *resource = wl_resource_create(client,
                                                      &wl_surface_interface,
                                                      wl_resource_get_version(compositor_resource),
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        WleCompositor *compositor = WLE_COMPOSITOR(wl_resource_get_user_data(compositor_resource));
        struct wl_surface *remote_surface = wl_compositor_create_surface(compositor->remote_compositor);
        if (remote_surface == NULL) {
            wl_resource_destroy(resource);
            wle_propagate_remote_error(compositor->remote_display, client, compositor_resource);
        } else {
            _wle_proxy_set_ours((struct wl_proxy *)remote_surface);

            WleView *view = wle_view_new(compositor->remote_display,
                                         compositor->remote_compositor,
                                         compositor->remote_subcompositor,
                                         remote_surface,
                                         resource);
            if (view == NULL) {
                wle_propagate_remote_error(compositor->remote_display, client, compositor_resource);
            } else {
                compositor->views = g_list_prepend(compositor->views, view);
                g_signal_connect(view, "destroy",
                                 G_CALLBACK(view_destroyed), compositor);
                g_signal_emit(compositor, signals[SIG_NEW_VIEW], 0, view);
            }
        }
    }
}

static void
local_compositor_create_region(struct wl_client *client, struct wl_resource *compositor_resource, uint32_t id) {
    struct wl_resource *resource = wl_resource_create(client,
                                                      &wl_region_interface,
                                                      wl_resource_get_version(compositor_resource),
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        WleCompositor *compositor = WLE_COMPOSITOR(wl_resource_get_user_data(compositor_resource));
        struct wl_region *remote_region = wl_compositor_create_region(compositor->remote_compositor);
        if (remote_region == NULL) {
            wle_propagate_remote_error(compositor->remote_display, client, compositor_resource);
            wl_resource_destroy(resource);
        } else {
            _wle_proxy_set_ours((struct wl_proxy *)remote_region);
            WleRegion *region = wle_region_new(remote_region, resource);
            wl_list_insert(&compositor->regions, wle_region_get_link(region));
        }
    }
}

static void
local_compositor_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_compositor_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleCompositor *compositor = WLE_COMPOSITOR(data);

    if (version > wl_global_get_version(compositor->local_compositor)) {
        wl_client_post_implementation_error(client, "unsupported wl_compositor version %d", version);
    } else {
        struct wl_resource *resource = wl_resource_create(client, &wl_compositor_interface, version, id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(resource, &local_compositor_impl, compositor, local_compositor_destroy_impl);
            wl_list_insert(&compositor->local_compositor_resources, wl_resource_get_link(resource));
        }
    }
}

static void
subsurface_destroyed(WleSubsurface *subsurface, WleCompositor *compositor) {
    compositor->subsurfaces = g_list_remove(compositor->subsurfaces, subsurface);
    g_signal_handlers_disconnect_by_data(subsurface, compositor);
    g_object_unref(subsurface);
}

static void
local_subcompositor_get_subsurface(struct wl_client *client,
                                   struct wl_resource *subcompositor,
                                   uint32_t id,
                                   struct wl_resource *surface_resource,
                                   struct wl_resource *parent_resource) {
    WleCompositor *compositor = WLE_COMPOSITOR(wl_resource_get_user_data(subcompositor));
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    WleView *parent = parent_resource != NULL ? WLE_VIEW(wl_resource_get_user_data(parent_resource)) : NULL;

    // TODO: ensure parent isn't a descendent of surface
    if (wle_view_get_role(view) != NULL) {
        wl_resource_post_error(surface_resource, WL_SUBCOMPOSITOR_ERROR_BAD_SURFACE, "surface already has a role");
    } else if (parent == NULL) {
        wl_resource_post_error(surface_resource, WL_SUBCOMPOSITOR_ERROR_BAD_PARENT, "surface parent cannot be null");
    } else if (parent == view) {
        wl_resource_post_error(surface_resource, WL_SUBCOMPOSITOR_ERROR_BAD_PARENT, "parent cannot be the same as surface");
    } else {
        struct wl_subsurface *remote_subsurface = wl_subcompositor_get_subsurface(compositor->remote_subcompositor,
                                                                                  wle_view_get_remote_surface(view),
                                                                                  wle_view_get_remote_surface(parent));
        if (remote_subsurface == NULL) {
            wle_propagate_remote_error(compositor->remote_display, client, subcompositor);
        } else {
            struct wl_resource *resource = wl_resource_create(client,
                                                              &wl_subsurface_interface,
                                                              wl_resource_get_version(subcompositor),
                                                              id);
            if (resource == NULL) {
                wl_subsurface_destroy(remote_subsurface);
                wl_client_post_no_memory(client);
            } else {
                WleSubsurface *subsurface = _wle_subsurface_new(view,
                                                                _wle_proxy_set_ours((struct wl_proxy *)remote_subsurface),
                                                                resource,
                                                                parent);
                compositor->subsurfaces = g_list_prepend(compositor->subsurfaces, subsurface);
                g_signal_connect(subsurface, "destroy",
                                 G_CALLBACK(subsurface_destroyed), compositor);
                wle_view_set_role(view, WLE_VIEW_ROLE(subsurface));
                g_signal_emit(compositor, signals[SIG_NEW_SUBSURFACE], 0, subsurface);
            }
        }
    }
}

static void
local_subcompositor_destroy(struct wl_client *client, struct wl_resource *subcompositor) {
    wl_resource_destroy(subcompositor);
}

static void
local_subcompositor_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_subcompositor_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleCompositor *compositor = data;

    if (wl_global_get_version(compositor->local_subcompositor) > version) {
        wl_client_post_implementation_error(client, "unsupported wl_subcompositor version %d", version);
    } else {
        struct wl_resource *resource = wl_resource_create(client, &wl_subcompositor_interface, version, id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(resource,
                                           &local_subcompositor_impl,
                                           compositor,
                                           local_subcompositor_destroy_impl);
            wl_list_insert(&compositor->local_subcompositor_resources, wl_resource_get_link(resource));
        }
    }
}

WleCompositor *
wle_compositor_new(struct wl_display *remote_display,
                   struct wl_compositor *remote_compositor,
                   struct wl_subcompositor *remote_subcompositor,
                   struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_compositor != NULL, NULL);
    g_return_val_if_fail(remote_subcompositor != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleCompositor *compositor = g_object_new(WLE_TYPE_COMPOSITOR, NULL);
    compositor->local_compositor = wl_global_create(local_display,
                                                    &wl_compositor_interface,
                                                    wl_proxy_get_version((struct wl_proxy *)remote_compositor),
                                                    compositor,
                                                    local_compositor_bind);
    compositor->local_subcompositor = wl_global_create(local_display,
                                                       &wl_subcompositor_interface,
                                                       wl_proxy_get_version((struct wl_proxy *)remote_subcompositor),
                                                       compositor,
                                                       local_subcompositor_bind);

    if (compositor->local_compositor == NULL || compositor->local_subcompositor == NULL) {
        if (compositor->local_compositor != NULL) {
            wl_global_destroy(compositor->local_compositor);
        }
        if (compositor->local_subcompositor != NULL) {
            wl_global_destroy(compositor->local_subcompositor);
        }
        g_free(compositor);
        return NULL;
    } else {
        compositor->remote_display = remote_display;
        compositor->remote_compositor = remote_compositor;
        compositor->remote_subcompositor = remote_subcompositor;
        return compositor;
    }
}
