/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SECTION:wle-embedded-compositor
 * @title: WleEmbeddedCompositor
 * @short_description: The main embedded compositor instance object
 * @stability: Unstable
 * @include: libwlembed/libwlembed.h
 *
 * #WleEmbeddedCompositor is an embedded compositor instance.  It
 * needs to be passed a @wl_display instance of the currently-running
 * compositor, and will open a nested display with the given socket name.
 *
 * Embedded applications can be launched using @wle_embedded_compositor_spawn()
 * and @wle_embedded_compositor_spawn_command_line().  Before running the child
 * process, %WAYLAND_DISPLAY will be set in the child's environment to point to
 * the embedded compositor.  Child processes can access to the parent
 * compositor if needed by consulting %WAYLAND_PARENT_DISPLAY.
 **/

#include <gio/gio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wayland-client.h>
#include <wayland-server-core.h>

#include "common/wle-debug.h"
#include "wle-child-process-manager.h"
#include "wle-compositor-bridge.h"
#include "wle-compositor.h"
#include "wle-embedded-compositor-private.h"
#include "wle-embedded-compositor.h"
#include "wle-embedded-view-private.h"
#include "wle-embedding-manager.h"
#include "wle-error.h"
#include "wle-marshal.h"
#include "wle-toplevel.h"
#include "wle-view.h"
#include "libwlembed-visibility.h"

struct _WleEmbeddedCompositor {
    GObject parent;

    struct wl_display *display;
    char *socket_name;
    int display_fd;

    GList *outputs;  // WleOutput

    struct wl_display *remote_display;

    WleCompositorBridge *bridge;
    WleEmbeddingManager *embedding_manager;

    GList *embedded_views;  // WleEmbeddedView

    WleChildProcessManager *child_process_manager;

    guint32 manage_child_processes : 1,
        closed : 1;
};

enum {
    PROP0,
    PROP_SOCKET_NAME,
    PROP_REMOTE_DISPLAY,
    PROP_MANAGE_CHILD_PROCESSES,
};

enum {
    SIG_NEW_EMBEDDED_VIEW,
    SIG_ERROR,
    SIG_CLOSED,

    N_SIGNALS,
};


static void wle_embedded_compositor_initable_init(GInitableIface *iface);

static gboolean wle_embedded_compositor_initable_real_init(GInitable *initable,
                                                           GCancellable *cancellable,
                                                           GError **error);
static void wle_embedded_compositor_set_property(GObject *object,
                                                 guint prop_id,
                                                 const GValue *value,
                                                 GParamSpec *pspec);
static void wle_embedded_compositor_get_property(GObject *object,
                                                 guint prop_id,
                                                 GValue *value,
                                                 GParamSpec *pspec);
static void wle_embedded_compositor_finalize(GObject *object);

static gboolean bool_handled_accum(GSignalInvocationHint *hint,
                                   GValue *return_accum,
                                   const GValue *handler_return,
                                   gpointer data);

static void new_seat(WleCompositorBridge *bridge,
                     WleSeat *seat,
                     WleEmbeddedCompositor *ec);

static void new_view(WleCompositor *compositor,
                     WleView *view,
                     WleEmbeddedCompositor *ec);

static WleEmbeddedView *embedding_manager_new_embedded_surface(WleEmbeddingManager *manager,
                                                               WleView *view,
                                                               const gchar *embedding_token,
                                                               WleEmbeddedCompositor *ec);
static void embedding_manager_error(WleEmbeddingManager *manager,
                                    GError *error,
                                    WleEmbeddedCompositor *ec);

static void child_process_exited(GPid pid,
                                 gint wait_status,
                                 gpointer user_data);


G_DEFINE_FINAL_TYPE_WITH_CODE(WleEmbeddedCompositor,
                              wle_embedded_compositor,
                              G_TYPE_OBJECT,
                              G_IMPLEMENT_INTERFACE(G_TYPE_INITABLE, wle_embedded_compositor_initable_init))


static guint signals[N_SIGNALS] = { 0 };


static void
wle_embedded_compositor_class_init(WleEmbeddedCompositorClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->set_property = wle_embedded_compositor_set_property;
    gobject_class->get_property = wle_embedded_compositor_get_property;
    gobject_class->finalize = wle_embedded_compositor_finalize;

    /**
     * WleEmbeddedCompositor:socket-name:
     *
     * The name of the embedded compositor display's socket.
     **/
    g_object_class_install_property(gobject_class,
                                    PROP_SOCKET_NAME,
                                    g_param_spec_string("socket-name",
                                                        "socket-name",
                                                        "The name to use for the Wayland socket",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    /**
     * WleEmbeddedCompositor:remote-display:
     *
     * The #wl_display handle of the parent compositor.
     **/
    g_object_class_install_property(gobject_class,
                                    PROP_REMOTE_DISPLAY,
                                    g_param_spec_pointer("remote-display",
                                                         "remote-display",
                                                         "The Wayland display that we are connected to",
                                                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    /**
     * WleEmbeddedCompositor:manage-child-processes:
     *
     * Whether or not the embedded compositor should terminate child processes
     * when the embedded compositor shuts down.
     *
     * The processes will be terminated by sending SIGTERM; if a process does
     * not respond and terminate, it will remain running, and the embedded
     * compositor will no longer track it.
     *
     * If @G_SPAWN_DO_NOT_REAP_CHILD is passed to
     * @wle_embedded_compositor_spawn(), the embedded compositor will be unable
     * to track and manage the process for you; you will be responsible for
     * managing its lifecycle in that case.
     *
     * When changing this property, the new value will also take effect for
     * processes started before the change.
     *
     * We will only terminate child processes that were created using
     * @wle_embedded_compositor_spawn() or
     * @wle_embedded_compositor_spawn_command_line().  If you spawn child
     * processes in another manner, you will have to manage their lifecycle on
     * your own.
     **/
    g_object_class_install_property(gobject_class,
                                    PROP_MANAGE_CHILD_PROCESSES,
                                    g_param_spec_boolean("manage-child-processes",
                                                         "manage-child-processes",
                                                         "Whether or not the embedded compositor will terminate children before shutting down",
                                                         TRUE,
                                                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    /**
     * WleEmbeddedCompositor::new-embedded-view:
     * @ec: A #WleEmbeddedCompositor.
     * @embedding_token: The embedding token passed by the client.
     * @embedded_view: The new #WleEmbeddedView.
     *
     * Emitted when a surface created by a client connected to the embedded
     * compositor wishes to be embedded rather than forwarded to the parent
     * compositor.
     *
     * Return value: %TRUE if the new @embedded_view was expected and has been
     * accepted by the application, or %FALSE on error.
     **/
    signals[SIG_NEW_EMBEDDED_VIEW] = g_signal_new("new-embedded-view",
                                                  WLE_TYPE_EMBEDDED_COMPOSITOR,
                                                  G_SIGNAL_RUN_LAST,
                                                  0,
                                                  bool_handled_accum,
                                                  NULL,
                                                  _wle_marshal_BOOLEAN__STRING_OBJECT,
                                                  G_TYPE_BOOLEAN,
                                                  2,
                                                  G_TYPE_STRING,
                                                  WLE_TYPE_EMBEDDED_VIEW);

    /**
     * WleEmbeddedCompositor::error:
     * @embed: A #WleEmbeddedCompositor.
     * @error: A #GError.
     *
     * Emitted when the embedded compositor encounters a non-fatal error.
     * The compositor will still be running and can still be used, but it's
     * possible some applications or surfaces will not be displayed properly.
     **/
    signals[SIG_ERROR] = g_signal_new("error",
                                      WLE_TYPE_EMBEDDED_COMPOSITOR,
                                      G_SIGNAL_RUN_LAST,
                                      0,
                                      NULL, NULL,
                                      g_cclosure_marshal_VOID__BOXED,
                                      G_TYPE_NONE, 1,
                                      G_TYPE_ERROR);

    /**
     * WleEmbeddedCompositor::closed:
     * @embed: A #WleEmbeddedCompositor.
     * @error: (nullable): An error that describes why the compositor was
     *                     closed, or %NULL.
     *
     * Emitted when the embedded compositor has shut down. The compositor may
     * no longer be used once this signal is received, and embedded
     * applications have been terminated.
     **/
    signals[SIG_CLOSED] = g_signal_new("closed",
                                       WLE_TYPE_EMBEDDED_COMPOSITOR,
                                       G_SIGNAL_RUN_LAST,
                                       0,
                                       NULL, NULL,
                                       g_cclosure_marshal_VOID__BOXED,
                                       G_TYPE_NONE, 1,
                                       G_TYPE_ERROR);
}


static void
wle_embedded_compositor_initable_init(GInitableIface *iface) {
    iface->init = wle_embedded_compositor_initable_real_init;
}

static void
wle_embedded_compositor_init(WleEmbeddedCompositor *ec) {
    ec->child_process_manager = wle_child_process_manager_new(child_process_exited, ec);
    ec->manage_child_processes = TRUE;
}

static void
wle_embedded_compositor_set_property(GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec) {
    WleEmbeddedCompositor *ec = WLE_EMBEDDED_COMPOSITOR(object);

    switch (prop_id) {
        case PROP_SOCKET_NAME:
            ec->socket_name = g_value_dup_string(value);
            break;

        case PROP_REMOTE_DISPLAY:
            ec->remote_display = g_value_get_pointer(value);
            break;

        case PROP_MANAGE_CHILD_PROCESSES:
            wle_embedded_compositor_set_manage_child_processes(ec, g_value_get_boolean(value));
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_embedded_compositor_get_property(GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec) {
    WleEmbeddedCompositor *ec = WLE_EMBEDDED_COMPOSITOR(object);

    switch (prop_id) {
        case PROP_SOCKET_NAME:
            g_value_set_string(value, ec->socket_name);
            break;

        case PROP_REMOTE_DISPLAY:
            g_value_set_pointer(value, ec->remote_display);
            break;

        case PROP_MANAGE_CHILD_PROCESSES:
            g_value_set_boolean(value, ec->manage_child_processes);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_embedded_compositor_finalize(GObject *object) {
    WleEmbeddedCompositor *ec = WLE_EMBEDDED_COMPOSITOR(object);

    if (ec->manage_child_processes) {
        wle_child_process_manager_terminate_all(ec->child_process_manager);
    }
    wle_child_process_manager_destroy(ec->child_process_manager);

    for (GList *l = ec->embedded_views; l != NULL; l = l->next) {
        g_signal_handlers_disconnect_by_data(l->data, ec);
        g_object_unref(l->data);
    }
    g_list_free(ec->embedded_views);

    if (ec->embedding_manager != NULL) {
        g_object_unref(ec->embedding_manager);
    }

    g_list_free_full(ec->outputs, g_object_unref);

    if (ec->bridge != NULL) {
        g_object_unref(ec->bridge);
    }

    if (ec->display != NULL) {
        wl_display_destroy(ec->display);
    }

    g_free(ec->socket_name);

    G_OBJECT_CLASS(wle_embedded_compositor_parent_class)->finalize(object);
}

static gboolean
wle_embedded_compositor_initable_real_init(GInitable *initable, GCancellable *cancellable, GError **error) {
    WleEmbeddedCompositor *ec = WLE_EMBEDDED_COMPOSITOR(initable);

    ec->display = wl_display_create();
    if (ec->display == NULL) {
        g_set_error_literal(error, WLE_ERROR, WLE_ERROR_DISPLAY_ERROR, "Failed to create local Wayland display");
        return FALSE;
    }

    ec->bridge = wle_compositor_bridge_new(ec->remote_display,
                                           ec->display,
                                           error);
    if (ec->bridge == NULL) {
        // error should be set by _bridge_new() above
        return FALSE;
    }

    WleCompositor *compositor = wle_compositor_bridge_get_compositor(ec->bridge);
    g_signal_connect(compositor, "new-view",
                     G_CALLBACK(new_view), ec);

    ec->embedding_manager = wle_embedding_manager_new(ec->display);
    g_signal_connect(ec->embedding_manager, "new-embedded-surface",
                     G_CALLBACK(embedding_manager_new_embedded_surface), ec);
    g_signal_connect(ec->embedding_manager, "error",
                     G_CALLBACK(embedding_manager_error), ec);

    for (GList *l = wle_compositor_bridge_get_seats(ec->bridge); l != NULL; l = l->next) {
        WleSeat *seat = WLE_SEAT(l->data);
        new_seat(ec->bridge, seat, ec);
    }
    g_signal_connect(ec->bridge, "new-seat",
                     G_CALLBACK(new_seat), ec);

    if (wl_display_add_socket(ec->display, ec->socket_name) < 0) {
        g_set_error(error,
                    WLE_ERROR,
                    WLE_ERROR_DISPLAY_ERROR,
                    "Failed to create local compositor socket '%s'",
                    ec->socket_name);
        return FALSE;
    }

    struct wl_event_loop *loop = wl_display_get_event_loop(ec->display);
    ec->display_fd = wl_event_loop_get_fd(loop);

    if (ec->display_fd < 0) {
        g_set_error_literal(error,
                            WLE_ERROR,
                            WLE_ERROR_DISPLAY_ERROR,
                            "Invalid file descriptor for Wayland socket");
        return FALSE;
    }

    return TRUE;
}

static gboolean
bool_handled_accum(GSignalInvocationHint *hint, GValue *return_accum, const GValue *handler_return, gpointer data) {
    gboolean handled = g_value_get_boolean(handler_return);
    g_value_set_boolean(return_accum, handled);
    return !handled;
}

static WleEmbeddedView *
embedded_view_for_root_view(WleEmbeddedCompositor *ec, WleView *root_view) {
    for (GList *l = ec->embedded_views; l != NULL; l = l->next) {
        WleEmbeddedView *eview = WLE_EMBEDDED_VIEW(l->data);
        if (_wle_embedded_view_get_root_view(eview) == root_view) {
            return eview;
        }
    }
    return NULL;
}

static void
seat_focus_changed(WleSeat *seat, WleView *focused_view, uint32_t serial, WleEmbeddedCompositor *ec) {
    if (focused_view != NULL && _wle_view_is_embedded(focused_view)) {
        // If the focus change was synthesized (like due to a button click),
        // often the parent won't actually get a proper focus-in event, so
        // let's try to let them know.
        WleEmbeddedView *eview = embedded_view_for_root_view(ec, focused_view);
        if (eview != NULL) {
            _wle_embedded_view_focus_in(eview);
        }
    }
}

static void
new_seat(WleCompositorBridge *bridge, WleSeat *seat, WleEmbeddedCompositor *ec) {
    TRACE("entering");
    g_signal_connect(seat, "focus-changed",
                     G_CALLBACK(seat_focus_changed), ec);
}

static void
view_mapped(WleEmbeddedCompositor *ec, WleView *view) {
    TRACE("entering");
}

static void
view_unmapped(WleEmbeddedCompositor *ec, WleView *view) {
    TRACE("entering");
}

static void
view_activated(WleEmbeddedCompositor *ec, uint32_t serial, WleView *view) {
    DBG("focusing forwarded view");
}

static void
view_configured(WleEmbeddedCompositor *ec, uint32_t serial, WleView *view) {
    TRACE("entering");
    WleViewRole *view_role = wle_view_get_role(view);
    if (WLE_IS_XDG_SURFACE(view_role)) {
        int32_t width, height;
        wle_xdg_surface_get_size(WLE_XDG_SURFACE(view_role), &width, &height);
        if (width > 0 && height > 0) {
            if (WLE_IS_TOPLEVEL(view) && _wle_view_is_embedded(view)) {
                WleEmbeddedView *embedded_view = embedded_view_for_root_view(ec, view);
                if (embedded_view != NULL) {
                    _wle_embedded_view_size_request(embedded_view, width, height);
                }
            }
        }
    }
}

static void
view_role_changed(WleView *view, WleEmbeddedCompositor *ec) {
    if (wle_view_get_role(view) != NULL) {
        g_signal_connect_swapped(view, "map",
                                 G_CALLBACK(view_mapped), ec);
        g_signal_connect_swapped(view, "unmap",
                                 G_CALLBACK(view_unmapped), ec);
        g_signal_connect_swapped(view, "configure",
                                 G_CALLBACK(view_configured), ec);
        if (WLE_IS_TOPLEVEL(wle_view_get_role(view))) {
            g_signal_connect_swapped(view, "activated",
                                     G_CALLBACK(view_activated), ec);
        }
    }
}

static void
new_view(WleCompositor *compositor, WleView *view, WleEmbeddedCompositor *ec) {
    g_signal_connect(view, "role-changed",
                     G_CALLBACK(view_role_changed), ec);
}

static void
embedded_view_closed(WleEmbeddedView *eview, GError *error, WleEmbeddedCompositor *ec) {
    GList *link = g_list_find(ec->embedded_views, eview);
    if (G_LIKELY(link != NULL)) {
        g_signal_handlers_disconnect_by_data(eview, ec);
        ec->embedded_views = g_list_delete_link(ec->embedded_views, link);
        g_object_unref(eview);
    }
}

static WleEmbeddedView *
embedding_manager_new_embedded_surface(WleEmbeddingManager *manager,
                                       WleView *view,
                                       const gchar *embedding_token,
                                       WleEmbeddedCompositor *ec) {
    if (!_wle_view_set_embedded(view)) {
        g_message("Failed to embed surface for client presenting token %s", embedding_token);
        return FALSE;
        return NULL;
    } else {
        WleEmbeddedView *embedded_view = _wle_embedded_view_new(ec,
                                                                ec->bridge,
                                                                view);
        ec->embedded_views = g_list_prepend(ec->embedded_views, embedded_view);
        g_signal_connect_after(embedded_view, "closed",
                               G_CALLBACK(embedded_view_closed), ec);

        gboolean handled = FALSE;
        g_signal_emit(ec, signals[SIG_NEW_EMBEDDED_VIEW], 0, embedding_token, embedded_view, &handled);

        if (handled) {
            return embedded_view;
        } else {
            ec->embedded_views = g_list_remove(ec->embedded_views, embedded_view);
            g_signal_handlers_disconnect_by_data(embedded_view, ec);
            g_object_unref(embedded_view);
            return NULL;
        }
    }
}

static void
embedding_manager_error(WleEmbeddingManager *manager, GError *error, WleEmbeddedCompositor *ec) {
    g_signal_emit(ec, signals[SIG_ERROR], 0, error);
}

static void
child_process_exited(GPid pid, gint wait_status, gpointer user_data) {
    TRACE("entering");

    if (WIFEXITED(wait_status)) {
        if (WEXITSTATUS(wait_status) == 0) {
            DBG("Client exited normally");
        } else {
            g_message("Embedded compositor client child process %d exited with status %d",
                      pid,
                      WEXITSTATUS(wait_status));
        }
    } else if (WIFSIGNALED(wait_status)) {
        g_message("Embedded compositor client child process %d terminated with signal %d", pid, WTERMSIG(wait_status));
    } else if (WIFSTOPPED(wait_status)) {
        g_message("Embedded compositor client child process %d stopped with signal %d", pid, WSTOPSIG(wait_status));
    } else {
        g_message("Embedded compositor client child process %d terminated abnormally", pid);
    }
}

/**
 * wle_embedded_compositor_new: (constructor)
 * @socket_name: (not nullable): Name to use for the socket for the embedded
 *                               compositor.
 * @remote_display: (not nullable): A handle to the parent
 *                                  compositor's display.
 * @error: An optional #GError return location.
 *
 * Creates a new embedded compositor using the specified @socket_name,
 * connected to the specified @remote_display.
 *
 * If creation fails for any reason, @error will be set, and should be freed
 * with #g_error_free().
 *
 * Note that if you are using GTK, you will want to use
 * #wle_gtk_create_embedded_compositor() instead, as it will set up a #GSource
 * to handle the embedded display for you.
 *
 * Return value: (nullable) (transfer full): A new #WleEmbeddedCompositor
 * instance, or %NULL if an error occurs.
 **/
WleEmbeddedCompositor *
wle_embedded_compositor_new(const gchar *socket_name, struct wl_display *remote_display, GError **error) {
    return g_initable_new(WLE_TYPE_EMBEDDED_COMPOSITOR, NULL, error,
                          "socket-name", socket_name,
                          "remote-display", remote_display,
                          NULL);
}

/**
 * wle_embedded_compositor_get_socket_name:
 * @ec: A #WleEmbeddedCompositor.
 *
 * Returns the Wayland socket name for the embedded compositor.  If not using
 * the built-in process spawn functions, this can be used to pass
 * %WAYLAND_DISPLAY to child processes.
 *
 * Return value: (not nullable) (transfer none): A non-%NULL string.
 **/
const gchar *
wle_embedded_compositor_get_socket_name(WleEmbeddedCompositor *ec) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), NULL);
    return ec->socket_name;
}

/**
 * wle_embedded_compositor_get_local_display:
 * @ec: A #WleEmbeddedCompositor.
 *
 * Returns @ec's Wayland "server" display.  You will need to "run" this display
 * by calling the appropriate functions on it as a part of your main event
 * loop.
 *
 * Return value: (not nullable) (transfer none): A #struct wl_display.
 **/
struct wl_display *
wle_embedded_compositor_get_local_display(WleEmbeddedCompositor *ec) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), NULL);
    return ec->display;
}

void
wle_embedded_compositor_set_manage_child_processes(WleEmbeddedCompositor *ec, gboolean manage_child_processes) {
    g_return_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec));
    ec->manage_child_processes = manage_child_processes;
}

/**
 * wle_embedded_compositor_generate_embedding_token:
 * @ec: A #WleEmbeddedCompositor.
 *
 * Generates a token that can be used in a client application that wishes to be
 * embedded in the embedded compositor.  The value returned should be passed to
 * the wle_embedding_manager.create_embedded_surface() protocol request in the
 * child process.
 *
 * If you are using GTK3, see #WleGtkSocket and #WleGtkPlug for an easier way
 * of doing this.
 *
 * Return value: (transfer none): An opaque embedding token, owned by @ec.
 **/
const gchar *
wle_embedded_compositor_generate_embedding_token(WleEmbeddedCompositor *ec) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), NULL);
    return _wle_embedding_manager_generate_token(ec->embedding_manager);
}

/**
 * wle_embedded_compositor_spawn_with_pipes:
 * @ec: A #WleEmbeddedCompositor.
 * @working_directory: (type filename) (nullable): The working directory for
 *                                                 the launched application, or
 *                                                 %NULL to inherit the parent's.
 * @argv: (array zero-terminated=1) (element-type filename) (not nullable): The
 *        argument vector for the program to run, terminated by a %NULL element.
 * @envp: (array zero-terminated=1) (element-type filename) (nullable): A list
 *        of %=-separated environment variables to set in the new process,
 *        terminated by a %NULL string, or %NULL to inherit the parent's
 *        environment.
 * @flags: #GSpawnFlags flags.  Note that @G_SPAWN_DO_NOT_REAP_CHILD is not
 *         supported; if you need this functionality, you must spawn child
 *         processes yourself, being sure to pass the value of
 *         @wle_embedded_compositor_get_socket_name() to the child in the
 *         %WAYLAND_DISPLAY environment variable.
 * @child_setup: (nullable) (scope async) (closure child_setup_user_data): A
 *               function to run in the child process, but before launching
 *               the program specified in @argv.
 * @child_setup_user_data: (nullable): A user-data pointer passed to the
 *                                     @child_setup function.
 * @child_pid: (out) (optional): An optional location to store the process ID
 *                               of the spawned program.
 * @standard_input: (out) (optional): An optional locaiton to store a file
 *                                    descriptor that can be used to write to
 *                                    the child's standard input.
 * @standard_output: (out) (optional): An optional locaiton to store a file
 *                                     descriptor that can be used to read from
 *                                     the child's standard output.
 * @standard_error: (out) (optional): An optional locaiton to store a file
 *                                    descriptor that can be used to read from
 *                                    the child's standard error.
 * @error: An optional #GError return location.
 *
 * Launches a child process that can create surfaces that should be embedded in
 * the parent using the embedded compositor.
 *
 * The %WAYLAND_DISPLAY environment variable will be set in the child process
 * to refer to the embedded compositor.  %WAYLAND_PARENT_DISPLAY will be set to
 * refer to the parent compositor.
 *
 * You will probably want to pass the embedding token for this process somehow,
 * either as a command line argument in @argv, as an environment variable in
 * @envp, or by using @child_setup to set an environment variable
 *
 * Return value: %TRUE if the child was successfully spawned, %FALSE on error,
 * in which case @error will be set.
 **/
gboolean
wle_embedded_compositor_spawn_with_pipes(WleEmbeddedCompositor *ec,
                                         const gchar *working_directory,
                                         gchar **argv,
                                         gchar **envp,
                                         GSpawnFlags flags,
                                         GSpawnChildSetupFunc child_setup,
                                         gpointer child_setup_user_data,
                                         GPid *child_pid,
                                         gint *standard_input,
                                         gint *standard_output,
                                         gint *standard_error,
                                         GError **error) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), FALSE);
    g_return_val_if_fail(!ec->closed, FALSE);
    g_return_val_if_fail(argv != NULL && argv[0] != NULL && argv[0][0] != '\0', FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    gchar **cur_envp = NULL;
    if (envp == NULL) {
        envp = cur_envp = g_get_environ();
    }

    GStrvBuilder *builder = g_strv_builder_new();

    gchar *display_env = g_strconcat("WAYLAND_DISPLAY=", ec->socket_name, NULL);
    g_strv_builder_add(builder, display_env);
    g_free(display_env);

    const gchar *cur_display = g_getenv("WAYLAND_DISPLAY");
    if (G_LIKELY(cur_display != NULL)) {
        gchar *parent_display_env = g_strconcat("WAYLAND_PARENT_DISPLAY=", cur_display, NULL);
        g_strv_builder_add(builder, parent_display_env);
        g_free(parent_display_env);
    }

    for (gsize i = 0; envp[i] != NULL; ++i) {
        if (!g_str_has_prefix(envp[i], "WAYLAND_DISPLAY=") && !g_str_has_prefix(envp[i], "WAYLAND_PARENT_DISPLAY=")) {
            g_strv_builder_add(builder, envp[i]);
        }
    }

    gchar **new_envp = g_strv_builder_end(builder);
    g_strv_builder_unref(builder);

    // If the caller has passed G_SPAWN_DO_NOT_REAP_CHILD, then they will
    // presumably be calling g_child_watch_add() on the returned PID, which
    // means we cannot do the same (glib only allows active child watch per
    // PID).  So we can only manage the child process if that flag was not
    // passed.
    gboolean can_manage = (flags & G_SPAWN_DO_NOT_REAP_CHILD) == 0;

    GPid pid = 0;
    gboolean result = g_spawn_async_with_pipes(working_directory,
                                               argv,
                                               new_envp,
                                               flags | G_SPAWN_DO_NOT_REAP_CHILD,
                                               child_setup,
                                               child_setup_user_data,
                                               &pid,
                                               standard_input,
                                               standard_output,
                                               standard_error,
                                               error);

    if (result) {
        if (child_pid != NULL) {
            *child_pid = pid;
        }

        if (can_manage) {
            wle_child_process_manager_add_pid(ec->child_process_manager, pid);
        }
    }

    g_strfreev(new_envp);
    g_strfreev(cur_envp);

    return result;
}

/**
 * wle_embedded_compositor_spawn:
 * @ec: A #WleEmbeddedCompositor.
 * @working_directory: (type filename) (nullable): The working directory for
 *                                                 the launched application, or
 *                                                 %NULL to inherit the parent's.
 * @argv: (array zero-terminated=1) (element-type filename) (not nullable): The
 *        argument vector for the program to run, terminated by a %NULL element.
 * @envp: (array zero-terminated=1) (element-type filename) (nullable): A list
 *        of %=-separated environment variables to set in the new process,
 *        terminated by a %NULL string, or %NULL to inherit the parent's
 *        environment.
 * @flags: #GSpawnFlags flags.  Note that @G_SPAWN_DO_NOT_REAP_CHILD is not
 *         supported; if you need this functionality, you must spawn child
 *         processes yourself, being sure to pass the value of
 *         @wle_embedded_compositor_get_socket_name() to the child in the
 *         %WAYLAND_DISPLAY environment variable.
 * @child_setup: (nullable) (scope async) (closure child_setup_user_data): A
 *               function to run in the child process, but before launching
 *               the program specified in @argv.
 * @child_setup_user_data: (nullable): A user-data pointer passed to the
 *                                     @child_setup function.
 * @child_pid: (out) (optional): An optional location to store the process ID
 *                               of the spawned program.
 * @error: An optional #GError return location.
 *
 * Launches a child process that can create surfaces that should be embedded in
 * the parent using the embedded compositor.
 *
 * The %WAYLAND_DISPLAY environment variable will be set in the child process
 * to refer to the embedded compositor.  %WAYLAND_PARENT_DISPLAY will be set to
 * refer to the parent compositor.
 *
 * You will probably want to pass the embedding token for this process somehow,
 * either as a command line argument in @argv, as an environment variable in
 * @envp, or by using @child_setup to set an environment variable
 *
 * Return value: %TRUE if the child was successfully spawned, %FALSE on error,
 * in which case @error will be set.
 **/
gboolean
wle_embedded_compositor_spawn(WleEmbeddedCompositor *ec,
                              const gchar *working_directory,
                              gchar **argv,
                              gchar **envp,
                              GSpawnFlags flags,
                              GSpawnChildSetupFunc child_setup,
                              gpointer child_setup_user_data,
                              GPid *child_pid,
                              GError **error) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), FALSE);
    g_return_val_if_fail(!ec->closed, FALSE);
    g_return_val_if_fail(argv != NULL && argv[0] != NULL && argv[0][0] != '\0', FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    return wle_embedded_compositor_spawn_with_pipes(ec,
                                                    working_directory,
                                                    argv,
                                                    envp,
                                                    flags,
                                                    child_setup,
                                                    child_setup_user_data,
                                                    child_pid,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    error);
}

/**
 * wle_embedded_compositor_spawn_command_line:
 * @ec: A #WleEmbeddedCompositor.
 * @command_line: (not nullable): A command to run the embedded application.
 * @error: An optional #GError return location.
 *
 * Launches a child process that can create surfaces that should be embedded in
 * the parent using the embedded compositor.
 *
 * The %WAYLAND_DISPLAY environment variable will be set in the child process
 * to refer to the embedded compositor.  %WAYLAND_PARENT_DISPLAY will be set to
 * refer to the parent compositor.
 *
 * You will probably want to pass the embedding token for this process somehow,
 * probably through a command line argument.
 *
 * Return value: %TRUE if the child was successfully spawned, %FALSE on error,
 * in which case @error will be set.
 **/
gboolean
wle_embedded_compositor_spawn_command_line(WleEmbeddedCompositor *ec,
                                           const gchar *command_line,
                                           GError **error) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), FALSE);
    g_return_val_if_fail(!ec->closed, FALSE);
    g_return_val_if_fail(command_line != NULL && command_line[0] != '\0', FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    gchar **argv = NULL;
    if (!g_shell_parse_argv(command_line, NULL, &argv, error)) {
        return FALSE;
    }

    gboolean result = wle_embedded_compositor_spawn(ec,
                                                    NULL,
                                                    argv,
                                                    NULL,
                                                    G_SPAWN_SEARCH_PATH,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    error);

    g_strfreev(argv);

    return result;
}

/**
 * wle_embedded_compositor_list_seats:
 * @ec: A #WleEmbeddedCompositor.
 *
 * List all seats known to the embedded compositor.
 *
 * Return value: (not nullable) (element-type WleSeat) (transfer none): A
 * #GList of #WleSeat instances.  Both the instances and container are owned by
 * @ec.
 **/
GList *
wle_embedded_compositor_list_seats(WleEmbeddedCompositor *ec) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), NULL);
    return wle_compositor_bridge_get_seats(ec->bridge);
}

/**
 * wle_embedded_compositor_fatal_error:
 * @ec: A #WleEmbeddedCompositor.
 * @error: A #GError.
 *
 * Signals to @ec that a fatal error has occured (usually with the display
 * connection), and that it should shut down and emit its "closed" signal.
 **/
void
wle_embedded_compositor_fatal_error(WleEmbeddedCompositor *ec, GError *error) {
    g_return_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec));
    g_return_if_fail(error != NULL);

    ec->closed = TRUE;
    g_signal_emit(ec, signals[SIG_CLOSED], 0, error);
}

WleEmbeddingManager *
_wle_embedded_compositor_get_embedding_manager(WleEmbeddedCompositor *ec) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), NULL);
    return ec->embedding_manager;
}

#define __WLE_EMBEDDED_COMPOSITOR_C__
#include <libwlembed-visibility.c>
