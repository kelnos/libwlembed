/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_EMBEDDED_COMPOSITOR_PRIVATE_H__
#define __WLE_EMBEDDED_COMPOSITOR_PRIVATE_H__

#include "wle-embedded-compositor.h"
#include "wle-embedding-manager.h"

G_BEGIN_DECLS

WleEmbeddingManager *_wle_embedded_compositor_get_embedding_manager(WleEmbeddedCompositor *ec);

G_END_DECLS

#endif /* __WLE_EMBEDDED_COMPOSITOR_PRIVATE_H__ */
