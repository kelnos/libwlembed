/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "protocol/xdg-shell-protocol.h"
#include "wle-error.h"
#include "wle-popup.h"
#include "wle-seat-private.h"

struct _WlePopup {
    WleXdgSurface parent;

    struct xdg_popup *remote_xdg_popup;
    struct wl_resource *local_xdg_popup;

    WlePositioner *positioner;
    WleView *parent_view;

    int32_t parent_offset_x;
    int32_t parent_offset_y;
    struct {
        int32_t x;
        int32_t y;
        int32_t width;
        int32_t height;
    } parent_geometry;
};

static void wle_popup_finalize(GObject *object);

static void wle_popup_destroy(WleLocalResource *local_resource);

static void remote_xdg_popup_configure(void *data,
                                       struct xdg_popup *xdg_popup,
                                       int32_t x,
                                       int32_t y,
                                       int32_t width,
                                       int32_t height);
static void remote_xdg_popup_repositioned(void *data,
                                          struct xdg_popup *xdg_popup,
                                          uint32_t reposition_token);
static void remote_xdg_popup_done(void *data,
                                  struct xdg_popup *xdg_popup);

static void local_xdg_popup_grab(struct wl_client *client,
                                 struct wl_resource *xdg_popup_resource,
                                 struct wl_resource *seat_resource,
                                 uint32_t serial);
static void local_xdg_popup_reposition(struct wl_client *client,
                                       struct wl_resource *xdg_popup_resource,
                                       struct wl_resource *new_positioner_resource,
                                       uint32_t reposition_token);
static void local_xdg_popup_destroy(struct wl_client *client,
                                    struct wl_resource *xdg_popup_resource);


G_DEFINE_TYPE(WlePopup, wle_popup, WLE_TYPE_XDG_SURFACE)


static const struct xdg_popup_listener remote_xdg_popup_listener = {
    .configure = remote_xdg_popup_configure,
    .repositioned = remote_xdg_popup_repositioned,
    .popup_done = remote_xdg_popup_done,
};

static const struct xdg_popup_interface local_xdg_popup_impl = {
    .grab = local_xdg_popup_grab,
    .reposition = local_xdg_popup_reposition,
    .destroy = local_xdg_popup_destroy,
};

static void
wle_popup_class_init(WlePopupClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_popup_finalize;

    WleLocalResourceClass *local_resource_class = WLE_LOCAL_RESOURCE_CLASS(klass);
    local_resource_class->destroy = wle_popup_destroy;
}

static void
wle_popup_init(WlePopup *popup) {}

static void
wle_popup_finalize(GObject *object) {
    WlePopup *popup = WLE_POPUP(object);

    if (popup->parent_view != NULL) {
        g_signal_handlers_disconnect_by_data(popup->parent_view, popup);
        if (wle_view_get_role(popup->parent_view) != NULL) {
            g_signal_handlers_disconnect_by_data(wle_view_get_role(popup->parent_view), popup);
        }
    }

    if (popup->remote_xdg_popup != NULL) {
        xdg_popup_destroy(popup->remote_xdg_popup);
    }

    if (popup->local_xdg_popup != NULL) {
        wl_resource_destroy(popup->local_xdg_popup);
    }

    if (popup->positioner != NULL) {
        g_object_unref(popup->positioner);
    }

    G_OBJECT_CLASS(wle_popup_parent_class)->finalize(object);
}

static void
wle_popup_destroy(WleLocalResource *local_resource) {
    WlePopup *popup = WLE_POPUP(local_resource);

    if (popup->remote_xdg_popup != NULL) {
        xdg_popup_destroy(popup->remote_xdg_popup);
        popup->remote_xdg_popup = NULL;
    }

    if (popup->local_xdg_popup != NULL) {
        wl_resource_destroy(popup->local_xdg_popup);
        popup->local_xdg_popup = NULL;
    }

    WLE_LOCAL_RESOURCE_CLASS(wle_popup_parent_class)->destroy(local_resource);
}

static void
remote_xdg_popup_configure(void *data,
                           struct xdg_popup *xdg_popup,
                           int32_t x,
                           int32_t y,
                           int32_t width,
                           int32_t height) {
    WlePopup *popup = WLE_POPUP(data);
    wle_xdg_surface_update_size(WLE_XDG_SURFACE(popup), width, height);
    // XXX: do x & y need to be translated based on the parent when the parent is embedded?
    xdg_popup_send_configure(popup->local_xdg_popup, x, y, width, height);
}

static void
remote_xdg_popup_repositioned(void *data, struct xdg_popup *xdg_popup, uint32_t reposition_token) {
    WlePopup *popup = WLE_POPUP(data);
    if (wl_resource_get_version(popup->local_xdg_popup) >= XDG_POPUP_REPOSITIONED_SINCE_VERSION) {
        xdg_popup_send_repositioned(popup->local_xdg_popup, reposition_token);
    }
}

static void
remote_xdg_popup_done(void *data, struct xdg_popup *xdg_popup) {
    WlePopup *popup = WLE_POPUP(data);
    xdg_popup_send_popup_done(popup->local_xdg_popup);
}

static void
local_xdg_popup_grab(struct wl_client *client,
                     struct wl_resource *xdg_popup_resource,
                     struct wl_resource *seat_resource,
                     uint32_t serial) {
    WlePopup *popup = WLE_POPUP(wl_resource_get_user_data(xdg_popup_resource));
    if (popup->remote_xdg_popup != NULL) {
        WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(seat_resource));
        xdg_popup_grab(popup->remote_xdg_popup, _wle_seat_get_remote_seat(seat), serial);
    }
}

static void
local_xdg_popup_reposition(struct wl_client *client,
                           struct wl_resource *xdg_popup_resource,
                           struct wl_resource *new_positioner_resource,
                           uint32_t reposition_token) {
    WlePopup *popup = WLE_POPUP(wl_resource_get_user_data(xdg_popup_resource));
    WlePositioner *new_positioner = new_positioner_resource
                                        ? WLE_POSITIONER(wl_resource_get_user_data(new_positioner_resource))
                                        : NULL;

    if (new_positioner != NULL) {
        wl_resource_post_error(xdg_popup_resource,
                               WL_DISPLAY_ERROR_INVALID_OBJECT,
                               "null positioner object passed");
    } else {
        if (popup->positioner != NULL) {
            g_object_unref(popup->positioner);
            popup->positioner = NULL;
        }

        popup->positioner = g_object_ref(new_positioner);
        wle_positioner_set_parent_offset(popup->positioner, popup->parent_offset_x, popup->parent_offset_y);
        wle_positioner_set_parent_geometry(popup->positioner,
                                           popup->parent_geometry.x,
                                           popup->parent_geometry.y,
                                           popup->parent_geometry.width,
                                           popup->parent_geometry.height);

        if (popup->remote_xdg_popup != NULL) {
            xdg_popup_reposition(popup->remote_xdg_popup,
                                 wle_positioner_get_remote_positioner(popup->positioner),
                                 reposition_token);
        }
    }
}

static void
local_xdg_popup_destroy(struct wl_client *client, struct wl_resource *xdg_popup_resource) {
    wl_resource_destroy(xdg_popup_resource);
}

static void
local_xdg_popup_destroy_impl(struct wl_resource *resource) {
    WlePopup *popup = WLE_POPUP(wl_resource_get_user_data(resource));

    if (popup->remote_xdg_popup != NULL) {
        xdg_popup_destroy(popup->remote_xdg_popup);
        popup->remote_xdg_popup = NULL;
    }

    popup->local_xdg_popup = NULL;
    wle_local_resource_destroyed(WLE_LOCAL_RESOURCE(popup));
    g_object_unref(popup);
}

static void
parent_view_destroyed(WleView *parent_view, WlePopup *popup) {
    g_signal_handlers_disconnect_by_data(parent_view, popup);
    popup->parent_view = NULL;
}

static void
parent_role_destroyed(WleXdgSurface *parent_xdg_surface, WlePopup *popup) {
    g_signal_handlers_disconnect_by_data(parent_xdg_surface, popup);
}

static void
parent_view_embed_data_changed(WleView *parent_view, WlePopup *popup) {
    WleViewEmbedData *embed_data = wle_view_get_embed_data(WLE_VIEW(parent_view));
    if (embed_data != NULL) {
        popup->parent_offset_x = embed_data->offset_x;
        popup->parent_offset_y = embed_data->offset_y;
    } else {
        popup->parent_offset_x = 0;
        popup->parent_offset_y = 0;
    }

    if (popup->positioner != NULL) {
        wle_positioner_set_parent_offset(popup->positioner, popup->parent_offset_x, popup->parent_offset_y);
    }
}

static void
parent_xdg_surface_geometry_changed(WleXdgSurface *parent_view, WlePopup *popup) {
    if (popup->parent_view != NULL && WLE_IS_XDG_SURFACE(wle_view_get_role(popup->parent_view))) {
        wle_xdg_surface_get_window_geometry(WLE_XDG_SURFACE(wle_view_get_role(popup->parent_view)),
                                            &popup->parent_geometry.width,
                                            &popup->parent_geometry.height,
                                            &popup->parent_geometry.width,
                                            &popup->parent_geometry.height);

        if (popup->positioner != NULL) {
            wle_positioner_set_parent_geometry(popup->positioner,
                                               popup->parent_geometry.x,
                                               popup->parent_geometry.y,
                                               popup->parent_geometry.width,
                                               popup->parent_geometry.height);
        }
    }
}

static void
positioner_destroyed(WlePositioner *positioner, WlePopup *popup) {
    g_object_unref(popup->positioner);
    popup->positioner = NULL;
}

WlePopup *
_wle_popup_new(WleView *view,
               struct xdg_surface *remote_xdg_surface,
               struct wl_resource *local_xdg_surface,
               struct xdg_popup *remote_xdg_popup,
               struct wl_resource *local_xdg_popup,
               WlePositioner *positioner,
               WleView *parent_view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    g_return_val_if_fail(remote_xdg_surface != NULL, NULL);
    g_return_val_if_fail(local_xdg_surface != NULL, NULL);
    g_return_val_if_fail(remote_xdg_popup != NULL, NULL);
    g_return_val_if_fail(local_xdg_popup != NULL, NULL);
    g_return_val_if_fail(WLE_IS_POSITIONER(positioner), NULL);
    g_return_val_if_fail(WLE_IS_VIEW(parent_view), NULL);
    g_return_val_if_fail(WLE_IS_XDG_SURFACE(wle_view_get_role(parent_view)), NULL);

    WlePopup *popup = g_object_new(WLE_TYPE_POPUP,
                                   "role", WLE_ROLE_XDG_POPUP,
                                   "view", view,
                                   "remote-xdg-surface", remote_xdg_surface,
                                   "local-xdg-surface", local_xdg_surface,
                                   NULL);
    popup->remote_xdg_popup = remote_xdg_popup;
    popup->local_xdg_popup = local_xdg_popup;

    popup->positioner = g_object_ref(positioner);
    wle_positioner_get_parent_offset(positioner, &popup->parent_offset_x, &popup->parent_offset_y);
    wle_positioner_get_parent_geometry(positioner,
                                       &popup->parent_geometry.x,
                                       &popup->parent_geometry.y,
                                       &popup->parent_geometry.width,
                                       &popup->parent_geometry.height);
    g_signal_connect(positioner, "destroy",
                     G_CALLBACK(positioner_destroyed), popup);

    popup->parent_view = parent_view;
    g_signal_connect(parent_view, "destroy",
                     G_CALLBACK(parent_view_destroyed), popup);

    if (_wle_view_is_embedded(parent_view)) {
        WleXdgSurface *parent_xdg_surface = WLE_XDG_SURFACE(wle_view_get_role(parent_view));

        g_signal_connect(parent_view, "embed-data-changed",
                         G_CALLBACK(parent_view_embed_data_changed), popup);
        g_signal_connect(parent_xdg_surface, "geometry-changed",
                         G_CALLBACK(parent_xdg_surface_geometry_changed), popup);
        g_signal_connect(parent_xdg_surface, "destroy",
                         G_CALLBACK(parent_role_destroyed), popup);

        parent_view_embed_data_changed(parent_view, popup);
        parent_xdg_surface_geometry_changed(parent_xdg_surface, popup);
    }

    xdg_popup_add_listener(popup->remote_xdg_popup, &remote_xdg_popup_listener, popup);
    wl_resource_set_implementation(popup->local_xdg_popup, &local_xdg_popup_impl, popup, local_xdg_popup_destroy_impl);

    // local_xdg_popup takes a reference
    g_object_ref(popup);

    return popup;
}
