/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_DEBUG_H__
#define __WLE_DEBUG_H__

#include <glib.h>

#if defined(DEBUG) && defined(G_HAVE_GNUC_VARARGS)
#include <stdio.h>
#define DBG(fmt, args...) fprintf(stderr, "DBG[%s:%d] %s(): " fmt "\n", __FILE__, __LINE__, G_STRFUNC, ##args)
#elif defined(DEBUG) && defined(G_HAVE_ISO_VARARGS)
#include <stdio.h>
#define DBG(...) \
    G_STMT_START { \
        flockfile(stderr); \
        fprintf(stderr, "DBG[%s:%d] %s(): ", __FILE__, __LINE__, G_STRFUNC); \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); \
        funlockfile(stderr); \
    } \
    G_STMT_END
#else
#define DBG(fmt, ...) \
    G_STMT_START { (void)0; } \
    G_STMT_END
#endif

#if defined(DEBUG_TRACE) && defined(G_HAVE_GNUC_VARARGS)
#include <stdio.h>
#define TRACE(fmt, args...) fprintf(stderr, "TRACE[%s:%d] %s(): " fmt "\n", __FILE__, __LINE__, G_STRFUNC, ##args)
#elif defined(DEBUG_TRACE) && defined(G_HAVE_ISO_VARARGS)
#include <stdio.h>
#define TRACE(...) \
    G_STMT_START { \
        flockfile(stderr); \
        fprintf(stderr, "TRACE[%s:%d] %s(): ", __FILE__, __LINE__, G_STRFUNC); \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); \
        funlockfile(stderr); \
    } \
    G_STMT_END
#else
#define TRACE(fmt, ...) \
    G_STMT_START { (void)0; } \
    G_STMT_END
#endif

#endif /* __WLE_DEBUG_H__ */
