/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server-core.h>

#include "protocol/xdg-activation-v1-protocol.h"
#include "wle-seat-private.h"
#include "wle-util.h"
#include "wle-view.h"
#include "wle-xdg-activation.h"

struct _WleXdgActivation {
    struct wl_display *remote_display;
    struct xdg_activation_v1 *remote_activation;

    struct wl_global *local_activation;
    struct wl_list local_activation_resources;

    struct wl_list tokens;
};

typedef struct _WleActivationToken {
    struct xdg_activation_token_v1 *remote_activation_token;
    struct wl_resource *local_activation_token;
} WleActivationToken;

static void remote_activation_token_done(
    void *data,
    struct xdg_activation_token_v1 *remote_activation_token,
    const char *token);

static void local_activation_token_set_serial(
    struct wl_client *client,
    struct wl_resource *activation_token_resource,
    uint32_t serial,
    struct wl_resource *seat_resource);
static void local_activation_token_set_app_id(
    struct wl_client *client,
    struct wl_resource *activation_token_resource,
    const char *app_id);
static void local_activation_token_set_surface(
    struct wl_client *client,
    struct wl_resource *activation_token_resource,
    struct wl_resource *surface_resource);
static void local_activation_token_commit(
    struct wl_client *client,
    struct wl_resource *activation_token_resource);
static void local_activation_token_destroy(
    struct wl_client *client,
    struct wl_resource *activation_token_resource);


static void local_activation_destroy(
    struct wl_client *client,
    struct wl_resource *activation_resource);
static void local_activation_get_activation_token(
    struct wl_client *client,
    struct wl_resource *activation_resource,
    uint32_t id_id);
static void local_activation_activate(
    struct wl_client *client,
    struct wl_resource *activation_resource,
    const char *token,
    struct wl_resource *surface_resource);

static const struct xdg_activation_token_v1_listener remote_activation_token_listener = {
    .done = remote_activation_token_done,
};

static const struct xdg_activation_token_v1_interface local_activation_token_impl = {
    .set_serial = local_activation_token_set_serial,
    .set_app_id = local_activation_token_set_app_id,
    .set_surface = local_activation_token_set_surface,
    .commit = local_activation_token_commit,
    .destroy = local_activation_token_destroy,
};

static const struct xdg_activation_v1_interface local_activation_impl = {
    .destroy = local_activation_destroy,
    .get_activation_token = local_activation_get_activation_token,
    .activate = local_activation_activate,
};

static void
remote_activation_token_done(
    void *data,
    struct xdg_activation_token_v1 *remote_activation_token,
    const char *token) {
    WleActivationToken *activation_token = data;
    xdg_activation_token_v1_send_done(activation_token->local_activation_token, token);
}

static void
local_activation_token_set_serial(
    struct wl_client *client,
    struct wl_resource *activation_token_resource,
    uint32_t serial,
    struct wl_resource *seat_resource) {
    WleActivationToken *activation_token = wl_resource_get_user_data(activation_token_resource);
    WleSeat *seat = seat_resource != NULL ? WLE_SEAT(wl_resource_get_user_data(seat_resource)) : NULL;

    if (seat == NULL) {
        wl_client_post_implementation_error(client, "unknown or null seat");
    } else {
        xdg_activation_token_v1_set_serial(activation_token->remote_activation_token, serial, _wle_seat_get_remote_seat(seat));
    }
}

static void
local_activation_token_set_app_id(
    struct wl_client *client,
    struct wl_resource *activation_token_resource,
    const char *app_id) {
    WleActivationToken *activation_token = wl_resource_get_user_data(activation_token_resource);
    xdg_activation_token_v1_set_app_id(activation_token->remote_activation_token, app_id);
}

static void
local_activation_token_set_surface(
    struct wl_client *client,
    struct wl_resource *activation_token_resource,
    struct wl_resource *surface_resource) {
    WleActivationToken *activation_token = wl_resource_get_user_data(activation_token_resource);
    WleView *view = surface_resource != NULL ? WLE_VIEW(wl_resource_get_user_data(surface_resource)) : NULL;

    if (view == NULL) {
        wl_client_post_implementation_error(client, "unknown or null surface");
    } else {
        xdg_activation_token_v1_set_surface(activation_token->remote_activation_token, wle_view_get_remote_surface(view));
    }
}

static void
local_activation_token_commit(
    struct wl_client *client,
    struct wl_resource *activation_token_resource) {
    WleActivationToken *activation_token = wl_resource_get_user_data(activation_token_resource);
    xdg_activation_token_v1_commit(activation_token->remote_activation_token);
}

static void
local_activation_token_destroy(
    struct wl_client *client,
    struct wl_resource *activation_token_resource) {
    wl_resource_destroy(activation_token_resource);
}

static void
local_activation_token_destroy_impl(struct wl_resource *activation_token_resource) {
    WleActivationToken *activation_token = wl_resource_get_user_data(activation_token_resource);
    xdg_activation_token_v1_destroy(activation_token->remote_activation_token);
    wl_list_remove(wl_resource_get_link(activation_token_resource));
    g_free(activation_token);
}

static void
local_activation_activate(
    struct wl_client *client,
    struct wl_resource *activation_resource,
    const char *token,
    struct wl_resource *surface_resource) {
    WleXdgActivation *activation = wl_resource_get_user_data(activation_resource);
    WleView *view = surface_resource != NULL ? WLE_VIEW(wl_resource_get_user_data(surface_resource)) : NULL;

    if (view == NULL) {
        wl_client_post_implementation_error(client, "unknown or null surface");
    } else {
        xdg_activation_v1_activate(activation->remote_activation, token, wle_view_get_remote_surface(view));
    }
}

static void
local_activation_get_activation_token(
    struct wl_client *client,
    struct wl_resource *activation_resource,
    uint32_t new_id) {
    WleXdgActivation *activation = wl_resource_get_user_data(activation_resource);

    struct xdg_activation_token_v1 *remote_token = xdg_activation_v1_get_activation_token(activation->remote_activation);
    if (remote_token == NULL) {
        wle_propagate_remote_error(activation->remote_display, client, activation_resource);
    } else {
        struct wl_resource *local_token = wl_resource_create(client,
                                                             &xdg_activation_token_v1_interface,
                                                             wl_resource_get_version(activation_resource),
                                                             new_id);
        if (local_token == NULL) {
            wl_client_post_no_memory(client);
            xdg_activation_token_v1_destroy(remote_token);
        } else {
            WleActivationToken *token = g_new0(WleActivationToken, 1);
            token->remote_activation_token = remote_token;
            token->local_activation_token = local_token;
            wl_list_insert(&activation->tokens, wl_resource_get_link(local_token));

            wl_resource_set_implementation(local_token,
                                           &local_activation_token_impl,
                                           token,
                                           local_activation_token_destroy_impl);
            xdg_activation_token_v1_add_listener(remote_token, &remote_activation_token_listener, token);
        }
    }
}

static void
local_activation_destroy(
    struct wl_client *client,
    struct wl_resource *activation_resource) {
    wl_resource_destroy(activation_resource);
}

static void
local_activation_destroy_impl(struct wl_resource *activation_resource) {
    wl_list_remove(wl_resource_get_link(activation_resource));
}

static void
local_activation_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    struct wl_resource *resource = wl_resource_create(client,
                                                      &xdg_activation_v1_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        WleXdgActivation *activation = data;
        wl_resource_set_implementation(resource, &local_activation_impl, activation, local_activation_destroy_impl);
        wl_list_insert(&activation->local_activation_resources, wl_resource_get_link(resource));
    }
}

WleXdgActivation *
wle_xdg_activation_create(struct wl_display *remote_display,
                          struct xdg_activation_v1 *remote_activation,
                          struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_activation != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleXdgActivation *activation = g_new0(WleXdgActivation, 1);
    activation->local_activation = wl_global_create(local_display,
                                                    &xdg_activation_v1_interface,
                                                    wl_proxy_get_version((struct wl_proxy *)remote_activation),
                                                    activation,
                                                    local_activation_bind);

    if (activation->local_activation == NULL) {
        xdg_activation_v1_destroy(remote_activation);
        g_free(activation);
        return NULL;
    } else {
        activation->remote_display = remote_display;
        activation->remote_activation = remote_activation;
        wl_list_init(&activation->local_activation_resources);
        wl_list_init(&activation->tokens);
        return activation;
    }
}

void
wle_xdg_activation_destroy(WleXdgActivation *activation) {
    if (activation != NULL) {
        struct wl_resource *resource, *tmp_resource;

        wl_resource_for_each_safe(resource, tmp_resource, &activation->tokens) {
            wl_resource_destroy(resource);
        }

        wl_resource_for_each_safe(resource, tmp_resource, &activation->local_activation_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(activation->local_activation);

        xdg_activation_v1_destroy(activation->remote_activation);

        g_free(activation);
    }
}
