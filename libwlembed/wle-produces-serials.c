/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wle-produces-serials.h"

enum {
    SIG_NEW_SERIAL,

    N_SIGNALS,
};


G_DEFINE_INTERFACE(WleProducesSerials, wle_produces_serials, G_TYPE_OBJECT)


static guint signals[N_SIGNALS] = { 0 };

static void
wle_produces_serials_default_init(WleProducesSerialsIface *iface) {
    signals[SIG_NEW_SERIAL] = g_signal_new("new-serial",
                                           WLE_TYPE_PRODUCES_SERIALS,
                                           G_SIGNAL_RUN_LAST,
                                           0,
                                           NULL, NULL,
                                           g_cclosure_marshal_VOID__UINT,
                                           G_TYPE_NONE, 1,
                                           G_TYPE_UINT);
}

void
wle_produces_serials_new_serial(WleProducesSerials *producer, uint32_t serial) {
    g_return_if_fail(WLE_IS_PRODUCES_SERIALS(producer));
    g_signal_emit(producer, signals[SIG_NEW_SERIAL], 0, serial);
}
