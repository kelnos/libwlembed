/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <wayland-server-core.h>
#include <wayland-server.h>

#include "protocol/ext-foreign-toplevel-list-v1-client-protocol.h"
#include "protocol/ext-foreign-toplevel-list-v1-protocol.h"
#include "wle-ext-foreign-toplevel-list.h"
#include "wle-util.h"

typedef struct _WleExtForeignToplevelList {
    struct wl_display *remote_display;
    struct ext_foreign_toplevel_list_v1 *remote_toplevel_list;

    struct wl_global *local_toplevel_list;
    struct wl_list local_toplevel_list_resources;  // struct wl_resource.link
    struct wl_list local_stopped_toplevel_list_resources;  // struct wl_resource.link

    struct wl_list toplevels;  // WleExtForeignToplevelHandle.link
} WleExtForeignToplevelList;

typedef struct _WleExtForeignToplevelHandle {
    struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle;
    struct wl_list local_toplevel_handle_holders;  // WleExtForeignToplevelHandleResourceHolder.link

    gchar *identifier;
    gchar *app_id;
    gchar *title;

    struct wl_list link;
} WleExtForeignToplevelHandle;

typedef struct {
    struct wl_resource *local_toplevel_list;
    WleExtForeignToplevelHandle *toplevel;
    struct wl_resource *local_toplevel_handle;
    struct wl_list link;
} WleExtForeignToplevelHandleResourceHolder;

static void remote_toplevel_handle_closed(void *data,
                                          struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle);
static void remote_toplevel_handle_done(void *data,
                                        struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle);
static void remote_toplevel_handle_title(void *data,
                                         struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle,
                                         const char *title);
static void remote_toplevel_handle_app_id(void *data,
                                          struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle,
                                          const char *app_id);
static void remote_toplevel_handle_identifier(void *data,
                                              struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle,
                                              const char *identifier);

static void local_toplevel_handle_destroy(struct wl_client *client,
                                          struct wl_resource *toplevel_handle_resource);

static void remote_toplevel_list_toplevel(void *data,
                                          struct ext_foreign_toplevel_list_v1 *remote_toplevel_list,
                                          struct ext_foreign_toplevel_handle_v1 *remote_toplevel);
static void remote_toplevel_list_finished(void *data,
                                          struct ext_foreign_toplevel_list_v1 *remote_toplevel_list);

static void local_toplevel_list_stop(struct wl_client *client,
                                     struct wl_resource *toplevel_list_resource);
static void local_toplevel_list_destroy(struct wl_client *client,
                                        struct wl_resource *toplevel_list_resource);

static const struct ext_foreign_toplevel_handle_v1_listener remote_toplevel_handle_listener = {
    .closed = remote_toplevel_handle_closed,
    .done = remote_toplevel_handle_done,
    .title = remote_toplevel_handle_title,
    .app_id = remote_toplevel_handle_app_id,
    .identifier = remote_toplevel_handle_identifier,
};

static const struct ext_foreign_toplevel_handle_v1_interface local_toplevel_handle_impl = {
    .destroy = local_toplevel_handle_destroy,
};

static const struct ext_foreign_toplevel_list_v1_listener remote_toplevel_list_listener = {
    .toplevel = remote_toplevel_list_toplevel,
    .finished = remote_toplevel_list_finished,
};

static const struct ext_foreign_toplevel_list_v1_interface local_toplevel_list_impl = {
    .stop = local_toplevel_list_stop,
    .destroy = local_toplevel_list_destroy,
};

static void
remote_toplevel_handle_closed(void *data, struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle) {
    WleExtForeignToplevelHandle *toplevel = data;

    WleExtForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_handle_holders, link) {
        if (toplevel_holder->local_toplevel_list != NULL) {
            ext_foreign_toplevel_handle_v1_send_closed(toplevel_holder->local_toplevel_handle);
        }
    }

    g_clear_pointer(&toplevel->identifier, g_free);
    g_clear_pointer(&toplevel->app_id, g_free);
    g_clear_pointer(&toplevel->title, g_free);

    g_clear_pointer(&toplevel->remote_toplevel_handle, ext_foreign_toplevel_handle_v1_destroy);
}

static void
remote_toplevel_handle_done(void *data, struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle) {
    WleExtForeignToplevelHandle *toplevel = data;
    WleExtForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_handle_holders, link) {
        if (toplevel_holder->local_toplevel_list != NULL) {
            ext_foreign_toplevel_handle_v1_send_done(toplevel_holder->local_toplevel_handle);
        }
    }
}

static void
remote_toplevel_handle_title(void *data,
                             struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle,
                             const char *title) {
    WleExtForeignToplevelHandle *toplevel = data;
    g_free(toplevel->title);
    toplevel->title = g_strdup(title);

    WleExtForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_handle_holders, link) {
        if (toplevel_holder->local_toplevel_list != NULL) {
            ext_foreign_toplevel_handle_v1_send_title(toplevel_holder->local_toplevel_handle, title);
        }
    }
}

static void
remote_toplevel_handle_app_id(void *data,
                              struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle,
                              const char *app_id) {
    WleExtForeignToplevelHandle *toplevel = data;
    g_free(toplevel->app_id);
    toplevel->app_id = g_strdup(app_id);

    WleExtForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_handle_holders, link) {
        if (toplevel_holder->local_toplevel_list != NULL) {
            ext_foreign_toplevel_handle_v1_send_app_id(toplevel_holder->local_toplevel_handle, app_id);
        }
    }
}

static void
remote_toplevel_handle_identifier(void *data,
                                  struct ext_foreign_toplevel_handle_v1 *remote_toplevel_handle,
                                  const char *identifier) {
    WleExtForeignToplevelHandle *toplevel = data;
    g_free(toplevel->identifier);
    toplevel->identifier = g_strdup(identifier);

    WleExtForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_handle_holders, link) {
        if (toplevel_holder->local_toplevel_list != NULL) {
            ext_foreign_toplevel_handle_v1_send_identifier(toplevel_holder->local_toplevel_handle, identifier);
        }
    }
}

static void
local_toplevel_handle_destroy(struct wl_client *client, struct wl_resource *toplevel_handle_resource) {
    wl_resource_destroy(toplevel_handle_resource);
}

static void
local_toplevel_handle_destroy_impl(struct wl_resource *toplevel_handle_resource) {
    WleExtForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_handle_resource);
    WleExtForeignToplevelHandle *toplevel = toplevel_holder->toplevel;

    wl_list_remove(&toplevel_holder->link);
    g_free(toplevel_holder);

    if (toplevel->remote_toplevel_handle == NULL && wl_list_length(&toplevel->local_toplevel_handle_holders) == 0) {
        wl_list_remove(&toplevel->link);
        g_free(toplevel->identifier);
        g_free(toplevel->app_id);
        g_free(toplevel->title);
        g_free(toplevel);
    }
}

static void
remote_toplevel_list_toplevel(void *data,
                              struct ext_foreign_toplevel_list_v1 *remote_toplevel_list,
                              struct ext_foreign_toplevel_handle_v1 *remote_toplevel) {
    WleExtForeignToplevelList *toplevel_list = data;

    WleExtForeignToplevelHandle *toplevel = g_new0(WleExtForeignToplevelHandle, 1);
    toplevel->remote_toplevel_handle = _wle_proxy_set_ours((struct wl_proxy *)remote_toplevel);
    wl_list_init(&toplevel->local_toplevel_handle_holders);
    wl_list_insert(&toplevel_list->toplevels, &toplevel->link);

    struct wl_resource *resource;
    wl_resource_for_each(resource, &toplevel_list->local_toplevel_list_resources) {
        struct wl_resource *toplevel_resource = wl_resource_create(wl_resource_get_client(resource),
                                                                   &ext_foreign_toplevel_handle_v1_interface,
                                                                   wl_resource_get_version(resource),
                                                                   0);
        if (toplevel_resource != NULL) {
            WleExtForeignToplevelHandleResourceHolder *toplevel_holder = g_new0(WleExtForeignToplevelHandleResourceHolder, 1);
            toplevel_holder->local_toplevel_list = resource;
            toplevel_holder->toplevel = toplevel;
            toplevel_holder->local_toplevel_handle = toplevel_resource;
            wl_list_insert(&toplevel->local_toplevel_handle_holders, &toplevel_holder->link);

            wl_resource_set_implementation(toplevel_resource,
                                           &local_toplevel_handle_impl,
                                           toplevel_holder,
                                           local_toplevel_handle_destroy_impl);
            ext_foreign_toplevel_list_v1_send_toplevel(resource, toplevel_resource);
        }
    }

    ext_foreign_toplevel_handle_v1_add_listener(toplevel->remote_toplevel_handle,
                                                &remote_toplevel_handle_listener,
                                                toplevel);
}

static void
remote_toplevel_list_finished(void *data, struct ext_foreign_toplevel_list_v1 *remote_toplevel_list) {
    WleExtForeignToplevelList *toplevel_list = data;
    struct wl_resource *resource;
    wl_resource_for_each(resource, &toplevel_list->local_toplevel_list_resources) {
        ext_foreign_toplevel_list_v1_send_finished(resource);
    }
}

static void
local_toplevel_list_stop(struct wl_client *client, struct wl_resource *toplevel_list_resource) {
    wl_list_remove(wl_resource_get_link(toplevel_list_resource));
    WleExtForeignToplevelList *toplevel_list = wl_resource_get_user_data(toplevel_list_resource);
    wl_list_insert(&toplevel_list->local_stopped_toplevel_list_resources, wl_resource_get_link(toplevel_list_resource));

    WleExtForeignToplevelHandle *toplevel;
    wl_list_for_each(toplevel, &toplevel_list->toplevels, link) {
        WleExtForeignToplevelHandleResourceHolder *toplevel_holder;
        wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_handle_holders, link) {
            if (toplevel_holder->local_toplevel_list == toplevel_list_resource) {
                toplevel_holder->local_toplevel_list = NULL;
            }
        }
    }
}

static void
local_toplevel_list_destroy(struct wl_client *client, struct wl_resource *toplevel_list_resource) {
    wl_resource_destroy(toplevel_list_resource);
}

static void
local_toplevel_list_destroy_impl(struct wl_resource *toplevel_list_resource) {
    wl_list_remove(wl_resource_get_link(toplevel_list_resource));
}

static void
local_toplevel_list_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleExtForeignToplevelList *toplevel_list = data;

    struct wl_resource *resource = wl_resource_create(client,
                                                      &ext_foreign_toplevel_list_v1_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource,
                                       &local_toplevel_list_impl,
                                       toplevel_list,
                                       local_toplevel_list_destroy_impl);
        wl_list_insert(&toplevel_list->local_toplevel_list_resources, wl_resource_get_link(resource));

        WleExtForeignToplevelHandle *toplevel;
        wl_list_for_each(toplevel, &toplevel_list->toplevels, link) {
            if (toplevel->remote_toplevel_handle != NULL) {
                struct wl_resource *toplevel_resource = wl_resource_create(client,
                                                                           &ext_foreign_toplevel_handle_v1_interface,
                                                                           version,
                                                                           0);

                if (toplevel_resource != NULL) {
                    WleExtForeignToplevelHandleResourceHolder *toplevel_holder = g_new0(WleExtForeignToplevelHandleResourceHolder, 1);
                    toplevel_holder->local_toplevel_list = resource;
                    toplevel_holder->toplevel = toplevel;
                    toplevel_holder->local_toplevel_handle = toplevel_resource;
                    wl_list_insert(&toplevel->local_toplevel_handle_holders, &toplevel_holder->link);

                    wl_resource_set_implementation(toplevel_resource,
                                                   &local_toplevel_handle_impl,
                                                   toplevel_holder,
                                                   local_toplevel_handle_destroy_impl);
                    ext_foreign_toplevel_list_v1_send_toplevel(resource, toplevel_resource);

                    if (toplevel->identifier != NULL) {
                        ext_foreign_toplevel_handle_v1_send_identifier(toplevel_resource, toplevel->identifier);
                    }

                    if (toplevel->app_id != NULL) {
                        ext_foreign_toplevel_handle_v1_send_app_id(toplevel_resource, toplevel->app_id);
                    }

                    if (toplevel->title != NULL) {
                        ext_foreign_toplevel_handle_v1_send_title(toplevel_resource, toplevel->title);
                    }

                    ext_foreign_toplevel_handle_v1_send_done(toplevel_resource);
                }
            }
        }

        ext_foreign_toplevel_list_v1_send_finished(resource);
    }
}

WleExtForeignToplevelList *
wle_ext_foreign_toplevel_list_new(struct wl_display *remote_display,
                                  struct ext_foreign_toplevel_list_v1 *remote_toplevel_list,
                                  struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_toplevel_list != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleExtForeignToplevelList *toplevel_list = g_new0(WleExtForeignToplevelList, 1);
    toplevel_list->local_toplevel_list = wl_global_create(local_display,
                                                          &ext_foreign_toplevel_list_v1_interface,
                                                          ext_foreign_toplevel_list_v1_get_version(remote_toplevel_list),
                                                          toplevel_list,
                                                          local_toplevel_list_bind);

    if (toplevel_list->local_toplevel_list == NULL) {
        ext_foreign_toplevel_list_v1_stop(remote_toplevel_list);
        ext_foreign_toplevel_list_v1_destroy(remote_toplevel_list);
        g_free(toplevel_list);
        return NULL;
    } else {
        toplevel_list->remote_display = remote_display;
        toplevel_list->remote_toplevel_list = remote_toplevel_list;
        wl_list_init(&toplevel_list->local_toplevel_list_resources);
        wl_list_init(&toplevel_list->local_stopped_toplevel_list_resources);
        wl_list_init(&toplevel_list->toplevels);
        ext_foreign_toplevel_list_v1_add_listener(toplevel_list->remote_toplevel_list,
                                                  &remote_toplevel_list_listener,
                                                  toplevel_list);
        return toplevel_list;
    }
}

void
wle_ext_foreign_toplevel_list_destroy(WleExtForeignToplevelList *toplevel_list) {
    if (toplevel_list != NULL) {
        ext_foreign_toplevel_list_v1_stop(toplevel_list->remote_toplevel_list);
        ext_foreign_toplevel_list_v1_destroy(toplevel_list->remote_toplevel_list);

        WleExtForeignToplevelHandle *toplevel, *tmp_toplevel;
        wl_list_for_each_safe(toplevel, tmp_toplevel, &toplevel_list->toplevels, link) {
            g_clear_pointer(&toplevel->remote_toplevel_handle, ext_foreign_toplevel_handle_v1_destroy);

            WleExtForeignToplevelHandleResourceHolder *toplevel_holder, *tmp_toplevel_holder;
            wl_list_for_each_safe(toplevel_holder, tmp_toplevel_holder, &toplevel->local_toplevel_handle_holders, link) {
                wl_resource_destroy(toplevel_holder->local_toplevel_handle);
            }
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &toplevel_list->local_stopped_toplevel_list_resources) {
            wl_resource_destroy(resource);
        }
        wl_resource_for_each_safe(resource, tmp_resource, &toplevel_list->local_toplevel_list_resources) {
            wl_resource_destroy(resource);
        }

        wl_global_destroy(toplevel_list->local_toplevel_list);
        g_free(toplevel_list);
    }
}
