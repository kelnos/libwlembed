/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SECTION:wle-seat
 * @title: WleSeat
 * @short_description: Wrapper for a wl_seat instance
 * @stability: Unstable
 * @include: libwlembed/libwlembed.h
 *
 * #WleSeat is a wrapper for a @wl_seat instance, and is mainly here for
 * handling manual focus changes in a multi-seat environment.  You can match a
 * #WleSeat instance with one of your own wl_seat instances using
 * @wle_seat_get_name().
 *
 * A list of #WleSeat instances known to the library can be retrieved using
 * #wle_embedded_compositor_list_seats().
 *
 * If you do not support or care about multi-seat, any function taking a
 * #WleSeat instance will also accept %NULL (in which case the requested change
 * will be applied to all known seats).
 **/

#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client-protocol.h>
#include <wayland-client.h>
#include <wayland-server-core.h>
#include <wayland-server-protocol.h>
#include <wayland-server.h>
#include <xkbcommon/xkbcommon.h>

#include "common/wle-debug.h"
#include "wle-marshal.h"
#include "wle-produces-serials.h"
#include "wle-seat-private.h"
#include "wle-util.h"
#include "wle-view.h"
#include "libwlembed-visibility.h"

struct _WleSeat {
    GObject parent;

    struct wl_display *remote_display;

    // The 'canonical' seat (of the 3 below) that we use for all events except
    // pointer axis-related events
    struct wl_seat *remote_seat;

    struct wl_seat *remote_seat_v8_plus;
    struct wl_seat *remote_seat_v5_v7;
    struct wl_seat *remote_seat_v1_v4;

    gchar *name;
    enum wl_seat_capability capabilities;

    struct wl_global *local_seat;
    struct wl_list local_seat_resources;

    struct {
        struct wl_keyboard *remote_keyboard;

        int32_t repeat_rate;
        int32_t repeat_delay;
        struct xkb_keymap *keymap;
        WleView *keyboard_in_view;

        uint32_t mods_depressed;
        uint32_t mods_latched;
        uint32_t mods_locked;
        uint32_t group;

        struct wl_list local_keyboard_resources;
    } keyboard;

    struct {
        // The 'canonical' pointer (of the 3 below) that we use for all events
        // except pointer axis-related events
        struct wl_pointer *remote_pointer;

        struct wl_pointer *remote_pointer_v8_plus;
        struct wl_pointer *remote_pointer_v5_v7;
        struct wl_pointer *remote_pointer_v1_v4;

        WleView *pointer_in_view;
        gboolean leave_pending_frame;
        int32_t axis_discrete;
        uint32_t axis_source;

        struct wl_list local_pointer_resources;
    } pointer;

    struct {
        struct wl_touch *remote_touch;

        GHashTable *touches_in_views;

        struct wl_list local_touch_resources;
    } touch;

    WleView *focused_view;

    gboolean destroy_emitted;
};

enum {
    SIG_PRE_FOCUS_CHANGED,
    SIG_FOCUS_CHANGED,
    SIG_DESTROY,

    N_SIGNALS,
};

static void wle_seat_dispose(GObject *object);
static void wle_seat_finalize(GObject *object);

static void remote_seat_name(void *data,
                             struct wl_seat *remote_seat,
                             const char *name);
static void remote_seat_capabilities(void *data,
                                     struct wl_seat *remote_seat,
                                     uint32_t capabilities);

static void local_seat_get_keyboard(struct wl_client *client,
                                    struct wl_resource *seat_resource,
                                    uint32_t keyboard_id);
static void local_seat_get_pointer(struct wl_client *client,
                                   struct wl_resource *seat_resource,
                                   uint32_t pointer_id);
static void local_seat_get_touch(struct wl_client *client,
                                 struct wl_resource *seat_resource,
                                 uint32_t touch_id);
static void local_seat_release(struct wl_client *client,
                               struct wl_resource *seat_resource);

static void remote_keyboard_enter(void *data,
                                  struct wl_keyboard *keyboard,
                                  uint32_t serial,
                                  struct wl_surface *surface,
                                  struct wl_array *keys);
static void remote_keyboard_leave(void *data,
                                  struct wl_keyboard *keyboard,
                                  uint32_t serial,
                                  struct wl_surface *surface);
static void remote_keyboard_key(void *data,
                                struct wl_keyboard *wl_keyboard,
                                uint32_t serial,
                                uint32_t time,
                                uint32_t key,
                                uint32_t state);
static void remote_keyboard_modifiers(void *data,
                                      struct wl_keyboard *wl_keyboard,
                                      uint32_t serial,
                                      uint32_t mods_depressed,
                                      uint32_t mods_latched,
                                      uint32_t mods_locked,
                                      uint32_t group);
static void remote_keyboard_keymap(void *data,
                                   struct wl_keyboard *wl_keyboard,
                                   uint32_t format,
                                   int32_t fd,
                                   uint32_t size);
static void remote_keyboard_repeat_info(void *data,
                                        struct wl_keyboard *wl_keyboard,
                                        int32_t rate,
                                        int32_t delay);

static void local_keyboard_release(struct wl_client *client,
                                   struct wl_resource *keyboard_resource);

static void remote_pointer_enter(void *data,
                                 struct wl_pointer *pointer,
                                 uint32_t serial,
                                 struct wl_surface *surface,
                                 wl_fixed_t surface_x,
                                 wl_fixed_t surface_y);
static void remote_pointer_leave(void *data,
                                 struct wl_pointer *pointer,
                                 uint32_t serial,
                                 struct wl_surface *surface);
static void remote_pointer_motion(void *data,
                                  struct wl_pointer *wl_pointer,
                                  uint32_t time,
                                  wl_fixed_t surface_x,
                                  wl_fixed_t surface_y);
static void remote_pointer_button(void *data,
                                  struct wl_pointer *wl_pointer,
                                  uint32_t serial,
                                  uint32_t time,
                                  uint32_t button,
                                  uint32_t state);
static void remote_pointer_axis(void *data,
                                struct wl_pointer *wl_pointer,
                                uint32_t time,
                                uint32_t axis,
                                wl_fixed_t value);
#ifdef WL_POINTER_AXIS_VALUE120
static void remote_pointer_axis_value120(void *data,
                                         struct wl_pointer *wl_pointer,
                                         uint32_t axis,
                                         int32_t value120);
#endif
#ifdef WL_POINTER_AXIS_RELATIVE_DIRECTION
static void remote_pointer_axis_relative_direction(void *data,
                                                   struct wl_pointer *wl_pointer,
                                                   uint32_t axis,
                                                   uint32_t direction);
#endif
static void remote_pointer_axis_stop(void *data,
                                     struct wl_pointer *wl_pointer,
                                     uint32_t time,
                                     uint32_t axis);
static void remote_pointer_axis_discrete(void *data,
                                         struct wl_pointer *wl_pointer,
                                         uint32_t axis,
                                         int32_t discrete);
static void remote_pointer_axis_source(void *data,
                                       struct wl_pointer *wl_pointer,
                                       uint32_t axis_source);
static void remote_pointer_frame(void *data,
                                 struct wl_pointer *wl_pointer);

static void local_pointer_set_cursor(struct wl_client *client,
                                     struct wl_resource *pointer_resource,
                                     uint32_t serial,
                                     struct wl_resource *surface,
                                     int32_t hotspot_x,
                                     int32_t hotspot_y);
static void local_pointer_release(struct wl_client *client,
                                  struct wl_resource *pointer_resource);

static void remote_touch_down(void *data,
                              struct wl_touch *remote_touch,
                              uint32_t serial,
                              uint32_t time,
                              struct wl_surface *remote_surface,
                              int32_t id,
                              wl_fixed_t x,
                              wl_fixed_t y);
static void remote_touch_cancel(void *data,
                                struct wl_touch *touch);
static void remote_touch_frame(void *data,
                               struct wl_touch *touch);
static void remote_touch_motion(void *data,
                                struct wl_touch *touch,
                                uint32_t time,
                                int32_t id,
                                wl_fixed_t x,
                                wl_fixed_t y);
static void remote_touch_orientation(void *data,
                                     struct wl_touch *touch,
                                     int32_t id,
                                     wl_fixed_t orientation);
static void remote_touch_shape(void *data,
                               struct wl_touch *touch,
                               int32_t id,
                               wl_fixed_t major,
                               wl_fixed_t minor);
static void remote_touch_up(void *data,
                            struct wl_touch *touch,
                            uint32_t serial,
                            uint32_t time,
                            int32_t id);

static void local_touch_release(struct wl_client *client,
                                struct wl_resource *touch_resource);


G_DEFINE_FINAL_TYPE_WITH_CODE(WleSeat,
                              wle_seat,
                              G_TYPE_OBJECT,
                              G_IMPLEMENT_INTERFACE(WLE_TYPE_PRODUCES_SERIALS, NULL))


static guint signals[N_SIGNALS] = { 0 };

static const struct wl_seat_listener remote_seat_listener = {
    .name = remote_seat_name,
    .capabilities = remote_seat_capabilities,
};

static const struct wl_seat_interface local_seat_impl = {
    .get_keyboard = local_seat_get_keyboard,
    .get_pointer = local_seat_get_pointer,
    .get_touch = local_seat_get_touch,
    .release = local_seat_release,
};

static const struct wl_keyboard_listener remote_keyboard_listener = {
    .enter = remote_keyboard_enter,
    .leave = remote_keyboard_leave,
    .key = remote_keyboard_key,
    .modifiers = remote_keyboard_modifiers,
    .keymap = remote_keyboard_keymap,
    .repeat_info = remote_keyboard_repeat_info,
};

static const struct wl_keyboard_interface local_keyboard_impl = {
    .release = local_keyboard_release,
};

static const struct wl_pointer_listener remote_pointer_listener = {
    .enter = remote_pointer_enter,
    .leave = remote_pointer_leave,
    .motion = remote_pointer_motion,
    .button = remote_pointer_button,
    .axis = remote_pointer_axis,
#ifdef WL_POINTER_AXIS_VALUE120
    .axis_value120 = remote_pointer_axis_value120,
#endif
#ifdef WL_POINTER_AXIS_RELATIVE_DIRECTION
    .axis_relative_direction = remote_pointer_axis_relative_direction,
#endif
    .axis_stop = remote_pointer_axis_stop,
    .axis_discrete = remote_pointer_axis_discrete,
    .axis_source = remote_pointer_axis_source,
    .frame = remote_pointer_frame,
};

static const struct wl_pointer_interface local_pointer_impl = {
    .set_cursor = local_pointer_set_cursor,
    .release = local_pointer_release,
};

static const struct wl_touch_listener remote_touch_listener = {
    .down = remote_touch_down,
    .cancel = remote_touch_cancel,
    .frame = remote_touch_frame,
    .motion = remote_touch_motion,
    .orientation = remote_touch_orientation,
    .shape = remote_touch_shape,
    .up = remote_touch_up,
};

static const struct wl_touch_interface local_touch_impl = {
    .release = local_touch_release,
};

static void
wle_seat_class_init(WleSeatClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->dispose = wle_seat_dispose;
    gobject_class->finalize = wle_seat_finalize;

    /**
     * WleSeat::pre-focus-changed:
     *
     * This signal is private.  User code should not connect to this signal.
     * It can be changd or removed at any time, and depends on types private to
     * libwlembed.
     **/
    signals[SIG_PRE_FOCUS_CHANGED] = g_signal_new("pre-focus-changed",
                                                  WLE_TYPE_SEAT,
                                                  G_SIGNAL_RUN_LAST,
                                                  0,
                                                  NULL, NULL,
                                                  _wle_marshal_VOID__OBJECT_UINT,
                                                  G_TYPE_NONE, 2,
                                                  WLE_TYPE_VIEW,
                                                  G_TYPE_UINT);

    /**
     * WleSeat::focus-changed:
     *
     * This signal is private.  User code should not connect to this signal.
     * It can be changd or removed at any time, and depends on types private to
     * libwlembed.
     **/
    signals[SIG_FOCUS_CHANGED] = g_signal_new("focus-changed",
                                              WLE_TYPE_SEAT,
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL, NULL,
                                              _wle_marshal_VOID__OBJECT_UINT,
                                              G_TYPE_NONE, 2,
                                              WLE_TYPE_VIEW,
                                              G_TYPE_UINT);

    /**
     * WleSeat::destroy:
     * @seat: The #WleSeat that recieved the signal.
     *
     * Received when @seat is about to be destroyed.
     **/
    signals[SIG_DESTROY] = g_signal_new("destroy",
                                        WLE_TYPE_SEAT,
                                        G_SIGNAL_RUN_LAST,
                                        0,
                                        NULL, NULL,
                                        g_cclosure_marshal_VOID__VOID,
                                        G_TYPE_NONE, 0);
}

static void
wle_seat_init(WleSeat *seat) {
    wl_list_init(&seat->local_seat_resources);

    seat->keyboard.repeat_rate = -1;
    seat->keyboard.repeat_delay = -1;
    wl_list_init(&seat->keyboard.local_keyboard_resources);

    wl_list_init(&seat->pointer.local_pointer_resources);

    seat->touch.touches_in_views = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, g_object_unref);
    wl_list_init(&seat->touch.local_touch_resources);
}

static void
wle_seat_dispose(GObject *object) {
    WleSeat *seat = WLE_SEAT(object);

    if (!seat->destroy_emitted) {
        seat->destroy_emitted = TRUE;
        g_signal_emit(seat, signals[SIG_DESTROY], 0);
    }

    G_OBJECT_CLASS(wle_seat_parent_class)->dispose(object);
}

static void
wle_seat_finalize(GObject *object) {
    WleSeat *seat = WLE_SEAT(object);
    struct wl_resource *resource, *tmp_resource;

    g_clear_object(&seat->focused_view);
    g_clear_object(&seat->keyboard.keyboard_in_view);
    g_clear_object(&seat->pointer.pointer_in_view);
    g_hash_table_destroy(seat->touch.touches_in_views);

    if (seat->touch.remote_touch != NULL) {
        wl_touch_destroy(seat->touch.remote_touch);
    }
    wl_resource_for_each_safe(resource, tmp_resource, &seat->touch.local_touch_resources) {
        wl_resource_destroy(resource);
    }

    g_clear_pointer(&seat->pointer.remote_pointer_v8_plus, wl_pointer_release);
    g_clear_pointer(&seat->pointer.remote_pointer_v5_v7, wl_pointer_release);
    g_clear_pointer(&seat->pointer.remote_pointer_v1_v4, wl_pointer_destroy);
    wl_resource_for_each_safe(resource, tmp_resource, &seat->pointer.local_pointer_resources) {
        wl_resource_destroy(resource);
    }

    if (seat->keyboard.remote_keyboard != NULL) {
        wl_keyboard_destroy(seat->keyboard.remote_keyboard);
    }
    wl_resource_for_each_safe(resource, tmp_resource, &seat->keyboard.local_keyboard_resources) {
        wl_resource_destroy(resource);
    }
    if (seat->keyboard.keymap != NULL) {
        xkb_keymap_unref(seat->keyboard.keymap);
    }

    wl_resource_for_each_safe(resource, tmp_resource, &seat->local_seat_resources) {
        wl_resource_destroy(resource);
    }
    if (seat->local_seat != NULL) {
        wl_global_destroy(seat->local_seat);
    }

    g_free(seat->name);

    g_clear_pointer(&seat->remote_seat_v8_plus, wl_seat_release);
    g_clear_pointer(&seat->remote_seat_v5_v7, wl_seat_release);
    g_clear_pointer(&seat->remote_seat_v1_v4, wl_seat_destroy);

    G_OBJECT_CLASS(wle_seat_parent_class)->finalize(object);
}

static void
remote_keyboard_enter(void *data,
                      struct wl_keyboard *keyboard,
                      uint32_t serial,
                      struct wl_surface *remote_surface,
                      struct wl_array *keys) {
    WleSeat *seat = WLE_SEAT(data);
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(seat), serial);

    WleView *view = _wle_proxy_get_user_data((struct wl_proxy *)remote_surface);
    if (WLE_IS_VIEW(view)) {
        DBG("moving keyboard focus");

        g_signal_emit(seat, signals[SIG_PRE_FOCUS_CHANGED], 0, view, serial);

        g_clear_object(&seat->keyboard.keyboard_in_view);
        seat->keyboard.keyboard_in_view = g_object_ref(view);
        g_clear_object(&seat->focused_view);
        seat->focused_view = g_object_ref(view);

        struct wl_client *keyboard_client = wle_view_get_client(view);
        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
            if (wl_resource_get_client(resource) == keyboard_client) {
                wl_keyboard_send_enter(resource, serial, wle_view_get_local_surface(view), keys);
            }
        }

        g_signal_emit(seat, signals[SIG_FOCUS_CHANGED], 0, view, serial);
    }
}

static void
remote_keyboard_leave(void *data, struct wl_keyboard *keyboard, uint32_t serial, struct wl_surface *remote_surface) {
    WleSeat *seat = WLE_SEAT(data);
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(seat), serial);

    WleView *view = remote_surface != NULL
                        ? _wle_proxy_get_user_data((struct wl_proxy *)remote_surface)
                        : seat->keyboard.keyboard_in_view;
    if (WLE_IS_VIEW(view) && seat->keyboard.keyboard_in_view == view) {
        DBG("removing keyboard focus");

        g_signal_emit(seat, signals[SIG_PRE_FOCUS_CHANGED], 0, NULL, serial);

        struct wl_client *keyboard_client = wle_view_get_client(view);
        struct wl_resource *local_surface = wle_view_get_local_surface(view);
        g_clear_object(&seat->keyboard.keyboard_in_view);
        g_clear_object(&seat->focused_view);

        if (local_surface != NULL) {
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
                if (keyboard_client == NULL || wl_resource_get_client(resource) == keyboard_client) {
                    wl_keyboard_send_leave(resource, serial, local_surface);
                }
            }
        }

        g_signal_emit(seat, signals[SIG_FOCUS_CHANGED], 0, NULL, serial);
    }
}

static void
remote_keyboard_key(void *data,
                    struct wl_keyboard *wl_keyboard,
                    uint32_t serial,
                    uint32_t time,
                    uint32_t key,
                    uint32_t state) {
    TRACE("entering");
    WleSeat *seat = WLE_SEAT(data);
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(seat), serial);

    if (seat->keyboard.keyboard_in_view != NULL) {
        struct wl_client *keyboard_client = wle_view_get_client(seat->keyboard.keyboard_in_view);
        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
            if (wl_resource_get_client(resource) == keyboard_client) {
                DBG("forwarding remote key press");
                wl_keyboard_send_key(resource, serial, time, key, state);
            }
        }
    }
}

static void
remote_keyboard_modifiers(void *data,
                          struct wl_keyboard *wl_keyboard,
                          uint32_t serial,
                          uint32_t mods_depressed,
                          uint32_t mods_latched,
                          uint32_t mods_locked,
                          uint32_t group) {
    WleSeat *seat = WLE_SEAT(data);

    seat->keyboard.mods_depressed = mods_depressed;
    seat->keyboard.mods_latched = mods_latched;
    seat->keyboard.mods_locked = mods_locked;
    seat->keyboard.group = group;

    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(seat), serial);

    if (seat->keyboard.keyboard_in_view != NULL) {
        struct wl_client *keyboard_client = wle_view_get_client(seat->keyboard.keyboard_in_view);
        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
            if (wl_resource_get_client(resource) == keyboard_client) {
                wl_keyboard_send_modifiers(resource, serial, mods_depressed, mods_latched, mods_locked, group);
            }
        }
    }
}

static void
remote_keyboard_keymap(void *data, struct wl_keyboard *wl_keyboard, uint32_t format, int32_t fd, uint32_t size) {
    WleSeat *seat = WLE_SEAT(data);

    if (fd >= 0 && size > 0) {
        int mmap_flags = wl_proxy_get_version((struct wl_proxy *)wl_keyboard) < 7 ? MAP_SHARED : MAP_PRIVATE;
        void *keymap_data = mmap(NULL, size, PROT_READ, mmap_flags, fd, 0);
        if (keymap_data != MAP_FAILED) {
            struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
            struct xkb_keymap *keymap = xkb_keymap_new_from_string(context, (char *)keymap_data, format, XKB_KEYMAP_COMPILE_NO_FLAGS);

            if (keymap != NULL) {
                if (seat->keyboard.keymap != NULL) {
                    xkb_keymap_unref(seat->keyboard.keymap);
                }
                seat->keyboard.keymap = keymap;
            }

            xkb_context_unref(context);
            munmap(keymap_data, size);
        }
    }

    struct wl_resource *resource;
    wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
        wl_keyboard_send_keymap(resource, format, fd, size);
    }

    if (fd >= 0) {
        close(fd);
    }
}

static void
remote_keyboard_repeat_info(void *data, struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay) {
    WleSeat *seat = WLE_SEAT(data);
    seat->keyboard.repeat_rate = rate;
    seat->keyboard.repeat_delay = delay;

    struct wl_resource *resource;
    wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
        if (wl_resource_get_version(resource) >= WL_KEYBOARD_REPEAT_INFO_SINCE_VERSION) {
            wl_keyboard_send_repeat_info(resource, rate, delay);
        }
    }
}

static void
local_keyboard_release(struct wl_client *client, struct wl_resource *keyboard_resource) {
    wl_resource_destroy(keyboard_resource);
}

static void
local_keyboard_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
remote_pointer_enter(void *data,
                     struct wl_pointer *wl_pointer,
                     uint32_t serial,
                     struct wl_surface *remote_surface,
                     wl_fixed_t surface_x,
                     wl_fixed_t surface_y) {
    WleSeat *seat = WLE_SEAT(data);
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(seat), serial);

    if (wl_pointer == seat->pointer.remote_pointer) {
        WleView *view = _wle_proxy_get_user_data((struct wl_proxy *)remote_surface);
        if (WLE_IS_VIEW(view)) {
            DBG("pointer entering forwarded surface");

            if (seat->pointer.leave_pending_frame) {
                if (seat->pointer.pointer_in_view == view) {
                    seat->pointer.leave_pending_frame = FALSE;
                } else {
                    DBG("strange, got leave->enter on two different views with no frame; fixing it up");
                    remote_pointer_frame(data, wl_pointer);
                    seat->pointer.pointer_in_view = g_object_ref(view);
                }
            } else {
                seat->pointer.pointer_in_view = g_object_ref(view);
            }

            wle_view_pointer_enter(seat->pointer.pointer_in_view,
                                   seat,
                                   serial,
                                   wl_fixed_to_double(surface_x),
                                   wl_fixed_to_double(surface_y));

            struct wl_client *pointer_client = wle_view_get_client(view);
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                if (wl_resource_get_client(resource) == pointer_client) {
                    wl_pointer_send_enter(resource, serial, wle_view_get_local_surface(view), surface_x, surface_y);
                }
            }
        }
    }
}

static void
remote_pointer_leave(void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *remote_surface) {
    WleSeat *seat = WLE_SEAT(data);
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(seat), serial);

    if (wl_pointer == seat->pointer.remote_pointer) {
        WleView *view = remote_surface != NULL
                            ? _wle_proxy_get_user_data((struct wl_proxy *)remote_surface)
                            : seat->pointer.pointer_in_view;
        if (WLE_IS_VIEW(view) && seat->pointer.pointer_in_view == view) {
            DBG("pointer leaving forwarded surface");

            struct wl_client *pointer_client = wle_view_get_client(view);
            struct wl_resource *local_surface = wle_view_get_local_surface(view);

            g_object_ref(view);

            if (wl_proxy_get_version((struct wl_proxy *)wl_pointer) >= WL_POINTER_FRAME_SINCE_VERSION) {
                seat->pointer.leave_pending_frame = TRUE;
            } else {
                g_clear_object(&seat->pointer.pointer_in_view);
            }

            wle_view_pointer_leave(view, seat, serial);

            if (local_surface != NULL) {
                struct wl_resource *resource;
                wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                    if (pointer_client == NULL || wl_resource_get_client(resource) == pointer_client) {
                        wl_pointer_send_leave(resource, serial, local_surface);
                    }
                }
            }

            g_object_unref(view);
        }
    }
}

static void
remote_pointer_motion(void *data,
                      struct wl_pointer *wl_pointer,
                      uint32_t time,
                      wl_fixed_t surface_x,
                      wl_fixed_t surface_y) {
    WleSeat *seat = WLE_SEAT(data);

    if (wl_pointer == seat->pointer.remote_pointer) {
        if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
            // double sx = wl_fixed_to_double(surface_x);
            // double sy = wl_fixed_to_double(surface_y);
            // DBG("pointer motion on forwarded surface (%.02f, %.02f)", sx, sy);

            wle_view_pointer_motion(seat->pointer.pointer_in_view,
                                    seat,
                                    time,
                                    wl_fixed_to_double(surface_x),
                                    wl_fixed_to_double(surface_y));

            struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                if (wl_resource_get_client(resource) == pointer_client) {
                    wl_pointer_send_motion(resource, time, surface_x, surface_y);
                }
            }
        }
    }
}

static void
remote_pointer_button(void *data,
                      struct wl_pointer *wl_pointer,
                      uint32_t serial,
                      uint32_t time,
                      uint32_t button,
                      uint32_t state) {
    TRACE("entering");
    WleSeat *seat = WLE_SEAT(data);
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(seat), serial);

    if (wl_pointer == seat->pointer.remote_pointer) {
        if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
            // TODO: see if we also want keyboard_in_view to be NULL?
            if (seat->pointer.pointer_in_view != seat->keyboard.keyboard_in_view && _wle_view_is_embedded(seat->pointer.pointer_in_view)) {
                // Most wayland compositors do not give keyboard focus to
                // subsurfaces, so we'll never get keyboard.enter/.leave/.key
                // passed to our remote subsurface.  If we get a click on our
                // remote subsurface, though, we need to move keyboard focus to the
                // corresponding local surface.
                DBG("moving keyboard focus to embedded view");
                if (seat->keyboard.keyboard_in_view != NULL) {
                    _wle_seat_synthesize_keyboard_leave(seat, seat->keyboard.keyboard_in_view, serial);
                }
                _wle_seat_synthesize_keyboard_enter(seat, seat->pointer.pointer_in_view, serial);
            }

            struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                if (wl_resource_get_client(resource) == pointer_client) {
                    DBG("forwarding button press");
                    wl_pointer_send_button(resource, serial, time, button, state);
                }
            }
        }
    }
}

static gboolean
pointer_same_version_slot(struct wl_pointer *wl_pointer, struct wl_resource *resource) {
    int pv = wl_proxy_get_version((struct wl_proxy *)wl_pointer);
    int rv = wl_resource_get_version(resource);

    return (pv >= 8 && rv >= 8) || (pv >= 5 && pv < 8 && rv >= 5 && rv < 8) || (pv < 5 && rv < 5);
}

static void
remote_pointer_axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value) {
    WleSeat *seat = WLE_SEAT(data);
    if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
        struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
            if (wl_resource_get_client(resource) == pointer_client && pointer_same_version_slot(wl_pointer, resource))
            {
                wl_pointer_send_axis(resource, time, axis, value);
            }
        }
    }
}

#ifdef WL_POINTER_AXIS_VALUE120
static void
remote_pointer_axis_value120(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t value120) {
    WleSeat *seat = WLE_SEAT(data);
    if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
        seat->pointer.axis_discrete = value120;

        struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
            if (wl_resource_get_client(resource) == pointer_client && wl_resource_get_version(resource) >= WL_POINTER_AXIS_VALUE120_SINCE_VERSION)
            {
                wl_pointer_send_axis_value120(resource, axis, value120);
            }
        }
    }
}
#endif

#ifdef WL_POINTER_AXIS_RELATIVE_DIRECTION
static void
remote_pointer_axis_relative_direction(void *data, struct wl_pointer *wl_pointer, uint32_t axis, uint32_t direction) {
    WleSeat *seat = WLE_SEAT(data);
    if (wl_pointer == seat->pointer.remote_pointer) {
        if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
            struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                if (wl_resource_get_client(resource) == pointer_client && wl_resource_get_version(resource) >= WL_POINTER_AXIS_RELATIVE_DIRECTION_SINCE_VERSION)
                {
                    wl_pointer_send_axis_relative_direction(resource, axis, direction);
                }
            }
        }
    }
}
#endif

static void
remote_pointer_axis_stop(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis) {
    WleSeat *seat = WLE_SEAT(data);
    if (wl_pointer == seat->pointer.remote_pointer) {
        if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
            struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                if (wl_resource_get_client(resource) == pointer_client && wl_resource_get_version(resource) >= WL_POINTER_AXIS_STOP_SINCE_VERSION)
                {
                    wl_pointer_send_axis_stop(resource, time, axis);
                }
            }
        }
    }
}

static void
remote_pointer_axis_discrete(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t discrete) {
    WleSeat *seat = WLE_SEAT(data);
    if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
        seat->pointer.axis_discrete = discrete;

        struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
            int resource_version = wl_resource_get_version(resource);
            if (wl_resource_get_client(resource) == pointer_client && resource_version >= WL_POINTER_AXIS_DISCRETE_SINCE_VERSION && resource_version < WL_POINTER_AXIS_VALUE120_SINCE_VERSION)
            {
                wl_pointer_send_axis_discrete(resource, axis, discrete);
            }
        }
    }
}

static void
remote_pointer_axis_source(void *data, struct wl_pointer *wl_pointer, uint32_t axis_source) {
    WleSeat *seat = WLE_SEAT(data);

    if (wl_pointer == seat->pointer.remote_pointer) {
        if (seat->pointer.pointer_in_view != NULL && !seat->pointer.leave_pending_frame) {
            seat->pointer.axis_source = axis_source;

            struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                int resource_version = wl_resource_get_version(resource);
                if (wl_resource_get_client(resource) == pointer_client && resource_version >= WL_POINTER_AXIS_SOURCE_SINCE_VERSION && (axis_source != WL_POINTER_AXIS_SOURCE_WHEEL_TILT || resource_version >= WL_POINTER_AXIS_SOURCE_WHEEL_TILT_SINCE_VERSION))
                {
                    wl_pointer_send_axis_source(resource, axis_source);
                }
            }
        }
    }
}

static void
remote_pointer_frame(void *data, struct wl_pointer *wl_pointer) {
    WleSeat *seat = WLE_SEAT(data);
    if (wl_pointer == seat->pointer.remote_pointer) {
        if (seat->pointer.pointer_in_view != NULL) {
            struct wl_client *pointer_client = wle_view_get_client(seat->pointer.pointer_in_view);
            struct wl_resource *resource;
            wl_resource_for_each(resource, &seat->pointer.local_pointer_resources) {
                if (wl_resource_get_client(resource) == pointer_client && wl_resource_get_version(resource) >= WL_POINTER_FRAME_SINCE_VERSION)
                {
                    wl_pointer_send_frame(resource);
                }
            }

            wle_view_pointer_frame(seat->pointer.pointer_in_view, seat);

            if (seat->pointer.leave_pending_frame) {
                seat->pointer.leave_pending_frame = FALSE;
                g_clear_object(&seat->pointer.pointer_in_view);
            }
        }
    }
}

static void
local_pointer_set_cursor(struct wl_client *client,
                         struct wl_resource *pointer_resource,
                         uint32_t serial,
                         struct wl_resource *surface_resource,
                         int32_t hotspot_x,
                         int32_t hotspot_y) {
    WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(pointer_resource));

    if (seat->pointer.remote_pointer != NULL) {
        WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
        wl_pointer_set_cursor(seat->pointer.remote_pointer,
                              serial,
                              wle_view_get_remote_surface(view),
                              hotspot_x,
                              hotspot_y);
    }
}

static void
local_pointer_release(struct wl_client *client, struct wl_resource *pointer_resource) {
    wl_resource_destroy(pointer_resource);
}

static void
local_pointer_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
remote_touch_down(void *data,
                  struct wl_touch *remote_touch,
                  uint32_t serial,
                  uint32_t time,
                  struct wl_surface *remote_surface,
                  int32_t id,
                  wl_fixed_t x,
                  wl_fixed_t y) {
    WleSeat *seat = WLE_SEAT(data);
    WleView *view = _wle_proxy_get_user_data((struct wl_proxy *)remote_surface);

    if (WLE_IS_VIEW(view)) {
        // TODO: if this is the first touch, should we synthesize focus for an embedded view?

        g_hash_table_replace(seat->touch.touches_in_views, GINT_TO_POINTER(id), g_object_ref(view));

        struct wl_client *view_client = wle_view_get_client(view);
        struct wl_resource *local_touch;
        wl_resource_for_each(local_touch, &seat->touch.local_touch_resources) {
            if (wl_resource_get_client(local_touch) == view_client) {
                wl_touch_send_down(local_touch, serial, time, wle_view_get_local_surface(view), id, x, y);
            }
        }
    }
}

static void
remote_touch_cancel(void *data, struct wl_touch *touch) {
    WleSeat *seat = WLE_SEAT(data);

    GHashTable *clients_sent_already = g_hash_table_new(g_direct_hash, g_direct_equal);
    GHashTableIter iter;
    g_hash_table_iter_init(&iter, seat->touch.touches_in_views);

    WleView *view = NULL;
    while (g_hash_table_iter_next(&iter, NULL, (gpointer *)&view)) {
        struct wl_client *view_client = wle_view_get_client(view);
        if (!g_hash_table_contains(clients_sent_already, view_client)) {
            struct wl_resource *local_touch;
            wl_resource_for_each(local_touch, &seat->touch.local_touch_resources) {
                if (wl_resource_get_client(local_touch) == view_client) {
                    wl_touch_send_cancel(local_touch);
                }
            }

            g_hash_table_insert(clients_sent_already, view_client, GUINT_TO_POINTER(1));
        }
    }

    g_hash_table_remove_all(seat->touch.touches_in_views);
    g_hash_table_destroy(clients_sent_already);
}

static void
remote_touch_frame(void *data, struct wl_touch *touch) {
    WleSeat *seat = WLE_SEAT(data);

    GHashTable *clients_sent_already = g_hash_table_new(g_direct_hash, g_direct_equal);
    GHashTableIter iter;
    g_hash_table_iter_init(&iter, seat->touch.touches_in_views);

    WleView *view = NULL;
    while (g_hash_table_iter_next(&iter, NULL, (gpointer *)&view)) {
        struct wl_client *view_client = wle_view_get_client(view);
        if (!g_hash_table_contains(clients_sent_already, view_client)) {
            struct wl_resource *local_touch;
            wl_resource_for_each(local_touch, &seat->touch.local_touch_resources) {
                if (wl_resource_get_client(local_touch) == view_client) {
                    wl_touch_send_frame(local_touch);
                }
            }

            g_hash_table_insert(clients_sent_already, view_client, GUINT_TO_POINTER(1));
        }
    }

    g_hash_table_destroy(clients_sent_already);
}

static void
remote_touch_motion(void *data, struct wl_touch *touch, uint32_t time, int32_t id, wl_fixed_t x, wl_fixed_t y) {
    WleSeat *seat = WLE_SEAT(data);
    WleView *view = g_hash_table_lookup(seat->touch.touches_in_views, GINT_TO_POINTER(id));

    if (WLE_IS_VIEW(view)) {
        struct wl_client *view_client = wle_view_get_client(view);
        struct wl_resource *local_touch;
        wl_resource_for_each(local_touch, &seat->touch.local_touch_resources) {
            if (wl_resource_get_client(local_touch) == view_client) {
                wl_touch_send_motion(local_touch, time, id, x, y);
            }
        }
    }
}

static void
remote_touch_orientation(void *data, struct wl_touch *touch, int32_t id, wl_fixed_t orientation) {
    WleSeat *seat = WLE_SEAT(data);
    WleView *view = g_hash_table_lookup(seat->touch.touches_in_views, GINT_TO_POINTER(id));

    if (WLE_IS_VIEW(view)) {
        struct wl_client *view_client = wle_view_get_client(view);
        struct wl_resource *local_touch;
        wl_resource_for_each(local_touch, &seat->touch.local_touch_resources) {
            if (wl_resource_get_client(local_touch) == view_client && wl_resource_get_version(local_touch) >= WL_TOUCH_ORIENTATION_SINCE_VERSION)
            {
                wl_touch_send_orientation(local_touch, id, orientation);
            }
        }
    }
}

static void
remote_touch_shape(void *data, struct wl_touch *touch, int32_t id, wl_fixed_t major, wl_fixed_t minor) {
    WleSeat *seat = WLE_SEAT(data);
    WleView *view = g_hash_table_lookup(seat->touch.touches_in_views, GINT_TO_POINTER(id));

    if (WLE_IS_VIEW(view)) {
        struct wl_client *view_client = wle_view_get_client(view);
        struct wl_resource *local_touch;
        wl_resource_for_each(local_touch, &seat->touch.local_touch_resources) {
            if (wl_resource_get_client(local_touch) == view_client && wl_resource_get_version(local_touch) >= WL_TOUCH_SHAPE_SINCE_VERSION)
            {
                wl_touch_send_shape(local_touch, id, major, minor);
            }
        }
    }
}

static void
remote_touch_up(void *data, struct wl_touch *touch, uint32_t serial, uint32_t time, int32_t id) {
    WleSeat *seat = WLE_SEAT(data);
    WleView *view = g_hash_table_lookup(seat->touch.touches_in_views, GINT_TO_POINTER(id));

    if (WLE_IS_VIEW(view)) {
        struct wl_client *view_client = wle_view_get_client(view);
        struct wl_resource *local_touch;
        wl_resource_for_each(local_touch, &seat->touch.local_touch_resources) {
            if (wl_resource_get_client(local_touch) == view_client) {
                wl_touch_send_up(local_touch, serial, time, id);
            }
        }
    }

    g_hash_table_remove(seat->touch.touches_in_views, GINT_TO_POINTER(id));
}

static void
local_touch_release(struct wl_client *client, struct wl_resource *touch_resource) {
    wl_resource_destroy(touch_resource);
}

static void
local_touch_destroy_impl(struct wl_resource *touch_resource) {
    wl_list_remove(wl_resource_get_link(touch_resource));
}

static void
remote_seat_name(void *data, struct wl_seat *remote_seat, const char *name) {
    DBG("seat name: %s", name);
    WleSeat *seat = WLE_SEAT(data);

    if (remote_seat == seat->remote_seat) {
        g_free(seat->name);
        seat->name = g_strdup(name);

        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->local_seat_resources) {
            wl_seat_send_name(resource, name);
        }
    }
}

static void
remote_seat_capabilities(void *data, struct wl_seat *remote_seat, uint32_t capabilities) {
    WleSeat *seat = WLE_SEAT(data);

    struct wl_pointer **remote_pointer_p = NULL;
    if (remote_seat == seat->remote_seat_v8_plus) {
        remote_pointer_p = &seat->pointer.remote_pointer_v8_plus;
    } else if (remote_seat == seat->remote_seat_v5_v7) {
        remote_pointer_p = &seat->pointer.remote_pointer_v5_v7;
    } else if (remote_seat == seat->remote_seat_v1_v4) {
        remote_pointer_p = &seat->pointer.remote_pointer_v1_v4;
    } else {
        g_assert_not_reached();
    }

    if (*remote_pointer_p == NULL && (capabilities & WL_SEAT_CAPABILITY_POINTER) != 0) {
        *remote_pointer_p = wl_seat_get_pointer(remote_seat);
        _wle_proxy_set_ours((struct wl_proxy *)*remote_pointer_p);
        wl_pointer_add_listener(*remote_pointer_p, &remote_pointer_listener, seat);

        if (remote_seat == seat->remote_seat) {
            seat->pointer.remote_pointer = *remote_pointer_p;
        }
    } else if (*remote_pointer_p != NULL && (capabilities & WL_SEAT_CAPABILITY_POINTER) == 0) {
        if (wl_proxy_get_version((struct wl_proxy *)*remote_pointer_p) >= WL_POINTER_RELEASE_SINCE_VERSION) {
            wl_pointer_release(*remote_pointer_p);
        } else {
            wl_pointer_destroy(*remote_pointer_p);
        }
        *remote_pointer_p = NULL;
        if (remote_seat == seat->remote_seat) {
            seat->pointer.remote_pointer = NULL;
        }
    }

    if (remote_seat == seat->remote_seat) {
        if ((seat->capabilities & WL_SEAT_CAPABILITY_KEYBOARD) == 0 && (capabilities & WL_SEAT_CAPABILITY_KEYBOARD) != 0) {
            seat->keyboard.remote_keyboard = wl_seat_get_keyboard(remote_seat);
            _wle_proxy_set_ours((struct wl_proxy *)seat->keyboard.remote_keyboard);
            wl_keyboard_add_listener(seat->keyboard.remote_keyboard, &remote_keyboard_listener, seat);
        } else if ((seat->capabilities & WL_SEAT_CAPABILITY_KEYBOARD) != 0 && (capabilities & WL_SEAT_CAPABILITY_KEYBOARD) == 0) {
            if (seat->keyboard.remote_keyboard != NULL) {
                wl_keyboard_destroy(seat->keyboard.remote_keyboard);
                seat->keyboard.remote_keyboard = NULL;
            }
        }

        if ((seat->capabilities & WL_SEAT_CAPABILITY_TOUCH) == 0 && (capabilities & WL_SEAT_CAPABILITY_TOUCH) != 0) {
            seat->touch.remote_touch = wl_seat_get_touch(remote_seat);
            _wle_proxy_set_ours((struct wl_proxy *)seat->touch.remote_touch);
            wl_touch_add_listener(seat->touch.remote_touch, &remote_touch_listener, seat);
        } else {
            if (seat->touch.remote_touch != NULL) {
                wl_touch_destroy(seat->touch.remote_touch);
                seat->touch.remote_touch = NULL;
            }
        }

        seat->capabilities = capabilities;

        struct wl_resource *resource;
        wl_resource_for_each(resource, &seat->local_seat_resources) {
            wl_seat_send_capabilities(resource, capabilities);
        }
    }
}

static void
local_seat_get_keyboard(struct wl_client *client, struct wl_resource *seat_resource, uint32_t keyboard_id) {
    WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(seat_resource));
    if (seat->keyboard.remote_keyboard == NULL) {
        wl_resource_post_error(seat_resource, WL_SEAT_ERROR_MISSING_CAPABILITY, "no keyboard capability");
    } else {
        struct wl_resource *resource = wl_resource_create(client,
                                                          &wl_keyboard_interface,
                                                          wl_resource_get_version(seat_resource),
                                                          keyboard_id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(resource, &local_keyboard_impl, seat, local_keyboard_destroy_impl);
            wl_list_insert(&seat->keyboard.local_keyboard_resources, wl_resource_get_link(resource));

            if (seat->keyboard.keymap != NULL) {
                char *keymap_str = xkb_keymap_get_as_string(seat->keyboard.keymap, XKB_KEYMAP_FORMAT_TEXT_V1);
                if (keymap_str != NULL) {
                    gsize len = strlen(keymap_str);
                    if (len <= UINT32_MAX) {
                        int fd = _wle_shm_share_data(keymap_str, len);
                        if (fd >= 0) {
                            wl_keyboard_send_keymap(resource, WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1, fd, len);
                            close(fd);
                        }
                    }
                    free(keymap_str);
                }
            }
            wl_keyboard_send_repeat_info(resource, seat->keyboard.repeat_rate, seat->keyboard.repeat_delay);
        }
    }
}

static void
local_seat_get_pointer(struct wl_client *client, struct wl_resource *seat_resource, uint32_t pointer_id) {
    WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(seat_resource));
    if (seat->pointer.remote_pointer == NULL) {
        wl_resource_post_error(seat_resource, WL_SEAT_ERROR_MISSING_CAPABILITY, "no pointer capability");
    } else {
        struct wl_resource *resource = wl_resource_create(client,
                                                          &wl_pointer_interface,
                                                          wl_resource_get_version(seat_resource),
                                                          pointer_id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(resource, &local_pointer_impl, seat, local_pointer_destroy_impl);
            wl_list_insert(&seat->pointer.local_pointer_resources, wl_resource_get_link(resource));
        }
    }
}

static void
local_seat_get_touch(struct wl_client *client, struct wl_resource *seat_resource, uint32_t touch_id) {
    WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(seat_resource));
    if (seat->touch.remote_touch == NULL) {
        wl_resource_post_error(seat_resource, WL_SEAT_ERROR_MISSING_CAPABILITY, "no touch capability");
    } else {
        struct wl_resource *local_touch = wl_resource_create(client,
                                                             &wl_touch_interface,
                                                             wl_resource_get_version(seat_resource),
                                                             touch_id);
        if (local_touch == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(local_touch, &local_touch_impl, seat, local_touch_destroy_impl);
            wl_list_insert(&seat->touch.local_touch_resources, wl_resource_get_link(local_touch));
        }
    }
}

static void
local_seat_release(struct wl_client *client, struct wl_resource *seat_resource) {
    wl_resource_destroy(seat_resource);
}

static void
local_seat_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_seat_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleSeat *seat = WLE_SEAT(data);

    if (version > wl_global_get_version(seat->local_seat)) {
        wl_client_post_implementation_error(client, "unsupported wl_seat version %d", version);
    } else {
        struct wl_resource *resource = wl_resource_create(client, &wl_seat_interface, version, id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(resource, &local_seat_impl, seat, local_seat_destroy_impl);
            if (seat == NULL) {
                wl_list_init(wl_resource_get_link(resource));
            } else {
                wl_list_insert(&seat->local_seat_resources, wl_resource_get_link(resource));

                if (seat->name != NULL) {
                    wl_seat_send_name(resource, seat->name);
                }
                if (seat->capabilities != 0) {
                    wl_seat_send_capabilities(resource, seat->capabilities);
                }
            }
        }
    }
}

WleSeat *
_wle_seat_new(struct wl_display *remote_display,
              struct wl_registry *remote_registry,
              uint32_t remote_seat_name,
              uint32_t remote_seat_version,
              struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_registry != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    gboolean ok = TRUE;

    struct wl_seat *remote_seat_v8_plus = NULL;
    if (remote_seat_version >= 8) {
        remote_seat_v8_plus = _wle_registry_bind(remote_registry,
                                                 remote_seat_name,
                                                 &wl_seat_interface,
                                                 remote_seat_version);
        if (remote_seat_v8_plus == NULL) {
            ok = FALSE;
        }
    }

    struct wl_seat *remote_seat_v5_v7 = NULL;
    if (remote_seat_version >= 5) {
        remote_seat_v5_v7 = _wle_registry_bind(remote_registry,
                                               remote_seat_name,
                                               &wl_seat_interface,
                                               MIN(7, remote_seat_version));
        if (remote_seat_v5_v7 == NULL) {
            ok = FALSE;
        }
    }

    struct wl_seat *remote_seat_v1_v4 = _wle_registry_bind(remote_registry,
                                                           remote_seat_name,
                                                           &wl_seat_interface,
                                                           MIN(4, remote_seat_version));
    if (remote_seat_v1_v4 == NULL) {
        ok = FALSE;
    }

    WleSeat *seat = NULL;
    if (ok) {
        seat = g_object_new(WLE_TYPE_SEAT, NULL);
        seat->local_seat = wl_global_create(local_display,
                                            &wl_seat_interface,
                                            remote_seat_version,
                                            seat,
                                            local_seat_bind);
        if (seat->local_seat == NULL) {
            ok = FALSE;
        }
    }

    if (!ok) {
        g_clear_object(&seat);
        g_clear_pointer(&remote_seat_v8_plus, wl_seat_release);
        g_clear_pointer(&remote_seat_v5_v7, wl_seat_release);
        g_clear_pointer(&remote_seat_v1_v4, wl_seat_destroy);
        return NULL;
    } else {
        seat->remote_display = remote_display;
        seat->remote_seat = remote_seat_v8_plus != NULL
                                ? remote_seat_v8_plus
                                : (remote_seat_v5_v7 != NULL
                                       ? remote_seat_v5_v7
                                       : remote_seat_v1_v4);
        seat->remote_seat_v8_plus = remote_seat_v8_plus;
        seat->remote_seat_v5_v7 = remote_seat_v5_v7;
        seat->remote_seat_v1_v4 = remote_seat_v1_v4;

        if (remote_seat_v8_plus != NULL) {
            wl_seat_add_listener(remote_seat_v8_plus, &remote_seat_listener, seat);
        }
        if (remote_seat_v5_v7 != NULL) {
            wl_seat_add_listener(remote_seat_v5_v7, &remote_seat_listener, seat);
        }
        wl_seat_add_listener(remote_seat_v1_v4, &remote_seat_listener, seat);
        wl_display_roundtrip(remote_display);

        return seat;
    }
}

/**
 * wle_seat_get_name:
 * @seat: A #WleSeat.
 *
 * Retrieves the name of @seat as reported by the compositor.
 *
 * Return value: (nullable) (transfer none): A seat name, owned by @seat.
 **/
const gchar *
wle_seat_get_name(WleSeat *seat) {
    g_return_val_if_fail(WLE_SEAT(seat), NULL);
    return seat->name;
}

struct wl_seat *
_wle_seat_get_remote_seat(WleSeat *seat) {
    g_return_val_if_fail(WLE_SEAT(seat), NULL);
    return seat->remote_seat;
}

WleView *
_wle_seat_get_focused_view(WleSeat *seat) {
    g_return_val_if_fail(WLE_SEAT(seat), NULL);
    return seat->focused_view;
}

void
_wle_seat_set_focused_view(WleSeat *seat, WleView *view, uint32_t serial) {
    g_return_if_fail(WLE_IS_SEAT(seat));
    g_return_if_fail(view == NULL || WLE_IS_VIEW(view));

    return;

    if (seat->focused_view != view) {
        if (seat->keyboard.remote_keyboard != NULL) {
            if (seat->focused_view != NULL) {
                remote_keyboard_leave(seat,
                                      seat->keyboard.remote_keyboard,
                                      serial,
                                      wle_view_get_remote_surface(seat->focused_view));
            }
        }

        seat->focused_view = view;

        if (seat->keyboard.remote_keyboard != NULL) {
            if (view != NULL) {
                struct wl_array keys;
                wl_array_init(&keys);
                remote_keyboard_enter(seat,
                                      seat->keyboard.remote_keyboard,
                                      serial,
                                      wle_view_get_remote_surface(view),
                                      &keys);
                wl_array_release(&keys);
            }
        }
    }
}

void
_wle_seat_synthesize_keyboard_enter(WleSeat *seat, WleView *view, uint32_t serial) {
    TRACE("entering");
    g_return_if_fail(WLE_IS_SEAT(seat));
    g_return_if_fail(WLE_IS_VIEW(view));

    g_signal_emit(seat, signals[SIG_PRE_FOCUS_CHANGED], 0, view, serial);

    struct wl_client *view_client = wle_view_get_client(view);
    struct wl_resource *view_local_surface = wle_view_get_local_surface(view);

    struct wl_array keys;
    wl_array_init(&keys);

    g_clear_object(&seat->keyboard.keyboard_in_view);  // FIXME: send leave?
    seat->keyboard.keyboard_in_view = g_object_ref(view);
    g_clear_object(&seat->focused_view);
    seat->focused_view = g_object_ref(view);

    struct wl_resource *resource;
    wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
        if (wl_resource_get_client(resource) == view_client) {
            DBG("sending enter & modifiers");
            wl_keyboard_send_enter(resource, serial, view_local_surface, &keys);
            wl_keyboard_send_modifiers(resource,
                                       serial,
                                       seat->keyboard.mods_depressed,
                                       seat->keyboard.mods_latched,
                                       seat->keyboard.mods_locked,
                                       seat->keyboard.group);
        }
    }

    wl_array_release(&keys);

    g_signal_emit(seat, signals[SIG_FOCUS_CHANGED], 0, view, serial);
}

void
_wle_seat_synthesize_keyboard_leave(WleSeat *seat, WleView *view, uint32_t serial) {
    TRACE("entering");
    g_return_if_fail(WLE_IS_SEAT(seat));
    g_return_if_fail(WLE_IS_VIEW(view));

    struct wl_client *view_client = wle_view_get_client(view);
    struct wl_resource *view_local_surface = wle_view_get_local_surface(view);
    g_clear_object(&seat->keyboard.keyboard_in_view);
    g_clear_object(&seat->focused_view);

    struct wl_resource *resource;
    wl_resource_for_each(resource, &seat->keyboard.local_keyboard_resources) {
        if (wl_resource_get_client(resource) == view_client) {
            DBG("sending leave");
            wl_keyboard_send_leave(resource, serial, view_local_surface);
        }
    }
}

#define __WLE_SEAT_C__
#include <libwlembed-visibility.c>
