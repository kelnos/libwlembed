/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>
#include <glib-object.h>
#include <wayland-client.h>

#include "wle-compositor-bridge.h"
#include "wle-embedded-compositor.h"
#include "wle-embedded-view.h"
#include "wle-view.h"

WleEmbeddedView *_wle_embedded_view_new(WleEmbeddedCompositor *ec,
                                        WleCompositorBridge *bridge,
                                        WleView *root_view);

struct wl_surface *_wle_embedded_view_get_parent_surface(WleEmbeddedView *embedded_view);

WleView *_wle_embedded_view_get_root_view(WleEmbeddedView *embedded_view);

void _wle_embedded_view_size_request(WleEmbeddedView *embed,
                                     gint width,
                                     gint height);

void _wle_embedded_view_focus_in(WleEmbeddedView *embed);
void _wle_embedded_view_focus_out(WleEmbeddedView *embed);
void _wle_embedded_view_focus_next(WleEmbeddedView *embedded_view,
                                   uint32_t serial);
void _wle_embedded_view_focus_previous(WleEmbeddedView *embedded_view,
                                       uint32_t serial);

void _wle_embedded_view_closed(WleEmbeddedView *embed,
                               GError *error);
