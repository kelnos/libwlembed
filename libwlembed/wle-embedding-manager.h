/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_EMBEDDING_MANAGER_H__
#define __WLE_EMBEDDING_MANAGER_H__

#include <glib-object.h>
#include <wayland-server.h>

#include "wle-view.h"

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleEmbeddingManager, wle_embedding_manager, WLE, EMBEDDING_MANAGER, GObject)
#define WLE_TYPE_EMBEDDING_MANAGER (wle_embedding_manager_get_type())

WleEmbeddingManager *wle_embedding_manager_new(struct wl_display *local_display);

const gchar *_wle_embedding_manager_generate_token(WleEmbeddingManager *manager);

void _wle_embedding_manager_view_focus_first(WleEmbeddingManager *manager,
                                             WleView *view,
                                             uint32_t serial);
void _wle_embedding_manager_view_focus_last(WleEmbeddingManager *manager,
                                            WleView *view,
                                            uint32_t serial);

G_END_DECLS

#endif /* __WLE_EMBEDDING_MANAGER_H__ */
