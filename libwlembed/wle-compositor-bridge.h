/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_REMOTE_COMPOSITOR_H__
#define __WLE_REMOTE_COMPOSITOR_H__

#include <glib-object.h>
#include <wayland-client-protocol.h>

#include "wle-compositor.h"

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleCompositorBridge, wle_compositor_bridge, WLE, COMPOSITOR_BRIDGE, GObject)
#define WLE_TYPE_COMPOSITOR_BRIDGE (wle_compositor_bridge_get_type())

WleCompositorBridge *wle_compositor_bridge_new(struct wl_display *remote_display,
                                               struct wl_display *local_display,
                                               GError **error);

GList *wle_compositor_bridge_get_seats(WleCompositorBridge *bridge);

GList *wle_compositor_bridge_get_outputs(WleCompositorBridge *bridge);

WleCompositor *wle_compositor_bridge_get_compositor(WleCompositorBridge *bridge);

struct wl_display *wle_compositor_bridge_get_remote_display(WleCompositorBridge *bridge);
struct wl_registry *wle_compositor_bridge_get_remote_registry(WleCompositorBridge *bridge);

struct wl_display *wle_compositor_bridge_get_local_display(WleCompositorBridge *bridge);

G_END_DECLS

#endif /* __WLE_REMOTE_COMPOSITOR_H__ */
