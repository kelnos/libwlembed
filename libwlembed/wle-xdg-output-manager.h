/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_XDG_OUTPUT_MANAGER__
#define __WLE_XDG_OUTPUT_MANAGER__

#include <glib.h>

#include "protocol/xdg-output-unstable-v1-client-protocol.h"

G_BEGIN_DECLS

typedef struct _WleXdgOutputManager WleXdgOutputManager;

WleXdgOutputManager *wle_xdg_output_manager_new(struct wl_display *remote_display,
                                                struct zxdg_output_manager_v1 *remote_xdg_output_manager,
                                                struct wl_display *local_display);
void wle_xdg_output_manager_destroy(WleXdgOutputManager *xdg_output_manager);

G_END_DECLS

#endif /* __WLE_XDG_OUTPUT_MANAGER__ */
