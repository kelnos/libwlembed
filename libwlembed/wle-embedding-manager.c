/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server-core.h>

#include "common/wle-debug.h"
#include "protocol/wle-embedding-v1-protocol.h"
#include "wle-embedded-view-private.h"
#include "wle-embedding-manager.h"
#include "wle-marshal.h"
#include "wle-toplevel.h"
#include "wle-view.h"

#define WLE_EMBEDDED_SURFACE_KEY "--wle-embedded-surface"

struct _WleEmbeddingManager {
    GObject parent;

    struct wl_display *local_display;

    struct wl_global *local_manager;
    struct wl_list local_manager_resources;

    struct wl_list local_embedded_surface_resources;

    GHashTable *tokens;
    GHashTable *embedded_subprocesses_by_pid;
    GHashTable *embedded_subprocesses_by_view;
};

typedef struct {
    WleEmbeddingManager *manager;
    WleEmbeddedView *embedded_view;
    WleView *view;
    struct wl_resource *local_embedded_surface;
} WleEmbeddedSurface;

enum {
    SIG_NEW_EMBEDDED_SURFACE,
    SIG_ERROR,

    N_SIGNALS,
};

static void wle_embedding_manager_finalize(GObject *object);

static gboolean return_first_non_null(GSignalInvocationHint *ihint,
                                      GValue *return_accu,
                                      const GValue *handler_return,
                                      gpointer user_data);

static void local_surface_request_focus(struct wl_client *client,
                                        struct wl_resource *surface_resource,
                                        uint32_t serial);
static void local_surface_focus_next(struct wl_client *client,
                                     struct wl_resource *surface_resource,
                                     uint32_t serial);
static void local_surface_focus_previous(struct wl_client *client,
                                         struct wl_resource *surface_resource,
                                         uint32_t serial);
static void local_surface_destroy(struct wl_client *client,
                                  struct wl_resource *surface_resource);

static void local_manager_create_embedded_surface(struct wl_client *client,
                                                  struct wl_resource *manager_resource,
                                                  uint32_t new_id,
                                                  const char *token,
                                                  struct wl_resource *surface_resource);


G_DEFINE_TYPE(WleEmbeddingManager, wle_embedding_manager, G_TYPE_OBJECT)


static guint signals[N_SIGNALS] = { 0 };

static const struct wle_embedded_surface_v1_interface local_surface_impl = {
    .request_focus = local_surface_request_focus,
    .focus_next = local_surface_focus_next,
    .focus_previous = local_surface_focus_previous,
    .destroy = local_surface_destroy,
};

static const struct wle_embedding_manager_v1_interface local_manager_impl = {
    .create_embedded_surface = local_manager_create_embedded_surface,
};

static void
wle_embedding_manager_class_init(WleEmbeddingManagerClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_embedding_manager_finalize;

    signals[SIG_NEW_EMBEDDED_SURFACE] = g_signal_new("new-embedded-surface",
                                                     WLE_TYPE_EMBEDDING_MANAGER,
                                                     G_SIGNAL_RUN_LAST,
                                                     0,
                                                     return_first_non_null,
                                                     NULL,
                                                     _wle_marshal_OBJECT__OBJECT_STRING,
                                                     WLE_TYPE_EMBEDDED_VIEW,
                                                     2,
                                                     WLE_TYPE_VIEW,
                                                     G_TYPE_STRING);

    signals[SIG_ERROR] = g_signal_new("error",
                                      WLE_TYPE_EMBEDDING_MANAGER,
                                      G_SIGNAL_RUN_LAST,
                                      0,
                                      NULL,
                                      NULL,
                                      g_cclosure_marshal_VOID__BOXED,
                                      G_TYPE_NONE,
                                      1,
                                      G_TYPE_ERROR);
}

static void
wle_embedding_manager_init(WleEmbeddingManager *manager) {
    wl_list_init(&manager->local_manager_resources);
    wl_list_init(&manager->local_embedded_surface_resources);
    manager->tokens = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
    manager->embedded_subprocesses_by_pid = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, g_object_unref);
    manager->embedded_subprocesses_by_view = g_hash_table_new(g_direct_hash, g_direct_equal);
}

static void
wle_embedding_manager_finalize(GObject *object) {
    WleEmbeddingManager *manager = WLE_EMBEDDING_MANAGER(object);

    g_hash_table_destroy(manager->embedded_subprocesses_by_view);
    g_hash_table_destroy(manager->embedded_subprocesses_by_pid);
    g_hash_table_destroy(manager->tokens);

    struct wl_resource *resource, *tmp_resource;

    wl_resource_for_each_safe(resource, tmp_resource, &manager->local_embedded_surface_resources) {
        wl_resource_destroy(resource);
    }

    wl_resource_for_each_safe(resource, tmp_resource, &manager->local_manager_resources) {
        wl_resource_destroy(resource);
    }
    if (manager->local_manager != NULL) {
        wl_global_destroy(manager->local_manager);
    }

    G_OBJECT_CLASS(wle_embedding_manager_parent_class)->finalize(object);
}

static gboolean
return_first_non_null(GSignalInvocationHint *ihint,
                      GValue *return_accu,
                      const GValue *handler_return,
                      gpointer user_data) {
    if (G_VALUE_HOLDS_OBJECT(handler_return)) {
        g_value_set_object(return_accu, g_value_get_object(handler_return));
        return FALSE;
    } else {
        return TRUE;
    }
}

static void
local_surface_request_focus(struct wl_client *client, struct wl_resource *surface_resource, uint32_t serial) {
    TRACE("entering");
    WleEmbeddedSurface *embedded_surface = wl_resource_get_user_data(surface_resource);
    if (embedded_surface->embedded_view != NULL) {
        _wle_embedded_view_focus_in(embedded_surface->embedded_view);
    }
}

static void
local_surface_focus_next(struct wl_client *client, struct wl_resource *surface_resource, uint32_t serial) {
    TRACE("entering");
    WleEmbeddedSurface *embedded_surface = wl_resource_get_user_data(surface_resource);
    if (embedded_surface->embedded_view != NULL) {
        _wle_embedded_view_focus_next(embedded_surface->embedded_view, serial);
    }
}

static void
local_surface_focus_previous(struct wl_client *client, struct wl_resource *surface_resource, uint32_t serial) {
    TRACE("entering");
    WleEmbeddedSurface *embedded_surface = wl_resource_get_user_data(surface_resource);
    if (embedded_surface->embedded_view != NULL) {
        _wle_embedded_view_focus_previous(embedded_surface->embedded_view, serial);
    }
}

static void
local_surface_destroy(struct wl_client *client, struct wl_resource *surface_resource) {
    WleEmbeddedSurface *embedded_surface = wl_resource_get_user_data(surface_resource);
    wl_resource_destroy(embedded_surface->local_embedded_surface);
}

static void
local_surface_destroy_impl(struct wl_resource *surface_resource) {
    WleEmbeddedSurface *embedded_surface = wl_resource_get_user_data(surface_resource);

    if (embedded_surface->view != NULL) {
        g_signal_handlers_disconnect_by_data(embedded_surface->view, embedded_surface);
        g_object_set_data(G_OBJECT(embedded_surface->view), WLE_EMBEDDED_SURFACE_KEY, NULL);
    }

    wl_list_remove(wl_resource_get_link(surface_resource));
    g_free(embedded_surface);
}

static void
view_destroyed(WleView *view, WleEmbeddedSurface *embedded_surface) {
    embedded_surface->view = NULL;
    wl_resource_destroy(embedded_surface->local_embedded_surface);
}

static void
local_manager_create_embedded_surface(struct wl_client *client,
                                      struct wl_resource *manager_resource,
                                      uint32_t new_id,
                                      const char *token,
                                      struct wl_resource *surface_resource) {
    WleEmbeddingManager *manager = WLE_EMBEDDING_MANAGER(wl_resource_get_user_data(manager_resource));
    WleView *view = surface_resource != NULL ? WLE_VIEW(wl_resource_get_user_data(surface_resource)) : NULL;
    WleEmbeddedSurface *embedded_surface = view != NULL
                                               ? g_object_get_data(G_OBJECT(view), WLE_EMBEDDED_SURFACE_KEY)
                                               : NULL;

    if (!g_hash_table_remove(manager->tokens, token)) {
        wl_resource_post_error(manager_resource,
                               WLE_EMBEDDING_MANAGER_V1_WLE_EMBEDDING_MANAGER_ERROR_V1_INVALID_TOKEN,
                               "unknown or null token");
    } else if (view == NULL) {
        wl_client_post_implementation_error(client, "null or unknown wl_surface");
    } else if (!WLE_IS_TOPLEVEL(wle_view_get_role(view))) {
        wl_resource_post_error(manager_resource,
                               WLE_EMBEDDING_MANAGER_V1_WLE_EMBEDDING_MANAGER_ERROR_V1_INVALID_SURFACE,
                               "surface role is not xdg_toplevel");
    } else if (embedded_surface != NULL) {
        wl_resource_post_error(manager_resource,
                               WLE_EMBEDDING_MANAGER_V1_WLE_EMBEDDING_MANAGER_ERROR_V1_ALREADY_CREATED,
                               "surface already has an embedded surface");
    } else {
        struct wl_resource *local_embedded_surface = wl_resource_create(client,
                                                                        &wle_embedded_surface_v1_interface,
                                                                        wl_resource_get_version(manager_resource),
                                                                        new_id);
        if (local_embedded_surface == NULL) {
            wl_client_post_no_memory(client);
        } else {
            embedded_surface = g_new0(WleEmbeddedSurface, 1);
            embedded_surface->manager = manager;
            embedded_surface->view = view;
            embedded_surface->local_embedded_surface = local_embedded_surface;

            g_object_set_data(G_OBJECT(view), WLE_EMBEDDED_SURFACE_KEY, embedded_surface);

            pid_t pid = 0;
            wl_client_get_credentials(wle_view_get_client(view), &pid, NULL, NULL);
            DBG("new xdg surface is from pid %d", pid);

            WleEmbeddedView *eview = NULL;
            g_signal_emit(manager, signals[SIG_NEW_EMBEDDED_SURFACE], 0, view, token, &eview);
            if (!WLE_IS_EMBEDDED_VIEW(eview)) {
                g_object_set_data(G_OBJECT(view), WLE_EMBEDDED_SURFACE_KEY, NULL);
                wle_embedded_surface_v1_send_failed(local_embedded_surface);
                wl_resource_destroy(local_embedded_surface);
                g_free(embedded_surface);

                GError *error = g_error_new(WLE_ERROR,
                                            WLE_ERROR_FAILED,
                                            "Failed to embed surface for client with PID %d",
                                            pid);
                g_signal_emit(manager, signals[SIG_ERROR], 0, error);
                g_error_free(error);
            } else {
                embedded_surface->embedded_view = eview;

                wl_list_insert(&manager->local_embedded_surface_resources, wl_resource_get_link(local_embedded_surface));

                wl_resource_set_implementation(local_embedded_surface,
                                               &local_surface_impl,
                                               embedded_surface,
                                               local_surface_destroy_impl);
                g_signal_connect(view, "destroy",
                                 G_CALLBACK(view_destroyed), embedded_surface);
            }
        }
    }
}


static void
local_manager_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_manager_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    struct wl_resource *local_manager = wl_resource_create(client,
                                                           &wle_embedding_manager_v1_interface,
                                                           version,
                                                           id);
    if (local_manager == NULL) {
        wl_client_post_no_memory(client);
    } else {
        WleEmbeddingManager *manager = data;
        wl_resource_set_implementation(local_manager,
                                       &local_manager_impl,
                                       manager,
                                       local_manager_destroy_impl);
        wl_list_insert(&manager->local_manager_resources, wl_resource_get_link(local_manager));
    }
}

WleEmbeddingManager *
wle_embedding_manager_new(struct wl_display *local_display) {
    g_return_val_if_fail(local_display != NULL, NULL);

    WleEmbeddingManager *manager = g_object_new(WLE_TYPE_EMBEDDING_MANAGER, NULL);

    manager->local_manager = wl_global_create(local_display,
                                              &wle_embedding_manager_v1_interface,
                                              1,
                                              manager,
                                              local_manager_bind);
    if (manager->local_manager == NULL) {
        g_object_unref(manager);
        return NULL;
    } else {
        manager->local_display = local_display;
        return manager;
    }
}

const gchar *
_wle_embedding_manager_generate_token(WleEmbeddingManager *manager) {
    g_return_val_if_fail(WLE_IS_EMBEDDING_MANAGER(manager), NULL);

    gchar *token = g_uuid_string_random();
    g_hash_table_insert(manager->tokens, token, GUINT_TO_POINTER(TRUE));
    return token;
}

void
_wle_embedding_manager_view_focus_first(WleEmbeddingManager *manager, WleView *view, uint32_t serial) {
    g_return_if_fail(WLE_IS_EMBEDDING_MANAGER(manager));
    g_return_if_fail(WLE_IS_VIEW(view));

    WleEmbeddedSurface *embedded_surface = g_object_get_data(G_OBJECT(view), WLE_EMBEDDED_SURFACE_KEY);
    g_return_if_fail(embedded_surface != NULL);
    wle_embedded_surface_v1_send_focus_first(embedded_surface->local_embedded_surface, serial);
}

void
_wle_embedding_manager_view_focus_last(WleEmbeddingManager *manager, WleView *view, uint32_t serial) {
    g_return_if_fail(WLE_IS_EMBEDDING_MANAGER(manager));
    g_return_if_fail(WLE_IS_VIEW(view));

    WleEmbeddedSurface *embedded_surface = g_object_get_data(G_OBJECT(view), WLE_EMBEDDED_SURFACE_KEY);
    g_return_if_fail(embedded_surface != NULL);
    wle_embedded_surface_v1_send_focus_last(embedded_surface->local_embedded_surface, serial);
}
