/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_WL_DISPLAY_EVENT_SOURCE_H__
#define __WLE_WL_DISPLAY_EVENT_SOURCE_H__

#include <glib.h>
#include <wayland-server-core.h>

G_BEGIN_DECLS

GSource *
_wle_wl_display_event_source_new(struct wl_display *display,
                                 const gchar *socket_name);

G_END_DECLS

#endif /* __WLE_WL_DISPLAY_EVENT_SOURCE_H__ */
