/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wle-local-resource.h"

#define WLE_LOCAL_RESOURCE_GET_PRIVATE(obj) ((WleLocalResourcePrivate *)wle_local_resource_get_instance_private(WLE_LOCAL_RESOURCE(obj)))

typedef struct _WleLocalResourcePrivate {
    gboolean destroy_emitted;
} WleLocalResourcePrivate;

enum {
    SIG_DESTROY,

    N_SIGNALS,
};

static void wle_local_resource_dispose(GObject *object);

static void wle_local_resource_real_destroy(WleLocalResource *local_resource);


G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE(WleLocalResource, wle_local_resource, G_TYPE_OBJECT)


static guint signals[N_SIGNALS] = { 0 };

static void
wle_local_resource_class_init(WleLocalResourceClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->dispose = wle_local_resource_dispose;

    klass->destroy = wle_local_resource_real_destroy;

    signals[SIG_DESTROY] = g_signal_new("destroy",
                                        WLE_TYPE_LOCAL_RESOURCE,
                                        G_SIGNAL_RUN_LAST,
                                        0,
                                        NULL, NULL,
                                        g_cclosure_marshal_VOID__VOID,
                                        G_TYPE_NONE, 0);
}

static void
wle_local_resource_init(WleLocalResource *local_resource) {}

static void
wle_local_resource_dispose(GObject *object) {
    wle_local_resource_destroyed(WLE_LOCAL_RESOURCE(object));
    G_OBJECT_CLASS(wle_local_resource_parent_class)->dispose(object);
}


static void
wle_local_resource_real_destroy(WleLocalResource *local_resource) {
    wle_local_resource_destroyed(local_resource);
}

void
wle_local_resource_destroy(WleLocalResource *local_resource) {
    g_return_if_fail(WLE_IS_LOCAL_RESOURCE(local_resource));

    WleLocalResourceClass *klass = WLE_LOCAL_RESOURCE_GET_CLASS(local_resource);
    g_return_if_fail(klass->destroy != NULL);
    g_object_ref(local_resource);
    klass->destroy(local_resource);
    g_object_unref(local_resource);
}

void
wle_local_resource_destroyed(WleLocalResource *local_resource) {
    g_return_if_fail(WLE_IS_LOCAL_RESOURCE(local_resource));

    WleLocalResourcePrivate *priv = WLE_LOCAL_RESOURCE_GET_PRIVATE(local_resource);

    if (!priv->destroy_emitted) {
        priv->destroy_emitted = TRUE;
        g_signal_emit(local_resource, signals[SIG_DESTROY], 0);
    }
}
