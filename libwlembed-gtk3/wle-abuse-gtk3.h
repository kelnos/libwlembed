/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_ABUSE_GTK3_H__
#define __WLE_ABUSE_GTK3_H__

#include <gtk/gtk.h>
#include <wayland-client.h>

G_BEGIN_DECLS

struct xdg_surface;
struct xdg_toplevel;

gboolean abuse_gdk_window_get_shadow_width(GdkWindow *window,
                                           gint *left,
                                           gint *right,
                                           gint *top,
                                           gint *bottom);

struct xdg_surface *abuse_gdk_wayland_window_get_xdg_surface(GdkWindow *window);
struct xdg_toplevel *abuse_gdk_wayland_window_get_xdg_toplevel(GdkWindow *window);
gboolean abuse_gdk_wayland_window_get_window_geometry(GdkWindow *window,
                                                      GdkRectangle *geometry);

G_END_DECLS

#endif /* __WLE_ABUSE_GTK3_H__ */
