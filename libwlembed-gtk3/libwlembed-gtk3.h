/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __LIBWLEMBED_GTK3_H__
#define __LIBWLEMBED_GTK3_H__

#include <gtk/gtk.h>
#include <libwlembed/libwlembed.h>

#define __IN_LIBWLEMBED_GTK3_H__

#include <libwlembed-gtk3/wle-gtk-plug.h>
#include <libwlembed-gtk3/wle-gtk-socket.h>

#undef __IN_LIBWLEMBED_GTK3_H__

G_BEGIN_DECLS

WleEmbeddedCompositor *wle_gtk_create_embedded_compositor(const gchar *embedded_socket_name,
                                                          GError **error);

WleEmbeddedCompositor *wle_gtk_create_embedded_compositor_full(const gchar *embedded_socket_name,
                                                               GdkDisplay *parent_display,
                                                               GMainContext *main_context,
                                                               GError **error);

G_END_DECLS

#endif /* __LIBWLEMBED_GTK3_H__ */
