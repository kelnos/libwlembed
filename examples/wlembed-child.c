/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gdk/gdkwayland.h>
#include <gtk/gtk.h>
#include <libwlembed-gtk3/libwlembed-gtk3.h>
#include <stdio.h>
#include <stdlib.h>

static void
show_dialog(GtkWidget *button, GtkWidget *window) {
    GtkWidget *dialog = gtk_dialog_new_with_buttons("Some Dialog", GTK_WINDOW(window), GTK_DIALOG_DESTROY_WITH_PARENT, "OK", NULL);
    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

static void
show_menu(GtkWidget *button, GtkWidget *window) {
    GtkWidget *menu = gtk_menu_new();
    g_signal_connect(menu, "selection-done",
                     G_CALLBACK(gtk_widget_destroy), NULL);

    GtkWidget *mi = gtk_menu_item_new_with_label("Item 1");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), mi);

    mi = gtk_menu_item_new_with_label("Item 2");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), mi);

    mi = gtk_menu_item_new_with_label("Item 3");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), mi);

    gtk_widget_show_all(menu);

    gtk_menu_popup_at_widget(GTK_MENU(menu), button, GDK_GRAVITY_SOUTH_WEST, GDK_GRAVITY_NORTH_WEST, gtk_get_current_event());
}

static void
show_popover(GtkWidget *button, GtkWidget *window) {
    GtkWidget *popover = gtk_popover_new(button);

    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 100);
    gtk_container_add(GTK_CONTAINER(popover), box);

    GtkWidget *label = gtk_label_new("Pop over!");
    gtk_box_pack_start(GTK_BOX(box), label, TRUE, TRUE, 0);

    GtkWidget *close = gtk_button_new_with_mnemonic("_Close");
    gtk_box_pack_end(GTK_BOX(box), close, FALSE, TRUE, 0);
    g_signal_connect_swapped(close, "clicked",
                             G_CALLBACK(gtk_popover_popdown), popover);

    gtk_widget_show_all(box);

    gtk_popover_popup(GTK_POPOVER(popover));
}

int
main(int argc, char **argv) {
    if (g_getenv("CHILD_WAYLAND_DEBUG") != NULL) {
        g_setenv("WAYLAND_DEBUG", g_getenv("CHILD_WAYLAND_DEBUG"), TRUE);
    } else {
        g_unsetenv("WAYLAND_DEBUG");
    }

    gdk_set_allowed_backends("wayland");
    gtk_init(&argc, &argv);

    if (argc != 3) {
        fprintf(stderr, "Usage: %s --socket-token TOKEN\n", argv[0]);
        exit(1);
    }

    const gchar *token = argv[2];

    GtkWidget *window = wle_gtk_plug_new(token);
    g_signal_connect(window, "delete-event",
                     G_CALLBACK(gtk_main_quit), NULL);

    GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 4);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    GtkWidget *label = gtk_label_new("This is a cool embedded app");
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

    GtkWidget *entry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 0);

    GtkWidget *button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(button_box), GTK_BUTTONBOX_END);
    gtk_box_pack_start(GTK_BOX(vbox), button_box, TRUE, TRUE, 0);

    GtkWidget *button = gtk_button_new_with_mnemonic("_Show Dialog");
    gtk_widget_set_can_default(button, TRUE);
    gtk_container_add(GTK_CONTAINER(button_box), button);
    gtk_widget_grab_default(button);
    g_signal_connect(button, "clicked",
                     G_CALLBACK(show_dialog), window);

    button = gtk_button_new_with_mnemonic("Show _Menu");
    gtk_container_add(GTK_CONTAINER(button_box), button);
    g_signal_connect(button, "clicked",
                     G_CALLBACK(show_menu), window);

    button = gtk_button_new_with_mnemonic("Show _Popover");
    gtk_container_add(GTK_CONTAINER(button_box), button);
    g_signal_connect(button, "clicked",
                     G_CALLBACK(show_popover), window);

    button = gtk_button_new_with_mnemonic("_Close");
    gtk_container_add(GTK_CONTAINER(button_box), button);
    g_signal_connect(button, "clicked",
                     G_CALLBACK(gtk_main_quit), NULL);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
