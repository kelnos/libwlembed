/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SECTION:libwlembed-gtk
 * @title: Library Utilities
 * @short_description: Utility functions
 * @stability: Unstable
 * @include: libwlembed-gtk/libwlembed-gtk3.h
 *
 * This module contains utilities to help with creation of
 * #WleEmbeddedCompositor instances using GTK/GDK primitives.
 **/

#include <gdk/gdkwayland.h>

#include "common/wle-wl-display-event-source.h"
#include "libwlembed-gtk3.h"
#include "libwlembed/libwlembed.h"
#include "libwlembed-gtk3-visibility.h"

static gchar *
generate_socket_name(void) {
    GRand *rand = g_rand_new();
    gchar *socket_name = g_strdup_printf("wle-gtk-wayland-%u", g_rand_int(rand));
    g_rand_free(rand);
    return socket_name;
}

static void
free_event_source(GSource *source) {
    g_source_destroy(source);
    g_source_unref(source);
}

static gboolean
event_source_notify_error(gpointer data) {
    WleEmbeddedCompositor *ec = WLE_EMBEDDED_COMPOSITOR(data);

    GError *error = g_error_new_literal(WLE_ERROR, WLE_ERROR_DISPLAY_ERROR, "Error received on Wayland server socket");
    wle_embedded_compositor_fatal_error(ec, error);
    g_error_free(error);

    return G_SOURCE_REMOVE;
}


/**
 * wle_gtk_create_embedded_compositor:
 * @embedded_socket_name: (nullable): A string to use for the socket name for
 *                                    the embedded compositor.
 * @error: (nullable): An optional #GError return location.
 *
 * Creates an embedded compositor using reasonable defaults, and attaches a
 * glib #GSource to the default #GMainContext.
 *
 * Return value: (nullable) (transfer full): A new #WleEmbeddedCompositor
 * instance, or %NULL on error, in which case @error will be set.
 **/
WleEmbeddedCompositor *
wle_gtk_create_embedded_compositor(const gchar *embedded_socket_name, GError **error) {
    return wle_gtk_create_embedded_compositor_full(embedded_socket_name,
                                                   NULL,
                                                   NULL,
                                                   error);
}

/**
 * wle_gtk_create_embedded_compositor_full:
 * @embedded_socket_name: (nullable): A string to use for the socket name for
 *                                    the embedded compositor.
 * @parent_display: (nullable): A #GdkDisplay pointing to the parent
 *                              compositor.
 * @main_context: (nullable): The #GMainContext to attach the display's event
 *                            source to.
 * @error: (nullable): An optional #GError return location.
 *
 * Creates an embedded compositor and attaches a glib #GSource to the specified
 * @main_context.
 *
 * All params may be %NULL, in which case reasonable defaults will be used.
 *
 * Return value: (nullable) (transfer full): A new #WleEmbeddedCompositor
 * instance, or %NULL on error, in which case @error will be set.
 **/
WleEmbeddedCompositor *
wle_gtk_create_embedded_compositor_full(const gchar *embedded_socket_name,
                                        GdkDisplay *parent_display,
                                        GMainContext *main_context,
                                        GError **error) {
    g_return_val_if_fail(embedded_socket_name == NULL || embedded_socket_name[0] != '\0', NULL);
    g_return_val_if_fail(parent_display == NULL || GDK_IS_WAYLAND_DISPLAY(parent_display), NULL);
    g_return_val_if_fail(error == NULL || *error == NULL, NULL);

    gchar *generated_socket_name = NULL;
    if (embedded_socket_name == NULL) {
        generated_socket_name = generate_socket_name();
    }

    if (parent_display == NULL) {
        parent_display = gdk_display_get_default();
        g_return_val_if_fail(GDK_IS_WAYLAND_DISPLAY(parent_display), NULL);
    }

    WleEmbeddedCompositor *ec = wle_embedded_compositor_new(embedded_socket_name != NULL ? embedded_socket_name : generated_socket_name,
                                                            gdk_wayland_display_get_wl_display(parent_display),
                                                            error);

    if (ec != NULL) {
        struct wl_display *local_display = wle_embedded_compositor_get_local_display(ec);
        const gchar *socket_name = wle_embedded_compositor_get_socket_name(ec);

        GSource *event_source = _wle_wl_display_event_source_new(local_display, socket_name);
        g_source_set_callback(event_source, event_source_notify_error, ec, NULL);
        g_source_attach(event_source, main_context);

        g_object_set_data_full(G_OBJECT(ec),
                               "--libwlembed-gtk3-event-source",
                               event_source,
                               (GDestroyNotify)free_event_source);
    }

    g_free(generated_socket_name);

    return ec;
}

#define __LIBWLEMBED_GTK3_C__
#include <libwlembed-gtk3-visibility.c>
