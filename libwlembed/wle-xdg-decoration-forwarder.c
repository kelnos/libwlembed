/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server.h>

#include "common/wle-debug.h"
#include "protocol/server-decoration-protocol.h"
#include "protocol/xdg-decoration-unstable-v1-client-protocol.h"
#include "protocol/xdg-decoration-unstable-v1-protocol.h"
#include "wle-toplevel.h"
#include "wle-util.h"
#include "wle-view.h"
#include "wle-xdg-decoration-forwarder.h"

struct _WleXdgDecorationForwarder {
    struct wl_display *remote_display;
    ;
    struct zxdg_decoration_manager_v1 *remote_decoration_manager;

    struct wl_global *local_decoration_manager;
    struct wl_list local_decoration_manager_resources;

    struct wl_list decorations;
};

typedef struct {
    WleView *view;

    struct zxdg_toplevel_decoration_v1 *remote_toplevel_decoration;
    struct wl_resource *local_toplevel_decoration;

    struct wl_list link;
} XdgDecoration;

static void local_decoration_manager_get_toplevel_decoration(struct wl_client *client,
                                                             struct wl_resource *decoration_manager_resource,
                                                             uint32_t toplevel_decoration_id,
                                                             struct wl_resource *surface_resource);
static void local_decoration_manager_destroy(struct wl_client *client,
                                             struct wl_resource *decoration_manager_resource);

static void remote_xdg_decoration_configure(void *data,
                                            struct zxdg_toplevel_decoration_v1 *toplevel_decoration,
                                            enum zxdg_toplevel_decoration_v1_mode mode);

static void local_toplevel_decoration_set_mode(struct wl_client *client,
                                               struct wl_resource *toplevel_decoration_resource,
                                               uint32_t mode);
static void local_toplevel_decoration_unset_mode(struct wl_client *client,
                                                 struct wl_resource *toplevel_decoration_resource);
static void local_toplevel_decoration_destroy(struct wl_client *client,
                                              struct wl_resource *toplevel_decoration_resource);


static const struct zxdg_toplevel_decoration_v1_listener remote_toplevel_decoration_listener = {
    .configure = remote_xdg_decoration_configure,
};

static const struct zxdg_toplevel_decoration_v1_interface local_toplevel_decoration_impl = {
    .set_mode = local_toplevel_decoration_set_mode,
    .unset_mode = local_toplevel_decoration_unset_mode,
    .destroy = local_toplevel_decoration_destroy,
};

static const struct zxdg_decoration_manager_v1_interface local_decoration_manager_impl = {
    .get_toplevel_decoration = local_decoration_manager_get_toplevel_decoration,
    .destroy = local_decoration_manager_destroy,
};


static void
remote_xdg_decoration_configure(void *data,
                                struct zxdg_toplevel_decoration_v1 *toplevel_decoration,
                                enum zxdg_toplevel_decoration_v1_mode mode) {
    DBG("Remote compositor configured XDG decoration to mode %d", mode);
    XdgDecoration *decor = data;
    zxdg_toplevel_decoration_v1_send_configure(decor->local_toplevel_decoration, mode);
}

static void
local_toplevel_decoration_set_mode(struct wl_client *client,
                                   struct wl_resource *toplevel_decoration_resource,
                                   uint32_t mode) {
    XdgDecoration *decor = wl_resource_get_user_data(toplevel_decoration_resource);
    if (decor->remote_toplevel_decoration != NULL) {
        zxdg_toplevel_decoration_v1_set_mode(decor->remote_toplevel_decoration, mode);
    } else {
        zxdg_toplevel_decoration_v1_send_configure(decor->local_toplevel_decoration,
                                                   ZXDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
    }
}

static void
local_toplevel_decoration_unset_mode(struct wl_client *client,
                                     struct wl_resource *toplevel_decoration_resource) {
    XdgDecoration *decor = wl_resource_get_user_data(toplevel_decoration_resource);
    if (decor->remote_toplevel_decoration != NULL) {
        zxdg_toplevel_decoration_v1_unset_mode(decor->remote_toplevel_decoration);
    }
}

static void
local_toplevel_decoration_destroy(struct wl_client *client,
                                  struct wl_resource *toplevel_decoration_resource) {
    wl_resource_destroy(toplevel_decoration_resource);
}

static void
local_toplevel_decoration_destroy_impl(struct wl_resource *resource) {
    XdgDecoration *decor = wl_resource_get_user_data(resource);
    if (decor->view != NULL) {
        g_signal_handlers_disconnect_by_data(decor->view, decor);
    }
    if (decor->remote_toplevel_decoration != NULL) {
        zxdg_toplevel_decoration_v1_destroy(decor->remote_toplevel_decoration);
    }
    wl_list_remove(&decor->link);
    g_free(decor);
}

static void
view_mode_changed(WleView *view, XdgDecoration *decor) {
    if (_wle_view_is_embedded(view) && decor->remote_toplevel_decoration != NULL) {
        zxdg_toplevel_decoration_v1_destroy(decor->remote_toplevel_decoration);
        decor->remote_toplevel_decoration = NULL;
    }
}

static void
view_destroyed(WleView *view, XdgDecoration *decor) {
    decor->view = NULL;
}

static void
local_decoration_manager_get_toplevel_decoration(struct wl_client *client,
                                                 struct wl_resource *decoration_manager_resource,
                                                 uint32_t toplevel_decoration_id,
                                                 struct wl_resource *surface_resource) {
    WleXdgDecorationForwarder *forwarder = wl_resource_get_user_data(decoration_manager_resource);
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    DBG("new XDG decoration");

    if (!WLE_IS_TOPLEVEL(view)) {
        wl_resource_post_error(decoration_manager_resource,
                               ZXDG_TOPLEVEL_DECORATION_V1_ERROR_ORPHANED,
                               "surface is not an xdg-toplevel");
    } else {
        gboolean ok = TRUE;
        struct zxdg_toplevel_decoration_v1 *remote_decoration = NULL;
        if (_wle_view_is_forwarded(view)) {
            remote_decoration = zxdg_decoration_manager_v1_get_toplevel_decoration(forwarder->remote_decoration_manager,
                                                                                   _wle_toplevel_get_remote_xdg_toplevel(WLE_TOPLEVEL(view)));
            if (remote_decoration == NULL) {
                ok = FALSE;
                wle_propagate_remote_error(forwarder->remote_display, client, decoration_manager_resource);
            }
        }

        if (ok) {
            struct wl_resource *resource = wl_resource_create(client,
                                                              &zxdg_toplevel_decoration_v1_interface,
                                                              wl_resource_get_version(decoration_manager_resource),
                                                              toplevel_decoration_id);
            if (resource == NULL) {
                if (remote_decoration != NULL) {
                    zxdg_toplevel_decoration_v1_destroy(remote_decoration);
                }
                wl_client_post_no_memory(client);
            } else {
                XdgDecoration *decor = g_new0(XdgDecoration, 1);

                decor->view = view;
                g_signal_connect(view, "mode-changed",
                                 G_CALLBACK(view_mode_changed), decor);
                g_signal_connect(view, "destroy",
                                 G_CALLBACK(view_destroyed), decor);

                decor->local_toplevel_decoration = resource;
                wl_resource_set_implementation(resource,
                                               &local_toplevel_decoration_impl,
                                               decor,
                                               local_toplevel_decoration_destroy_impl);

                if (remote_decoration != NULL) {
                    decor->remote_toplevel_decoration = _wle_proxy_set_ours((struct wl_proxy *)remote_decoration);
                    zxdg_toplevel_decoration_v1_add_listener(remote_decoration,
                                                             &remote_toplevel_decoration_listener,
                                                             decor);
                }

                wl_list_insert(&forwarder->decorations, &decor->link);
            }
        }
    }
}

static void
local_decoration_manager_destroy(struct wl_client *client, struct wl_resource *decoration_manager_resource) {
    wl_resource_destroy(decoration_manager_resource);
}

static void
local_decoration_manager_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_decoration_manager_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleXdgDecorationForwarder *forwarder = data;
    struct wl_resource *resource = wl_resource_create(client,
                                                      &zxdg_decoration_manager_v1_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource, &local_decoration_manager_impl, forwarder, local_decoration_manager_destroy_impl);
        if (forwarder == NULL) {
            wl_list_init(wl_resource_get_link(resource));
        } else {
            wl_list_insert(&forwarder->local_decoration_manager_resources, wl_resource_get_link(resource));
        }
    }
}

WleXdgDecorationForwarder *
wle_xdg_decoration_forwarder_create(struct wl_display *remote_display,
                                    struct zxdg_decoration_manager_v1 *remote_decoration_manager,
                                    struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_decoration_manager != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleXdgDecorationForwarder *forwarder = g_new0(WleXdgDecorationForwarder, 1);
    forwarder->remote_display = remote_display;
    forwarder->remote_decoration_manager = remote_decoration_manager;
    forwarder->local_decoration_manager = wl_global_create(local_display,
                                                           &zxdg_decoration_manager_v1_interface,
                                                           wl_proxy_get_version((struct wl_proxy *)remote_decoration_manager),
                                                           forwarder,
                                                           local_decoration_manager_bind);
    if (forwarder->local_decoration_manager != NULL) {
        forwarder->remote_display = remote_display;
        forwarder->remote_decoration_manager = remote_decoration_manager;
        wl_list_init(&forwarder->local_decoration_manager_resources);
        wl_list_init(&forwarder->decorations);
        return forwarder;
    } else {
        zxdg_decoration_manager_v1_destroy(remote_decoration_manager);
        g_free(forwarder);
        return NULL;
    }
}

void
wle_xdg_decoration_forwarder_destroy(WleXdgDecorationForwarder *forwarder) {
    if (forwarder != NULL) {
        XdgDecoration *decor, *tmp_decor;
        wl_list_for_each_safe(decor, tmp_decor, &forwarder->decorations, link) {
            wl_resource_destroy(decor->local_toplevel_decoration);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &forwarder->local_decoration_manager_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(forwarder->local_decoration_manager);

        zxdg_decoration_manager_v1_destroy(forwarder->remote_decoration_manager);

        g_free(forwarder);
    }
}
