/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-client-protocol.h>
#include <wayland-server-protocol.h>

#include "common/wle-private.h"
#include "wle-error.h"
#include "wle-view-role.h"

#define WLE_VIEW_ROLE_GET_PRIVATE(obj) ((WleViewRolePrivate *)wle_view_role_get_instance_private(WLE_VIEW_ROLE(obj)));

typedef struct _WleViewRolePrivate {
    WleRole role;
} WleViewRolePrivate;

enum {
    PROP0,
    PROP_ROLE,
};

static void wle_view_role_set_property(GObject *object,
                                       guint prop_id,
                                       const GValue *value,
                                       GParamSpec *pspec);
static void wle_view_role_get_property(GObject *object,
                                       guint prop_id,
                                       GValue *value,
                                       GParamSpec *pspec);


WLE_G_DEFINE_ENUM_TYPE(WleRole, wle_role,
                       WLE_G_DEFINE_ENUM_VALUE(WLE_ROLE_NONE, "none"),
                       WLE_G_DEFINE_ENUM_VALUE(WLE_ROLE_SUBSURFACE, "subsurface"),
                       WLE_G_DEFINE_ENUM_VALUE(WLE_ROLE_XDG_POPUP, "xdg-popup"),
                       WLE_G_DEFINE_ENUM_VALUE(WLE_ROLE_XDG_TOPLEVEL, "xdg-toplevel"))

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE(WleViewRole, wle_view_role, WLE_TYPE_LOCAL_RESOURCE)


static void
wle_view_role_class_init(WleViewRoleClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->set_property = wle_view_role_set_property;
    gobject_class->get_property = wle_view_role_get_property;

    g_object_class_install_property(gobject_class,
                                    PROP_ROLE,
                                    g_param_spec_enum("role",
                                                      "role",
                                                      "role",
                                                      WLE_TYPE_ROLE,
                                                      WLE_ROLE_NONE,
                                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
}

static void
wle_view_role_init(WleViewRole *view_role) {}

static void
wle_view_role_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec) {
    WleViewRolePrivate *priv = WLE_VIEW_ROLE_GET_PRIVATE(object);

    switch (prop_id) {
        case PROP_ROLE:
            priv->role = g_value_get_enum(value);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void
wle_view_role_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec) {
    WleViewRolePrivate *priv = WLE_VIEW_ROLE_GET_PRIVATE(object);

    switch (prop_id) {
        case PROP_ROLE:
            g_value_set_enum(value, priv->role);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}

gboolean
wle_view_role_destroy_remote_role(WleViewRole *view_role) {
    g_return_val_if_fail(WLE_IS_VIEW_ROLE(view_role), FALSE);
    WleViewRoleClass *klass = WLE_VIEW_ROLE_GET_CLASS(view_role);
    if (klass->destroy_remote_role != NULL) {
        return klass->destroy_remote_role(view_role);
    } else {
        return FALSE;
    }
}

void
wle_view_role_set_size(WleViewRole *view_role, uint32_t serial, int32_t width, int32_t height) {
    g_return_if_fail(WLE_IS_VIEW_ROLE(view_role));
    WleViewRoleClass *klass = WLE_VIEW_ROLE_GET_CLASS(view_role);
    if (klass->set_size != NULL) {
        klass->set_size(view_role, serial, width, height);
    }
}
