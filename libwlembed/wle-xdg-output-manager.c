/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "protocol/xdg-output-unstable-v1-protocol.h"
#include "wle-output.h"
#include "wle-util.h"
#include "wle-xdg-output-manager.h"
#include "xdg-output-unstable-v1-client-protocol.h"

struct _WleXdgOutputManager {
    struct wl_display *remote_display;
    struct zxdg_output_manager_v1 *remote_manager;

    struct wl_global *local_manager;
    struct wl_list local_manager_resources;

    struct wl_list xdg_outputs;
};

typedef struct {
    struct zxdg_output_v1 *remote_xdg_output;
    struct wl_resource *local_xdg_output;
    struct wl_list link;
} WleXdgOutput;

static void remote_xdg_output_logical_position(void *data,
                                               struct zxdg_output_v1 *remote_xdg_output,
                                               int32_t x,
                                               int32_t y);
static void remote_xdg_output_logical_size(void *data,
                                           struct zxdg_output_v1 *remote_xdg_output,
                                           int32_t width,
                                           int32_t height);
static void remote_xdg_output_name(void *data,
                                   struct zxdg_output_v1 *remote_xdg_output,
                                   const char *name);
static void remote_xdg_output_description(void *data,
                                          struct zxdg_output_v1 *remote_xdg_output,
                                          const char *description);
static void remote_xdg_output_done(void *data,
                                   struct zxdg_output_v1 *remote_xdg_output);

static void local_xdg_output_destroy(struct wl_client *client,
                                     struct wl_resource *xdg_output_resource);


static void local_xdg_output_manager_get_output(struct wl_client *client,
                                                struct wl_resource *manager_resource,
                                                uint32_t xdg_output_id,
                                                struct wl_resource *output_resource);
static void local_xdg_output_manager_destroy(struct wl_client *client,
                                             struct wl_resource *manager_resource);

static const struct zxdg_output_v1_listener remote_xdg_output_listener = {
    .logical_position = remote_xdg_output_logical_position,
    .logical_size = remote_xdg_output_logical_size,
    .name = remote_xdg_output_name,
    .description = remote_xdg_output_description,
    .done = remote_xdg_output_done,
};

static const struct zxdg_output_v1_interface local_xdg_output_impl = {
    .destroy = local_xdg_output_destroy,
};

static const struct zxdg_output_manager_v1_interface local_xdg_output_manager_impl = {
    .get_xdg_output = local_xdg_output_manager_get_output,
    .destroy = local_xdg_output_manager_destroy,
};

static void
remote_xdg_output_logical_position(void *data, struct zxdg_output_v1 *remote_xdg_output, int32_t x, int32_t y) {
    WleXdgOutput *xdg_output = data;
    zxdg_output_v1_send_logical_position(xdg_output->local_xdg_output, x, y);
}

static void
remote_xdg_output_logical_size(void *data, struct zxdg_output_v1 *remote_xdg_output, int32_t width, int32_t height) {
    WleXdgOutput *xdg_output = data;
    zxdg_output_v1_send_logical_size(xdg_output->local_xdg_output, width, height);
}

static void
remote_xdg_output_name(void *data, struct zxdg_output_v1 *remote_xdg_output, const char *name) {
    WleXdgOutput *xdg_output = data;
    zxdg_output_v1_send_name(xdg_output->local_xdg_output, name);
}

static void
remote_xdg_output_description(void *data, struct zxdg_output_v1 *remote_xdg_output, const char *description) {
    WleXdgOutput *xdg_output = data;
    zxdg_output_v1_send_description(xdg_output->local_xdg_output, description);
}

static void
remote_xdg_output_done(void *data, struct zxdg_output_v1 *remote_xdg_output) {
    WleXdgOutput *xdg_output = data;
    zxdg_output_v1_send_done(xdg_output->local_xdg_output);
}

static void
local_xdg_output_destroy(struct wl_client *client, struct wl_resource *xdg_output_resource) {
    wl_resource_destroy(xdg_output_resource);
}

static void
local_xdg_output_destroy_impl(struct wl_resource *xdg_output_resource) {
    WleXdgOutput *xdg_output = wl_resource_get_user_data(xdg_output_resource);
    zxdg_output_v1_destroy(xdg_output->remote_xdg_output);
    wl_list_remove(&xdg_output->link);
    g_free(xdg_output);
}

static void
local_xdg_output_manager_get_output(struct wl_client *client,
                                    struct wl_resource *manager_resource,
                                    uint32_t xdg_output_id,
                                    struct wl_resource *output_resource) {
    WleXdgOutputManager *manager = wl_resource_get_user_data(manager_resource);
    WleOutput *output = output_resource != NULL ? WLE_OUTPUT(wl_resource_get_user_data(output_resource)) : NULL;

    if (output == NULL) {
        wl_resource_post_error(manager_resource,
                               WL_DISPLAY_ERROR_INVALID_OBJECT,
                               "passed output is null or unknown");
    } else {
        struct zxdg_output_v1 *remote_xdg_output = zxdg_output_manager_v1_get_xdg_output(manager->remote_manager,
                                                                                         wle_output_get_remote_output(output));
        if (remote_xdg_output == NULL) {
            wle_propagate_remote_error(manager->remote_display, client, manager_resource);
        } else {
            struct wl_resource *xdg_output_resource = wl_resource_create(client,
                                                                         &zxdg_output_v1_interface,
                                                                         wl_resource_get_version(manager_resource),
                                                                         xdg_output_id);
            if (xdg_output_resource == NULL) {
                zxdg_output_v1_destroy(remote_xdg_output);
                wl_client_post_no_memory(client);
            } else {
                WleXdgOutput *xdg_output = g_new0(WleXdgOutput, 1);
                xdg_output->remote_xdg_output = remote_xdg_output;
                xdg_output->local_xdg_output = xdg_output_resource;
                wl_list_insert(&manager->xdg_outputs, &xdg_output->link);
                zxdg_output_v1_add_listener(remote_xdg_output, &remote_xdg_output_listener, xdg_output);
                wl_resource_set_implementation(xdg_output_resource,
                                               &local_xdg_output_impl,
                                               xdg_output,
                                               local_xdg_output_destroy_impl);
            }
        }
    }
}

static void
local_xdg_output_manager_destroy(struct wl_client *client, struct wl_resource *manager_resource) {
    wl_resource_destroy(manager_resource);
}

static void
local_xdg_output_manager_destroy_impl(struct wl_resource *manager_resource) {
    wl_list_remove(wl_resource_get_link(manager_resource));
}

static void
local_xdg_output_manager_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleXdgOutputManager *manager = data;
    struct wl_resource *manager_resource = wl_resource_create(client,
                                                              &zxdg_output_manager_v1_interface,
                                                              version,
                                                              id);
    if (manager_resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(manager_resource,
                                       &local_xdg_output_manager_impl,
                                       manager,
                                       local_xdg_output_manager_destroy_impl);
        if (manager == NULL) {
            wl_list_init(wl_resource_get_link(manager_resource));
        } else {
            wl_list_insert(&manager->local_manager_resources, wl_resource_get_link(manager_resource));
        }
    }
}

WleXdgOutputManager *
wle_xdg_output_manager_new(struct wl_display *remote_display,
                           struct zxdg_output_manager_v1 *remote_xdg_output_manager,
                           struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_xdg_output_manager != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleXdgOutputManager *manager = g_new0(WleXdgOutputManager, 1);
    manager->local_manager = wl_global_create(local_display,
                                              &zxdg_output_manager_v1_interface,
                                              wl_proxy_get_version((struct wl_proxy *)remote_xdg_output_manager),
                                              manager,
                                              local_xdg_output_manager_bind);
    if (manager->local_manager == NULL) {
        zxdg_output_manager_v1_destroy(remote_xdg_output_manager);
        g_free(manager);
        return NULL;
    } else {
        manager->remote_manager = remote_xdg_output_manager;
        wl_list_init(&manager->local_manager_resources);
        wl_list_init(&manager->xdg_outputs);
        return manager;
    }
}

void
wle_xdg_output_manager_destroy(WleXdgOutputManager *xdg_output_manager) {
    if (xdg_output_manager != NULL) {
        WleXdgOutput *xdg_output, *tmp_xdg_output;
        wl_list_for_each_safe(xdg_output, tmp_xdg_output, &xdg_output_manager->xdg_outputs, link) {
            wl_resource_destroy(xdg_output->local_xdg_output);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &xdg_output_manager->local_manager_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(xdg_output_manager->local_manager);

        zxdg_output_manager_v1_destroy(xdg_output_manager->remote_manager);

        g_free(xdg_output_manager);
    }
}
