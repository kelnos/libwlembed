/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_GTK_PLUG_H__
#define __WLE_GTK_PLUG_H__

#if !defined(__IN_LIBWLEMBED_GTK3_H__) && !defined(LIBWLEMBED_COMPILATION)
#error "You may not include this header directly; instead, use <libwlembed-gtk/libwlembed-gtk3.h>"
#endif

#include <gtk/gtk.h>

G_DECLARE_DERIVABLE_TYPE(WleGtkPlug, wle_gtk_plug, WLE, GTK_PLUG, GtkWindow)
#define WLE_TYPE_GTK_PLUG (wle_gtk_plug_get_type())

struct _WleGtkPlugClass {
    GtkWindowClass parent_class;

    /*< private >*/
    void (*_wle_reserved0)(void);
    void (*_wle_reserved1)(void);
    void (*_wle_reserved2)(void);
    void (*_wle_reserved3)(void);
};

GtkWidget *wle_gtk_plug_new(const gchar *embedding_token);

#endif /* __WLE_GTK_PLUG_H__ */
