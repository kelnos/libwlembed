/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_ERROR_H__
#define __WLE_ERROR_H__

#if !defined(__IN_LIBWLEMBED_H__) && !defined(LIBWLEMBED_COMPILATION)
#error "You may not include this header directly; instead, use <libwlembed/libwlembed.h>"
#endif

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * WLE_ERROR:
 *
 * The error domain used for errors returned by this library.
 **/
#define WLE_ERROR (wle_error_quark())

/**
 * WleError:
 * @WLE_ERROR_DISPLAY_ERROR: An error occurred on the #wl_display.
 * @WLE_ERROR_MISSING_PROTOCOL: The remote compositor is missing a
 *                              Wayland protocol that the library requires.
 * @WLE_ERROR_REMOTE_ERROR: An error occurred on the remote compositor.
 * @WLE_ERROR_LOCAL_ERROR: An error occurred on the local compositor.
 * @WLE_ERROR_CLIENT_ERROR: An error occurred with the embedded client.
 * @WLE_ERROR_FAILED: A general error occurred.
 *
 * Various error codes used in the #WLE_ERROR domain.
 **/
typedef enum _WleError {
    WLE_ERROR_DISPLAY_ERROR,
    WLE_ERROR_MISSING_PROTOCOL,
    WLE_ERROR_REMOTE_ERROR,
    WLE_ERROR_LOCAL_ERROR,
    WLE_ERROR_CLIENT_ERROR,
    WLE_ERROR_FAILED,
} WleError;

GQuark wle_error_quark(void) G_GNUC_CONST;

G_END_DECLS

#endif /* __WLE_ERROR_H__ */
