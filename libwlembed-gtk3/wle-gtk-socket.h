/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_GTK_SOCKET_H__
#define __WLE_GTK_SOCKET_H__

#if !defined(__IN_LIBWLEMBED_GTK3_H__) && !defined(LIBWLEMBED_COMPILATION)
#error "You may not include this header directly; instead, use <libwlembed-gtk/libwlembed-gtk3.h>"
#endif

#include <gtk/gtk.h>
#include <libwlembed/libwlembed.h>

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleGtkSocket, wle_gtk_socket, WLE, GTK_SOCKET, GtkBin)
#define WLE_TYPE_GTK_SOCKET (wle_gtk_socket_get_type())

GtkWidget *wle_gtk_socket_new(WleEmbeddedCompositor *ec);

WleEmbeddedCompositor *wle_gtk_socket_get_embedded_compositor(WleGtkSocket *socket);
const gchar *wle_gtk_socket_get_embedding_token(WleGtkSocket *socket);

void wle_gtk_socket_set_size_constraints(WleGtkSocket *socket,
                                         gint minimum_width,
                                         gint minimum_height,
                                         gint maximum_width,
                                         gint maximum_height);

void wle_gtk_socket_destroy_embedded_view(WleGtkSocket *socket);

G_END_DECLS

#endif /* __WLE_GTK_SOCKET_H__ */
