/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wle-wl-display-event-source.h"

typedef struct {
    GSource source;
    struct wl_display *display;
    GPollFD fds;
} WleWlDisplayEventSource;

static gboolean
prepare(GSource *source, gint *timeout) {
    WleWlDisplayEventSource *wl_source = (WleWlDisplayEventSource *)source;
    wl_display_flush_clients(wl_source->display);
    *timeout = -1;
    return FALSE;
}

static gboolean
check(GSource *source) {
    return ((WleWlDisplayEventSource *)source)->fds.revents != 0;
}

static gboolean
dispatch(GSource *source, GSourceFunc callback, gpointer data) {
    WleWlDisplayEventSource *wl_source = (WleWlDisplayEventSource *)source;

    if ((wl_source->fds.revents & G_IO_IN) != 0) {
        struct wl_event_loop *loop = wl_display_get_event_loop(wl_source->display);
        wl_event_loop_dispatch(loop, 0);
        wl_source->fds.revents = 0;
    } else if ((wl_source->fds.revents & (G_IO_ERR | G_IO_HUP)) != 0) {
        if (callback != NULL) {
            callback(data);
        }
    }

    return TRUE;
}

static const GSourceFuncs source_funcs = {
    .prepare = prepare,
    .check = check,
    .dispatch = dispatch,
};

GSource *
_wle_wl_display_event_source_new(struct wl_display *display, const gchar *socket_name) {
    GSource *source = g_source_new((GSourceFuncs *)&source_funcs, sizeof(WleWlDisplayEventSource));
    WleWlDisplayEventSource *wl_source = (WleWlDisplayEventSource *)source;

    gchar *name = g_strdup_printf("Wayland Display (%s)", socket_name);
    g_source_set_name(source, name);
    g_source_set_priority(source, G_PRIORITY_DEFAULT);
    g_source_set_can_recurse(source, TRUE);

    wl_source->display = display;
    wl_source->fds.fd = wl_event_loop_get_fd(wl_display_get_event_loop(display));
    wl_source->fds.events = G_IO_IN | G_IO_ERR | G_IO_HUP;
    g_source_add_poll(source, &wl_source->fds);

    g_free(name);

    return source;
}
