/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server-core.h>

#include "common/wle-debug.h"
#include "protocol/wlr-layer-shell-unstable-v1-client-protocol.h"
#include "protocol/xdg-shell-client-protocol.h"
#include "protocol/xdg-shell-protocol.h"
#include "wle-popup.h"
#include "wle-positioner.h"
#include "wle-produces-serials.h"
#include "wle-toplevel.h"
#include "wle-util.h"
#include "wle-view.h"
#include "wle-xdg-shell.h"

struct _WleXdgShell {
    GObject parent;

    struct wl_display *remote_display;
    struct wl_compositor *remote_compositor;
    struct xdg_wm_base *remote_xdg_wm_base;

    struct wl_global *local_xdg_wm_base;
    struct wl_list local_xdg_wm_base_resources;

    GList *holders;  // WleXdgSurfaceHolder
    GList *positioners;  // WlePositioner
    uint32_t last_pong_serial;
};

enum {
    SIG_NEW_TOPLEVEL,
    SIG_NEW_POPUP,

    N_SIGNALS,
};

typedef struct {
    WleXdgShell *shell;
    WleView *view;
    struct xdg_surface *remote_xdg_surface;
    struct wl_resource *local_xdg_surface;
    WleXdgSurface *xdg_surface;

    struct {
        int32_t x;
        int32_t y;
        int32_t width;
        int32_t height;
    } geometry;
} WleXdgSurfaceHolder;

static void wle_xdg_shell_finalize(GObject *object);

static void remote_xdg_wm_base_ping(void *data,
                                    struct xdg_wm_base *base,
                                    uint32_t serial);

static void local_xdg_wm_base_get_xdg_surface(struct wl_client *client,
                                              struct wl_resource *base_resource,
                                              uint32_t xdg_surface_id,
                                              struct wl_resource *surface_resource);
static void local_xdg_wm_base_create_positioner(struct wl_client *client,
                                                struct wl_resource *base_resource,
                                                uint32_t positioner_id);
static void local_xdg_wm_base_pong(struct wl_client *client,
                                   struct wl_resource *base_resource,
                                   uint32_t serial);
static void local_xdg_wm_base_destroy(struct wl_client *client,
                                      struct wl_resource *base_resource);

static void remote_xdg_surface_configure(void *data,
                                         struct xdg_surface *xdg_surface,
                                         uint32_t serial);

static void local_xdg_surface_get_toplevel(struct wl_client *client,
                                           struct wl_resource *xdg_surface_resource,
                                           uint32_t toplevel_id);
static void local_xdg_surface_get_popup(struct wl_client *client,
                                        struct wl_resource *xdg_surface_resource,
                                        uint32_t popup_id,
                                        struct wl_resource *xdg_surface_parent_resource,
                                        struct wl_resource *positioner_resource);
static void local_xdg_surface_set_window_geometry(struct wl_client *client,
                                                  struct wl_resource *xdg_surface_resource,
                                                  int32_t x,
                                                  int32_t y,
                                                  int32_t width,
                                                  int32_t height);
static void local_xdg_surface_ack_configure(struct wl_client *client,
                                            struct wl_resource *xdg_surface_resource,
                                            uint32_t serial);
static void local_xdg_surface_destroy(struct wl_client *client,
                                      struct wl_resource *xdg_surface_resource);

static void xdg_surface_holder_free(WleXdgSurfaceHolder *holder);


G_DEFINE_FINAL_TYPE_WITH_CODE(WleXdgShell,
                              wle_xdg_shell,
                              G_TYPE_OBJECT,
                              G_IMPLEMENT_INTERFACE(WLE_TYPE_PRODUCES_SERIALS, NULL))


static guint signals[N_SIGNALS] = { 0 };

static const struct xdg_wm_base_listener remote_xdg_wm_base_listener = {
    .ping = remote_xdg_wm_base_ping,
};

static const struct xdg_wm_base_interface local_xdg_wm_base_impl = {
    .get_xdg_surface = local_xdg_wm_base_get_xdg_surface,
    .create_positioner = local_xdg_wm_base_create_positioner,
    .pong = local_xdg_wm_base_pong,
    .destroy = local_xdg_wm_base_destroy,
};

static const struct xdg_surface_listener remote_xdg_surface_listener = {
    .configure = remote_xdg_surface_configure,
};

static const struct xdg_surface_interface local_xdg_surface_impl = {
    .get_toplevel = local_xdg_surface_get_toplevel,
    .get_popup = local_xdg_surface_get_popup,
    .set_window_geometry = local_xdg_surface_set_window_geometry,
    .ack_configure = local_xdg_surface_ack_configure,
    .destroy = local_xdg_surface_destroy,
};

static void
wle_xdg_shell_class_init(WleXdgShellClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->finalize = wle_xdg_shell_finalize;

    signals[SIG_NEW_TOPLEVEL] = g_signal_new("new-toplevel",
                                             WLE_TYPE_XDG_SHELL,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__OBJECT,
                                             G_TYPE_NONE, 1,
                                             WLE_TYPE_TOPLEVEL);

    signals[SIG_NEW_POPUP] = g_signal_new("new-popup",
                                          WLE_TYPE_XDG_SHELL,
                                          G_SIGNAL_RUN_LAST,
                                          0,
                                          NULL, NULL,
                                          g_cclosure_marshal_VOID__OBJECT,
                                          G_TYPE_NONE, 1,
                                          WLE_TYPE_POPUP);
}

static void
wle_xdg_shell_init(WleXdgShell *shell) {
    wl_list_init(&shell->local_xdg_wm_base_resources);
}

static void
wle_xdg_shell_finalize(GObject *object) {
    WleXdgShell *shell = WLE_XDG_SHELL(object);

    GList *l = shell->holders;
    while (l != NULL) {
        GList *tmp = l;
        l = l->next;
        xdg_surface_holder_free(tmp->data);
    }

    struct wl_resource *resource, *tmp_resource;
    wl_resource_for_each_safe(resource, tmp_resource, &shell->local_xdg_wm_base_resources) {
        wl_resource_destroy(resource);
    }

    if (shell->local_xdg_wm_base != NULL) {
        wl_global_destroy(shell->local_xdg_wm_base);
    }

    xdg_wm_base_destroy(shell->remote_xdg_wm_base);

    G_OBJECT_CLASS(wle_xdg_shell_parent_class)->finalize(object);
}

static void
remote_xdg_wm_base_ping(void *data, struct xdg_wm_base *base, uint32_t serial) {
    WleXdgShell *shell = data;
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(shell), serial);
    struct wl_resource *resource;
    wl_resource_for_each(resource, &shell->local_xdg_wm_base_resources) {
        // XXX: do we need a local-compositor-specific serial?
        xdg_wm_base_send_ping(resource, serial);
    }
}

static void
remote_xdg_surface_configure(void *data,
                             struct xdg_surface *xdg_surface,
                             uint32_t serial) {
    WleXdgSurfaceHolder *holder = data;
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(holder->shell), serial);
    if (holder->local_xdg_surface != NULL) {
        xdg_surface_send_configure(holder->local_xdg_surface, serial);
    } else if (holder->xdg_surface != NULL) {
        wle_xdg_surface_send_configure(holder->xdg_surface, serial);
    }
}

static void
xdg_view_role_destroyed(WleXdgSurface *xdg_surface, WleXdgSurfaceHolder *holder) {
    holder->remote_xdg_surface = wle_xdg_surface_steal_remote_xdg_surface(xdg_surface);
    holder->local_xdg_surface = wle_xdg_surface_steal_local_xdg_surface(xdg_surface);

    g_signal_handlers_disconnect_by_data(xdg_surface, holder);
    g_clear_object(&holder->xdg_surface);

    if (holder->remote_xdg_surface == NULL && holder->local_xdg_surface == NULL) {
        xdg_surface_holder_free(holder);
    }
}

static void
local_xdg_surface_get_toplevel(struct wl_client *client,
                               struct wl_resource *xdg_surface_resource,
                               uint32_t toplevel_id) {
    WleXdgSurfaceHolder *holder = wl_resource_get_user_data(xdg_surface_resource);
    if (holder->xdg_surface != NULL) {
        wl_resource_post_error(xdg_surface_resource,
                               XDG_WM_BASE_ERROR_ROLE,
                               "xdg_surface already has a role");
    } else {
        struct xdg_toplevel *remote_xdg_toplevel = xdg_surface_get_toplevel(holder->remote_xdg_surface);
        if (remote_xdg_toplevel == NULL) {
            wle_propagate_remote_error(holder->shell->remote_display, client, xdg_surface_resource);
        } else {
            struct wl_resource *resource = wl_resource_create(client,
                                                              &xdg_toplevel_interface,
                                                              wl_resource_get_version(xdg_surface_resource),
                                                              toplevel_id);
            if (resource == NULL) {
                xdg_toplevel_destroy(remote_xdg_toplevel);
                wl_client_post_no_memory(client);
            } else {
                WleToplevel *toplevel = _wle_toplevel_new(holder->view,
                                                          holder->remote_xdg_surface,
                                                          holder->local_xdg_surface,
                                                          _wle_proxy_set_ours((struct wl_proxy *)remote_xdg_toplevel),
                                                          resource);
                g_signal_connect(toplevel, "destroy",
                                 G_CALLBACK(xdg_view_role_destroyed), holder);
                holder->xdg_surface = WLE_XDG_SURFACE(toplevel);
                holder->local_xdg_surface = NULL;  // toplevel takes ownership
                holder->remote_xdg_surface = NULL;  // toplevel takes ownership
                wle_xdg_surface_set_window_geometry(holder->xdg_surface,
                                                    holder->geometry.x,
                                                    holder->geometry.y,
                                                    holder->geometry.width,
                                                    holder->geometry.height);
                wle_view_set_role(holder->view, WLE_VIEW_ROLE(toplevel));
                g_signal_emit(holder->shell, signals[SIG_NEW_TOPLEVEL], 0, toplevel);
            }
        }
    }
}

static void
local_xdg_surface_get_popup(struct wl_client *client,
                            struct wl_resource *xdg_surface_resource,
                            uint32_t popup_id,
                            struct wl_resource *xdg_surface_parent_resource,
                            struct wl_resource *positioner_resource) {
    WleXdgSurfaceHolder *holder = wl_resource_get_user_data(xdg_surface_resource);
    WleXdgSurfaceHolder *parent_holder = xdg_surface_parent_resource != NULL
                                             ? wl_resource_get_user_data(xdg_surface_parent_resource)
                                             : NULL;
    WlePositioner *positioner = positioner_resource != NULL
                                    ? WLE_POSITIONER(wl_resource_get_user_data(positioner_resource))
                                    : NULL;

    g_return_if_fail(holder != NULL);
    g_return_if_fail(WLE_IS_POSITIONER(positioner));

    if (holder->xdg_surface != NULL) {
        wl_resource_post_error(xdg_surface_resource,
                               XDG_WM_BASE_ERROR_ROLE,
                               "xdg_surface already has a role");
    } else if (parent_holder == NULL) {
        wl_resource_post_error(xdg_surface_resource,
                               XDG_WM_BASE_ERROR_INVALID_POPUP_PARENT,
                               "xdg_popup without parent not supported");
    } else if (parent_holder->xdg_surface == NULL) {
        wl_resource_post_error(xdg_surface_resource,
                               XDG_WM_BASE_ERROR_INVALID_POPUP_PARENT,
                               "parent xdg_surface is not constructed");
    } else if (positioner == NULL) {
        wl_resource_post_error(xdg_surface_resource,
                               XDG_WM_BASE_ERROR_INVALID_POSITIONER,
                               "positioner not supplied");
    } else {
        int32_t parent_offset_x = 0;
        int32_t parent_offset_y = 0;
        struct {
            int32_t x;
            int32_t y;
            int32_t width;
            int32_t height;
        } parent_geometry = { 0, 0, 0, 0 };
        struct xdg_surface *remote_parent_xdg_surface = NULL;
        struct zwlr_layer_surface_v1 *remote_parent_layer_surface = NULL;

        if (_wle_view_is_forwarded(WLE_VIEW(parent_holder->view))) {
            remote_parent_xdg_surface = wle_xdg_surface_get_remote_xdg_surface(parent_holder->xdg_surface);
        } else {
            WleViewEmbedData *embed_data = wle_view_get_embed_data(parent_holder->view);
            if (embed_data != NULL) {
                remote_parent_xdg_surface = embed_data->remote_toplevel_xdg_surface;
                remote_parent_layer_surface = embed_data->remote_toplevel_layer_surface;
                parent_offset_x = embed_data->offset_x;
                parent_offset_y = embed_data->offset_y;
                wle_xdg_surface_get_window_geometry(parent_holder->xdg_surface,
                                                    &parent_geometry.x,
                                                    &parent_geometry.y,
                                                    &parent_geometry.width,
                                                    &parent_geometry.height);
            }
        }

        if (remote_parent_xdg_surface == NULL && remote_parent_layer_surface == NULL) {
            wl_resource_post_error(xdg_surface_resource,
                                   XDG_WM_BASE_ERROR_INVALID_POPUP_PARENT,
                                   "unable to find parent surface for popup");
        } else {
            // wlroots at least only looks at the positioner's state when
            // the popup is created, and, at any rate, toolkits like GTK
            // destroy the positioner immediately after destroying the
            // popup, so we have to set the parent offset an size before
            // creating the popup.
            wle_positioner_set_parent_offset(positioner, parent_offset_x, parent_offset_y);
            wle_positioner_set_parent_geometry(positioner,
                                               parent_geometry.x,
                                               parent_geometry.y,
                                               parent_geometry.width,
                                               parent_geometry.height);

            struct xdg_popup *remote_xdg_popup = xdg_surface_get_popup(holder->remote_xdg_surface,
                                                                       remote_parent_xdg_surface,
                                                                       wle_positioner_get_remote_positioner(positioner));
            if (remote_xdg_popup == NULL) {
                wle_propagate_remote_error(holder->shell->remote_display, client, xdg_surface_resource);
            } else {
                if (remote_parent_xdg_surface == NULL) {
                    zwlr_layer_surface_v1_get_popup(remote_parent_layer_surface, remote_xdg_popup);
                }

                struct wl_resource *resource = wl_resource_create(client,
                                                                  &xdg_popup_interface,
                                                                  wl_resource_get_version(xdg_surface_resource),
                                                                  popup_id);
                if (resource == NULL) {
                    xdg_popup_destroy(remote_xdg_popup);
                    wl_client_post_no_memory(client);
                } else {
                    // TODO: set positioner offsets
                    WlePopup *popup = _wle_popup_new(holder->view,
                                                     holder->remote_xdg_surface,
                                                     holder->local_xdg_surface,
                                                     _wle_proxy_set_ours((struct wl_proxy *)remote_xdg_popup),
                                                     resource,
                                                     positioner,
                                                     parent_holder->view);
                    g_signal_connect(popup, "destroy",
                                     G_CALLBACK(xdg_view_role_destroyed), holder);
                    holder->xdg_surface = WLE_XDG_SURFACE(popup);
                    holder->local_xdg_surface = NULL;  // popup takes ownership
                    holder->remote_xdg_surface = NULL;  // popup takes ownership
                    wle_xdg_surface_set_window_geometry(holder->xdg_surface,
                                                        holder->geometry.x,
                                                        holder->geometry.y,
                                                        holder->geometry.width,
                                                        holder->geometry.height);
                    wle_view_set_role(holder->view, WLE_VIEW_ROLE(popup));
                    g_signal_emit(holder->shell, signals[SIG_NEW_POPUP], 0, popup);
                }
            }
        }
    }
}

static void
local_xdg_surface_set_window_geometry(struct wl_client *client,
                                      struct wl_resource *xdg_surface_resource,
                                      int32_t x,
                                      int32_t y,
                                      int32_t width,
                                      int32_t height) {
    WleXdgSurfaceHolder *holder = wl_resource_get_user_data(xdg_surface_resource);
    holder->geometry.x = x;
    holder->geometry.y = y;
    holder->geometry.width = width;
    holder->geometry.height = height;
    if (holder->remote_xdg_surface != NULL) {
        xdg_surface_set_window_geometry(holder->remote_xdg_surface, x, y, width, height);
    } else if (holder->xdg_surface != NULL) {
        wle_xdg_surface_set_window_geometry(holder->xdg_surface, x, y, width, height);
    }
}

static void
local_xdg_surface_ack_configure(struct wl_client *client, struct wl_resource *xdg_surface_resource, uint32_t serial) {
    WleXdgSurfaceHolder *holder = wl_resource_get_user_data(xdg_surface_resource);
    if (holder->remote_xdg_surface != NULL) {
        xdg_surface_ack_configure(holder->remote_xdg_surface, serial);
    } else if (holder->xdg_surface != NULL) {
        wle_xdg_surface_ack_configure(holder->xdg_surface, serial);
    }
}

static void
local_xdg_surface_destroy(struct wl_client *client, struct wl_resource *xdg_surface_resource) {
    WleXdgSurfaceHolder *holder = wl_resource_get_user_data(xdg_surface_resource);
    if (holder->xdg_surface != NULL) {
        holder->remote_xdg_surface = wle_xdg_surface_steal_remote_xdg_surface(holder->xdg_surface);
        holder->local_xdg_surface = wle_xdg_surface_steal_local_xdg_surface(holder->xdg_surface);
    }
    wl_resource_destroy(xdg_surface_resource);
}

static void
local_xdg_surface_destroy_impl(struct wl_resource *resource) {
    TRACE("entering");
    WleXdgSurfaceHolder *holder = wl_resource_get_user_data(resource);

    if (holder->xdg_surface != NULL) {
        holder->remote_xdg_surface = wle_xdg_surface_steal_remote_xdg_surface(holder->xdg_surface);
        holder->local_xdg_surface = wle_xdg_surface_steal_local_xdg_surface(holder->xdg_surface);
    }

    if (holder->local_xdg_surface != NULL) {
        holder->local_xdg_surface = NULL;
        xdg_surface_holder_free(holder);
    }
}

static void
local_xdg_wm_base_get_xdg_surface(struct wl_client *client,
                                  struct wl_resource *base_resource,
                                  uint32_t xdg_surface_id,
                                  struct wl_resource *surface_resource) {
    WleXdgShell *shell = WLE_XDG_SHELL(wl_resource_get_user_data(base_resource));
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    struct wl_resource *resource = wl_resource_create(client,
                                                      &xdg_surface_interface,
                                                      wl_resource_get_version(base_resource),
                                                      xdg_surface_id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        struct xdg_surface *remote_xdg_surface = xdg_wm_base_get_xdg_surface(shell->remote_xdg_wm_base,
                                                                             wle_view_get_remote_surface(view));
        if (remote_xdg_surface == NULL) {
            wle_propagate_remote_error(shell->remote_display, client, base_resource);
            wl_resource_destroy(resource);
        } else {
            _wle_proxy_set_ours((struct wl_proxy *)remote_xdg_surface);

            WleXdgSurfaceHolder *holder = g_new0(WleXdgSurfaceHolder, 1);
            holder->shell = shell;
            holder->view = view;
            holder->remote_xdg_surface = remote_xdg_surface;
            holder->local_xdg_surface = resource;
            shell->holders = g_list_prepend(shell->holders, holder);
            xdg_surface_add_listener(remote_xdg_surface, &remote_xdg_surface_listener, holder);
            wl_resource_set_implementation(resource, &local_xdg_surface_impl, holder, local_xdg_surface_destroy_impl);
        }
    }
}

static void
positioner_destroyed(WlePositioner *positioner, WleXdgShell *shell) {
    shell->positioners = g_list_remove(shell->positioners, positioner);
}

static void
local_xdg_wm_base_create_positioner(struct wl_client *client,
                                    struct wl_resource *base_resource,
                                    uint32_t positioner_id) {
    struct wl_resource *resource = wl_resource_create(client,
                                                      &xdg_positioner_interface,
                                                      wl_resource_get_version(base_resource),
                                                      positioner_id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        WleXdgShell *shell = wl_resource_get_user_data(base_resource);
        struct xdg_positioner *remote_positioner = xdg_wm_base_create_positioner(shell->remote_xdg_wm_base);
        if (remote_positioner == NULL) {
            wle_propagate_remote_error(shell->remote_display, client, base_resource);
            wl_resource_destroy(resource);
        } else {
            _wle_proxy_set_ours((struct wl_proxy *)remote_positioner);
            WlePositioner *positioner = wle_positioner_new(remote_positioner, resource);
            g_signal_connect(positioner, "destroy",
                             G_CALLBACK(positioner_destroyed), shell);
            shell->positioners = g_list_prepend(shell->positioners, positioner);
        }
    }
}

static void
local_xdg_wm_base_pong(struct wl_client *client, struct wl_resource *base_resource, uint32_t serial) {
    WleXdgShell *shell = wl_resource_get_user_data(base_resource);
    if (shell->last_pong_serial < serial) {
        shell->last_pong_serial = serial;
        xdg_wm_base_pong(shell->remote_xdg_wm_base, serial);
    }
}

static void
local_xdg_wm_base_destroy(struct wl_client *client, struct wl_resource *base_resource) {
    wl_resource_destroy(base_resource);
}

static void
local_xdg_wm_base_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_xdg_wm_base_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleXdgShell *shell = data;
    if (version > wl_global_get_version(shell->local_xdg_wm_base)) {
        wl_client_post_implementation_error(client, "unsupported xdg_wm_base version");
    } else {
        struct wl_resource *resource = wl_resource_create(client, &xdg_wm_base_interface, version, id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(resource, &local_xdg_wm_base_impl, shell, local_xdg_wm_base_destroy_impl);
            if (shell == NULL) {
                wl_list_init(wl_resource_get_link(resource));
            } else {
                wl_list_insert(&shell->local_xdg_wm_base_resources, wl_resource_get_link(resource));
            }
        }
    }
}

static void
xdg_surface_holder_free(WleXdgSurfaceHolder *holder) {
    if (holder->xdg_surface != NULL) {
        g_signal_handlers_disconnect_by_data(holder->xdg_surface, holder);
        holder->remote_xdg_surface = wle_xdg_surface_steal_remote_xdg_surface(holder->xdg_surface);
        holder->local_xdg_surface = wle_xdg_surface_steal_local_xdg_surface(holder->xdg_surface);
        wle_local_resource_destroy(WLE_LOCAL_RESOURCE(holder->xdg_surface));
        g_object_unref(holder->xdg_surface);
    }

    if (holder->remote_xdg_surface != NULL) {
        xdg_surface_destroy(holder->remote_xdg_surface);
    }

    if (holder->local_xdg_surface != NULL) {
        struct wl_resource *local_xdg_surface = holder->local_xdg_surface;
        holder->local_xdg_surface = NULL;
        wl_resource_destroy(local_xdg_surface);
    }

    holder->shell->holders = g_list_remove(holder->shell->holders, holder);
    g_free(holder);
}

WleXdgShell *
wle_xdg_shell_new(struct wl_display *remote_display,
                  struct wl_compositor *remote_compositor,
                  struct xdg_wm_base *remote_xdg_wm_base,
                  struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_compositor != NULL, NULL);
    g_return_val_if_fail(remote_xdg_wm_base != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleXdgShell *shell = g_object_new(WLE_TYPE_XDG_SHELL, NULL);
    shell->remote_display = remote_display;
    shell->remote_compositor = remote_compositor;
    shell->remote_xdg_wm_base = remote_xdg_wm_base;
    shell->local_xdg_wm_base = wl_global_create(local_display,
                                                &xdg_wm_base_interface,
                                                wl_proxy_get_version((struct wl_proxy *)remote_xdg_wm_base),
                                                shell,
                                                local_xdg_wm_base_bind);
    if (shell->local_xdg_wm_base == NULL) {
        g_object_unref(shell);
        return NULL;
    } else {
        xdg_wm_base_add_listener(remote_xdg_wm_base, &remote_xdg_wm_base_listener, shell);
        return shell;
    }
}
