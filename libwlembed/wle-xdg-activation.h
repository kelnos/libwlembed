/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_XDG_ACTIVATION_H__
#define __WLE_XDG_ACTIVATION_H__

#include <glib.h>
#include <wayland-server.h>

#include "protocol/xdg-activation-v1-client-protocol.h"

G_BEGIN_DECLS

typedef struct _WleXdgActivation WleXdgActivation;

WleXdgActivation *wle_xdg_activation_create(struct wl_display *remote_display,
                                            struct xdg_activation_v1 *remote_activation,
                                            struct wl_display *local_display);
void wle_xdg_activation_destroy(WleXdgActivation *activation);

G_END_DECLS

#endif /* __WLE_XDG_ACTIVATION_H__ */
