/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_KDE_DECORATION_FORWARDER_H__
#define __WLE_KDE_DECORATION_FORWARDER_H__

#include <glib-object.h>
#include <wayland-client-core.h>

#include "protocol/server-decoration-client-protocol.h"

G_BEGIN_DECLS

typedef struct _WleKdeDecorationForwarder WleKdeDecorationForwarder;

WleKdeDecorationForwarder *wle_kde_decoration_forwarder_create(struct wl_display *remote_display,
                                                               struct org_kde_kwin_server_decoration_manager *remote_manager,
                                                               struct wl_display *local_display);
void wle_kde_decoration_forwarder_destroy(WleKdeDecorationForwarder *forwarder);

G_END_DECLS

#endif
