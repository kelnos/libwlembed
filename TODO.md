# Broken

* Occasionally a popup is mispositioned, not sure why, often dismissing
  it and opening it again fixes it.
* When a popup or popover is shown, clicking another part of the embed
  area dismisses it, but clicking in another part of the same window
  does not.
* `WleGtkSocket` focus chain works in the forward direction but not the
  reverse direction (seems to trigger the focus loop detection).
* `WleGtkSocket` does not get `GDK_ENTER_NOTIFY` or `GDK_LEAVE_NOTIFY`
  events, because GDK doesn't get `wl_pointer.enter`/`leave` events that
  it recognizes.  I'm forwarding those events, but GDK doesn't appear to
  handle `GDK_WINDOW_SUBSURFACE` very well and can't figure out how to
  associate those forwarded events with the right `GdkWindow`.
* DnD sometimes results in Wayland protocol errors that kill the app.
* There's a `g_warning` printed when doing same-app DnD and you drag
  something from outside the embed area into the embed area.  The reason
  is because GDK isn't getting any `wl_data_device.enter` events that it
  recognizes, and so it can't figure out or set the destination
  `GdkWindow` on the `GdkDragContext`.  I can't really forward events,
  because I don't have a `wl_data_offer` that GDK will recognize, and
  using one of libwlembed's will break things, as GDK will want to set a
  listener on it.

# Missing

* A slew of protocols should probably be implemented in the embedded
  compositor, which should be forwarded to the parent, or handled in
  some way.  This list isn't exhaustive, just the ones I'm thinking
  about right now.
  * input-method-unstable-v1
  * gtk-shell1 (might be obsolete and superseded by a bunch of others)
  * pointer-gestures-unstable-v1
  * tablet-unstable-v2
  * wlr-data-control-unstable-v1
  * wlr-export-dmabuf-unstable-v1
  * wlr-gamma-control-unstable-v1
  * wlr-screencopy-unstable-v1
  * xdg-foreign-unstable-v1
  * xdg-foreign-unstable-v2
