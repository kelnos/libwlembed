/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SECTION:wle-error
 * @title: Errors
 * @short_description: Error type and enumeration
 * @stability: Unstable
 * @include: libwlembed/libwlembed.h
 *
 * This file includes the error type and enumeration used by the
 * library.
 **/

#include "wle-error.h"
#include "libwlembed-visibility.h"

/**
 * wle_error_quark:
 *
 * Gets the libwlembed error domain quark.
 *
 * Returns: a #GQuark.
 **/
G_DEFINE_QUARK(wle - error - quark, wle_error)

#define __WLE_ERROR_C__
#include <libwlembed-visibility.c>
