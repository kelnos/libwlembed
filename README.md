# libwlembed

`libwlembed` is a Wayland compositor library that allows you to embed
surfaces from one application into another by way of what's called an
"embedded compositor".

This project consists of two libraries:

* `libwlembed`, a toolkit-agnostic library that implements the embedded
  compositor
* `libwlembed-gtk3`, a GTK3 library that wraps the embedded compositor
  into an easy-to-use `GtkWidget` called `WleGtkSocket`

The API/ABI may change in releases, hence the '0' major version.
Semantic versioning will be used, however, so you can expect
compatibility by comparing versions according to semantic versioning
rules.

## Building

Prerequisites:

* `xfce4-dev-tools` (4.19.2 or newer)
* `glib`/`gobject`/`gio`
* `libwayland-client`
* `libwayland-server`
* `wayland-protocols`
* `wayland-scanner`
* `xkbcommon`

In addition, if you would like to build `libwlembed-gtk3` or the example
app, you will need:

* `gtk3`
* `gtk-layer-shell` (optional)

First, clone with:

```bash
git clone --recursive https://gitlab.xfce.org/kelnos/libwlembed.git
```

Then build with:

```bash
meson setup --prefix=/some/prefix build
meson compile -Cbuild
meson install -Cbuild
```

## Usage

Two `pkg-config` module files are provided, called `libwlembed-0` and
`libwlembed-gtk3-0`.  Some documentation is provided, but it is
incomplete.

Again, the library is incomplete, certainly has bugs, and will undergo
breaking API and ABI changes in the near future.

## Example

See [`examples/`](examples/) for a simple embedding example.

## Support

The following Wayland protocols are supported for apps running under the
embedded compositor:

* Wayland core (except `wl_shell` and `wl_shell_surface`)
* kde-server-decoration
* presentation-time
* wp-fractional-scale-v1
* wp-linux-dmabuf-v1
* wp-viewporter
* wp-primary-selection
* xdg-decoration-unstable-v1
* xdg-output-unstable-v1
* xdg-shell
* ext-foreign-toplevel-list
* wlr-foreign-toplevel-management-unstable-v1

See [the TODO list](TODO.md) for a list of protocols we plan to support
in the future.  Feel free to request others.  (Also feel free to submit
a merge request!)
