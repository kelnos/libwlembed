/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_TOPLEVEL_H__
#define __WLE_TOPLEVEL_H__

#include <glib-object.h>
#include <wayland-server.h>

#include "protocol/xdg-shell-client-protocol.h"
#include "wle-view.h"
#include "wle-xdg-surface.h"

G_DECLARE_FINAL_TYPE(WleToplevel, wle_toplevel, WLE, TOPLEVEL, WleXdgSurface)
#define WLE_TYPE_TOPLEVEL (wle_toplevel_get_type())

WleToplevel *_wle_toplevel_new(WleView *view,
                               struct xdg_surface *remote_xdg_surface,
                               struct wl_resource *local_xdg_surface,
                               struct xdg_toplevel *remote_xdg_toplevel,
                               struct wl_resource *local_xdg_toplevel);

struct xdg_toplevel *_wle_toplevel_get_remote_xdg_toplevel(WleToplevel *toplevel);

gboolean wle_toplevel_is_visible(WleToplevel *toplevel);

void wle_toplevel_show(WleToplevel *toplevel);
void wle_toplevel_hide(WleToplevel *toplevel);

void wle_toplevel_activate(WleToplevel *toplevel,
                           uint32_t serial);
void wle_toplevel_deactivate(WleToplevel *toplevel,
                             uint32_t serial);

#endif /* __WLE_TOPLEVEL_H__ */
