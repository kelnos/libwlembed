/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server.h>

#include "common/wle-debug.h"
#include "wle-output.h"

#define WL_OUTPUT_VERSION 4

typedef struct {
    int32_t width;
    int32_t height;
    int32_t refresh;  // millihertz
} WleMode;

struct _WleOutput {
    GObject parent;

    gchar *name;
    gchar *description;
    gchar *make;
    gchar *model;

    int32_t x;
    int32_t y;
    int32_t physical_width;
    int32_t physical_height;

    int32_t scale;
    int32_t subpixel;
    int32_t transform;

    WleMode *current_mode;
    WleMode *preferred_mode;
    GList *modes;

    struct wl_output *remote_output;

    struct wl_global *local_output;
    struct wl_list local_output_resources;
};

static void wle_output_finalize(GObject *object);

static void output_name(void *data,
                        struct wl_output *wl_output,
                        const char *name);
static void output_description(void *data,
                               struct wl_output *wl_output,
                               const char *description);
static void output_mode(void *data,
                        struct wl_output *wl_output,
                        uint32_t flags,
                        int32_t width,
                        int32_t height,
                        int32_t refresh);
static void output_geometry(void *data,
                            struct wl_output *wl_output,
                            int32_t x,
                            int32_t y,
                            int32_t physical_width,
                            int32_t physical_height,
                            int32_t subpixel,
                            const char *make,
                            const char *model,
                            int32_t transform);
static void output_scale(void *data,
                         struct wl_output *wl_output,
                         int32_t scale);
static void output_done(void *data,
                        struct wl_output *wl_output);

static void local_output_release(struct wl_client *client,
                                 struct wl_resource *resource);


G_DEFINE_FINAL_TYPE(WleOutput, wle_output, G_TYPE_OBJECT)


static const struct wl_output_listener output_listener = {
    .name = output_name,
    .description = output_description,
    .mode = output_mode,
    .geometry = output_geometry,
    .scale = output_scale,
    .done = output_done,
};

static struct wl_output_interface wl_output_impl = {
    .release = local_output_release,
};

static void
wle_output_class_init(WleOutputClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->finalize = wle_output_finalize;
}

static void
wle_output_init(WleOutput *output) {
    wl_list_init(&output->local_output_resources);
}

static void
wle_output_finalize(GObject *object) {
    WleOutput *output = WLE_OUTPUT(object);

    wl_output_destroy(output->remote_output);

    struct wl_resource *resource, *tmp_resource;
    wl_resource_for_each_safe(resource, tmp_resource, &output->local_output_resources) {
        wl_resource_destroy(resource);
    }

    wl_global_destroy(output->local_output);

    g_free(output->make);
    g_free(output->model);
    g_free(output->name);
    g_free(output->description);

    GList *l = output->modes;
    while (l != NULL) {
        GList *next = l->next;
        g_slice_free(WleMode, l->data);
        output->modes = g_list_delete_link(output->modes, l);
        l = next;
    }

    G_OBJECT_CLASS(wle_output_parent_class)->finalize(object);
}

static void
notify_geometry(WleOutput *output, struct wl_resource *resource) {
    wl_output_send_geometry(resource,
                            output->x,
                            output->y,
                            output->physical_width,
                            output->physical_height,
                            output->subpixel,
                            output->make != NULL ? output->make : "Unknown",
                            output->model != NULL ? output->model : "Unknown",
                            output->transform);
}

static void
notify_mode(WleOutput *output, struct wl_resource *resource, WleMode *mode) {
    uint32_t flags = 0;
    if (mode == output->preferred_mode) {
        flags |= WL_OUTPUT_MODE_PREFERRED;
    }
    if (mode == output->current_mode) {
        flags |= WL_OUTPUT_MODE_CURRENT;
    }
    wl_output_send_mode(resource, flags, mode->width, mode->height, mode->refresh);
}

static void
notify_scale(WleOutput *output, struct wl_resource *resource) {
    if (wl_resource_get_version(resource) >= WL_OUTPUT_SCALE_SINCE_VERSION) {
        wl_output_send_scale(resource, ceil(output->scale));
    }
}

static void
notify_name(WleOutput *output, struct wl_resource *resource) {
    if (wl_resource_get_version(resource) >= WL_OUTPUT_NAME_SINCE_VERSION) {
        wl_output_send_name(resource, output->name);
    }
}

static void
notify_description(WleOutput *output, struct wl_resource *resource) {
    if (wl_resource_get_version(resource) >= WL_OUTPUT_DESCRIPTION_SINCE_VERSION) {
        wl_output_send_description(resource, output->description);
    }
}

static void
notify_done(WleOutput *output, struct wl_resource *resource) {
    if (wl_resource_get_version(resource) >= WL_OUTPUT_DONE_SINCE_VERSION) {
        wl_output_send_done(resource);
    }
}

static void
output_name(void *data, struct wl_output *wl_output, const char *name) {
    WleOutput *output = WLE_OUTPUT(data);
    if (g_strcmp0(name, output->name) != 0) {
        g_free(output->name);
        output->name = g_strdup(name);

        struct wl_resource *resource;
        wl_resource_for_each(resource, &output->local_output_resources) {
            notify_name(output, resource);
        }
    }
}

static void
output_description(void *data, struct wl_output *wl_output, const char *description) {
    WleOutput *output = WLE_OUTPUT(data);
    if (g_strcmp0(description, output->description) != 0) {
        g_free(output->description);
        output->description = g_strdup(description);

        struct wl_resource *resource;
        wl_resource_for_each(resource, &output->local_output_resources) {
            notify_description(output, resource);
        }
    }
}

static void
output_mode(void *data, struct wl_output *wl_output, uint32_t flags, int32_t width, int32_t height, int32_t refresh) {
    WleOutput *output = WLE_OUTPUT(data);
    WleMode *mode = NULL;

    for (GList *l = output->modes; l != NULL; l = l->next) {
        WleMode *cur = l->data;
        if (cur->width == width && cur->height == height && cur->refresh == refresh) {
            mode = cur;
            break;
        }
    }

    if (mode == NULL) {
        mode = g_slice_new0(WleMode);
        output->modes = g_list_append(output->modes, mode);
    }

    mode->width = width;
    mode->height = height;
    mode->refresh = refresh;
    if ((flags & WL_OUTPUT_MODE_CURRENT) != 0) {
        output->current_mode = mode;
    }
    if ((flags & WL_OUTPUT_MODE_PREFERRED) != 0) {
        output->preferred_mode = mode;
    }

    struct wl_resource *resource;
    wl_resource_for_each(resource, &output->local_output_resources) {
        notify_mode(output, resource, mode);
    }
}

static void
output_geometry(void *data,
                struct wl_output *wl_output,
                int32_t x,
                int32_t y,
                int32_t physical_width,
                int32_t physical_height,
                int32_t subpixel,
                const char *make,
                const char *model,
                int32_t transform) {
    WleOutput *output = WLE_OUTPUT(data);

    g_free(output->make);
    g_free(output->model);

    output->make = g_strdup(make);
    output->model = g_strdup(model);
    output->x = x;
    output->y = y;
    output->physical_width = physical_width;
    output->physical_height = physical_height;
    output->subpixel = subpixel;
    output->transform = transform;

    struct wl_resource *resource;
    wl_resource_for_each(resource, &output->local_output_resources) {
        notify_geometry(output, resource);
    }
}

static void
output_scale(void *data, struct wl_output *wl_output, int32_t scale) {
    WleOutput *output = WLE_OUTPUT(data);

    if (output->scale != scale) {
        output->scale = scale;

        struct wl_resource *resource;
        wl_resource_for_each(resource, &output->local_output_resources) {
            notify_scale(output, resource);
        }
    }
}

static void
output_done(void *data, struct wl_output *wl_output) {
    WleOutput *output = WLE_OUTPUT(data);
    DBG("Got 'done' event for output '%s' (%s): %dx%d+%d+%d@%.2fMHz, %dmm x %dmm, scale=%d",
        output->name,
        output->description,
        output->current_mode != NULL ? output->current_mode->width : 0,
        output->current_mode != NULL ? output->current_mode->height : 0,
        output->x,
        output->y,
        output->current_mode != NULL ? (gdouble)output->current_mode->refresh / 1000 : 0,
        output->physical_width,
        output->physical_height,
        output->scale);

    struct wl_resource *resource;
    wl_resource_for_each(resource, &output->local_output_resources) {
        notify_done(output, resource);
    }
}

static void
local_output_release(struct wl_client *client, struct wl_resource *resource) {
    wl_resource_destroy(resource);
}

static void
local_output_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_output_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleOutput *output = WLE_OUTPUT(data);

    struct wl_resource *resource = wl_resource_create(client, &wl_output_interface, version, id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource, &wl_output_impl, output, local_output_destroy_impl);

        if (output == NULL) {
            wl_list_init(wl_resource_get_link(resource));
        } else {
            wl_list_insert(&output->local_output_resources, wl_resource_get_link(resource));

            notify_geometry(output, resource);
            for (GList *l = output->modes; l != NULL; l = l->next) {
                notify_mode(output, resource, l->data);
            }
            notify_scale(output, resource);
            notify_name(output, resource);
            notify_description(output, resource);
            notify_done(output, resource);
        }
    }
}

WleOutput *
wle_output_new(struct wl_output *remote_output, struct wl_display *local_display) {
    g_return_val_if_fail(remote_output != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleOutput *output = g_object_new(WLE_TYPE_OUTPUT, NULL);
    output->local_output = wl_global_create(local_display,
                                            &wl_output_interface,
                                            wl_proxy_get_version((struct wl_proxy *)remote_output),
                                            output,
                                            local_output_bind);
    if (output->local_output == NULL) {
        g_object_unref(output);
        return NULL;
    } else {
        output->remote_output = remote_output;
        wl_output_add_listener(remote_output, &output_listener, output);
        return output;
    }
}

struct wl_output *
wle_output_get_remote_output(WleOutput *output) {
    g_return_val_if_fail(WLE_IS_OUTPUT(output), NULL);
    return output->remote_output;
}

GList *
wle_output_get_local_outputs(WleOutput *output, struct wl_client *client) {
    g_return_val_if_fail(WLE_IS_OUTPUT(output), NULL);
    GList *outputs = NULL;
    struct wl_resource *resource;
    wl_resource_for_each(resource, &output->local_output_resources) {
        if (wl_resource_get_client(resource) == client) {
            outputs = g_list_prepend(outputs, resource);
        }
    }
    return outputs;
}
