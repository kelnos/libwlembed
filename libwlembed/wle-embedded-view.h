/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_EMBEDDED_VIEW_H__
#define __WLE_EMBEDDED_VIEW_H__

#if !defined(__IN_LIBWLEMBED_H__) && !defined(LIBWLEMBED_COMPILATION)
#error "You may not include this header directly; instead, use <libwlembed/libwlembed.h>"
#endif

#include <glib-object.h>
#include <libwlembed/wle-seat.h>
#include <wayland-client-protocol.h>

G_BEGIN_DECLS

struct xdg_surface;
struct xdg_toplevel;
struct zwlr_layer_surface_v1;

G_DECLARE_FINAL_TYPE(WleEmbeddedView, wle_embedded_view, WLE, EMBEDDED_VIEW, GObject)
#define WLE_TYPE_EMBEDDED_VIEW (wle_embedded_view_get_type())

void wle_embedded_view_set_size(WleEmbeddedView *view,
                                gint width,
                                gint height);
void wle_embedded_view_get_size(WleEmbeddedView *view,
                                gint *width,
                                gint *height);

void wle_embedded_view_move(WleEmbeddedView *view,
                            gint offset_x,
                            gint offset_y);

void wle_embedded_view_show(WleEmbeddedView *view,
                            struct wl_surface *new_parent_surface);
void wle_embedded_view_hide(WleEmbeddedView *view);

struct wl_surface *wle_embedded_view_get_parent_surface(WleEmbeddedView *view);

void wle_embedded_view_set_toplevel_xdg_surface(WleEmbeddedView *view,
                                                struct xdg_surface *toplevel_xdg_surface,
                                                struct xdg_toplevel *toplevel_xdg_toplevel);
void wle_embedded_view_set_toplevel_layer_surface(WleEmbeddedView *view,
                                                  struct zwlr_layer_surface_v1 *toplevel_layer_surface);
void wle_embedded_view_set_toplevel_geometry(WleEmbeddedView *view,
                                             gint toplevel_x,
                                             gint toplevel_y,
                                             gint toplevel_width,
                                             gint toplevel_height);

void wle_embedded_view_toplevel_activated(WleEmbeddedView *view,
                                          WleSeat *seat);
void wle_embedded_view_toplevel_deactivated(WleEmbeddedView *view,
                                            WleSeat *seat);

void wle_embedded_view_focus(WleEmbeddedView *view,
                             WleSeat *seat);
void wle_embedded_view_unfocus(WleEmbeddedView *view,
                               WleSeat *seat);

guint32 wle_embedded_view_focus_first(WleEmbeddedView *view);
guint32 wle_embedded_view_focus_last(WleEmbeddedView *view);

void wle_embedded_view_destroy(WleEmbeddedView *eview);

G_END_DECLS

#endif /* __WLE_EMBEDDED_VIEW_H__ */
