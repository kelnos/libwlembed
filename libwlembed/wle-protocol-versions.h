/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_INTERFACE_VERSION_H__
#define __WLE_INTERFACE_VERSION_H__

#include <wayland-server-protocol.h>

// These are the max interface versions we support.  If the parent compositor
// supports newer versions, we will use the versions below.  If the parent
// compositor supports lower versions, we will (mostly, hopefully) support them
// as well.

#if defined(WL_POINTER_AXIS_RELATIVE_DIRECTION)
#define WL_SEAT_VERSION 9
#elif defined(WL_POINTER_AXIS_VALUE120)
#define WL_SEAT_VERSION 8
#else
#define WL_SEAT_VERSION 7
#endif

#if defined(WL_SURFACE_PREFERRED_BUFFER_SCALE) && defined(WL_SURFACE_PREFERRED_BUFFER_TRANSFORM)
#define WL_COMPOSITOR_VERSION 6
#else
#define WL_COMPOSITOR_VERSION 5
#endif

#define WL_SUBCOMPOSITOR_VERSION 1
#define WL_SHM_VERSION 2
#define WL_OUTPUT_VERSION 4
#define WL_DATA_DEVICE_MANAGER_VERSION 3

#define WP_VIEWPORTER_VERSION 1
#define WP_PRESENTATION_TIME_VERSION 1
#define WP_FRACTIONAL_SCALE_VERSION 1
#define WP_PRIMARY_SELECTION_VERSION 1
#define WP_LINUX_DMABUF_VERSION 5

#define XDG_SHELL_VERSION 6
#define XDG_OUTPUT_VERSION 3
#define XDG_DECORATION_VERSION 1
#define XDG_ACTIVATION_VERSION 1

#define EXT_FOREIGN_TOPLEVEL_LIST_VERSION 1

#define WLR_FOREIGN_TOPLEVEL_MANAGEMENT_VERSION 3

#define KDE_SERVER_DECORATION_VERSION 1

#endif /* __WLE_INTERFACE_VERSION_H__ */
