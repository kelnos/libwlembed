/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_COMPOSITOR_H__
#define __WLE_COMPOSITOR_H__

#include <glib-object.h>
#include <wayland-client.h>
#include <wayland-server.h>

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleCompositor, wle_compositor, WLE, COMPOSITOR, GObject)
#define WLE_TYPE_COMPOSITOR (wle_compositor_get_type())

typedef struct _WleRegion WleRegion;

WleCompositor *wle_compositor_new(struct wl_display *remote_display,
                                  struct wl_compositor *remote_compositor,
                                  struct wl_subcompositor *remote_subcompositor,
                                  struct wl_display *local_display);

G_END_DECLS

#endif /* __WLE_COMPOSITOR_H__ */
