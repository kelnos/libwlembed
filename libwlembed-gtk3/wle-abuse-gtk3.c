/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <dlfcn.h>
#include <gdk/gdkwayland.h>

#include "wle-abuse-gtk3.h"
#include "libwlembed-gtk3-visibility.h"

#define SHADOW_WIDTH_KEY "--abuse-gtk-shadow-width"

// From gtk/gdk/gdkwindowimpl.h
typedef struct {
    GObject parent;
} AbuseGdkWindowImpl;

// From gtk/gdk/gdkinternals.h
typedef struct {
    GObject parent;

    AbuseGdkWindowImpl *impl;

    // More fields we don't care about
} AbuseGdkWindow;

// From gtk/gdk/wayland/gdkwindow-wayland.c
typedef enum _PositionMethod {
    DUMMY,
} PositionMethod;

// From gtk/gdk/wayland/gdkwindow-wayland.c
typedef struct {
    AbuseGdkWindowImpl parent;

    GdkWindow *wrapper;

    struct {
        GSList *outputs;
        struct wl_surface *wl_surface;
        struct xdg_surface *xdg_surface;
        struct xdg_toplevel *xdg_toplevel;
        struct xdg_popup *xdg_popup;
        struct zxdg_surface_v6 *zxdg_surface_v6;
        struct zxdg_toplevel_v6 *zxdg_toplevel_v6;
        struct zxdg_popup_v6 *zxdg_popup_v6;
        struct gtk_surface1 *gtk_surface;
        struct wl_subsurface *wl_subsurface;
        struct wl_egl_window *egl_window;
        struct wl_egl_window *dummy_egl_window;
        struct zxdg_exported_v1 *xdg_exported;
        struct org_kde_kwin_server_decoration *server_decoration;
    } display_server;


    gpointer egl_surface;
    gpointer dummy_egl_surface;

    unsigned int initial_configure_received : 1;
    unsigned int configuring_popup : 1;
    unsigned int mapped : 1;
    unsigned int use_custom_surface : 1;
    unsigned int pending_buffer_attached : 1;
    unsigned int pending_commit : 1;
    unsigned int awaiting_frame : 1;
    unsigned int using_csd : 1;
    GdkWindowTypeHint hint;
    GdkWindow *transient_for;
    GdkWindow *popup_parent;
    PositionMethod position_method;

    cairo_surface_t *staging_cairo_surface;
    cairo_surface_t *committed_cairo_surface;
    cairo_surface_t *backfill_cairo_surface;

    int pending_buffer_offset_x;
    int pending_buffer_offset_y;

    int subsurface_x;
    int subsurface_y;

    gchar *title;

    struct {
        gboolean was_set;

        gchar *application_id;
        gchar *app_menu_path;
        gchar *menubar_path;
        gchar *window_object_path;
        gchar *application_object_path;
        gchar *unique_bus_name;
    } application;

    GdkGeometry geometry_hints;
    GdkWindowHints geometry_mask;

    GdkSeat *grab_input_seat;

    gint64 pending_frame_counter;
    guint32 scale;

    int margin_left;
    int margin_right;
    int margin_top;
    int margin_bottom;
    gboolean margin_dirty;

    // More fields we don't care about
} AbuseGdkWindowImplWayland;

static GOnce once_init = G_ONCE_INIT;

static void (*real_gdk_window_set_shadow_width)(GdkWindow *window,
                                                gint left,
                                                gint right,
                                                gint top,
                                                gint bottom);

static gpointer
abuse_gdk_wayland_init(gpointer data) {
    static void *handle = NULL;

    handle = dlopen("libgdk-3." G_MODULE_SUFFIX ".0", RTLD_LAZY);
    if (handle == NULL) {
        handle = dlopen("libgdk-3." G_MODULE_SUFFIX, RTLD_LAZY);
    }
    if (handle == NULL) {
        g_critical("Can't find libgdk-3 in order to dlopen() it");
        return NULL;
    }

    real_gdk_window_set_shadow_width = dlsym(handle, "gdk_window_set_shadow_width");
    if (real_gdk_window_set_shadow_width == NULL) {
        g_critical("Can't find gdk_window_set_shadow_width symbol");
        dlclose(handle);
        handle = NULL;
    }

    return NULL;
}

void
gdk_window_set_shadow_width(GdkWindow *window,
                            gint left,
                            gint right,
                            gint top,
                            gint bottom) {
    g_return_if_fail(GDK_IS_WINDOW(window));

    GdkRectangle *rect = g_object_get_data(G_OBJECT(window), SHADOW_WIDTH_KEY);
    if (rect == NULL) {
        rect = g_new0(GdkRectangle, 1);
        g_object_set_data_full(G_OBJECT(window), SHADOW_WIDTH_KEY, rect, g_free);
    }

    rect->x = left;
    rect->y = top;
    rect->width = right;
    rect->height = bottom;

    g_once(&once_init, abuse_gdk_wayland_init, NULL);
    g_return_if_fail(real_gdk_window_set_shadow_width != NULL);

    real_gdk_window_set_shadow_width(window, left, right, top, bottom);
}

gboolean
abuse_gdk_window_get_shadow_width(GdkWindow *window,
                                  gint *left,
                                  gint *right,
                                  gint *top,
                                  gint *bottom) {
    g_return_val_if_fail(GDK_IS_WINDOW(window), FALSE);

    GdkRectangle *rect = g_object_get_data(G_OBJECT(window), SHADOW_WIDTH_KEY);
    if (rect != NULL) {
        if (left != NULL) {
            *left = rect->x;
        }
        if (right != NULL) {
            *right = rect->width;
        }
        if (top != NULL) {
            *top = rect->y;
        }
        if (bottom != NULL) {
            *bottom = rect->height;
        }
        return TRUE;
    } else {
        return FALSE;
    }
}

static AbuseGdkWindowImplWayland *
get_window_impl(GdkWindow *window) {
    g_return_val_if_fail(gtk_check_version(3, 22, 30) == NULL, NULL);
    g_return_val_if_fail(GDK_IS_WAYLAND_WINDOW(window), NULL);
    AbuseGdkWindow *abuse_window = (AbuseGdkWindow *)window;
    return (AbuseGdkWindowImplWayland *)abuse_window->impl;
}

struct xdg_surface *
abuse_gdk_wayland_window_get_xdg_surface(GdkWindow *window) {
    AbuseGdkWindowImplWayland *abuse_window_wayland_impl = get_window_impl(window);
    return abuse_window_wayland_impl->display_server.xdg_surface;
}

struct xdg_toplevel *
abuse_gdk_wayland_window_get_xdg_toplevel(GdkWindow *window) {
    AbuseGdkWindowImplWayland *abuse_window_wayland_impl = get_window_impl(window);
    return abuse_window_wayland_impl->display_server.xdg_toplevel;
}

gboolean
abuse_gdk_wayland_window_get_window_geometry(GdkWindow *window, GdkRectangle *geometry) {
    AbuseGdkWindowImplWayland *abuse_window_wayland_impl = get_window_impl(window);
    if (abuse_window_wayland_impl != NULL) {
        gint width = gdk_window_get_width(window);
        gint height = gdk_window_get_height(window);
        *geometry = (GdkRectangle){
            .x = abuse_window_wayland_impl->margin_left,
            .y = abuse_window_wayland_impl->margin_top,
            .width = width - (abuse_window_wayland_impl->margin_left + abuse_window_wayland_impl->margin_right),
            .height = height - (abuse_window_wayland_impl->margin_top + abuse_window_wayland_impl->margin_bottom),
        };
        return TRUE;
    } else {
        return FALSE;
    }
}

#define __WLE_ABUSE_GTK3_C__
#include <libwlembed-gtk3-visibility.c>
