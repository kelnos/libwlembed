/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_OUTPUT_H__
#define __WLE_OUTPUT_H__

#include <glib-object.h>
#include <wayland-client-protocol.h>
#include <wayland-server-core.h>

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleOutput, wle_output, WLE, OUTPUT, GObject)
#define WLE_TYPE_OUTPUT (wle_output_get_type())

WleOutput *wle_output_new(struct wl_output *remote_output,
                          struct wl_display *local_display);

struct wl_output *wle_output_get_remote_output(WleOutput *output);
GList *wle_output_get_local_outputs(WleOutput *output,
                                    struct wl_client *client);

G_END_DECLS

#endif /* __WLE_OUTPUT_H__ */
