/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_EMBEDDED_COMPOSITOR_H__
#define __WLE_EMBEDDED_COMPOSITOR_H__

#if !defined(__IN_LIBWLEMBED_H__) && !defined(LIBWLEMBED_COMPILATION)
#error "You may not include this header directly; instead, use <libwlembed/libwlembed.h>"
#endif

#include <glib-object.h>
#include <libwlembed/wle-embedded-view.h>
#include <libwlembed/wle-error.h>
#include <wayland-server.h>

struct xdg_surface;
struct xdg_toplevel;

G_DECLARE_FINAL_TYPE(WleEmbeddedCompositor, wle_embedded_compositor, WLE, EMBEDDED_COMPOSITOR, GObject)
#define WLE_TYPE_EMBEDDED_COMPOSITOR (wle_embedded_compositor_get_type())

WleEmbeddedCompositor *wle_embedded_compositor_new(const gchar *socket_name,
                                                   struct wl_display *remote_display,
                                                   GError **error);

const gchar *wle_embedded_compositor_get_socket_name(WleEmbeddedCompositor *ec);
struct wl_display *wle_embedded_compositor_get_local_display(WleEmbeddedCompositor *ec);

void wle_embedded_compositor_set_manage_child_processes(WleEmbeddedCompositor *ec,
                                                        gboolean manage_child_processes);

const gchar *wle_embedded_compositor_generate_embedding_token(WleEmbeddedCompositor *ec);

gboolean wle_embedded_compositor_spawn_with_pipes(WleEmbeddedCompositor *ec,
                                                  const gchar *working_directory,
                                                  gchar **argv,
                                                  gchar **envp,
                                                  GSpawnFlags flags,
                                                  GSpawnChildSetupFunc child_setup,
                                                  gpointer child_setup_user_data,
                                                  GPid *child_pid,
                                                  gint *standard_input,
                                                  gint *standard_output,
                                                  gint *standard_error,
                                                  GError **error);

gboolean wle_embedded_compositor_spawn(WleEmbeddedCompositor *ec,
                                       const gchar *working_directory,
                                       gchar **argv,
                                       gchar **envp,
                                       GSpawnFlags flags,
                                       GSpawnChildSetupFunc child_setup,
                                       gpointer child_setup_user_data,
                                       GPid *child_pid,
                                       GError **error);

gboolean wle_embedded_compositor_spawn_command_line(WleEmbeddedCompositor *ec,
                                                    const gchar *command_line,
                                                    GError **error);

GList *wle_embedded_compositor_list_seats(WleEmbeddedCompositor *ec);

void wle_embedded_compositor_fatal_error(WleEmbeddedCompositor *ec,
                                         GError *error);

#endif /* __WLE_EMBEDDED_COMPOSITOR_H__ */
