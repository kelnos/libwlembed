/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SECTION:wle-embedded-view
 * @title: WleEmbeddedView
 * @short_description: An embedded applciation window
 * @stability: Unstable
 * @include: libwlembed/libwlembed.h
 *
 * #WleEmbeddedView represents an instance of an embedded application
 * window that a subprocess has created.
 **/

#include "common/wle-debug.h"
#include "wle-compositor-bridge.h"
#include "wle-embedded-compositor-private.h"
#include "wle-embedded-view-private.h"
#include "wle-embedding-manager.h"
#include "wle-local-resource.h"
#include "wle-marshal.h"
#include "wle-seat-private.h"
#include "wle-seat.h"
#include "wle-toplevel.h"
#include "wle-view.h"
#include "libwlembed-visibility.h"

struct _WleEmbeddedView {
    GObject parent;

    WleEmbeddedCompositor *ec;
    WleCompositorBridge *bridge;
    WleView *root_view;

    struct wl_surface *parent_surface;

    gint width;
    gint height;

    gint last_requested_width;
    gint last_requested_height;

    gint last_set_width;
    gint last_set_height;
};

enum {
    SIG_SIZE_REQUEST,
    SIG_FOCUS_IN,
    SIG_FOCUS_OUT,
    SIG_FOCUS_NEXT,
    SIG_FOCUS_PREVIOUS,
    SIG_CLOSED,

    SIG_POINTER_ENTER,
    SIG_POINTER_LEAVE,
    SIG_POINTER_MOTION,
    SIG_POINTER_FRAME,

    N_SIGNALS,
};

static void wle_embedded_view_finalize(GObject *object);


G_DEFINE_FINAL_TYPE(WleEmbeddedView, wle_embedded_view, G_TYPE_OBJECT)


static guint signals[N_SIGNALS] = { 0 };

static void
wle_embedded_view_class_init(WleEmbeddedViewClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->finalize = wle_embedded_view_finalize;

    /**
     * WleEmbeddedView::size-request:
     * @view: A #WleEmbeddedView.
     * @width: The requested width.
     * @height: The requested height.
     *
     * Emitted when the embedded application wishes to set the size of its
     * drawing area.  The parent application should call @wle_embedded_view_set_size()
     * to either accept the requested size, or require the embedded application
     * to constrain itself to a different size.
     **/
    signals[SIG_SIZE_REQUEST] = g_signal_new("size-request",
                                             WLE_TYPE_EMBEDDED_VIEW,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             _wle_marshal_VOID__INT_INT,
                                             G_TYPE_NONE, 2,
                                             G_TYPE_INT, G_TYPE_INT);

    /**
     * WleEmbeddedView::focus-in:
     * @view: A #WleEmbeddedView.
     *
     * Emitted when the embedded application could receives focus, such as when
     * the user clicks inside the embedded application's drawing area.
     **/
    signals[SIG_FOCUS_IN] = g_signal_new("focus-in",
                                         WLE_TYPE_EMBEDDED_VIEW,
                                         G_SIGNAL_RUN_LAST,
                                         0,
                                         NULL, NULL,
                                         g_cclosure_marshal_VOID__VOID,
                                         G_TYPE_NONE, 0);

    /**
     * WleEmbeddedView::focus-out:
     * @view: A #WleEmbeddedView.
     *
     * Emitted when the embedded application should lose focus, such as when
     * the user clicks outside the embedded application's drawing area.
     **/
    signals[SIG_FOCUS_OUT] = g_signal_new("focus-out",
                                          WLE_TYPE_EMBEDDED_VIEW,
                                          G_SIGNAL_RUN_LAST,
                                          0,
                                          NULL, NULL,
                                          g_cclosure_marshal_VOID__VOID,
                                          G_TYPE_NONE, 0);

    /**
     * WleEmbeddedView::focus-next:
     * @view: A #WleEmbeddedView.
     * @serial: A number representing ordering and spacing of focus events.
     *
     * Emitted when the embedded application as focus, but the user has
     * requested (e.g. by pressing the TAB key) that focus be moved to the next
     * element, but there are no more elements inside the embedded application,
     * so focus should move to the next widget in the parent application.
     *
     * The @serial number can be used to detect focus-chain loops.
     **/
    signals[SIG_FOCUS_NEXT] = g_signal_new("focus-next",
                                           WLE_TYPE_EMBEDDED_VIEW,
                                           G_SIGNAL_RUN_LAST,
                                           0,
                                           NULL, NULL,
                                           g_cclosure_marshal_VOID__UINT,
                                           G_TYPE_NONE, 1,
                                           G_TYPE_UINT);

    /**
     * WleEmbeddedView::focus-previous:
     * @view: A #WleEmbeddedView.
     * @serial: A number representing ordering and spacing of focus events.
     *
     * Emitted when the embedded application as focus, but the user has
     * requested (e.g. by pressing the SHIFT-TAB keys) that focus be moved to
     * the previous element, but there are no more elements inside the embedded
     * application, so focus should move to the previous widget in the parent
     * application.
     *
     * The @serial number can be used to detect focus-chain loops.
     **/
    signals[SIG_FOCUS_PREVIOUS] = g_signal_new("focus-previous",
                                               WLE_TYPE_EMBEDDED_VIEW,
                                               G_SIGNAL_RUN_LAST,
                                               0,
                                               NULL, NULL,
                                               g_cclosure_marshal_VOID__UINT,
                                               G_TYPE_NONE, 1,
                                               G_TYPE_UINT);

    /**
     * WleEmbeddedView::closed:
     * @view: A #WleEmbeddedView.
     * @error: An error describing how the view closed, or %NULL if it closed
     *         normally.
     *
     * Emitted when the embedded application has quit, or has unmapped its
     * embedded toplevel surface.
     **/
    signals[SIG_CLOSED] = g_signal_new("closed",
                                       WLE_TYPE_EMBEDDED_VIEW,
                                       G_SIGNAL_RUN_LAST,
                                       0,
                                       NULL, NULL,
                                       g_cclosure_marshal_VOID__BOXED,
                                       G_TYPE_NONE, 1,
                                       G_TYPE_ERROR);

    signals[SIG_POINTER_ENTER] = g_signal_new("pointer-enter",
                                              WLE_TYPE_EMBEDDED_VIEW,
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL,
                                              NULL,
                                              _wle_marshal_VOID__OBJECT_UINT_DOUBLE_DOUBLE,
                                              G_TYPE_NONE,
                                              4,
                                              WLE_TYPE_SEAT,
                                              G_TYPE_UINT,
                                              G_TYPE_DOUBLE,
                                              G_TYPE_DOUBLE);

    signals[SIG_POINTER_LEAVE] = g_signal_new("pointer-leave",
                                              WLE_TYPE_EMBEDDED_VIEW,
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL,
                                              NULL,
                                              _wle_marshal_VOID__OBJECT_UINT,
                                              G_TYPE_NONE,
                                              2,
                                              WLE_TYPE_SEAT,
                                              G_TYPE_UINT);

    signals[SIG_POINTER_MOTION] = g_signal_new("pointer-motion",
                                               WLE_TYPE_EMBEDDED_VIEW,
                                               G_SIGNAL_RUN_LAST,
                                               0,
                                               NULL,
                                               NULL,
                                               _wle_marshal_VOID__UINT_DOUBLE_DOUBLE,
                                               G_TYPE_NONE,
                                               3,
                                               G_TYPE_UINT,
                                               G_TYPE_DOUBLE,
                                               G_TYPE_DOUBLE);

    signals[SIG_POINTER_FRAME] = g_signal_new("pointer-frame",
                                              WLE_TYPE_EMBEDDED_VIEW,
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL,
                                              NULL,
                                              g_cclosure_marshal_VOID__OBJECT,
                                              G_TYPE_NONE,
                                              1,
                                              WLE_TYPE_SEAT);
}

static void
wle_embedded_view_init(WleEmbeddedView *view) {}


static void
wle_embedded_view_finalize(GObject *object) {
    WleEmbeddedView *eview = WLE_EMBEDDED_VIEW(object);

    if (eview->root_view != NULL) {
        g_signal_handlers_disconnect_by_data(eview->root_view, eview);
    }

    G_OBJECT_CLASS(wle_embedded_view_parent_class)->finalize(object);
}

static void
focus_for_seat(WleEmbeddedView *view, WleSeat *seat) {
    TRACE("entering");
    uint32_t serial = wl_display_get_serial(wle_compositor_bridge_get_local_display(view->bridge));
    if (seat != NULL) {
        _wle_seat_synthesize_keyboard_enter(seat, view->root_view, serial);
    } else {
        for (GList *l = wle_compositor_bridge_get_seats(view->bridge); l != NULL; l = l->next) {
            _wle_seat_synthesize_keyboard_enter(WLE_SEAT(l->data), view->root_view, serial);
        }
    }
}

static void
unfocus_for_seat(WleEmbeddedView *view, WleSeat *seat) {
    TRACE("entering");
    uint32_t serial = wl_display_get_serial(wle_compositor_bridge_get_local_display(view->bridge));
    if (seat != NULL) {
        _wle_seat_synthesize_keyboard_leave(seat, view->root_view, serial);
    } else {
        for (GList *l = wle_compositor_bridge_get_seats(view->bridge); l != NULL; l = l->next) {
            _wle_seat_synthesize_keyboard_leave(WLE_SEAT(l->data), view->root_view, serial);
        }
    }
}

static void
root_view_min_size_changed(WleView *view, gint width, gint height, WleEmbeddedView *embedded_view) {
    _wle_embedded_view_size_request(embedded_view, width, height);
}

static void
root_view_destroyed(WleView *view, WleEmbeddedView *embedded_view) {
    _wle_embedded_view_closed(embedded_view, NULL);
}

static void
root_view_pointer_enter(WleEmbeddedView *embedded_view, WleSeat *seat, uint32_t serial, gdouble x, gdouble y) {
    g_signal_emit(embedded_view, signals[SIG_POINTER_ENTER], 0, seat, serial, x, y);
}

static void
root_view_pointer_leave(WleEmbeddedView *embedded_view, WleSeat *seat, uint32_t serial) {
    g_signal_emit(embedded_view, signals[SIG_POINTER_LEAVE], 0, seat, serial);
}

static void
root_view_pointer_motion(WleEmbeddedView *embedded_view, WleSeat *seat, uint32_t time, gdouble x, gdouble y) {
    g_signal_emit(embedded_view, signals[SIG_POINTER_MOTION], 0, seat, time, x, y);
}

static void
root_view_pointer_frame(WleEmbeddedView *embedded_view, WleSeat *seat) {
    g_signal_emit(embedded_view, signals[SIG_POINTER_FRAME], 0, seat);
}

WleEmbeddedView *
_wle_embedded_view_new(WleEmbeddedCompositor *ec, WleCompositorBridge *bridge, WleView *root_view) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), NULL);
    g_return_val_if_fail(WLE_IS_COMPOSITOR_BRIDGE(bridge), NULL);
    g_return_val_if_fail(WLE_IS_VIEW(root_view), NULL);

    WleEmbeddedView *view = g_object_new(WLE_TYPE_EMBEDDED_VIEW, NULL);
    view->ec = ec;
    view->bridge = bridge;
    view->root_view = root_view,

    g_signal_connect(view->root_view, "min-size-changed",
                     G_CALLBACK(root_view_min_size_changed), view);
    g_signal_connect(view->root_view, "destroy",
                     G_CALLBACK(root_view_destroyed), view);
    g_signal_connect_swapped(view->root_view, "pointer-enter",
                             G_CALLBACK(root_view_pointer_enter), view);
    g_signal_connect_swapped(view->root_view, "pointer-leave",
                             G_CALLBACK(root_view_pointer_leave), view);
    g_signal_connect_swapped(view->root_view, "pointer-motion",
                             G_CALLBACK(root_view_pointer_motion), view);
    g_signal_connect_swapped(view->root_view, "pointer-frame",
                             G_CALLBACK(root_view_pointer_frame), view);

    uint32_t serial = wl_display_next_serial(wle_compositor_bridge_get_local_display(bridge));
    _wle_view_set_size(root_view, serial, 0, 0);

    return view;
}

struct wl_surface *
_wle_embedded_view_get_parent_surface(WleEmbeddedView *embedded_view) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_VIEW(embedded_view), NULL);
    return embedded_view->parent_surface;
}

/**
 * wle_embedded_view_show:
 * @view: A #WleEmbeddedView.
 * @new_parent_surface: (not nullable): A #struct wl_surface.
 *
 * Shows @view, asking the compositor to draw the embedded view's contents into
 * the area described by @new_parent_surface.  @new_parent_surface may be a new
 * surface, or a previously-used surface.
 *
 * (Note that the view will not actually be drawn into
 * @new_remote_parent_surface; a wl_subsurface will be created with
 * @new_parent_surface as its parent.)
 **/
void
wle_embedded_view_show(WleEmbeddedView *view, struct wl_surface *new_parent_surface) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(new_parent_surface != NULL);
    g_return_if_fail(view->root_view != NULL);

    if (view->parent_surface != new_parent_surface) {
        view->parent_surface = new_parent_surface;
        _wle_view_embedder_shown(view->root_view, new_parent_surface);
        wle_toplevel_show(WLE_TOPLEVEL(wle_view_get_role(view->root_view)));

        if (view->last_set_width > 0 && view->last_set_height > 0) {
            uint32_t serial = wl_display_next_serial(wle_compositor_bridge_get_local_display(view->bridge));
            _wle_view_set_size(view->root_view, serial, view->last_set_width, view->last_set_height);
        }
    }
}

/**
 * wle_embedded_view_hide:
 * @view: A #WleEmbeddedView.
 *
 * Hides @view.  Resources associated with displaying view on the parent
 * compositor may be destroyed.
 **/
void
wle_embedded_view_hide(WleEmbeddedView *view) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));

    if (view->parent_surface != NULL) {
        view->parent_surface = NULL;

        if (view->root_view != NULL) {
            wle_toplevel_hide(WLE_TOPLEVEL(wle_view_get_role(view->root_view)));
            _wle_view_embedder_hidden(view->root_view);
        }
    }
}

/**
 * wle_embedded_view_get_parent_surface:
 * @view: A #WleEmbeddedView.
 *
 * Retrieves the current parent surface of @view, if any.
 *
 * Return value: (nullable) (transfer none): a #wl_surface or %NULL.
 **/
struct wl_surface *
wle_embedded_view_get_parent_surface(WleEmbeddedView *view) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_VIEW(view), NULL);
    return view->parent_surface;
}

/**
 * wle_embedded_view_set_toplevel_xdg_surface:
 * @view: A #WleEmbeddedView.
 * @toplevel_xdg_surface: (nullable): The XDG surface corresponding to the view
 *                                    surface on the remote compositor.
 * @toplevel_xdg_toplevel: (nullable): The XDG toplevel corresponding to the
 *                                     view surface on the remote compositor.
 *
 * Sets the XDG surface and XDG toplevel (if any) associated with the
 * parent/toplevel window that this @view is embedded in.
 *
 * Without the @xdg_surface, the embedded application will not be able to
 * create popups (like menus).
 *
 * Without the @xdg_toplevel, the embedded application will not be able to set
 * a parent on other toplevel windows that are forwarded to the remote
 * compositor.
 *
 * Setting a XDG surface on @view will unset any layer surface set.
 *
 * See also @wle_embedded_view_set_toplevel_layer_surface() if you are working with
 * layer surfaces instead of XDG surfaces.
 **/
void
wle_embedded_view_set_toplevel_xdg_surface(WleEmbeddedView *view,
                                           struct xdg_surface *toplevel_xdg_surface,
                                           struct xdg_toplevel *toplevel_xdg_toplevel) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));
    wle_view_set_embedded_xdg_toplevel(view->root_view, toplevel_xdg_surface, toplevel_xdg_toplevel);
}

/**
 * wle_embedded_view_set_toplevel_layer_surface:
 * @view: A #WleEmbeddedView.
 * @toplevel_layer_surface: (nullable): The layer surface corresponding to the
 *                                      view surface on the remote compositor.
 *
 * Sets the layer surface associated with the parent/toplevel window that this
 * @view is embedded in.
 *
 * Without the @layer_surface, the embedded application will not be able to
 * create popups.
 *
 * Note that, with a layer surface as the parent, the embedded application will
 * be unable to set a parent for other toplevel windows that are forwarded to
 * the remote compositor.
 *
 * Setting a layer surface on @view will unset any XDG surface set.
 *
 * See also @wle_embedded_view_set_toplevel_xdg_surface() if you are working with XDG
 * surfaces instead of layer surfaces.
 **/
void
wle_embedded_view_set_toplevel_layer_surface(WleEmbeddedView *view,
                                             struct zwlr_layer_surface_v1 *toplevel_layer_surface) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));
    wle_view_set_embedded_layer_toplevel(view->root_view, toplevel_layer_surface);
}

/**
 * wle_embedded_view_set_size:
 * @view: A #WleEmbeddedView.
 * @width: The width.
 * @height: The height.
 *
 * Forces the size of the embedded appliction's drawing area to be @width x
 * @height.
 *
 * The values should be in scaled application units, not device pixels.
 **/
void
wle_embedded_view_set_size(WleEmbeddedView *view, gint width, gint height) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));
    g_return_if_fail(width > 0 || width == -1);
    g_return_if_fail(height > 0 || height == -1);

    if (view->last_set_width != width || view->last_set_height != height) {
        view->last_set_width = width;
        view->last_set_height = height;

        if (view->parent_surface != NULL) {
            uint32_t serial = wl_display_next_serial(wle_compositor_bridge_get_local_display(view->bridge));
            _wle_view_set_size(view->root_view, serial, width, height);
        }
    }
}

/**
 * wle_embedded_view_get_size:
 * @view: A #WleEmbeddedView.
 * @width: (out) (optional): A location to store the width.
 * @height: (out) (optional): A location to store the height.
 *
 * Retrieves the actual size of the embedded application's drawing area.
 *
 * The values returned are in scaled application units, not device pixels.
 **/
void
wle_embedded_view_get_size(WleEmbeddedView *view, gint *width, gint *height) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));

    if (width != NULL) {
        *width = view->width;
    }
    if (height != NULL) {
        *height = view->height;
    }
}

/**
 * wle_embedded_view_move:
 * @view: A #WleEmbeddedView.
 * @offset_x: The x offset inside the view surface where the embedded
 *            application window is located.
 * @offset_y: The y offset inside the view surface where the embedded
 *            application window is located.
 *
 * Informs @view that it has been moved inside the surface in which it is
 * embedded.
 **/
void
wle_embedded_view_move(WleEmbeddedView *view, gint offset_x, gint offset_y) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));
    wle_view_set_embedded_offset(view->root_view, offset_x, offset_y);
}

/**
 * wle_embedded_view_set_toplevel_geometry:
 * @view: A #WleEmbeddedView.
 * @toplevel_x: The x offset into the wineow where @view is embedded.
 * @toplevel_y: The y offset into the wineow where @view is embedded.
 * @toplevel_width: The width of the window where @view is embedded.
 * @toplevel_height: The heigh of the window where @view is embedded.
 *
 * Informs @view of the geometry of the window it is embedded in.  This
 * geometry recangle should only include the user-visible portion of the
 * window; e.g. it should exclude space reserved for drop shadows.
 *
 * This is used to position popups that are positioned relative to
 * parts of the embedded application window.
 **/
void
wle_embedded_view_set_toplevel_geometry(WleEmbeddedView *view,
                                        gint toplevel_x,
                                        gint toplevel_y,
                                        gint toplevel_width,
                                        gint toplevel_height) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));

    WleViewRole *view_role = wle_view_get_role(view->root_view);
    if (WLE_IS_XDG_SURFACE(view_role)) {
        wle_xdg_surface_set_window_geometry(WLE_XDG_SURFACE(view_role),
                                            toplevel_x,
                                            toplevel_y,
                                            toplevel_width,
                                            toplevel_height);
    }
}

/**
 * wle_embedded_view_toplevel_activated:
 * @view: A #WleEmbeddedView.
 * @seat: (nullable): A #WleSeat.
 *
 * Informs the embedded view that the toplevel it is embedded in has been
 * activated for @seat.
 *
 * If the seat is unknown, pass %NULL.
 **/
void
wle_embedded_view_toplevel_activated(WleEmbeddedView *view, WleSeat *seat) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));
    TRACE("entering, embed=%p, root_view=%p, seat=%p", view, view->root_view, seat);

    uint32_t serial = wl_display_next_serial(wle_compositor_bridge_get_local_display(view->bridge));
    WleViewRole *role = wle_view_get_role(view->root_view);
    if (WLE_IS_TOPLEVEL(role)) {
        wle_toplevel_activate(WLE_TOPLEVEL(role), serial);
    }
}

/**
 * wle_embedded_view_toplevel_deactivated:
 * @view: A #WleEmbeddedView.
 * @seat: (nullable): A #WleSeat.
 *
 * Informs the embedded view that the toplevel it is embedded in has been
 * deactivated for @seat.
 *
 * If the seat is unknown, pass %NULL.
 **/
void
wle_embedded_view_toplevel_deactivated(WleEmbeddedView *view, WleSeat *seat) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));
    TRACE("entering, embed=%p, root_view=%p, seat=%p", view, view->root_view, seat);

    uint32_t serial = wl_display_next_serial(wle_compositor_bridge_get_local_display(view->bridge));
    WleViewRole *role = wle_view_get_role(view->root_view);
    if (WLE_IS_TOPLEVEL(role)) {
        wle_toplevel_deactivate(WLE_TOPLEVEL(role), serial);
    }
}

/**
 * wle_embedded_view_focus:
 * @view: A #WleEmbeddedView.
 * @seat: A #WleSeat.
 *
 * Requests that the compositor give keyboard focus to @view on @seat.
 *
 * This is usually used to forward focus-in events from the parent container to
 * the embedded application.  An example might be when the view location is in
 * a the &lt;tab&gt; focus-chain, and the user has moved focus from a previous
 * or next widget in the focus chain.
 *
 * If %NULL is passed for @seat, focus will be set on all known seats.  Note
 * that, since the #WleSeat's underlying #wl_seat is private to #libwlembed, a
 * #wl_seat instance known to the application will not "match" with any
 * #wl_seat instance known to #libwlembed.  In order to match your seat
 * instance to a #WleSeat instance, you will have to use some other means, such
 * as comparing the seat's name, or the order in which it was discovered from
 * the compositor.
 **/
void
wle_embedded_view_focus(WleEmbeddedView *view, WleSeat *seat) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));

    if (seat != NULL) {
        focus_for_seat(view, seat);
    } else {
        for (GList *l = wle_compositor_bridge_get_seats(view->bridge); l != NULL; l = l->next) {
            focus_for_seat(view, WLE_SEAT(l->data));
        }
    }

    _wle_embedded_view_focus_in(view);
}

/**
 * wle_embedded_view_unfocus:
 * @view: A #WleEmbeddedView.
 * @seat: A #WleSeat.
 *
 * Requests that the compositor remove keyboard focus from @view on @seat.
 *
 * This is often used to forward focus-out events from the parent countainer to
 * the embedded application.  An example might be when the user clicks on
 * another UI element outside the embedded application.
 *
 * If %NULL is passed for @seat, focus will be removed from all known seats.
 * Note that, since the #WleSeat's underlying #wl_seat is private to
 * #libwlembed, a #wl_seat instance known to the application will not "match"
 * with any #wl_seat instance known to #libwlembed.  In order to match your
 * seat instance to a #WleSeat instance, you will have to use some other means,
 * such as comparing the seat's name, or the order in which it was discovered
 * from the compositor.
 **/
void
wle_embedded_view_unfocus(WleEmbeddedView *view, WleSeat *seat) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_return_if_fail(WLE_IS_VIEW(view->root_view));

    if (seat != NULL) {
        unfocus_for_seat(view, seat);
    } else {
        for (GList *l = wle_compositor_bridge_get_seats(view->bridge); l != NULL; l = l->next) {
            unfocus_for_seat(view, WLE_SEAT(l->data));
        }
    }

    _wle_embedded_view_focus_out(view);
}

/**
 * wle_embedded_view_focus_first:
 * @view: A #WleEmbeddedView.
 *
 * Requests that the embedded view focus the first widget in its focus chain.
 *
 * Return value: A serial number that must be passed to future focus requests,
 * and can be used by the compositor to detect infinite focus loops.
 **/
guint32
wle_embedded_view_focus_first(WleEmbeddedView *view) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_VIEW(view), 0);
    g_return_val_if_fail(WLE_IS_VIEW(view->root_view), 0);

    uint32_t serial = wl_display_next_serial(wle_compositor_bridge_get_local_display(view->bridge));
    _wle_embedding_manager_view_focus_first(_wle_embedded_compositor_get_embedding_manager(view->ec),
                                            view->root_view,
                                            serial);
    return serial;
}

/**
 * wle_embedded_view_focus_last:
 * @view: A #WleEmbeddedView.
 *
 * Requests that the embedded view focus the last widget in its focus chain.
 *
 * Return value: A serial number that must be passed to future focus requests,
 * and can be used by the compositor to detect infinite focus loops.
 **/
guint32
wle_embedded_view_focus_last(WleEmbeddedView *view) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_VIEW(view), 0);
    g_return_val_if_fail(WLE_IS_VIEW(view->root_view), 0);

    uint32_t serial = wl_display_next_serial(wle_compositor_bridge_get_local_display(view->bridge));
    _wle_embedding_manager_view_focus_last(_wle_embedded_compositor_get_embedding_manager(view->ec),
                                           view->root_view,
                                           serial);
    return serial;
}

/**
 * wle_embedded_view_destroy:
 * @eview: A #WleEmbeddedView.
 *
 * Destroys the embedded view.
 **/
void
wle_embedded_view_destroy(WleEmbeddedView *eview) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(eview));

    if (eview->root_view != NULL) {
        wle_local_resource_destroy(WLE_LOCAL_RESOURCE(eview->root_view));
    }
}

WleView *
_wle_embedded_view_get_root_view(WleEmbeddedView *embedded_view) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_VIEW(embedded_view), NULL);
    return embedded_view->root_view;
}

void
_wle_embedded_view_size_request(WleEmbeddedView *view, gint width, gint height) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));

    if (view->last_requested_height != width || view->last_requested_height != height) {
        view->last_requested_width = width;
        view->last_requested_height = height;

        g_signal_emit(view, signals[SIG_SIZE_REQUEST], 0, width, height);
    }
}

void
_wle_embedded_view_focus_in(WleEmbeddedView *view) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_signal_emit(view, signals[SIG_FOCUS_IN], 0);
}

void
_wle_embedded_view_focus_out(WleEmbeddedView *view) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    g_signal_emit(view, signals[SIG_FOCUS_OUT], 0);
}

void
_wle_embedded_view_focus_next(WleEmbeddedView *embedded_view, uint32_t serial) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(embedded_view));
    g_signal_emit(embedded_view, signals[SIG_FOCUS_NEXT], 0, serial);
}

void
_wle_embedded_view_focus_previous(WleEmbeddedView *embedded_view, uint32_t serial) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(embedded_view));
    g_signal_emit(embedded_view, signals[SIG_FOCUS_PREVIOUS], 0, serial);
}

void
_wle_embedded_view_closed(WleEmbeddedView *view, GError *error) {
    g_return_if_fail(WLE_IS_EMBEDDED_VIEW(view));
    if (view->root_view != NULL) {
        g_signal_handlers_disconnect_by_data(view->root_view, view);
        view->root_view = NULL;
    }
    g_signal_emit(view, signals[SIG_CLOSED], 0, error);
}

#define __WLE_EMBEDDED_VIEW_C__
#include <libwlembed-visibility.c>
