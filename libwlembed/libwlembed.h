/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __LIBWLEMBED_H__
#define __LIBWLEMBED_H__

#define __IN_LIBWLEMBED_H__

#include <libwlembed/wle-embedded-compositor.h>
#include <libwlembed/wle-embedded-view.h>
#include <libwlembed/wle-error.h>
#include <libwlembed/wle-seat.h>

#undef __IN_LIBWLEMBED_H__

#endif /* __LIBWLEMBED_H__ */
