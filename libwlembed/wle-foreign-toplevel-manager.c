/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <wayland-server-core.h>
#include <wayland-server.h>
#include <wayland-util.h>

// The server protocol header must be included before the client protocol
// header, otherwise we lose the definition of
// zwlr_foreign_toplevel_handle_v1_state_is_valid().
// clang-format off
#include "protocol/wlr-foreign-toplevel-management-unstable-v1-protocol.h"
#include "protocol/wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"
// clang-format on
#include "wle-foreign-toplevel-manager.h"
#include "wle-output.h"
#include "wle-seat-private.h"
#include "wle-seat.h"
#include "wle-util.h"
#include "wle-view.h"

struct _WleForeignToplevelManager {
    struct wl_display *remote_display;
    struct zwlr_foreign_toplevel_manager_v1 *remote_manager;

    struct wl_global *local_manager;
    struct wl_list local_manager_resources;  // wl_resource.link
    struct wl_list local_manager_stopped_resources;  // wl_resource.link

    struct wl_list toplevels;  // WleForeignToplevelHandle.link
};

typedef struct _WleForeignToplevelHandle WleForeignToplevelHandle;
struct _WleForeignToplevelHandle {
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel;
    struct wl_list local_toplevel_holders;  // WleForeignToplevelHandleResourceHolder.link

    char *app_id;
    char *title;
    struct wl_array state;
    WleForeignToplevelHandle *parent;
    GList *outputs;  // WleOutput

    struct wl_list link;
};

typedef struct {
    struct wl_resource *local_toplevel_manager;
    WleForeignToplevelHandle *toplevel;
    struct wl_resource *local_toplevel;
    struct wl_list link;
} WleForeignToplevelHandleResourceHolder;

static void remote_toplevel_title(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    const char *title);
static void remote_toplevel_app_id(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    const char *app_id);
static void remote_toplevel_output_enter(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct wl_output *remote_output);
static void remote_toplevel_output_leave(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct wl_output *remote_output);
static void remote_toplevel_state(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct wl_array *state);
static void remote_toplevel_done(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel);
static void remote_toplevel_closed(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel);
static void remote_toplevel_parent(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct zwlr_foreign_toplevel_handle_v1 *remote_parent /* nullable */);

static void local_toplevel_set_maximized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource);
static void local_toplevel_unset_maximized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource);
static void local_toplevel_set_minimized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource);
static void local_toplevel_unset_minimized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource);
static void local_toplevel_activate(
    struct wl_client *client,
    struct wl_resource *toplevel_resource,
    struct wl_resource *seat_resource);
static void local_toplevel_close(
    struct wl_client *client,
    struct wl_resource *toplevel_resource);
static void local_toplevel_set_rectangle(
    struct wl_client *client,
    struct wl_resource *toplevel_resource,
    struct wl_resource *surface_resource,
    int32_t x,
    int32_t y,
    int32_t width,
    int32_t height);
static void local_toplevel_destroy(
    struct wl_client *client,
    struct wl_resource *toplevel_resource);
static void local_toplevel_set_fullscreen(
    struct wl_client *client,
    struct wl_resource *toplevel_resource,
    struct wl_resource *output_resource /* nullable */);
static void local_toplevel_unset_fullscreen(
    struct wl_client *client,
    struct wl_resource *toplevel_resource);

static void remote_manager_toplevel(
    void *data,
    struct zwlr_foreign_toplevel_manager_v1 *remote_manager,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel);
static void remote_manager_finished(
    void *data,
    struct zwlr_foreign_toplevel_manager_v1 *remote_manager);

static void local_manager_stop(
    struct wl_client *client,
    struct wl_resource *manager_resource);

#ifndef HAVE_SCANNER_IS_VALID_FUNCTIONS
static inline gboolean zwlr_foreign_toplevel_handle_v1_state_is_valid(uint32_t value,
                                                                      uint32_t version);
#endif

static const struct zwlr_foreign_toplevel_handle_v1_listener remote_toplevel_listener = {
    .title = remote_toplevel_title,
    .app_id = remote_toplevel_app_id,
    .output_enter = remote_toplevel_output_enter,
    .output_leave = remote_toplevel_output_leave,
    .state = remote_toplevel_state,
    .done = remote_toplevel_done,
    .closed = remote_toplevel_closed,
    .parent = remote_toplevel_parent,
};

static const struct zwlr_foreign_toplevel_handle_v1_interface local_toplevel_impl = {
    .set_maximized = local_toplevel_set_maximized,
    .unset_maximized = local_toplevel_unset_maximized,
    .set_minimized = local_toplevel_set_minimized,
    .unset_minimized = local_toplevel_unset_minimized,
    .activate = local_toplevel_activate,
    .close = local_toplevel_close,
    .set_rectangle = local_toplevel_set_rectangle,
    .destroy = local_toplevel_destroy,
    .set_fullscreen = local_toplevel_set_fullscreen,
    .unset_fullscreen = local_toplevel_unset_fullscreen,
};

static const struct zwlr_foreign_toplevel_manager_v1_listener remote_manager_listener = {
    .toplevel = remote_manager_toplevel,
    .finished = remote_manager_finished,
};

static const struct zwlr_foreign_toplevel_manager_v1_interface local_manager_impl = {
    .stop = local_manager_stop,
};

static void
remote_toplevel_title(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    const char *title) {
    WleForeignToplevelHandle *toplevel = data;

    g_free(toplevel->title);
    toplevel->title = g_strdup(title);

    WleForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
        if (toplevel_holder->local_toplevel_manager != NULL) {
            zwlr_foreign_toplevel_handle_v1_send_title(toplevel_holder->local_toplevel, title);
        }
    }
}

static void
remote_toplevel_app_id(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    const char *app_id) {
    WleForeignToplevelHandle *toplevel = data;

    g_free(toplevel->app_id);
    toplevel->app_id = g_strdup(app_id);

    WleForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
        if (toplevel_holder->local_toplevel_manager != NULL) {
            zwlr_foreign_toplevel_handle_v1_send_app_id(toplevel_holder->local_toplevel, app_id);
        }
    }
}

static void
remote_toplevel_output_enter(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct wl_output *remote_output) {
    WleForeignToplevelHandle *toplevel = data;
    WleOutput *output = _wle_proxy_get_user_data((struct wl_proxy *)remote_toplevel);

    if (WLE_IS_OUTPUT(output)) {
        if (g_list_find(toplevel->outputs, output) == NULL) {
            toplevel->outputs = g_list_append(toplevel->outputs, output);

            WleForeignToplevelHandleResourceHolder *toplevel_holder;
            wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
                if (toplevel_holder->local_toplevel_manager != NULL) {
                    GList *local_outputs = wle_output_get_local_outputs(output,
                                                                        wl_resource_get_client(toplevel_holder->local_toplevel));
                    for (GList *l = local_outputs; l != NULL; l = l->next) {
                        struct wl_resource *local_output = l->data;
                        zwlr_foreign_toplevel_handle_v1_send_output_enter(toplevel_holder->local_toplevel, local_output);
                    }
                    g_list_free(local_outputs);
                }
            }
        }
    }
}

static void
remote_toplevel_output_leave(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct wl_output *remote_output) {
    WleForeignToplevelHandle *toplevel = data;
    WleOutput *output = _wle_proxy_get_user_data((struct wl_proxy *)remote_toplevel);

    if (WLE_IS_OUTPUT(output)) {
        GList *link = g_list_find(toplevel->outputs, output);
        if (link != NULL) {
            toplevel->outputs = g_list_delete_link(toplevel->outputs, link);

            WleForeignToplevelHandleResourceHolder *toplevel_holder;
            wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
                if (toplevel_holder->local_toplevel_manager != NULL) {
                    GList *local_outputs = wle_output_get_local_outputs(output,
                                                                        wl_resource_get_client(toplevel_holder->local_toplevel));
                    for (GList *l = local_outputs; l != NULL; l = l->next) {
                        struct wl_resource *local_output = l->data;
                        zwlr_foreign_toplevel_handle_v1_send_output_leave(toplevel_holder->local_toplevel, local_output);
                    }
                    g_list_free(local_outputs);
                }
            }
        }
    }
}

static void
filter_states_for_client(struct wl_resource *toplevel_resource, struct wl_array *dest, struct wl_array *src) {
    uint32_t resource_version = wl_resource_get_version(toplevel_resource);

    wl_array_init(dest);
    enum zwlr_foreign_toplevel_handle_v1_state *state_elem;
    wl_array_for_each(state_elem, src) {
        if (zwlr_foreign_toplevel_handle_v1_state_is_valid(*state_elem, resource_version)) {
            enum zwlr_foreign_toplevel_handle_v1_state *new_state_elem = _wle_assert_nonnull(wl_array_add(dest, sizeof(*state_elem)));
            *new_state_elem = *state_elem;
        }
    }
}

static void
remote_toplevel_state(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct wl_array *state) {
    WleForeignToplevelHandle *toplevel = data;

    wl_array_release(&toplevel->state);
    wl_array_init(&toplevel->state);
    wl_array_copy(&toplevel->state, state);

    WleForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
        if (toplevel_holder->local_toplevel_manager != NULL) {
            struct wl_array state_filtered;
            filter_states_for_client(toplevel_holder->local_toplevel, &state_filtered, state);
            zwlr_foreign_toplevel_handle_v1_send_state(toplevel_holder->local_toplevel, &state_filtered);
            wl_array_release(&state_filtered);
        }
    }
}

static void
remote_toplevel_done(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel) {
    WleForeignToplevelHandle *toplevel = data;

    WleForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
        if (toplevel_holder->local_toplevel_manager != NULL) {
            zwlr_foreign_toplevel_handle_v1_send_done(toplevel_holder->local_toplevel);
        }
    }
}

static void
remote_toplevel_closed(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel) {
    WleForeignToplevelHandle *toplevel = data;

    WleForeignToplevelHandleResourceHolder *toplevel_holder;
    wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
        if (toplevel_holder->local_toplevel_manager != NULL) {
            zwlr_foreign_toplevel_handle_v1_send_closed(toplevel_holder->local_toplevel);
        }
    }

    zwlr_foreign_toplevel_handle_v1_destroy(toplevel->remote_toplevel);
    toplevel->remote_toplevel = NULL;
}

static void
remote_toplevel_parent(
    void *data,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel,
    struct zwlr_foreign_toplevel_handle_v1 *remote_parent /* nullable */
) {
    WleForeignToplevelHandle *toplevel = data;
    WleForeignToplevelHandle *parent = _wle_proxy_get_user_data((struct wl_proxy *)remote_parent);

    if ((remote_parent != NULL && parent != NULL) || (remote_parent == NULL && parent == NULL)) {
        toplevel->parent = parent;

        WleForeignToplevelHandleResourceHolder *toplevel_holder;
        wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
            if (toplevel_holder->local_toplevel_manager != NULL) {
                if (wl_resource_get_version(toplevel_holder->local_toplevel) >= ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_PARENT_SINCE_VERSION) {
                    if (parent != NULL) {
                        struct wl_client *client = wl_resource_get_client(toplevel_holder->local_toplevel);
                        WleForeignToplevelHandleResourceHolder *parent_toplevel_holder;
                        wl_list_for_each(parent_toplevel_holder, &parent->local_toplevel_holders, link) {
                            if (parent_toplevel_holder->local_toplevel_manager != NULL
                                && client == wl_resource_get_client(parent_toplevel_holder->local_toplevel))
                            {
                                zwlr_foreign_toplevel_handle_v1_send_parent(toplevel_holder->local_toplevel,
                                                                            parent_toplevel_holder->local_toplevel);
                            }
                        }
                    } else {
                        zwlr_foreign_toplevel_handle_v1_send_parent(toplevel_holder->local_toplevel, NULL);
                    }
                }
            }
        }
    }
}

static void
local_toplevel_set_maximized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        zwlr_foreign_toplevel_handle_v1_set_maximized(toplevel->remote_toplevel);
    }
}

static void
local_toplevel_unset_maximized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        zwlr_foreign_toplevel_handle_v1_unset_maximized(toplevel->remote_toplevel);
    }
}

static void
local_toplevel_set_minimized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        zwlr_foreign_toplevel_handle_v1_set_minimized(toplevel->remote_toplevel);
    }
}

static void
local_toplevel_unset_minimized(
    struct wl_client *client,
    struct wl_resource *toplevel_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        zwlr_foreign_toplevel_handle_v1_unset_minimized(toplevel->remote_toplevel);
    }
}

static void
local_toplevel_activate(
    struct wl_client *client,
    struct wl_resource *toplevel_resource,
    struct wl_resource *seat_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        WleSeat *seat = seat_resource != NULL
                            ? WLE_SEAT(wl_resource_get_user_data(seat_resource))
                            : NULL;

        if (seat != NULL) {
            zwlr_foreign_toplevel_handle_v1_activate(toplevel->remote_toplevel, _wle_seat_get_remote_seat(seat));
        }
    }
}

static void
local_toplevel_close(
    struct wl_client *client,
    struct wl_resource *toplevel_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        zwlr_foreign_toplevel_handle_v1_close(toplevel->remote_toplevel);
    }
}

static void
local_toplevel_set_rectangle(
    struct wl_client *client,
    struct wl_resource *toplevel_resource,
    struct wl_resource *surface_resource,
    int32_t x,
    int32_t y,
    int32_t width,
    int32_t height) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        WleView *view = surface_resource != NULL
                            ? WLE_VIEW(wl_resource_get_user_data(surface_resource))
                            : NULL;

        if (view != NULL) {
            zwlr_foreign_toplevel_handle_v1_set_rectangle(toplevel->remote_toplevel,
                                                          wle_view_get_remote_surface(view),
                                                          x,
                                                          y,
                                                          width,
                                                          height);
        }
    }
}

static void
local_toplevel_destroy(
    struct wl_client *client,
    struct wl_resource *toplevel_resource) {
    wl_resource_destroy(toplevel_resource);
}

static void
local_toplevel_set_fullscreen(
    struct wl_client *client,
    struct wl_resource *toplevel_resource,
    struct wl_resource *output_resource /* nullable */
) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        WleOutput *output = output_resource != NULL
                                ? WLE_OUTPUT(wl_resource_get_user_data(output_resource))
                                : NULL;

        if (output != NULL) {
            zwlr_foreign_toplevel_handle_v1_set_fullscreen(toplevel->remote_toplevel, wle_output_get_remote_output(output));
        }
    }
}

static void
local_toplevel_unset_fullscreen(
    struct wl_client *client,
    struct wl_resource *toplevel_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;
    if (toplevel->remote_toplevel != NULL) {
        if (toplevel->remote_toplevel != NULL) {
            zwlr_foreign_toplevel_handle_v1_unset_fullscreen(toplevel->remote_toplevel);
        }
    }
}

static void
local_toplevel_destroy_impl(struct wl_resource *toplevel_resource) {
    WleForeignToplevelHandleResourceHolder *toplevel_holder = wl_resource_get_user_data(toplevel_resource);
    WleForeignToplevelHandle *toplevel = toplevel_holder->toplevel;

    wl_list_remove(&toplevel_holder->link);
    g_free(toplevel_holder);

    if (toplevel->remote_toplevel == NULL && wl_list_length(&toplevel->local_toplevel_holders) == 0) {
        wl_list_remove(&toplevel->link);
        g_free(toplevel->app_id);
        g_free(toplevel->title);
        wl_array_release(&toplevel->state);
        g_list_free(toplevel->outputs);
        g_free(toplevel);
    }
}

static void
remote_manager_toplevel(
    void *data,
    struct zwlr_foreign_toplevel_manager_v1 *remote_manager,
    struct zwlr_foreign_toplevel_handle_v1 *remote_toplevel) {
    WleForeignToplevelManager *manager = data;

    WleForeignToplevelHandle *toplevel = g_new0(WleForeignToplevelHandle, 1);
    toplevel->remote_toplevel = _wle_proxy_set_ours((struct wl_proxy *)remote_toplevel);
    wl_array_init(&toplevel->state);
    wl_list_init(&toplevel->local_toplevel_holders);
    wl_list_insert(&manager->toplevels, &toplevel->link);

    struct wl_resource *manager_resource;
    wl_resource_for_each(manager_resource, &manager->local_manager_resources) {
        struct wl_resource *toplevel_resource = wl_resource_create(wl_resource_get_client(manager_resource),
                                                                   &zwlr_foreign_toplevel_handle_v1_interface,
                                                                   wl_resource_get_version(manager_resource),
                                                                   0);
        if (toplevel_resource != NULL) {
            WleForeignToplevelHandleResourceHolder *toplevel_holder = g_new0(WleForeignToplevelHandleResourceHolder, 1);
            toplevel_holder->local_toplevel_manager = manager_resource;
            toplevel_holder->toplevel = toplevel;
            toplevel_holder->local_toplevel = toplevel_resource;
            wl_list_insert(&toplevel->local_toplevel_holders, &toplevel_holder->link);

            wl_resource_set_implementation(toplevel_resource,
                                           &local_toplevel_impl,
                                           toplevel_holder,
                                           local_toplevel_destroy_impl);
            zwlr_foreign_toplevel_manager_v1_send_toplevel(manager_resource, toplevel_resource);
        }
    }

    zwlr_foreign_toplevel_handle_v1_add_listener(remote_toplevel, &remote_toplevel_listener, toplevel);
}

static void
remote_manager_finished(
    void *data,
    struct zwlr_foreign_toplevel_manager_v1 *remote_manager) {
    WleForeignToplevelManager *manager = data;

    struct wl_resource *resource;
    wl_resource_for_each(resource, &manager->local_manager_resources) {
        zwlr_foreign_toplevel_manager_v1_send_finished(resource);
    }
}

static void
local_manager_stop(
    struct wl_client *client,
    struct wl_resource *manager_resource) {
    WleForeignToplevelManager *manager = wl_resource_get_user_data(manager_resource);
    wl_list_remove(wl_resource_get_link(manager_resource));
    wl_list_insert(&manager->local_manager_stopped_resources, wl_resource_get_link(manager_resource));

    WleForeignToplevelHandle *toplevel;
    wl_list_for_each(toplevel, &manager->toplevels, link) {
        WleForeignToplevelHandleResourceHolder *toplevel_holder;
        wl_list_for_each(toplevel_holder, &toplevel->local_toplevel_holders, link) {
            if (toplevel_holder->local_toplevel_manager == manager_resource) {
                toplevel_holder->local_toplevel_manager = NULL;
            }
        }
    }
}

static void
local_manager_destroy_impl(struct wl_resource *manager_resource) {
    wl_list_remove(wl_resource_get_link(manager_resource));
}

static void
local_manager_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleForeignToplevelManager *manager = data;

    struct wl_resource *resource = wl_resource_create(client,
                                                      &zwlr_foreign_toplevel_manager_v1_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource, &local_manager_impl, manager, local_manager_destroy_impl);
        wl_list_insert(&manager->local_manager_resources, wl_resource_get_link(resource));

        WleForeignToplevelHandle *toplevel;
        wl_list_for_each(toplevel, &manager->toplevels, link) {
            if (toplevel->remote_toplevel != NULL) {
                struct wl_resource *toplevel_resource = wl_resource_create(wl_resource_get_client(resource),
                                                                           &zwlr_foreign_toplevel_handle_v1_interface,
                                                                           version,
                                                                           0);
                if (toplevel_resource != NULL) {
                    WleForeignToplevelHandleResourceHolder *toplevel_holder = g_new0(WleForeignToplevelHandleResourceHolder, 1);
                    toplevel_holder->local_toplevel_manager = resource;
                    toplevel_holder->toplevel = toplevel;
                    toplevel_holder->local_toplevel = toplevel_resource;
                    wl_list_insert(&toplevel->local_toplevel_holders, &toplevel_holder->link);

                    wl_resource_set_implementation(toplevel_resource,
                                                   &local_toplevel_impl,
                                                   toplevel_holder,
                                                   local_toplevel_destroy_impl);
                    zwlr_foreign_toplevel_manager_v1_send_toplevel(resource, toplevel_resource);

                    if (toplevel->app_id != NULL) {
                        zwlr_foreign_toplevel_handle_v1_send_app_id(toplevel_resource, toplevel->app_id);
                    }

                    if (toplevel->title != NULL) {
                        zwlr_foreign_toplevel_handle_v1_send_title(toplevel_resource, toplevel->title);
                    }

                    if (version >= ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_FULLSCREEN_SINCE_VERSION) {
                        zwlr_foreign_toplevel_handle_v1_send_state(toplevel_resource, &toplevel->state);
                    } else {
                        struct wl_array state;
                        filter_states_for_client(toplevel_resource, &state, &toplevel->state);
                        zwlr_foreign_toplevel_handle_v1_send_state(toplevel_resource, &state);
                        wl_array_release(&state);
                    }

                    for (GList *lo = toplevel->outputs; lo != NULL; lo = lo->next) {
                        WleOutput *output = WLE_OUTPUT(lo->data);
                        GList *output_resources = wle_output_get_local_outputs(output, client);
                        for (GList *lr = output_resources; lr != NULL; lr = lr->next) {
                            struct wl_resource *output_resource = lr->data;
                            zwlr_foreign_toplevel_handle_v1_send_output_enter(toplevel_resource, output_resource);
                        }
                    }

                    if (toplevel->parent != NULL && version >= ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_PARENT_SINCE_VERSION) {
                        WleForeignToplevelHandleResourceHolder *parent_toplevel_holder;
                        wl_list_for_each(parent_toplevel_holder, &toplevel->parent->local_toplevel_holders, link) {
                            if (parent_toplevel_holder->local_toplevel_manager != NULL
                                && client == wl_resource_get_client(parent_toplevel_holder->local_toplevel))
                            {
                                zwlr_foreign_toplevel_handle_v1_send_parent(toplevel_resource,
                                                                            parent_toplevel_holder->local_toplevel);
                            }
                        }
                    }

                    zwlr_foreign_toplevel_handle_v1_send_done(toplevel_resource);
                }
            }
        }

        zwlr_foreign_toplevel_manager_v1_send_finished(resource);
    }
}

#ifndef HAVE_SCANNER_IS_VALID_FUNCTIONS
static inline gboolean
zwlr_foreign_toplevel_handle_v1_state_is_valid(uint32_t value, uint32_t version) {
    switch (value) {
        case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_MAXIMIZED:
            return version >= 1;
        case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_MINIMIZED:
            return version >= 1;
        case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_ACTIVATED:
            return version >= 1;
        case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_FULLSCREEN:
            return version >= 2;
        default:
            return FALSE;
    }
}
#endif

WleForeignToplevelManager *
wle_foreign_toplevel_manager_new(struct wl_display *remote_display,
                                 struct zwlr_foreign_toplevel_manager_v1 *remote_foreign_toplevel_manager,
                                 struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_foreign_toplevel_manager != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleForeignToplevelManager *manager = g_new0(WleForeignToplevelManager, 1);
    manager->local_manager = wl_global_create(local_display,
                                              &zwlr_foreign_toplevel_manager_v1_interface,
                                              wl_proxy_get_version((struct wl_proxy *)remote_foreign_toplevel_manager),
                                              manager,
                                              local_manager_bind);
    if (manager->local_manager != NULL) {
        manager->remote_display = remote_display;
        manager->remote_manager = remote_foreign_toplevel_manager;
        wl_list_init(&manager->local_manager_resources);
        wl_list_init(&manager->local_manager_stopped_resources);
        wl_list_init(&manager->toplevels);
        zwlr_foreign_toplevel_manager_v1_add_listener(remote_foreign_toplevel_manager,
                                                      &remote_manager_listener, manager);
        return manager;
    } else {
        zwlr_foreign_toplevel_manager_v1_stop(remote_foreign_toplevel_manager);
        zwlr_foreign_toplevel_manager_v1_destroy(remote_foreign_toplevel_manager);
        g_free(manager);
        return NULL;
    }
}

void
wle_foreign_toplevel_manager_destroy(WleForeignToplevelManager *manager) {
    if (manager != NULL) {
        struct wl_resource *resource, *tmp;

        WleForeignToplevelHandle *toplevel, *tmp_toplevel;
        wl_list_for_each_safe(toplevel, tmp_toplevel, &manager->toplevels, link) {
            if (toplevel->remote_toplevel != NULL) {
                zwlr_foreign_toplevel_handle_v1_destroy(toplevel->remote_toplevel);
                toplevel->remote_toplevel = NULL;
            }

            WleForeignToplevelHandleResourceHolder *toplevel_holder, *tmp_toplevel_holder;
            wl_list_for_each_safe(toplevel_holder, tmp_toplevel_holder, &toplevel->local_toplevel_holders, link) {
                wl_resource_destroy(toplevel_holder->local_toplevel);
            }
        }

        wl_resource_for_each_safe(resource, tmp, &manager->local_manager_resources) {
            wl_resource_destroy(resource);
        }
        wl_resource_for_each_safe(resource, tmp, &manager->local_manager_stopped_resources) {
            wl_resource_destroy(resource);
        }

        wl_global_destroy(manager->local_manager);
        zwlr_foreign_toplevel_manager_v1_stop(manager->remote_manager);
        zwlr_foreign_toplevel_manager_v1_destroy(manager->remote_manager);

        g_free(manager);
    }
}
