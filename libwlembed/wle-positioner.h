/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_POSITIONER_H__
#define __WLE_POSITIONER_H__

#include <glib-object.h>
#include <wayland-server-core.h>

#include "protocol/xdg-shell-client-protocol.h"
#include "wle-local-resource.h"

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WlePositioner, wle_positioner, WLE, POSITIONER, WleLocalResource)
#define WLE_TYPE_POSITIONER (wle_positioner_get_type())

WlePositioner *wle_positioner_new(struct xdg_positioner *remote_positioner,
                                  struct wl_resource *local_positioner);

struct xdg_positioner *wle_positioner_get_remote_positioner(WlePositioner *positioner);

void wle_positioner_set_parent_offset(WlePositioner *positioner,
                                      int32_t x,
                                      int32_t y);
void wle_positioner_get_parent_offset(WlePositioner *positioner,
                                      int32_t *x,
                                      int32_t *y);

void wle_positioner_set_parent_geometry(WlePositioner *positioner,
                                        int32_t x,
                                        int32_t y,
                                        int32_t width,
                                        int32_t height);
void wle_positioner_get_parent_geometry(WlePositioner *positioner,
                                        int32_t *x,
                                        int32_t *y,
                                        int32_t *width,
                                        int32_t *height);

G_END_DECLS

#endif /* __WLE_POSITIONER_H__ */
