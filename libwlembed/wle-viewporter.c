/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "protocol/viewporter-client-protocol.h"
#include "protocol/viewporter-protocol.h"
#include "wle-util.h"
#include "wle-view.h"
#include "wle-viewporter.h"

#define VIEWPORT_KEY "wle-viewport"

struct _WleViewporter {
    struct wl_display *remote_display;
    struct wp_viewporter *remote_viewporter;

    struct wl_global *local_viewporter;
    struct wl_list local_viewporter_resources;

    struct wl_list viewports;  // WleViewport
};

typedef struct {
    WleView *view;
    struct wp_viewport *remote_viewport;
    struct wl_resource *local_viewport;
    struct wl_list link;
} WleViewport;

static void local_viewport_set_source(struct wl_client *client,
                                      struct wl_resource *viewport,
                                      wl_fixed_t x,
                                      wl_fixed_t y,
                                      wl_fixed_t width,
                                      wl_fixed_t height);
static void local_viewport_set_destination(struct wl_client *client,
                                           struct wl_resource *viewport,
                                           int32_t width,
                                           int32_t height);
static void local_viewport_destroy(struct wl_client *client,
                                   struct wl_resource *viewport);


static void local_viewporter_get_viewport(struct wl_client *client,
                                          struct wl_resource *viewporter_resource,
                                          uint32_t viewport_id,
                                          struct wl_resource *surface_resource);
static void local_viewporter_destroy(struct wl_client *client,
                                     struct wl_resource *viewporter_resource);

static const struct wp_viewport_interface local_viewport_impl = {
    .set_source = local_viewport_set_source,
    .set_destination = local_viewport_set_destination,
    .destroy = local_viewport_destroy,
};

static const struct wp_viewporter_interface local_viewporter_impl = {
    .get_viewport = local_viewporter_get_viewport,
    .destroy = local_viewporter_destroy,
};

static void
local_viewport_set_source(struct wl_client *client,
                          struct wl_resource *viewport_resource,
                          wl_fixed_t x,
                          wl_fixed_t y,
                          wl_fixed_t width,
                          wl_fixed_t height) {
    WleViewport *viewport = wl_resource_get_user_data(viewport_resource);
    wp_viewport_set_source(viewport->remote_viewport, x, y, width, height);
}

static void
local_viewport_set_destination(struct wl_client *client,
                               struct wl_resource *viewport_resource,
                               int32_t width,
                               int32_t height) {
    WleViewport *viewport = wl_resource_get_user_data(viewport_resource);
    wp_viewport_set_destination(viewport->remote_viewport, width, height);
}

static void
local_viewport_destroy(struct wl_client *client, struct wl_resource *viewport_resource) {
    wl_resource_destroy(viewport_resource);
}

static void
local_viewport_destroy_impl(struct wl_resource *resource) {
    WleViewport *viewport = wl_resource_get_user_data(resource);
    if (viewport->remote_viewport != NULL) {
        wp_viewport_destroy(viewport->remote_viewport);
        viewport->remote_viewport = NULL;
    }
    g_object_set_data(G_OBJECT(viewport->view), VIEWPORT_KEY, NULL);
    wl_list_remove(&viewport->link);
    g_free(viewport);
}

static void
view_destroyed(WleView *view, WleViewport *viewport) {
    wl_resource_destroy(viewport->local_viewport);
}

static void
local_viewporter_get_viewport(struct wl_client *client,
                              struct wl_resource *viewporter_resource,
                              uint32_t viewport_id,
                              struct wl_resource *surface_resource) {
    WleViewporter *viewporter = wl_resource_get_user_data(viewporter_resource);
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    WleViewport *viewport = g_object_get_data(G_OBJECT(view), VIEWPORT_KEY);

    if (viewport != NULL) {
        wl_resource_post_error(viewporter_resource,
                               WP_VIEWPORTER_ERROR_VIEWPORT_EXISTS,
                               "viewport already exists for this surface");
    } else {
        struct wl_resource *resource = wl_resource_create(client,
                                                          &wp_viewport_interface,
                                                          wl_resource_get_version(viewporter_resource),
                                                          viewport_id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            struct wp_viewport *remote_viewport = wp_viewporter_get_viewport(viewporter->remote_viewporter,
                                                                             wle_view_get_remote_surface(view));
            if (remote_viewport == NULL) {
                wle_propagate_remote_error(viewporter->remote_display, client, viewporter_resource);
            } else {
                viewport = g_new0(WleViewport, 1);
                viewport->view = view;
                viewport->remote_viewport = _wle_proxy_set_ours((struct wl_proxy *)remote_viewport);
                viewport->local_viewport = resource;
                wl_list_insert(&viewporter->viewports, &viewport->link);
                g_object_set_data(G_OBJECT(view), VIEWPORT_KEY, viewport);
                g_signal_connect(view, "destroy",
                                 G_CALLBACK(view_destroyed), viewport);
                wl_resource_set_implementation(resource, &local_viewport_impl, viewport, local_viewport_destroy_impl);
            }
        }
    }
}

static void
local_viewporter_destroy(struct wl_client *client, struct wl_resource *viewporter_resource) {
    wl_resource_destroy(viewporter_resource);
}

static void
local_viewporter_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_viewporter_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleViewporter *viewporter = data;

    struct wl_resource *resource = wl_resource_create(client,
                                                      &wp_viewporter_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource,
                                       &local_viewporter_impl,
                                       viewporter,
                                       local_viewporter_destroy_impl);
        if (viewporter == NULL) {
            wl_list_init(wl_resource_get_link(resource));
        } else {
            wl_list_insert(&viewporter->local_viewporter_resources, wl_resource_get_link(resource));
        }
    }
}

WleViewporter *
wle_viewporter_new(struct wl_display *remote_display,
                   struct wp_viewporter *remote_viewporter,
                   struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_viewporter != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleViewporter *viewporter = g_new0(WleViewporter, 1);
    uint32_t version = wl_proxy_get_version((struct wl_proxy *)remote_viewporter);
    viewporter->local_viewporter = wl_global_create(local_display,
                                                    &wp_viewporter_interface,
                                                    version,
                                                    viewporter,
                                                    local_viewporter_bind);
    if (viewporter->local_viewporter == NULL) {
        wp_viewporter_destroy(remote_viewporter);
        g_free(viewporter);
        return NULL;
    } else {
        viewporter->remote_display = remote_display;
        viewporter->remote_viewporter = remote_viewporter;
        wl_list_init(&viewporter->local_viewporter_resources);
        wl_list_init(&viewporter->viewports);
        return viewporter;
    }
}

void
wle_viewporter_destroy(WleViewporter *viewporter) {
    if (viewporter != NULL) {
        WleViewport *viewport, *tmp_viewport;
        wl_list_for_each_safe(viewport, tmp_viewport, &viewporter->viewports, link) {
            wl_resource_destroy(viewport->local_viewport);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &viewporter->local_viewporter_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(viewporter->local_viewporter);

        wp_viewporter_destroy(viewporter->remote_viewporter);

        g_free(viewporter);
    }
}
