/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common/wle-debug.h"
#include "common/wle-private.h"
#include "wle-compositor.h"
#include "wle-error.h"
#include "wle-marshal.h"
#include "wle-output.h"
#include "wle-private-types.h"
#include "wle-seat.h"
#include "wle-shm-forwarder.h"
#include "wle-toplevel.h"
#include "wle-util.h"
#include "wle-view.h"

typedef struct {
    struct wl_callback *remote_callback;
    struct wl_resource *local_callback;
    WleView *view;
} WleCallback;

struct _WleView {
    WleLocalResource parent;

    WleMode mode;
    WleViewRole *role;

    struct wl_display *remote_display;
    struct wl_compositor *remote_compositor;
    struct wl_subcompositor *remote_subcompositor;
    struct wl_surface *remote_surface;

    struct wl_resource *local_surface;

    GList *outputs;

    WleViewEmbedData *embed_data;

    uint32_t pending_serial;
    int32_t pending_set_width;
    int32_t pending_set_height;

    GList *callbacks;
    uint32_t last_callback_data;
    gint64 last_callback_time;

    guint has_buffer : 1,
        needs_map_emit : 1,
        needs_unmap_emit : 1;
};

enum {
    SIG_ROLE_CHANGED,
    SIG_MIN_SIZE_CHANGED,
    SIG_MAX_SIZE_CHANGED,
    SIG_CONFIGURE,
    SIG_MAP,
    SIG_UNMAP,
    SIG_ACTIVATED,
    SIG_OUTPUT_ENTER,
    SIG_OUTPUT_LEAVE,
    SIG_MODE_CHANGED,
    SIG_EMBED_DATA_CHANGED,

    SIG_POINTER_ENTER,
    SIG_POINTER_LEAVE,
    SIG_POINTER_MOTION,
    SIG_POINTER_FRAME,

    N_SIGNALS,
};

static void wle_view_finalize(GObject *object);

static void wle_view_destroy(WleLocalResource *local_resource);

static void remote_surface_enter(void *data,
                                 struct wl_surface *view,
                                 struct wl_output *output);
static void remote_surface_leave(void *data,
                                 struct wl_surface *view,
                                 struct wl_output *output);
#ifdef WL_SURFACE_PREFERRED_BUFFER_SCALE
static void remote_surface_preferred_buffer_scale(void *data,
                                                  struct wl_surface *view,
                                                  int32_t scale);
#endif
#ifdef WL_SURFACE_PREFERRED_BUFFER_TRANSFORM
static void remote_surface_preferred_buffer_transform(void *data,
                                                      struct wl_surface *view,
                                                      uint32_t transform);
#endif

static void local_surface_attach(struct wl_client *client,
                                 struct wl_resource *view,
                                 struct wl_resource *buffer,
                                 int32_t x,
                                 int32_t y);
static void local_surface_commit(struct wl_client *client,
                                 struct wl_resource *view);
static void local_surface_damage(struct wl_client *client,
                                 struct wl_resource *view,
                                 int32_t x,
                                 int32_t y,
                                 int32_t width,
                                 int32_t height);
static void local_surface_damage_buffer(struct wl_client *client,
                                        struct wl_resource *view,
                                        int32_t x,
                                        int32_t y,
                                        int32_t width,
                                        int32_t height);
static void local_surface_set_buffer_scale(struct wl_client *client,
                                           struct wl_resource *view,
                                           int32_t scale);
static void local_surface_set_buffer_transform(struct wl_client *client,
                                               struct wl_resource *view,
                                               int32_t transform);
static void local_surface_frame(struct wl_client *client,
                                struct wl_resource *view,
                                uint32_t callback_id);
static void local_surface_offset(struct wl_client *client,
                                 struct wl_resource *view,
                                 int32_t x,
                                 int32_t y);
static void local_surface_set_input_region(struct wl_client *client,
                                           struct wl_resource *view,
                                           struct wl_resource *region);
static void local_surface_set_opaque_region(struct wl_client *client,
                                            struct wl_resource *view,
                                            struct wl_resource *region);
static void local_surface_destroy(struct wl_client *client,
                                  struct wl_resource *view);

static void destroy_roles(WleView *view);


WLE_G_DEFINE_ENUM_TYPE(WleMode, wle_mode,
                       WLE_G_DEFINE_ENUM_VALUE(WLE_MODE_EMBEDDED, "embedded"),
                       WLE_G_DEFINE_ENUM_VALUE(WLE_MODE_FORWARDED, "forwarded"))
G_DEFINE_FINAL_TYPE(WleView, wle_view, WLE_TYPE_LOCAL_RESOURCE)


static guint signals[N_SIGNALS] = { 0 };

static const struct wl_surface_listener remote_surface_listener = {
    .enter = remote_surface_enter,
    .leave = remote_surface_leave,
#ifdef WL_SURFACE_PREFERRED_BUFFER_SCALE
    .preferred_buffer_scale = remote_surface_preferred_buffer_scale,
#endif
#ifdef WL_SURFACE_PREFERRED_BUFFER_TRANSFORM
    .preferred_buffer_transform = remote_surface_preferred_buffer_transform,
#endif
};

static const struct wl_surface_interface local_surface_impl = {
    .attach = local_surface_attach,
    .commit = local_surface_commit,
    .damage = local_surface_damage,
    .damage_buffer = local_surface_damage_buffer,
    .set_buffer_scale = local_surface_set_buffer_scale,
    .set_buffer_transform = local_surface_set_buffer_transform,
    .frame = local_surface_frame,
    .offset = local_surface_offset,
    .set_input_region = local_surface_set_input_region,
    .set_opaque_region = local_surface_set_opaque_region,
    .destroy = local_surface_destroy,
};

static void
wle_view_class_init(WleViewClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_view_finalize;

    WleLocalResourceClass *local_resource_class = WLE_LOCAL_RESOURCE_CLASS(klass);
    local_resource_class->destroy = wle_view_destroy;

    signals[SIG_ROLE_CHANGED] = g_signal_new("role-changed",
                                             WLE_TYPE_VIEW,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__VOID,
                                             G_TYPE_NONE, 0);

    signals[SIG_MIN_SIZE_CHANGED] = g_signal_new("min-size-changed",
                                                 WLE_TYPE_VIEW,
                                                 G_SIGNAL_RUN_LAST,
                                                 0,
                                                 NULL, NULL,
                                                 _wle_marshal_VOID__INT_INT,
                                                 G_TYPE_NONE, 2,
                                                 G_TYPE_INT,
                                                 G_TYPE_INT);

    signals[SIG_MAX_SIZE_CHANGED] = g_signal_new("max-size-changed",
                                                 WLE_TYPE_VIEW,
                                                 G_SIGNAL_RUN_LAST,
                                                 0,
                                                 NULL, NULL,
                                                 _wle_marshal_VOID__INT_INT,
                                                 G_TYPE_NONE, 2,
                                                 G_TYPE_INT,
                                                 G_TYPE_INT);

    signals[SIG_CONFIGURE] = g_signal_new("configure",
                                          WLE_TYPE_VIEW,
                                          G_SIGNAL_RUN_LAST,
                                          0,
                                          NULL, NULL,
                                          g_cclosure_marshal_VOID__UINT,
                                          G_TYPE_NONE, 1,
                                          G_TYPE_UINT);

    signals[SIG_MAP] = g_signal_new("map",
                                    WLE_TYPE_VIEW,
                                    G_SIGNAL_RUN_LAST,
                                    0,
                                    NULL, NULL,
                                    g_cclosure_marshal_VOID__VOID,
                                    G_TYPE_NONE, 0);

    signals[SIG_UNMAP] = g_signal_new("unmap",
                                      WLE_TYPE_VIEW,
                                      G_SIGNAL_RUN_LAST,
                                      0,
                                      NULL, NULL,
                                      g_cclosure_marshal_VOID__VOID,
                                      G_TYPE_NONE, 0);

    signals[SIG_ACTIVATED] = g_signal_new("activated",
                                          WLE_TYPE_VIEW,
                                          G_SIGNAL_RUN_LAST,
                                          0,
                                          NULL, NULL,
                                          g_cclosure_marshal_VOID__UINT,
                                          G_TYPE_NONE, 1,
                                          G_TYPE_UINT);

    signals[SIG_OUTPUT_ENTER] = g_signal_new("output-enter",
                                             WLE_TYPE_VIEW,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__OBJECT,
                                             G_TYPE_NONE, 1,
                                             WLE_TYPE_OUTPUT);

    signals[SIG_OUTPUT_LEAVE] = g_signal_new("output-leave",
                                             WLE_TYPE_VIEW,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__OBJECT,
                                             G_TYPE_NONE, 1,
                                             WLE_TYPE_OUTPUT);

    signals[SIG_MODE_CHANGED] = g_signal_new("mode-changed",
                                             WLE_TYPE_VIEW,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__VOID,
                                             G_TYPE_NONE, 0);

    signals[SIG_EMBED_DATA_CHANGED] = g_signal_new("embed-data-changed",
                                                   WLE_TYPE_VIEW,
                                                   G_SIGNAL_RUN_LAST,
                                                   0,
                                                   NULL, NULL,
                                                   g_cclosure_marshal_VOID__VOID,
                                                   G_TYPE_NONE, 0);


    signals[SIG_POINTER_ENTER] = g_signal_new("pointer-enter",
                                              WLE_TYPE_VIEW,
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL,
                                              NULL,
                                              _wle_marshal_VOID__OBJECT_UINT_DOUBLE_DOUBLE,
                                              G_TYPE_NONE,
                                              4,
                                              WLE_TYPE_SEAT,
                                              G_TYPE_UINT,
                                              G_TYPE_DOUBLE,
                                              G_TYPE_DOUBLE);

    signals[SIG_POINTER_LEAVE] = g_signal_new("pointer-leave",
                                              WLE_TYPE_VIEW,
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL,
                                              NULL,
                                              _wle_marshal_VOID__OBJECT_UINT,
                                              G_TYPE_NONE,
                                              2,
                                              WLE_TYPE_SEAT,
                                              G_TYPE_UINT);

    signals[SIG_POINTER_MOTION] = g_signal_new("pointer-motion",
                                               WLE_TYPE_VIEW,
                                               G_SIGNAL_RUN_LAST,
                                               0,
                                               NULL,
                                               NULL,
                                               _wle_marshal_VOID__UINT_DOUBLE_DOUBLE,
                                               G_TYPE_NONE,
                                               3,
                                               G_TYPE_UINT,
                                               G_TYPE_DOUBLE,
                                               G_TYPE_DOUBLE);

    signals[SIG_POINTER_FRAME] = g_signal_new("pointer-frame",
                                              WLE_TYPE_VIEW,
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL,
                                              NULL,
                                              g_cclosure_marshal_VOID__OBJECT,
                                              G_TYPE_NONE,
                                              1,
                                              WLE_TYPE_SEAT);
}

static void
wle_view_init(WleView *view) {
    view->mode = WLE_MODE_FORWARDED;
}

static void
wle_view_finalize(GObject *object) {
    WleView *view = WLE_VIEW(object);

    GList *l = view->callbacks;
    while (l != NULL) {
        GList *next = l->next;
        WleCallback *callback = l->data;
        wl_resource_destroy(callback->local_callback);
        l = next;
    }
    // List should be freed by local_callback_destroy_impl()

    destroy_roles(view);

    if (view->remote_surface != NULL) {
        wl_surface_destroy(view->remote_surface);
    }

    if (view->local_surface != NULL) {
        wl_resource_destroy(view->local_surface);
    }

    G_OBJECT_CLASS(wle_view_parent_class)->finalize(object);
}

static void
wle_view_destroy(WleLocalResource *local_resource) {
    WleView *view = WLE_VIEW(local_resource);

    destroy_roles(view);

    if (view->remote_surface != NULL) {
        wl_surface_destroy(view->remote_surface);
        view->remote_surface = NULL;
    }

    if (view->local_surface != NULL) {
        wl_resource_destroy(view->local_surface);
        view->local_surface = NULL;
    }

    WLE_LOCAL_RESOURCE_CLASS(wle_view_parent_class)->destroy(local_resource);
}

static void
remote_surface_enter(void *data, struct wl_surface *wl_surface, struct wl_output *wl_output) {
    WleView *view = WLE_VIEW(data);
    WleOutput *output = _wle_proxy_get_user_data((struct wl_proxy *)wl_output);

    if (WLE_IS_OUTPUT(output) && g_list_find(view->outputs, output) == NULL) {
        view->outputs = g_list_append(view->outputs, output);
        g_signal_emit(view, signals[SIG_OUTPUT_ENTER], 0, output);
        GList *local_outputs = wle_output_get_local_outputs(output, wl_resource_get_client(view->local_surface));
        for (GList *l = local_outputs; l != NULL; l = l->next) {
            struct wl_resource *local_output = l->data;
            wl_surface_send_enter(view->local_surface, local_output);
        }
        g_list_free(local_outputs);
    }
}

static void
remote_surface_leave(void *data, struct wl_surface *wl_surface, struct wl_output *wl_output) {
    WleView *view = WLE_VIEW(data);
    WleOutput *output = _wle_proxy_get_user_data((struct wl_proxy *)wl_output);

    if (WLE_IS_OUTPUT(output) && g_list_find(view->outputs, output) != NULL) {
        view->outputs = g_list_remove(view->outputs, output);
        g_signal_emit(view, signals[SIG_OUTPUT_LEAVE], 0, output);
        GList *local_outputs = wle_output_get_local_outputs(output, wl_resource_get_client(view->local_surface));
        for (GList *l = local_outputs; l != NULL; l = l->next) {
            struct wl_resource *local_output = l->data;
            wl_surface_send_leave(view->local_surface, local_output);
        }
        g_list_free(local_outputs);
    }
}

#ifdef WL_SURFACE_PREFERRED_BUFFER_SCALE
static void
remote_surface_preferred_buffer_scale(void *data, struct wl_surface *wl_surface, int32_t scale) {
    WleView *view = WLE_VIEW(data);
    if (wl_resource_get_version(view->local_surface) >= WL_SURFACE_PREFERRED_BUFFER_SCALE_SINCE_VERSION) {
        wl_surface_send_preferred_buffer_scale(view->local_surface, scale);
    }
}
#endif

#ifdef WL_SURFACE_PREFERRED_BUFFER_TRANSFORM
static void
remote_surface_preferred_buffer_transform(void *data, struct wl_surface *wl_surface, uint32_t transform) {
    WleView *view = WLE_VIEW(data);
    if (wl_resource_get_version(view->local_surface) >= WL_SURFACE_PREFERRED_BUFFER_TRANSFORM_SINCE_VERSION) {
        wl_surface_send_preferred_buffer_transform(view->local_surface, transform);
    }
}
#endif

static void
local_surface_attach(struct wl_client *client,
                     struct wl_resource *surface_resource,
                     struct wl_resource *buffer_resource,
                     int32_t x,
                     int32_t y) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    WleBuffer *buffer = buffer_resource != NULL ? wl_resource_get_user_data(buffer_resource) : NULL;
    struct wl_buffer *remote_buffer = buffer != NULL ? wle_buffer_get_remote_buffer(buffer) : NULL;


    if (wl_resource_get_version(surface_resource) >= WL_SURFACE_OFFSET_SINCE_VERSION && (x != 0 || y != 0)) {
        wl_resource_post_error(surface_resource,
                               WL_SURFACE_ERROR_INVALID_OFFSET,
                               "non-zero offsets not valid on attach for this wl_surface version");
    } else {
        if (view->remote_surface != NULL
            && wl_resource_get_version(surface_resource) < WL_SURFACE_OFFSET_SINCE_VERSION
            && wl_proxy_get_version((struct wl_proxy *)view->remote_surface) >= WL_SURFACE_OFFSET_SINCE_VERSION)
        {
            // If the client is using wl_surface v4 or below, but the server is
            // using v5 or above, the client can set offsets using
            // wl_surface.attach, but we have to set them on the server using
            // wl_surface.offset.
            wl_surface_offset(view->remote_surface, x, y);
            x = 0;
            y = 0;
        }

        if (view->remote_surface != NULL) {
            if (buffer != NULL) {
                wle_buffer_ref(buffer);
                wle_buffer_set_forward_release(buffer, TRUE);
            }
            wl_surface_attach(view->remote_surface, remote_buffer, x, y);
        }

        if (buffer != NULL && !view->has_buffer) {
            view->has_buffer = TRUE;
            view->needs_map_emit = TRUE;
            view->needs_unmap_emit = FALSE;
        } else if (buffer == NULL && view->has_buffer) {
            view->has_buffer = FALSE;
            view->needs_map_emit = FALSE;
            view->needs_unmap_emit = TRUE;
        }

        if (view->remote_surface == NULL && buffer != NULL) {
            wl_buffer_send_release(wle_buffer_get_local_buffer(buffer));
        }
    }
}

static void
local_surface_commit(struct wl_client *client, struct wl_resource *surface_resource) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    if (view->remote_surface != NULL) {
        wl_surface_commit(view->remote_surface);
        if (view->embed_data != NULL && view->embed_data->remote_parent_surface != NULL) {
            wl_surface_commit(view->embed_data->remote_parent_surface);
        }
    }

    // TODO: do we also need this in _set_role() / _transform_embedded()?
    if (view->needs_map_emit && (view->role != NULL || view->embed_data != NULL)) {
        g_signal_emit(view, signals[SIG_MAP], 0);
        view->needs_map_emit = FALSE;
    } else if (view->needs_unmap_emit) {
        g_signal_emit(view, signals[SIG_UNMAP], 0);
        view->needs_unmap_emit = FALSE;
    }
}

static void
local_surface_damage(struct wl_client *client,
                     struct wl_resource *surface_resource,
                     int32_t x,
                     int32_t y,
                     int32_t width,
                     int32_t height) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    if (view->remote_surface != NULL) {
        wl_surface_damage(view->remote_surface, x, y, width, height);
    }
}

static void
local_surface_damage_buffer(struct wl_client *client,
                            struct wl_resource *surface_resource,
                            int32_t x,
                            int32_t y,
                            int32_t width,
                            int32_t height) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    if (view->remote_surface != NULL) {
        wl_surface_damage_buffer(view->remote_surface, x, y, width, height);
    }
}

static void
local_surface_set_buffer_scale(struct wl_client *client, struct wl_resource *surface_resource, int32_t scale) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    if (view->remote_surface != NULL) {
        wl_surface_set_buffer_scale(view->remote_surface, scale);
    }
}

static void
local_surface_set_buffer_transform(struct wl_client *client, struct wl_resource *surface_resource, int32_t transform) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    if (view->remote_surface != NULL) {
        wl_surface_set_buffer_transform(view->remote_surface, transform);
    }
}

static void
remote_callback_done(void *data, struct wl_callback *remote_callback, uint32_t callback_data) {
    WleCallback *callback = data;

    callback->view->last_callback_data = callback_data;
    callback->view->last_callback_time = g_get_monotonic_time();

    wl_callback_send_done(callback->local_callback, callback_data);
}

static const struct wl_callback_listener remote_callback_listener = {
    .done = remote_callback_done,
};

static void
local_callback_destroy_impl(struct wl_resource *resource) {
    WleCallback *callback = wl_resource_get_user_data(resource);

    if (callback->remote_callback != NULL) {
        wl_callback_destroy(callback->remote_callback);
    }

    callback->view->callbacks = g_list_remove(callback->view->callbacks, callback);
    g_free(callback);
}

static void
local_surface_frame(struct wl_client *client, struct wl_resource *surface_resource, uint32_t callback_id) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    struct wl_callback *remote_callback = NULL;

    if (view->remote_surface != NULL) {
        remote_callback = wl_surface_frame(view->remote_surface);
    }

    if (view->remote_surface != NULL && remote_callback == NULL) {
        wle_propagate_remote_error(view->remote_display, client, surface_resource);
    } else {
        struct wl_resource *resource = wl_resource_create(client, &wl_callback_interface, 1, callback_id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
            if (remote_callback != NULL) {
                wl_callback_destroy(remote_callback);
            }
        } else {
            WleCallback *callback = g_new0(WleCallback, 1);
            callback->remote_callback = remote_callback != NULL ? _wle_proxy_set_ours((struct wl_proxy *)remote_callback) : NULL;
            callback->local_callback = resource;
            callback->view = view;

            wl_resource_set_implementation(resource, NULL, callback, local_callback_destroy_impl);
            if (remote_callback != NULL) {
                wl_callback_add_listener(remote_callback, &remote_callback_listener, callback);
            }

            view->callbacks = g_list_prepend(view->callbacks, callback);
        }
    }
}

static void
local_surface_offset(struct wl_client *client, struct wl_resource *surface_resource, int32_t x, int32_t y) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    if (view->remote_surface != NULL) {
        wl_surface_offset(view->remote_surface, x, y);
    }
}

static void
local_surface_set_input_region(struct wl_client *client,
                               struct wl_resource *surface_resource,
                               struct wl_resource *region_resource) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    WleRegion *region = region_resource != NULL ? wl_resource_get_user_data(region_resource) : NULL;

    if (view->remote_surface != NULL) {
        wl_surface_set_input_region(view->remote_surface, region != NULL ? wle_region_get_remote_region(region) : NULL);
    }
}

static void
local_surface_set_opaque_region(struct wl_client *client,
                                struct wl_resource *surface_resource,
                                struct wl_resource *region_resource) {
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));
    WleRegion *region = region_resource != NULL ? wl_resource_get_user_data(region_resource) : NULL;

    if (view->remote_surface != NULL) {
        wl_surface_set_opaque_region(view->remote_surface, region != NULL ? wle_region_get_remote_region(region) : NULL);
    }
}

static void
local_surface_destroy(struct wl_client *client, struct wl_resource *surface_resource) {
    wl_resource_destroy(surface_resource);
}

static void
local_surface_destroy_impl(struct wl_resource *resource) {
    WleView *view = wl_resource_get_user_data(resource);

    destroy_roles(view);

    if (view->remote_surface != NULL) {
        wl_surface_destroy(view->remote_surface);
        view->remote_surface = NULL;
    }

    view->local_surface = NULL;
    wle_local_resource_destroyed(WLE_LOCAL_RESOURCE(view));
    g_object_unref(view);
}

static void
destroy_roles(WleView *view) {
    wle_view_set_role(view, NULL);

    if (view->embed_data != NULL) {
        if (view->embed_data->remote_parent_surface != NULL) {
            wl_subsurface_destroy(view->embed_data->remote_subsurface);
        }

        g_free(view->embed_data);
        view->embed_data = NULL;
    }
}

static void
view_role_destroyed(WleViewRole *view_role, WleView *view) {
    if (view->role == view_role) {
        g_signal_handlers_disconnect_by_data(view->role, view);
        view->role = NULL;
        g_signal_emit(view, signals[SIG_ROLE_CHANGED], 0);
    }
}

WleView *
wle_view_new(struct wl_display *remote_display,
             struct wl_compositor *remote_compositor,
             struct wl_subcompositor *remote_subcompositor,
             struct wl_surface *remote_surface,
             struct wl_resource *local_surface) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_surface != NULL, NULL);
    g_return_val_if_fail(remote_compositor != NULL, NULL);
    g_return_val_if_fail(remote_subcompositor != NULL, NULL);
    g_return_val_if_fail(local_surface != NULL, NULL);

    WleView *view = g_object_new(WLE_TYPE_VIEW, NULL);
    view->remote_display = remote_display;
    view->remote_compositor = remote_compositor;
    view->remote_subcompositor = remote_subcompositor;
    view->remote_surface = remote_surface;
    view->local_surface = local_surface;

    wl_surface_add_listener(remote_surface, &remote_surface_listener, view);
    wl_resource_set_implementation(local_surface, &local_surface_impl, view, local_surface_destroy_impl);

    // local_surface takes a refefence
    g_object_ref(view);

    return view;
}

gboolean
_wle_view_is_embedded(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), FALSE);
    return view->mode == WLE_MODE_EMBEDDED;
}

gboolean
_wle_view_is_forwarded(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), FALSE);
    return view->mode == WLE_MODE_FORWARDED;
}

void
wle_view_set_role(WleView *view, WleViewRole *view_role) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(view_role == NULL || WLE_IS_VIEW_ROLE(view_role));

    if (view->role != view_role) {
        if (view->role != NULL) {
            g_signal_handlers_disconnect_by_data(view->role, view);
            wle_local_resource_destroy(WLE_LOCAL_RESOURCE(view->role));
            view->role = NULL;
        }

        view->role = view_role;
        if (view->role != NULL) {
            if (view->pending_set_width > 0 && view->pending_set_height > 0) {
                wle_view_role_set_size(view->role,
                                       view->pending_serial,
                                       view->pending_set_width,
                                       view->pending_set_height);
                view->pending_set_width = 0;
                view->pending_set_height = 0;
            }
            g_signal_connect(view->role, "destroy",
                             G_CALLBACK(view_role_destroyed), view);
        }

        g_signal_emit(view, signals[SIG_ROLE_CHANGED], 0);
    }
}

WleViewRole *
wle_view_get_role(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    return view->role;
}

static gboolean
create_embedding_surfaces(WleView *view, struct wl_surface *embedded_parent_surface) {
    view->remote_surface = wl_compositor_create_surface(view->remote_compositor);
    wl_surface_add_listener(view->remote_surface, &remote_surface_listener, view);
    _wle_proxy_set_ours((struct wl_proxy *)view->remote_surface);

    DBG("embedding our remote surface %p into app's %p", view->remote_surface, embedded_parent_surface);

    view->embed_data->remote_subsurface = wl_subcompositor_get_subsurface(view->remote_subcompositor,
                                                                          view->remote_surface,
                                                                          embedded_parent_surface);
    if (view->embed_data->remote_subsurface == NULL) {
        return FALSE;
    } else {
        _wle_proxy_set_ours((struct wl_proxy *)view->embed_data->remote_subsurface);
        view->embed_data->remote_parent_surface = embedded_parent_surface;

        wl_subsurface_place_above(view->embed_data->remote_subsurface, embedded_parent_surface);
        wl_subsurface_set_desync(view->embed_data->remote_subsurface);
        wl_surface_commit(view->remote_surface);

        if (WLE_IS_TOPLEVEL(view->role)) {
            WleToplevel *toplevel = WLE_TOPLEVEL(view->role);
            gboolean was_visible = wle_toplevel_is_visible(toplevel);

            if (was_visible) {
                DBG("hiding and showing");
                wle_toplevel_hide(toplevel);
                wle_toplevel_show(toplevel);
            }
        }

        return TRUE;
    }
}

gboolean
_wle_view_set_embedded(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), FALSE);
    g_return_val_if_fail(view->role != NULL, FALSE);
    g_return_val_if_fail(view->mode == WLE_MODE_FORWARDED, FALSE);
    g_return_val_if_fail(view->embed_data == NULL, FALSE);

    if (!wle_view_role_destroy_remote_role(view->role)) {
        return FALSE;
    } else {
        view->embed_data = g_new0(WleViewEmbedData, 1);
        wl_surface_destroy(view->remote_surface);
        view->remote_surface = NULL;

        view->mode = WLE_MODE_EMBEDDED;
        g_signal_emit(view, signals[SIG_MODE_CHANGED], 0);

        return TRUE;
    }
}

void
_wle_view_embedder_shown(WleView *view, struct wl_surface *new_remote_parent_surface) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(new_remote_parent_surface != NULL);
    g_return_if_fail(view->mode == WLE_MODE_EMBEDDED);
    g_return_if_fail(view->embed_data != NULL);

    if (view->embed_data->remote_parent_surface != new_remote_parent_surface) {
        if (view->embed_data->remote_parent_surface != NULL) {
            // This shouldn't happen, but clean things up
            _wle_view_embedder_hidden(view);
        }

        if (create_embedding_surfaces(view, new_remote_parent_surface)) {
            uint32_t next_callback_data = view->last_callback_data + (g_get_monotonic_time() - view->last_callback_time);
            GList *l = g_list_last(view->callbacks);
            while (l != NULL) {
                WleCallback *callback = l->data;
                if (callback->remote_callback == NULL) {
                    // This callback was created while we were hidden, so
                    // there's no remote callback to send the 'done' event, so
                    // synthesize it.
                    GList *prev = l->prev;
                    DBG("synthesize callback done(%u)", next_callback_data);
                    wl_callback_send_done(callback->local_callback, next_callback_data);
                    l = prev;
                } else {
                    l = l->prev;
                }
            }
        }
    }
}

void
_wle_view_embedder_hidden(WleView *view) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(view->mode == WLE_MODE_EMBEDDED);
    g_return_if_fail(view->embed_data != NULL);

    if (view->embed_data->remote_subsurface != NULL) {
        wl_subsurface_destroy(view->embed_data->remote_subsurface);
        view->embed_data->remote_subsurface = NULL;
    }

    if (view->remote_surface != NULL) {
        wl_surface_destroy(view->remote_surface);
        view->remote_surface = NULL;
    }

    view->embed_data->remote_parent_surface = NULL;
    view->embed_data->offset_x = 0;
    view->embed_data->offset_y = 0;

    // Disconnect any existing callbacks from the remote compositor; this way
    // if the view is ever shown again, we can synthesize 'done' events to the
    // client to get it to start drawing again.
    for (GList *l = view->callbacks; l != NULL; l = l->next) {
        WleCallback *callback = l->data;
        if (callback->remote_callback != NULL) {
            wl_callback_destroy(callback->remote_callback);
            callback->remote_callback = NULL;
        }
    }
}

struct wl_client *
wle_view_get_client(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    return view->local_surface != NULL ? wl_resource_get_client(view->local_surface) : NULL;
}

void
_wle_view_min_size_changed(WleView *view, gint width, gint height) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(width >= 0);
    g_return_if_fail(height >= 0);
    g_signal_emit(view, signals[SIG_MIN_SIZE_CHANGED], 0, width, height);
}

void
_wle_view_max_size_changed(WleView *view, gint width, gint height) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(width >= 0);
    g_return_if_fail(height >= 0);
    g_signal_emit(view, signals[SIG_MAX_SIZE_CHANGED], 0, width, height);
}

void
_wle_view_configured(WleView *view, uint32_t serial) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_signal_emit(view, signals[SIG_CONFIGURE], 0, serial);
}

void
_wle_view_map(WleView *view) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_signal_emit(view, signals[SIG_MAP], 0);
}

void
_wle_view_unmap(WleView *view) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_signal_emit(view, signals[SIG_UNMAP], 0);
}

void
_wle_view_activated(WleView *view, uint32_t serial) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_signal_emit(view, signals[SIG_ACTIVATED], 0, serial);
}

void
_wle_view_set_size(WleView *view, uint32_t serial, int32_t width, int32_t height) {
    g_return_if_fail(WLE_IS_VIEW(view));
    if (view->role != NULL) {
        wle_view_role_set_size(view->role, serial, width, height);
    } else {
        view->pending_serial = serial;
        view->pending_set_width = width;
        view->pending_set_height = height;
    }
}

struct wl_surface *
wle_view_get_remote_surface(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    return view->remote_surface;
}

struct wl_resource *
wle_view_get_local_surface(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    return view->local_surface;
}

void
wle_view_set_embedded_offset(WleView *view, int32_t x, int32_t y) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(view->embed_data != NULL);

    if (view->embed_data->remote_subsurface != NULL && (view->embed_data->offset_x != x || view->embed_data->offset_y != y)) {
        DBG("setting wl_subsurface %d position to (%d,%d)", wl_proxy_get_id((struct wl_proxy *)view->embed_data->remote_subsurface), x, y);
        wl_subsurface_set_position(view->embed_data->remote_subsurface, x, y);
        wl_surface_commit(view->remote_surface);
        view->embed_data->offset_x = x;
        view->embed_data->offset_y = y;
        g_signal_emit(view, signals[SIG_EMBED_DATA_CHANGED], 0);
    }
}

void
wle_view_set_embedded_xdg_toplevel(WleView *view, struct xdg_surface *xdg_surface, struct xdg_toplevel *xdg_toplevel) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(view->embed_data != NULL);

    if (view->embed_data->remote_toplevel_xdg_surface != xdg_surface
        || view->embed_data->remote_toplevel_xdg_toplevel != xdg_toplevel)
    {
        view->embed_data->remote_toplevel_xdg_surface = xdg_surface;
        view->embed_data->remote_toplevel_xdg_toplevel = xdg_toplevel;

        if (xdg_surface != NULL || xdg_toplevel != NULL) {
            view->embed_data->remote_toplevel_layer_surface = NULL;
        }

        g_signal_emit(view, signals[SIG_EMBED_DATA_CHANGED], 0);
    }
}

void
wle_view_set_embedded_layer_toplevel(WleView *view, struct zwlr_layer_surface_v1 *layer_surface) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(view->embed_data != NULL);

    if (view->embed_data->remote_toplevel_layer_surface != layer_surface) {
        view->embed_data->remote_toplevel_layer_surface = layer_surface;

        if (layer_surface != NULL) {
            view->embed_data->remote_toplevel_xdg_surface = NULL;
            view->embed_data->remote_toplevel_xdg_toplevel = NULL;
        }

        g_signal_emit(view, signals[SIG_EMBED_DATA_CHANGED], 0);
    }
}

GList *
wle_view_get_outputs(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    return view->outputs;
}

WleViewEmbedData *
wle_view_get_embed_data(WleView *view) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    return view->embed_data;
}

void
wle_view_pointer_enter(WleView *view, WleSeat *seat, uint32_t serial, gdouble x, gdouble y) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(WLE_IS_SEAT(seat));
    g_signal_emit(view, signals[SIG_POINTER_ENTER], 0, seat, serial, x, y);
}

void
wle_view_pointer_leave(WleView *view, WleSeat *seat, uint32_t serial) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(WLE_IS_SEAT(seat));
    g_signal_emit(view, signals[SIG_POINTER_LEAVE], 0, seat, serial);
}

void
wle_view_pointer_motion(WleView *view, WleSeat *seat, uint32_t time, gdouble x, gdouble y) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(WLE_IS_SEAT(seat));
    g_signal_emit(view, signals[SIG_POINTER_MOTION], 0, seat, time, x, y);
}

void
wle_view_pointer_frame(WleView *view, WleSeat *seat) {
    g_return_if_fail(WLE_IS_VIEW(view));
    g_return_if_fail(WLE_IS_SEAT(seat));
    g_signal_emit(view, signals[SIG_POINTER_FRAME], 0, seat);
}
