/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_XDG_SHELL__
#define __WLE_XDG_SHELL__

#include <glib-object.h>
#include <wayland-client-core.h>

#include "protocol/xdg-shell-client-protocol.h"

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleXdgShell, wle_xdg_shell, WLE, XDG_SHELL, GObject)
#define WLE_TYPE_XDG_SHELL (wle_xdg_shell_get_type())

WleXdgShell *wle_xdg_shell_new(struct wl_display *remote_display,
                               struct wl_compositor *remote_compositor,
                               struct xdg_wm_base *remote_xdg_wm_base,
                               struct wl_display *local_display);

G_END_DECLS

#endif /* __WLE_XDG_SHELL__ */
