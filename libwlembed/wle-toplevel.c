/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>
#include <wayland-util.h>

#include "common/wle-debug.h"
#include "protocol/xdg-shell-protocol.h"
#include "wle-error.h"
#include "wle-output.h"
#include "wle-seat-private.h"
#include "wle-toplevel.h"
#include "wle-util.h"
#include "wle-view.h"
#include "wle-xdg-surface.h"

struct _WleToplevel {
    WleXdgSurface parent;

    struct xdg_toplevel *remote_xdg_toplevel;
    struct wl_resource *local_xdg_toplevel;

    // Only used if we are embedded (remote_toplevel will be NULL)
    guint32 visible : 1,
        activated : 1;
};

static void wle_toplevel_finalize(GObject *object);

static void wle_toplevel_commit_state(WleXdgSurface *surface,
                                      uint32_t serial);

static gboolean wle_toplevel_destroy_remote_role(WleViewRole *view_role);
static void wle_toplevel_set_size(WleViewRole *view_role,
                                  uint32_t serial,
                                  int32_t width,
                                  int32_t height);

static void wle_toplevel_destroy(WleLocalResource *local_resource);

static void local_xdg_toplevel_set_app_id(struct wl_client *client,
                                          struct wl_resource *xdg_toplevel_resource,
                                          const char *app_id);
static void local_xdg_toplevel_set_title(struct wl_client *client,
                                         struct wl_resource *xdg_toplevel_resource,
                                         const char *title);
static void local_xdg_toplevel_set_parent(struct wl_client *client,
                                          struct wl_resource *xdg_toplevel_resource,
                                          struct wl_resource *parent_xdg_toplevel);
static void local_xdg_toplevel_show_window_menu(struct wl_client *client,
                                                struct wl_resource *xdg_toplevel_resource,
                                                struct wl_resource *seat_resource,
                                                uint32_t serial,
                                                int32_t x,
                                                int32_t y);
static void local_xdg_toplevel_move(struct wl_client *client,
                                    struct wl_resource *xdg_toplevel_resource,
                                    struct wl_resource *seat_resource,
                                    uint32_t serial);
static void local_xdg_toplevel_resize(struct wl_client *client,
                                      struct wl_resource *xdg_toplevel_resource,
                                      struct wl_resource *seat_resource,
                                      uint32_t serial,
                                      uint32_t edges);
static void local_xdg_toplevel_set_min_size(struct wl_client *client,
                                            struct wl_resource *xdg_toplevel_resource,
                                            int32_t width,
                                            int32_t height);
static void local_xdg_toplevel_set_max_size(struct wl_client *client,
                                            struct wl_resource *xdg_toplevel_resource,
                                            int32_t width,
                                            int32_t height);
static void local_xdg_toplevel_set_maximized(struct wl_client *client,
                                             struct wl_resource *xdg_toplevel_resource);
static void local_xdg_toplevel_unset_maximized(struct wl_client *client,
                                               struct wl_resource *xdg_toplevel_resource);
static void local_xdg_toplevel_set_minimized(struct wl_client *client,
                                             struct wl_resource *xdg_toplevel_resource);
static void local_xdg_toplevel_set_fullscreen(struct wl_client *client,
                                              struct wl_resource *xdg_toplevel_resource,
                                              struct wl_resource *output_resource);
static void local_xdg_toplevel_unset_fullscreen(struct wl_client *client,
                                                struct wl_resource *xdg_toplevel_resource);
static void local_xdg_toplevel_destroy(struct wl_client *client,
                                       struct wl_resource *xdg_toplevel_resource);

static void remote_xdg_toplevel_close(void *data,
                                      struct xdg_toplevel *xdg_toplevel);
static void remote_xdg_toplevel_configure(void *data,
                                          struct xdg_toplevel *xdg_toplevel,
                                          int32_t width,
                                          int32_t height,
                                          struct wl_array *states);
static void remote_xdg_toplevel_configure_bounds(void *data,
                                                 struct xdg_toplevel *xdg_toplevel,
                                                 int32_t width,
                                                 int32_t height);
static void remote_xdg_toplevel_wm_capabilities(void *data,
                                                struct xdg_toplevel *xdg_toplevel,
                                                struct wl_array *capabilities);

#ifndef HAVE_SCANNER_IS_VALID_FUNCTIONS
static inline gboolean xdg_toplevel_state_is_valid(uint32_t value,
                                                   uint32_t version);
#endif

G_DEFINE_FINAL_TYPE(WleToplevel, wle_toplevel, WLE_TYPE_XDG_SURFACE)


static const struct xdg_toplevel_listener remote_xdg_toplevel_listener = {
    .close = remote_xdg_toplevel_close,
    .configure = remote_xdg_toplevel_configure,
    .configure_bounds = remote_xdg_toplevel_configure_bounds,
    .wm_capabilities = remote_xdg_toplevel_wm_capabilities,
};

static const struct xdg_toplevel_interface local_xdg_toplevel_impl = {
    .set_app_id = local_xdg_toplevel_set_app_id,
    .set_title = local_xdg_toplevel_set_title,
    .set_parent = local_xdg_toplevel_set_parent,
    .show_window_menu = local_xdg_toplevel_show_window_menu,
    .move = local_xdg_toplevel_move,
    .resize = local_xdg_toplevel_resize,
    .set_min_size = local_xdg_toplevel_set_min_size,
    .set_max_size = local_xdg_toplevel_set_max_size,
    .set_minimized = local_xdg_toplevel_set_minimized,
    .set_maximized = local_xdg_toplevel_set_maximized,
    .unset_maximized = local_xdg_toplevel_unset_maximized,
    .set_fullscreen = local_xdg_toplevel_set_fullscreen,
    .unset_fullscreen = local_xdg_toplevel_unset_fullscreen,
    .destroy = local_xdg_toplevel_destroy,
};

static void
wle_toplevel_class_init(WleToplevelClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_toplevel_finalize;

    WleXdgSurfaceClass *xdg_surface_class = WLE_XDG_SURFACE_CLASS(klass);
    xdg_surface_class->commit_state = wle_toplevel_commit_state;

    WleViewRoleClass *view_role_class = WLE_VIEW_ROLE_CLASS(klass);
    view_role_class->destroy_remote_role = wle_toplevel_destroy_remote_role;
    view_role_class->set_size = wle_toplevel_set_size;

    WleLocalResourceClass *local_resource_class = WLE_LOCAL_RESOURCE_CLASS(klass);
    local_resource_class->destroy = wle_toplevel_destroy;
}

static void
wle_toplevel_init(WleToplevel *toplevel) {
    toplevel->visible = TRUE;
}

static void
wle_toplevel_finalize(GObject *object) {
    WleToplevel *toplevel = WLE_TOPLEVEL(object);

    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_destroy(toplevel->remote_xdg_toplevel);
    }

    if (toplevel->local_xdg_toplevel != NULL) {
        wl_resource_destroy(toplevel->local_xdg_toplevel);
    }

    G_OBJECT_CLASS(wle_toplevel_parent_class)->finalize(object);
}

static void
wle_toplevel_commit_state(WleXdgSurface *xdg_surface, uint32_t serial) {
    WleToplevel *toplevel = WLE_TOPLEVEL(xdg_surface);
    g_return_if_fail(toplevel->remote_xdg_toplevel == NULL);

    struct wl_array states;
    wl_array_init(&states);

    if (toplevel->activated && toplevel->visible) {
        enum xdg_toplevel_state *state = _wle_assert_nonnull(wl_array_add(&states, sizeof(*state)));
        *state = XDG_TOPLEVEL_STATE_ACTIVATED;
    }

    int32_t width, height;
    if (toplevel->visible) {
        wle_xdg_surface_get_size(WLE_XDG_SURFACE(toplevel), &width, &height);
    } else {
        width = height = 0;
    }

    DBG("configuring toplevel with size %dx%d; serial %u; visible? %d; activated? %d",
        width, height, serial, toplevel->visible, states.size > 1);
    xdg_toplevel_send_configure(toplevel->local_xdg_toplevel, width, height, &states);

    wl_array_release(&states);
}

static gboolean
wle_toplevel_destroy_remote_role(WleViewRole *view_role) {
    WleToplevel *toplevel = WLE_TOPLEVEL(view_role);
    if (toplevel->remote_xdg_toplevel != NULL) {
        DBG("destroying remote xdg toplevel");
        xdg_toplevel_destroy(toplevel->remote_xdg_toplevel);
        toplevel->remote_xdg_toplevel = NULL;
    }
    return WLE_VIEW_ROLE_CLASS(wle_toplevel_parent_class)->destroy_remote_role(view_role);
}

static void
wle_toplevel_set_size(WleViewRole *view_role, uint32_t serial, int32_t width, int32_t height) {
    WleToplevel *toplevel = WLE_TOPLEVEL(view_role);
    if (toplevel->local_xdg_toplevel != NULL) {
        DBG("constraining toplevel size to %dx%d", width, height);
        wle_xdg_surface_update_size(WLE_XDG_SURFACE(toplevel), width, height);
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), serial);
    }
}

static void
wle_toplevel_destroy(WleLocalResource *local_resource) {
    WleToplevel *toplevel = WLE_TOPLEVEL(local_resource);

    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_destroy(toplevel->remote_xdg_toplevel);
        toplevel->remote_xdg_toplevel = NULL;
    }

    if (toplevel->local_xdg_toplevel != NULL) {
        wl_resource_destroy(toplevel->local_xdg_toplevel);
        toplevel->local_xdg_toplevel = NULL;
    }

    WLE_LOCAL_RESOURCE_CLASS(wle_toplevel_parent_class)->destroy(local_resource);
}

static void
local_xdg_toplevel_set_app_id(struct wl_client *client, struct wl_resource *xdg_toplevel_resource, const char *app_id) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_set_app_id(toplevel->remote_xdg_toplevel, app_id);
    }
}

static void
local_xdg_toplevel_set_title(struct wl_client *client, struct wl_resource *xdg_toplevel_resource, const char *title) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_set_title(toplevel->remote_xdg_toplevel, title);
    }
}

static void
local_xdg_toplevel_set_parent(struct wl_client *client,
                              struct wl_resource *xdg_toplevel_resource,
                              struct wl_resource *parent_xdg_toplevel) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        if (parent_xdg_toplevel != NULL) {
            WleToplevel *parent = WLE_TOPLEVEL(wl_resource_get_user_data(parent_xdg_toplevel));
            WleViewEmbedData *embed_data = wle_view_get_embed_data(wle_xdg_surface_get_view(WLE_XDG_SURFACE(parent)));
            if (embed_data != NULL) {
                if (embed_data->remote_toplevel_xdg_surface) {
                    xdg_toplevel_set_parent(toplevel->remote_xdg_toplevel, embed_data->remote_toplevel_xdg_toplevel);
                }
            } else if (parent->remote_xdg_toplevel != NULL) {
                xdg_toplevel_set_parent(toplevel->remote_xdg_toplevel, parent->remote_xdg_toplevel);
            } else {
                g_warning("unable to set toplevel parent");
            }
        } else {
            xdg_toplevel_set_parent(toplevel->remote_xdg_toplevel, NULL);
        }
    }
}

static void
local_xdg_toplevel_show_window_menu(struct wl_client *client,
                                    struct wl_resource *xdg_toplevel_resource,
                                    struct wl_resource *seat_resource,
                                    uint32_t serial,
                                    int32_t x,
                                    int32_t y) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(seat_resource));
        xdg_toplevel_show_window_menu(toplevel->remote_xdg_toplevel, _wle_seat_get_remote_seat(seat), serial, x, y);
    }
}

static void
local_xdg_toplevel_move(struct wl_client *client,
                        struct wl_resource *xdg_toplevel_resource,
                        struct wl_resource *seat_resource,
                        uint32_t serial) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(seat_resource));
        xdg_toplevel_move(toplevel->remote_xdg_toplevel, _wle_seat_get_remote_seat(seat), serial);
    }
}

static void
local_xdg_toplevel_resize(struct wl_client *client,
                          struct wl_resource *xdg_toplevel_resource,
                          struct wl_resource *seat_resource,
                          uint32_t serial,
                          uint32_t edges) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        WleSeat *seat = WLE_SEAT(wl_resource_get_user_data(seat_resource));
        xdg_toplevel_resize(toplevel->remote_xdg_toplevel, _wle_seat_get_remote_seat(seat), serial, edges);
    }
}

static void
local_xdg_toplevel_set_min_size(struct wl_client *client,
                                struct wl_resource *xdg_toplevel_resource,
                                int32_t width,
                                int32_t height) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_set_min_size(toplevel->remote_xdg_toplevel, width, height);
    }
    _wle_view_min_size_changed(wle_xdg_surface_get_view(WLE_XDG_SURFACE(toplevel)), width, height);
}

static void
local_xdg_toplevel_set_max_size(struct wl_client *client,
                                struct wl_resource *xdg_toplevel_resource,
                                int32_t width,
                                int32_t height) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_set_max_size(toplevel->remote_xdg_toplevel, width, height);
    }
    _wle_view_max_size_changed(wle_xdg_surface_get_view(WLE_XDG_SURFACE(toplevel)), width, height);
}

static void
local_xdg_toplevel_set_maximized(struct wl_client *client, struct wl_resource *xdg_toplevel_resource) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_set_maximized(toplevel->remote_xdg_toplevel);
    } else {
        struct wl_display *local_display = wl_client_get_display(client);
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), wl_display_next_serial(local_display));
    }
}

static void
local_xdg_toplevel_unset_maximized(struct wl_client *client, struct wl_resource *xdg_toplevel_resource) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_unset_maximized(toplevel->remote_xdg_toplevel);
    } else {
        struct wl_display *local_display = wl_client_get_display(client);
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), wl_display_next_serial(local_display));
    }
}

static void
local_xdg_toplevel_set_minimized(struct wl_client *client, struct wl_resource *xdg_toplevel_resource) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_set_minimized(toplevel->remote_xdg_toplevel);
    } else {
        struct wl_display *local_display = wl_client_get_display(client);
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), wl_display_next_serial(local_display));
    }
}

static void
local_xdg_toplevel_set_fullscreen(struct wl_client *client,
                                  struct wl_resource *xdg_toplevel_resource,
                                  struct wl_resource *output_resource) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        WleOutput *output = output_resource != NULL ? WLE_OUTPUT(wl_resource_get_user_data(output_resource)) : NULL;
        xdg_toplevel_set_fullscreen(toplevel->remote_xdg_toplevel,
                                    output != NULL ? wle_output_get_remote_output(output) : NULL);
    } else {
        struct wl_display *local_display = wl_client_get_display(client);
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), wl_display_next_serial(local_display));
    }
}

static void
local_xdg_toplevel_unset_fullscreen(struct wl_client *client, struct wl_resource *xdg_toplevel_resource) {
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(xdg_toplevel_resource));
    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_unset_fullscreen(toplevel->remote_xdg_toplevel);
    } else {
        struct wl_display *local_display = wl_client_get_display(client);
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), wl_display_next_serial(local_display));
    }
}

static void
local_xdg_toplevel_destroy(struct wl_client *client, struct wl_resource *xdg_toplevel_resource) {
    wl_resource_destroy(xdg_toplevel_resource);
}

static void
local_xdg_toplevel_destroy_impl(struct wl_resource *resource) {
    TRACE("entering");
    WleToplevel *toplevel = WLE_TOPLEVEL(wl_resource_get_user_data(resource));

    if (toplevel->remote_xdg_toplevel != NULL) {
        xdg_toplevel_destroy(toplevel->remote_xdg_toplevel);
        toplevel->remote_xdg_toplevel = NULL;
    }

    toplevel->local_xdg_toplevel = NULL;
    wle_local_resource_destroyed(WLE_LOCAL_RESOURCE(toplevel));
    g_object_unref(toplevel);
}

static void
remote_xdg_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel) {
    TRACE("entering");
    WleToplevel *toplevel = WLE_TOPLEVEL(data);
    xdg_toplevel_send_close(toplevel->local_xdg_toplevel);
}

static void
remote_xdg_toplevel_configure(void *data,
                              struct xdg_toplevel *xdg_toplevel,
                              int32_t width,
                              int32_t height,
                              struct wl_array *states) {
    WleToplevel *toplevel = WLE_TOPLEVEL(data);
    DBG("toplevel configure: %dx%d", width, height);

    struct wl_array filtered_states;
    wl_array_init(&filtered_states);

    int resource_version = wl_resource_get_version(toplevel->local_xdg_toplevel);
    enum xdg_toplevel_state *state;
    wl_array_for_each(state, states) {
        DBG("toplevel configure state: %d", *state);

        if (xdg_toplevel_state_is_valid(*state, resource_version)) {
            enum xdg_toplevel_state *new_state = _wle_assert_nonnull(wl_array_add(&filtered_states, sizeof(*new_state)));
            *new_state = *state;
        }
    }

    wle_xdg_surface_update_size(WLE_XDG_SURFACE(toplevel), width, height);
    xdg_toplevel_send_configure(toplevel->local_xdg_toplevel, width, height, &filtered_states);

    wl_array_release(&filtered_states);
}

static void
remote_xdg_toplevel_configure_bounds(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height) {
    DBG("bounds: %dx%d", width, height);
    WleToplevel *toplevel = WLE_TOPLEVEL(data);
    if (wl_resource_get_version(toplevel->local_xdg_toplevel) >= XDG_TOPLEVEL_CONFIGURE_BOUNDS_SINCE_VERSION) {
        xdg_toplevel_send_configure_bounds(toplevel->local_xdg_toplevel, width, height);
    }
}

static void
remote_xdg_toplevel_wm_capabilities(void *data, struct xdg_toplevel *xdg_toplevel, struct wl_array *capabilities) {
    DBG("wm caps");
    WleToplevel *toplevel = WLE_TOPLEVEL(data);
    if (wl_resource_get_version(toplevel->local_xdg_toplevel) >= XDG_TOPLEVEL_WM_CAPABILITIES_SINCE_VERSION) {
        xdg_toplevel_send_wm_capabilities(toplevel->local_xdg_toplevel, capabilities);
    }
}

#ifndef HAVE_SCANNER_IS_VALID_FUNCTIONS
static inline gboolean
xdg_toplevel_state_is_valid(uint32_t value, uint32_t version) {
    switch (value) {
        case XDG_TOPLEVEL_STATE_MAXIMIZED:
            return version >= 1;
        case XDG_TOPLEVEL_STATE_FULLSCREEN:
            return version >= 1;
        case XDG_TOPLEVEL_STATE_RESIZING:
            return version >= 1;
        case XDG_TOPLEVEL_STATE_ACTIVATED:
            return version >= 1;
        case XDG_TOPLEVEL_STATE_TILED_LEFT:
            return version >= 2;
        case XDG_TOPLEVEL_STATE_TILED_RIGHT:
            return version >= 2;
        case XDG_TOPLEVEL_STATE_TILED_TOP:
            return version >= 2;
        case XDG_TOPLEVEL_STATE_TILED_BOTTOM:
            return version >= 2;
#ifdef XDG_TOPLEVEL_STATE_SUSPENDED_SINCE_VERSION
        case XDG_TOPLEVEL_STATE_SUSPENDED:
            return version >= 6;
#endif
        default:
            return FALSE;
    }
}
#endif

WleToplevel *
_wle_toplevel_new(WleView *view,
                  struct xdg_surface *remote_xdg_surface,
                  struct wl_resource *local_xdg_surface,
                  struct xdg_toplevel *remote_xdg_toplevel,
                  struct wl_resource *local_xdg_toplevel) {
    g_return_val_if_fail(WLE_IS_VIEW(view), NULL);
    g_return_val_if_fail(remote_xdg_surface != NULL, NULL);
    g_return_val_if_fail(local_xdg_surface != NULL, NULL);
    g_return_val_if_fail(remote_xdg_toplevel != NULL, NULL);
    g_return_val_if_fail(local_xdg_toplevel != NULL, NULL);

    WleToplevel *toplevel = g_object_new(WLE_TYPE_TOPLEVEL,
                                         "role", WLE_ROLE_XDG_TOPLEVEL,
                                         "view", view,
                                         "remote-xdg-surface", remote_xdg_surface,
                                         "local-xdg-surface", local_xdg_surface,
                                         NULL);
    toplevel->local_xdg_toplevel = local_xdg_toplevel;
    toplevel->remote_xdg_toplevel = remote_xdg_toplevel;

    xdg_toplevel_add_listener(remote_xdg_toplevel, &remote_xdg_toplevel_listener, toplevel);
    wl_resource_set_implementation(local_xdg_toplevel,
                                   &local_xdg_toplevel_impl,
                                   toplevel,
                                   local_xdg_toplevel_destroy_impl);

    // local_xdg_toplevel takes a reference
    g_object_ref(toplevel);

    return toplevel;
}

struct xdg_toplevel *
_wle_toplevel_get_remote_xdg_toplevel(WleToplevel *toplevel) {
    g_return_val_if_fail(WLE_IS_TOPLEVEL(toplevel), NULL);
    return toplevel->remote_xdg_toplevel;
}

gboolean
wle_toplevel_is_visible(WleToplevel *toplevel) {
    g_return_val_if_fail(WLE_IS_TOPLEVEL(toplevel), FALSE);
    return toplevel->visible;
}

void
wle_toplevel_show(WleToplevel *toplevel) {
    g_return_if_fail(WLE_IS_TOPLEVEL(toplevel));

    if (toplevel->remote_xdg_toplevel == NULL && !toplevel->visible) {
        toplevel->visible = TRUE;
        uint32_t serial = wl_display_get_serial(wl_client_get_display(wl_resource_get_client(toplevel->local_xdg_toplevel)));  // FIXME
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), serial);
    }
}

void
wle_toplevel_hide(WleToplevel *toplevel) {
    g_return_if_fail(WLE_IS_TOPLEVEL(toplevel));

    if (toplevel->remote_xdg_toplevel == NULL && toplevel->visible) {
        toplevel->visible = FALSE;
        uint32_t serial = wl_display_get_serial(wl_client_get_display(wl_resource_get_client(toplevel->local_xdg_toplevel)));  // FIXME
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), serial);
    }
}

void
wle_toplevel_activate(WleToplevel *toplevel, uint32_t serial) {
    TRACE("entering");
    g_return_if_fail(WLE_IS_TOPLEVEL(toplevel));

    if (toplevel->remote_xdg_toplevel == NULL) {
        toplevel->activated = TRUE;
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), serial);
    }
}

void
wle_toplevel_deactivate(WleToplevel *toplevel, uint32_t serial) {
    TRACE("entering");
    g_return_if_fail(WLE_IS_TOPLEVEL(toplevel));

    if (toplevel->remote_xdg_toplevel == NULL) {
        toplevel->activated = FALSE;
        wle_xdg_surface_send_configure(WLE_XDG_SURFACE(toplevel), serial);
    }
}
