/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_VIEW_ROLE_H__
#define __WLE_VIEW_ROLE_H__

#include <glib-object.h>
#include <stdint.h>

#include "wle-local-resource.h"

G_BEGIN_DECLS

G_DECLARE_DERIVABLE_TYPE(WleViewRole, wle_view_role, WLE, VIEW_ROLE, WleLocalResource)
#define WLE_TYPE_VIEW_ROLE (wle_view_role_get_type())
#define WLE_TYPE_ROLE (wle_role_get_type())

struct _WleViewRoleClass {
    WleLocalResourceClass parent_class;

    gboolean (*destroy_remote_role)(WleViewRole *view_role);

    void (*set_size)(WleViewRole *view_role,
                     uint32_t serial,
                     int32_t width,
                     int32_t height);
};

typedef enum {
    WLE_ROLE_NONE,
    WLE_ROLE_SUBSURFACE,
    WLE_ROLE_XDG_POPUP,
    WLE_ROLE_XDG_TOPLEVEL,
} WleRole;

GType wle_role_get_type(void) G_GNUC_CONST;

gboolean wle_view_role_destroy_remote_role(WleViewRole *view_role);

void wle_view_role_set_size(WleViewRole *view_role,
                            uint32_t serial,
                            int32_t width,
                            int32_t height);

G_END_DECLS

#endif /* __WLE_VIEW_ROLE_H__ */
