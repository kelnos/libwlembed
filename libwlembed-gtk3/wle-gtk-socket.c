/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SECTION:wle-gtk-socket
 * @title: WleGtkSocket
 * @short_description: a GTK widget container for an embedded application
 *                     (parent-side)
 * @stability: Unstable
 * @include: libwlembed-gtk/libwlembed-gtk3.h
 *
 * #WleGtkSocket is a container that holds the drawing area for an embedded
 * application.  It can be used somewhat analogously to #GtkSocket, and hides
 * many of the details around sizing and event handling for you.
 **/

#include <gdk/gdkwayland.h>
#include <libwlembed/libwlembed.h>

#ifdef HAVE_GTK3_LAYER_SHELL
#include <gtk-layer-shell.h>
#endif

#include "common/wle-debug.h"
#include "wle-abuse-gtk3.h"
#include "wle-gtk-socket.h"
#include "libwlembed-gtk3-visibility.h"

#define WLE_SEAT_KEY "--wle-seat"

struct _WleGtkSocket {
    GtkEventBox parent;

    WleEmbeddedCompositor *ec;
    gchar *embedding_token;

    WleEmbeddedView *embedded_view;

    gint minimum_width;
    gint minimum_height;
    gint maximum_width;
    gint maximum_height;

    gint requested_width;
    gint requested_height;

    GtkWidget *parent_window_widget;
    GtkWidget *toplevel;
    uint32_t last_focus_serial;

    GList *entered_seats;
    uint32_t last_pointer_serial;
};

enum {
    PROP0,
    PROP_EMBEDDED_COMPOSITOR,
    PROP_EMBEDDED_VIEW,
};

enum {
    SIG_PLUG_ADDED,
    SIG_PLUG_REMOVED,

    N_SIGNALS,
};


static void wle_gtk_socket_constructed(GObject *object);
static void wle_gtk_socket_set_property(GObject *object,
                                        guint prop_id,
                                        const GValue *value,
                                        GParamSpec *pspec);
static void wle_gtk_socket_get_property(GObject *object,
                                        guint prop_id,
                                        GValue *value,
                                        GParamSpec *pspec);
static void wle_gtk_socket_finalize(GObject *object);

static void wle_gtk_socket_size_allocate(GtkWidget *widget,
                                         GtkAllocation *allocation);
static void wle_gtk_socket_get_preferred_height(GtkWidget *widget,
                                                gint *minimum_height,
                                                gint *natural_height);
static void wle_gtk_socket_get_preferred_height_for_width(GtkWidget *widget,
                                                          gint width,
                                                          gint *minimum_height,
                                                          gint *natural_height);
static void wle_gtk_socket_get_preferred_width(GtkWidget *widget,
                                               gint *minimum_width,
                                               gint *natural_width);
static void wle_gtk_socket_get_preferred_width_for_height(GtkWidget *widget,
                                                          gint height,
                                                          gint *minimum_width,
                                                          gint *natural_width);
static void wle_gtk_socket_hierarchy_changed(GtkWidget *widget,
                                             GtkWidget *previous_toplevel);
static void wle_gtk_socket_realize(GtkWidget *widget);
static void wle_gtk_socket_map(GtkWidget *widget);
static void wle_gtk_socket_unmap(GtkWidget *widget);
static gboolean wle_gtk_socket_key_press(GtkWidget *widget,
                                         GdkEventKey *event);
static gboolean wle_gtk_socket_key_release(GtkWidget *widget,
                                           GdkEventKey *event);
static gboolean wle_gtk_socket_button_press(GtkWidget *widget,
                                            GdkEventButton *event);
static gboolean wle_gtk_socket_button_release(GtkWidget *widget,
                                              GdkEventButton *event);
static gboolean wle_gtk_socket_touch(GtkWidget *widget,
                                     GdkEventTouch *event);
static gboolean wle_gtk_socket_scroll(GtkWidget *widget,
                                      GdkEventScroll *event);
static gboolean wle_gtk_socket_motion_notify(GtkWidget *widget,
                                             GdkEventMotion *event);
static gboolean wle_gtk_socket_focus_in(GtkWidget *widget,
                                        GdkEventFocus *event);
static gboolean wle_gtk_socket_focus_out(GtkWidget *widget,
                                         GdkEventFocus *event);
static gboolean wle_gtk_socket_focus(GtkWidget *widget,
                                     GtkDirectionType direction);

static void update_toplevel_surfaces(WleGtkSocket *socket);

static void toplevel_map(GtkWidget *widget,
                         WleGtkSocket *socket);
static void toplevel_unmap(GtkWidget *widget,
                           WleGtkSocket *socket);
static void toplevel_size_allocate(GtkWidget *widget,
                                   GtkAllocation *allocation,
                                   WleGtkSocket *socket);
static void toplevel_is_active_changed(GtkWidget *toplevel,
                                       GParamSpec *pspec,
                                       WleGtkSocket *socket);
static void toplevel_destroy(GtkWidget *widget,
                             WleGtkSocket *socket);

static void parent_window_widget_destroy(GtkWidget *widget,
                                         WleGtkSocket *socket);

static void embedded_view_size_request(WleEmbeddedView *view,
                                       gint width,
                                       gint height,
                                       WleGtkSocket *socket);
static void embedded_view_focused(WleEmbeddedView *view,
                                  WleGtkSocket *socket);
static void embedded_view_unfocused(WleEmbeddedView *view,
                                    WleGtkSocket *socket);

static gboolean embedded_compositor_new_embedded_view(WleEmbeddedCompositor *ec,
                                                      const gchar *embedding_token,
                                                      WleEmbeddedView *embedded_view,
                                                      WleGtkSocket *socket);
static void embedded_compositor_closed(WleEmbeddedCompositor *ec,
                                       GError *error,
                                       WleGtkSocket *socket);

static WleSeat *get_wle_seat_for_gdk_event(WleGtkSocket *socket,
                                           GdkEvent *event);

static void update_parent_window_widget(WleGtkSocket *socket);
static void show_embedded_view(WleGtkSocket *socket);
static void hide_embedded_view(WleGtkSocket *socket);
static void update_embedded_view_geometry(WleGtkSocket *socket);

static void embedded_view_pointer_leave(WleGtkSocket *socket,
                                        WleSeat *seat,
                                        uint32_t serial);

G_DEFINE_TYPE(WleGtkSocket, wle_gtk_socket, GTK_TYPE_BIN)


static guint signals[N_SIGNALS] = { 0 };


static void
wle_gtk_socket_class_init(WleGtkSocketClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->constructed = wle_gtk_socket_constructed;
    gobject_class->set_property = wle_gtk_socket_set_property;
    gobject_class->get_property = wle_gtk_socket_get_property;
    gobject_class->finalize = wle_gtk_socket_finalize;

    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
    widget_class->size_allocate = wle_gtk_socket_size_allocate;
    widget_class->get_preferred_height = wle_gtk_socket_get_preferred_height;
    widget_class->get_preferred_height_for_width = wle_gtk_socket_get_preferred_height_for_width;
    widget_class->get_preferred_width = wle_gtk_socket_get_preferred_width;
    widget_class->get_preferred_width_for_height = wle_gtk_socket_get_preferred_width_for_height;
    widget_class->hierarchy_changed = wle_gtk_socket_hierarchy_changed;
    widget_class->realize = wle_gtk_socket_realize;
    widget_class->unmap = wle_gtk_socket_unmap;
    widget_class->key_press_event = wle_gtk_socket_key_press;
    widget_class->key_release_event = wle_gtk_socket_key_release;
    widget_class->button_press_event = wle_gtk_socket_button_press;
    widget_class->button_release_event = wle_gtk_socket_button_release;
    widget_class->touch_event = wle_gtk_socket_touch;
    widget_class->motion_notify_event = wle_gtk_socket_motion_notify;
    widget_class->scroll_event = wle_gtk_socket_scroll;
    widget_class->focus_in_event = wle_gtk_socket_focus_in;
    widget_class->focus_out_event = wle_gtk_socket_focus_out;
    widget_class->focus = wle_gtk_socket_focus;

    /**
     * WleGtkSocket:embedded-compositor:
     *
     * The #WleEmbeddedCompositor that manages this socket.
     **/
    g_object_class_install_property(gobject_class,
                                    PROP_EMBEDDED_COMPOSITOR,
                                    g_param_spec_object("embedded-compositor",
                                                        "embedded-compositor",
                                                        "embedded-compositor",
                                                        WLE_TYPE_EMBEDDED_COMPOSITOR,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    /**
     * WleGtkSocket:embedded-view:
     *
     * The #WleEmbeddedView instance, present after the embedded
     * application has been embedded into the socket.
     **/
    g_object_class_install_property(gobject_class,
                                    PROP_EMBEDDED_VIEW,
                                    g_param_spec_object("embedded-view",
                                                        "embedded-view",
                                                        "embedded-view",
                                                        WLE_TYPE_EMBEDDED_VIEW,
                                                        G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

    /**
     * WleGtkSocket::plug-added:
     * @socket: A #WleGtkSocket.
     *
     * Emitted after the embedded application has mapped its first toplevel
     * window.
     **/
    signals[SIG_PLUG_ADDED] = g_signal_new("plug-added",
                                           WLE_TYPE_GTK_SOCKET,
                                           G_SIGNAL_RUN_LAST,
                                           0,
                                           NULL, NULL,
                                           g_cclosure_marshal_VOID__VOID,
                                           G_TYPE_NONE, 0);

    /**
     * WleGtkSocket::plug-removed:
     * @socket: A #WleGtkSocket.
     * @error: An error describing how the embed closed, or %NULL if it closed
     *         normally.
     *
     * Emitted after the embedded application has quit or unmapped its
     * embedded window.
     **/
    signals[SIG_PLUG_REMOVED] = g_signal_new("plug-removed",
                                             WLE_TYPE_GTK_SOCKET,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__BOXED,
                                             G_TYPE_NONE, 1,
                                             G_TYPE_ERROR);
}

static void
wle_gtk_socket_init(WleGtkSocket *socket) {
    socket->maximum_width = -1;
    socket->maximum_height = -1;

    gtk_widget_set_can_focus(GTK_WIDGET(socket), TRUE);
    gtk_widget_set_has_window(GTK_WIDGET(socket), TRUE);
    gtk_widget_add_events(GTK_WIDGET(socket),
                          GDK_ENTER_NOTIFY_MASK
                              | GDK_LEAVE_NOTIFY_MASK
                              | GDK_POINTER_MOTION_MASK
                              | GDK_BUTTON_PRESS_MASK
                              | GDK_BUTTON_RELEASE_MASK
                              | GDK_BUTTON_MOTION_MASK);

    g_signal_connect(socket, "map",
                     G_CALLBACK(wle_gtk_socket_map), NULL);
}

static void
wle_gtk_socket_constructed(GObject *object) {
    G_OBJECT_CLASS(wle_gtk_socket_parent_class)->constructed(object);

    WleGtkSocket *socket = WLE_GTK_SOCKET(object);
    socket->embedding_token = g_strdup(wle_embedded_compositor_generate_embedding_token(socket->ec));

    g_signal_connect(socket->ec, "new-embedded-view",
                     G_CALLBACK(embedded_compositor_new_embedded_view), socket);
    g_signal_connect_swapped(socket->ec, "closed",
                             G_CALLBACK(embedded_compositor_closed), socket);
}

static void
wle_gtk_socket_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec) {
    WleGtkSocket *socket = WLE_GTK_SOCKET(object);

    switch (prop_id) {
        case PROP_EMBEDDED_COMPOSITOR:
            socket->ec = g_value_get_object(value);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_gtk_socket_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec) {
    WleGtkSocket *socket = WLE_GTK_SOCKET(object);

    switch (prop_id) {
        case PROP_EMBEDDED_COMPOSITOR:
            g_value_set_object(value, socket->ec);
            break;

        case PROP_EMBEDDED_VIEW:
            g_value_set_object(value, socket->embedded_view);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_gtk_socket_finalize(GObject *object) {
    WleGtkSocket *socket = WLE_GTK_SOCKET(object);

    g_free(socket->embedding_token);

    if (socket->parent_window_widget != NULL) {
        parent_window_widget_destroy(socket->parent_window_widget, socket);
    }

    if (socket->toplevel != NULL) {
        toplevel_destroy(socket->toplevel, socket);
    }

    g_list_free(socket->entered_seats);

    if (socket->embedded_view != NULL) {
        g_signal_handlers_disconnect_by_data(socket->embedded_view, socket);
        wle_embedded_view_destroy(socket->embedded_view);
    }

    if (socket->ec != NULL) {
        g_signal_handlers_disconnect_by_data(socket->ec, socket);
    }

    G_OBJECT_CLASS(wle_gtk_socket_parent_class)->finalize(object);
}

static void
wle_gtk_socket_size_allocate(GtkWidget *widget, GtkAllocation *allocation) {
    GTK_WIDGET_CLASS(wle_gtk_socket_parent_class)->size_allocate(widget, allocation);
    update_embedded_view_geometry(WLE_GTK_SOCKET(widget));
}

static void
wle_gtk_socket_get_preferred_height(GtkWidget *widget, gint *minimum_height, gint *natural_height) {
    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);

    if (minimum_height != NULL) {
        *minimum_height = MAX(socket->minimum_height, socket->requested_height);
    }
    if (natural_height != NULL) {
        if (socket->requested_height > 0) {
            *natural_height = socket->requested_height;
        } else if (socket->maximum_height != -1) {
            *natural_height = socket->maximum_height;
        } else {
            *natural_height = socket->minimum_height;
        }
    }
}

static void
wle_gtk_socket_get_preferred_height_for_width(GtkWidget *widget,
                                              gint width,
                                              gint *minimum_height,
                                              gint *natural_height) {
    wle_gtk_socket_get_preferred_height(widget, minimum_height, natural_height);
}


static void
wle_gtk_socket_get_preferred_width(GtkWidget *widget, gint *minimum_width, gint *natural_width) {
    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);

    if (minimum_width != NULL) {
        *minimum_width = MAX(socket->minimum_width, socket->requested_width);
    }
    if (natural_width != NULL) {
        if (socket->requested_width > 0) {
            *natural_width = socket->requested_width;
        } else if (socket->maximum_width != -1) {
            *natural_width = socket->maximum_width;
        } else {
            *natural_width = socket->minimum_width;
        }
    }
}

static void
wle_gtk_socket_get_preferred_width_for_height(GtkWidget *widget,
                                              gint height,
                                              gint *minimum_width,
                                              gint *natural_width) {
    wle_gtk_socket_get_preferred_width(widget, minimum_width, natural_width);
}

static void
wle_gtk_socket_hierarchy_changed(GtkWidget *widget, GtkWidget *previous_toplevel) {
    TRACE("entering");

    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(wle_gtk_socket_parent_class);
    if (widget_class->hierarchy_changed != NULL) {
        widget_class->hierarchy_changed(widget, previous_toplevel);
    }

    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);
    if (socket->toplevel != gtk_widget_get_toplevel(widget)) {
        if (socket->toplevel != NULL) {
            g_signal_handlers_disconnect_by_data(socket->toplevel, socket);
            socket->toplevel = NULL;
        }

        GtkWidget *toplevel = gtk_widget_get_toplevel(widget);
        if (GTK_IS_WINDOW(toplevel)) {
            socket->toplevel = toplevel;

            g_signal_connect(socket->toplevel, "map",
                             G_CALLBACK(toplevel_map), socket);
            g_signal_connect(socket->toplevel, "unmap",
                             G_CALLBACK(toplevel_unmap), socket);
            g_signal_connect(socket->toplevel, "size-allocate",
                             G_CALLBACK(toplevel_size_allocate), socket);
            g_signal_connect(socket->toplevel, "notify::is-active",
                             G_CALLBACK(toplevel_is_active_changed), socket);
            g_signal_connect(socket->toplevel, "destroy",
                             G_CALLBACK(toplevel_destroy), socket);
        }

        update_parent_window_widget(socket);
        update_toplevel_surfaces(socket);

        if (socket->embedded_view != NULL && socket->toplevel != NULL && gtk_widget_get_mapped(socket->toplevel)) {
            GtkAllocation toplevel_allocation;
            gtk_widget_get_allocation(socket->toplevel, &toplevel_allocation);
            if (toplevel_allocation.width > 1 && toplevel_allocation.height > 1) {
                toplevel_size_allocate(socket->toplevel, &toplevel_allocation, socket);
            }
            toplevel_is_active_changed(socket->toplevel, NULL, socket);
        }
    }
}

static void
wle_gtk_socket_realize(GtkWidget *widget) {
    gtk_widget_set_realized(widget, TRUE);

    GtkAllocation allocation;
    gtk_widget_get_allocation(widget, &allocation);

    GdkWindowAttr attributes;
    attributes.x = allocation.x;
    attributes.y = allocation.y;
    attributes.width = allocation.width;
    attributes.height = allocation.height;
    attributes.window_type = GDK_WINDOW_SUBSURFACE;
    attributes.event_mask = gtk_widget_get_events(widget)
                            | GDK_BUTTON_MOTION_MASK
                            | GDK_BUTTON_PRESS_MASK
                            | GDK_BUTTON_RELEASE_MASK
                            | GDK_EXPOSURE_MASK
                            | GDK_ENTER_NOTIFY_MASK
                            | GDK_LEAVE_NOTIFY_MASK;
    attributes.visual = gtk_widget_get_visual(widget);
    attributes.wclass = GDK_INPUT_OUTPUT;

    GdkWindow *window = gdk_window_new(gtk_widget_get_parent_window(widget),
                                       &attributes,
                                       GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL);
    gtk_widget_set_window(widget, window);
    gtk_widget_register_window(widget, window);

    update_parent_window_widget(WLE_GTK_SOCKET(widget));
}

static void
wle_gtk_socket_map(GtkWidget *widget) {
    // GTK_WIDGET_CLASS(wle_gtk_socket_parent_class)->map(widget);

    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);
    if (socket->embedded_view != NULL
        && socket->toplevel != NULL
        && gtk_widget_get_mapped(socket->toplevel)
        && gdk_wayland_window_get_wl_surface(gtk_widget_get_window(socket->toplevel)) != NULL)
    {
        update_parent_window_widget(socket);
        toplevel_map(socket->toplevel, socket);
    }
}

static void
wle_gtk_socket_unmap(GtkWidget *widget) {
    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);
    hide_embedded_view(socket);
    GTK_WIDGET_CLASS(wle_gtk_socket_parent_class)->unmap(widget);
}

static gboolean
wle_gtk_socket_key_press(GtkWidget *widget, GdkEventKey *event) {
    return WLE_GTK_SOCKET(widget)->embedded_view != NULL;
}

static gboolean
wle_gtk_socket_key_release(GtkWidget *widget, GdkEventKey *event) {
    return WLE_GTK_SOCKET(widget)->embedded_view != NULL;
}

static gboolean
wle_gtk_socket_button_press(GtkWidget *widget, GdkEventButton *event) {
    return WLE_GTK_SOCKET(widget)->embedded_view != NULL;
}

static gboolean
wle_gtk_socket_button_release(GtkWidget *widget, GdkEventButton *event) {
    return WLE_GTK_SOCKET(widget)->embedded_view != NULL;
}

static gboolean
wle_gtk_socket_touch(GtkWidget *widget, GdkEventTouch *event) {
    return WLE_GTK_SOCKET(widget)->embedded_view != NULL;
}

static gboolean
wle_gtk_socket_scroll(GtkWidget *widget, GdkEventScroll *event) {
    return WLE_GTK_SOCKET(widget)->embedded_view != NULL;
}

static gboolean
wle_gtk_socket_motion_notify(GtkWidget *widget, GdkEventMotion *event) {
    return WLE_GTK_SOCKET(widget)->embedded_view != NULL;
}

static gboolean
wle_gtk_socket_focus_in(GtkWidget *widget, GdkEventFocus *event) {
    TRACE("entering");
    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);
    if (socket->embedded_view != NULL) {
        WleSeat *seat = get_wle_seat_for_gdk_event(socket, (GdkEvent *)event);
        wle_embedded_view_focus(socket->embedded_view, seat);
    }

    return FALSE;
}

static gboolean
wle_gtk_socket_focus_out(GtkWidget *widget, GdkEventFocus *event) {
    TRACE("entering");
    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);
    if (socket->embedded_view != NULL) {
        WleSeat *seat = get_wle_seat_for_gdk_event(socket, (GdkEvent *)event);
        wle_embedded_view_unfocus(socket->embedded_view, seat);
    }

    return FALSE;
}

static gboolean
wle_gtk_socket_focus(GtkWidget *widget, GtkDirectionType direction) {
    WleGtkSocket *socket = WLE_GTK_SOCKET(widget);

    if (socket->embedded_view != NULL && !gtk_widget_is_focus(widget)) {
        switch (direction) {
            case GTK_DIR_UP:
            case GTK_DIR_LEFT:
            case GTK_DIR_TAB_BACKWARD:
                socket->last_focus_serial = wle_embedded_view_focus_last(socket->embedded_view);
                break;
            case GTK_DIR_DOWN:
            case GTK_DIR_RIGHT:
            case GTK_DIR_TAB_FORWARD:
                socket->last_focus_serial = wle_embedded_view_focus_first(socket->embedded_view);
                break;
        }

        gtk_widget_grab_focus(widget);
        return TRUE;
    } else {
        return FALSE;
    }
}

static void
update_toplevel_surfaces(WleGtkSocket *socket) {
    if (socket->embedded_view != NULL) {
        if (socket->toplevel != NULL && gtk_widget_get_mapped(socket->toplevel)) {
#ifdef HAVE_GTK3_LAYER_SHELL
            if (GTK_IS_WINDOW(socket->toplevel) && gtk_layer_is_layer_window(GTK_WINDOW(socket->toplevel))) {
                wle_embedded_view_set_toplevel_layer_surface(socket->embedded_view,
                                                             gtk_layer_get_zwlr_layer_surface_v1(GTK_WINDOW(socket->toplevel)));
            } else
#endif
            {
                GdkWindow *parent_window = gtk_widget_get_window(socket->toplevel);
                if (parent_window != NULL) {
                    struct xdg_surface *parent_xdg_surface = abuse_gdk_wayland_window_get_xdg_surface(parent_window);
                    struct xdg_toplevel *parent_xdg_toplevel = abuse_gdk_wayland_window_get_xdg_toplevel(parent_window);
                    wle_embedded_view_set_toplevel_xdg_surface(socket->embedded_view, parent_xdg_surface, parent_xdg_toplevel);
                }
            }
        } else {
#ifdef HAVE_GTK3_LAYER_SHELL
            wle_embedded_view_set_toplevel_layer_surface(socket->embedded_view, NULL);
#endif
            wle_embedded_view_set_toplevel_xdg_surface(socket->embedded_view, NULL, NULL);
        }
    }
}

static void
update_embedded_view_geometry(WleGtkSocket *socket) {
    if (socket->embedded_view != NULL
        && socket->parent_window_widget != NULL
        && gtk_widget_is_visible(GTK_WIDGET(socket)))
    {
        GtkAllocation allocation;
        gtk_widget_get_allocation(GTK_WIDGET(socket), &allocation);
        gint xoff = allocation.x;
        gint yoff = allocation.y;

        TRACE("initial offset: (%d, %d)", xoff, yoff);

        GtkWidget *widget = gtk_widget_get_parent(GTK_WIDGET(socket));
        while (widget != NULL) {
            if (gtk_widget_get_has_window(widget)) {
                GdkWindow *window = gtk_widget_get_window(widget);
                gint x, y;
                gdk_window_get_geometry(window, &x, &y, NULL, NULL);
                xoff += x;
                yoff += y;

#ifdef DEBUG_TRACE
                TRACE("got a %s, adding offset (%d, %d)",
                      g_type_name_from_instance((GTypeInstance *)widget),
                      x, y);
#endif

                if (widget == socket->parent_window_widget) {
                    break;
                }
            }

            widget = gtk_widget_get_parent(widget);
        }

        if (widget == socket->parent_window_widget) {
            wle_embedded_view_move(socket->embedded_view, xoff, yoff);
            wle_embedded_view_set_size(socket->embedded_view, allocation.width, allocation.height);
        }
    }
}

static void
show_embedded_view(WleGtkSocket *socket) {
    TRACE("entering");

    if (socket->embedded_view != NULL
        && socket->parent_window_widget != NULL
        && gtk_widget_is_visible(GTK_WIDGET(socket)))
    {
        GdkWindow *parent_window = gtk_widget_get_window(socket->parent_window_widget);
        struct wl_surface *parent_surface = gdk_wayland_window_get_wl_surface(parent_window);
        if (parent_surface != NULL && parent_surface != wle_embedded_view_get_parent_surface(socket->embedded_view)) {
            TRACE("showing embedded view");
            wle_embedded_view_show(socket->embedded_view, parent_surface);
            update_embedded_view_geometry(socket);
        }
    }
}

static void
hide_embedded_view(WleGtkSocket *socket) {
    if (socket->embedded_view != NULL) {
        for (GList *l = socket->entered_seats; l != NULL;) {
            WleSeat *seat = WLE_SEAT(l->data);
            GList *next = l->next;
            embedded_view_pointer_leave(socket, seat, socket->last_pointer_serial);
            l = next;
        }

        TRACE("hiding embedded view");
        wle_embedded_view_hide(socket->embedded_view);
    }
}

static void
parent_window_widget_mapped(GtkWidget *widget, WleGtkSocket *socket) {
    show_embedded_view(socket);
}

static void
parent_window_widget_unmapped(GtkWidget *widget, WleGtkSocket *socket) {
    hide_embedded_view(socket);
}

static void
parent_window_widget_destroy(GtkWidget *widget, WleGtkSocket *socket) {
    g_signal_handlers_disconnect_by_data(widget, socket);
    socket->parent_window_widget = NULL;
}

static void
update_parent_window_widget(WleGtkSocket *socket) {
    if (socket->parent_window_widget != NULL) {
        g_signal_handlers_disconnect_by_data(socket->parent_window_widget, socket);
    }
    socket->parent_window_widget = NULL;

    if (gtk_widget_get_realized(GTK_WIDGET(socket))) {
        GtkWidget *parent = gtk_widget_get_parent(GTK_WIDGET(socket));
        while (parent != NULL) {
            if (!gtk_widget_get_realized(parent)) {
                break;
            } else if (gtk_widget_get_has_window(parent)) {
                GdkWindow *parent_window = gtk_widget_get_window(parent);
                GdkWindowType window_type = gdk_window_get_window_type(parent_window);
                if (window_type == GDK_WINDOW_TOPLEVEL
                    || window_type == GDK_WINDOW_TEMP
                    || window_type == GDK_WINDOW_SUBSURFACE)
                {
                    socket->parent_window_widget = parent;
                    break;
                }
            }

            parent = gtk_widget_get_parent(parent);
        }

        if (socket->parent_window_widget != NULL) {
            GdkWindow *window = gtk_widget_get_window(GTK_WIDGET(socket));
            GdkWindow *parent_window = gtk_widget_get_window(GTK_WIDGET(socket->parent_window_widget));
            gdk_window_set_transient_for(window, parent_window);

            g_signal_connect(socket->parent_window_widget, "map",
                             G_CALLBACK(parent_window_widget_mapped), socket);
            g_signal_connect(socket->parent_window_widget, "unmap",
                             G_CALLBACK(parent_window_widget_unmapped), socket);
            g_signal_connect(socket->parent_window_widget, "destroy",
                             G_CALLBACK(parent_window_widget_destroy), socket);

            show_embedded_view(socket);
        } else {
            hide_embedded_view(socket);
        }
    }
}

static void
toplevel_map(GtkWidget *widget, WleGtkSocket *socket) {
    TRACE("entering");

    update_toplevel_surfaces(socket);

    GtkAllocation toplevel_allocation;
    gtk_widget_get_allocation(socket->toplevel, &toplevel_allocation);
    toplevel_size_allocate(socket->toplevel, &toplevel_allocation, socket);

    toplevel_is_active_changed(socket->toplevel, NULL, socket);
}

static void
toplevel_unmap(GtkWidget *widget, WleGtkSocket *socket) {
    update_toplevel_surfaces(socket);
}

static void
toplevel_size_allocate(GtkWidget *widget, GtkAllocation *allocation, WleGtkSocket *socket) {
    if (socket->embedded_view != NULL) {
        gint left, right, top, bottom;
        GdkRectangle geometry;
        GdkWindow *window = gtk_widget_get_window(widget);
        if (window != NULL) {
            if (abuse_gdk_window_get_shadow_width(window, &left, &right, &top, &bottom)) {
                geometry = (GdkRectangle){
                    .x = left,
                    .y = top,
                    .width = gdk_window_get_width(window) - (left + right),
                    .height = gdk_window_get_height(window) - (top + bottom),
                };
            } else {
                abuse_gdk_wayland_window_get_window_geometry(window, &geometry);
            }

            wle_embedded_view_set_toplevel_geometry(socket->embedded_view,
                                                    geometry.x,
                                                    geometry.y,
                                                    geometry.width,
                                                    geometry.height);
        }
    }
}

static void
toplevel_is_active_changed(GtkWidget *toplevel, GParamSpec *pspec, WleGtkSocket *socket) {
    TRACE("entering");

    if (socket->embedded_view != NULL) {
        GdkEvent *event = gtk_get_current_event();
        WleSeat *seat = get_wle_seat_for_gdk_event(socket, (GdkEvent *)event);
        if (event != NULL) {
            gdk_event_free(event);
        }

        if (gtk_window_is_active(GTK_WINDOW(toplevel))) {
            if (gtk_widget_has_focus(GTK_WIDGET(socket))) {
                DBG("socket has focus");
                wle_embedded_view_focus(socket->embedded_view, seat);
            }
            wle_embedded_view_toplevel_activated(socket->embedded_view, seat);
        } else {
            wle_embedded_view_toplevel_deactivated(socket->embedded_view, seat);
        }
    }
}

static void
toplevel_destroy(GtkWidget *widget, WleGtkSocket *socket) {
    if (socket->toplevel == widget) {
        g_signal_handlers_disconnect_by_data(socket->toplevel, socket);
        socket->toplevel = NULL;
    }
}

static void
embedded_view_size_request(WleEmbeddedView *view, gint width, gint height, WleGtkSocket *socket) {
    socket->requested_width = width;
    socket->requested_height = height;

    gint cur_width, cur_height;
    if (socket->requested_width > 0 && socket->requested_height > 0) {
        cur_width = socket->requested_width;
        cur_height = socket->requested_height;
    } else {
        wle_embedded_view_get_size(socket->embedded_view, &cur_width, &cur_height);
    }

    if (cur_width > 0 && cur_height > 0) {
        gint allowed_width = CLAMP(cur_width,
                                   socket->minimum_width,
                                   socket->maximum_width != -1 ? socket->maximum_width : G_MAXINT);
        gint allowed_height = CLAMP(cur_height,
                                    socket->minimum_height,
                                    socket->maximum_height != -1 ? socket->maximum_height : G_MAXINT);

        if (allowed_width > 0 && allowed_height > 0 && (allowed_width != width || allowed_height != height)) {
            wle_embedded_view_set_size(socket->embedded_view, allowed_width, allowed_height);
        }
    }

    gtk_widget_queue_resize(GTK_WIDGET(socket));
}

static void
embedded_view_focused(WleEmbeddedView *view, WleGtkSocket *socket) {
    gtk_widget_grab_focus(GTK_WIDGET(socket));
}

static void
embedded_view_unfocused(WleEmbeddedView *view, WleGtkSocket *socket) {
    GtkWidget *toplevel = gtk_widget_get_toplevel(GTK_WIDGET(socket));
    if (GTK_IS_WINDOW(toplevel) && gtk_window_has_toplevel_focus(GTK_WINDOW(toplevel))) {
        GtkWidget *last_child = GTK_WIDGET(socket);
        GtkWidget *container = gtk_widget_get_parent(last_child);

        while (container != NULL && !GTK_IS_CONTAINER(container)) {
            last_child = container;
            container = gtk_widget_get_parent(container);
        }

        if (GTK_IS_CONTAINER(container)) {
            GtkWidget *next_focus = NULL;

            // TODO: skip widgets that do not accept focus
            GList *children = gtk_container_get_children(GTK_CONTAINER(container));
            for (GList *l = children; l != NULL; l = l->next) {
                if (l->data == last_child) {
                    if (l->next != NULL) {
                        next_focus = GTK_WIDGET(l->next->data);
                    } else if (children->data != last_child) {
                        next_focus = GTK_WIDGET(children->data);
                    }
                    break;
                }
            }
            g_list_free(children);

            if (next_focus != NULL) {
                gtk_container_set_focus_child(GTK_CONTAINER(container), next_focus);
            }
        }
    }
}

static void
remove_embedded_view(WleGtkSocket *socket, GError *error) {
    if (socket->embedded_view != NULL) {
        hide_embedded_view(socket);

        g_signal_handlers_disconnect_by_data(socket->embedded_view, socket);
        socket->embedded_view = NULL;
        g_object_notify(G_OBJECT(socket), "embedded-view");

        g_free(socket->embedding_token);
        socket->embedding_token = g_strdup(wle_embedded_compositor_generate_embedding_token(socket->ec));

        socket->minimum_width = 0;
        socket->minimum_height = 0;
        socket->maximum_width = -1;
        socket->maximum_height = -1;
        socket->requested_width = 0;
        socket->requested_height = 0;
        gtk_widget_queue_resize(GTK_WIDGET(socket));

        g_signal_emit(socket, signals[SIG_PLUG_REMOVED], 0, error);
    }
}

static void
embedded_compositor_closed(WleEmbeddedCompositor *ec, GError *error, WleGtkSocket *socket) {
    remove_embedded_view(socket, error);
    g_signal_handlers_disconnect_by_data(ec, socket);
    socket->ec = NULL;
}

static void
seat_destroyed(gpointer data, GObject *where_the_object_was) {
    GdkSeat *gseat = GDK_SEAT(data);
    g_object_set_data(G_OBJECT(gseat), WLE_SEAT_KEY, NULL);
    g_object_unref(gseat);
}

// Unfortunately GDK doesn't provide us much useful means to match a GdkSeat
// with some other list of seats, so we kinda guess.  We assume that the
// GdkSeats are listed in the same order as GDK's wl_registry reported them,
// and that all wl_registry instances on the same wl_display will report seats
// in the same order.
static WleSeat *
get_wle_seat_for_gdk_event(WleGtkSocket *socket, GdkEvent *event) {
    GdkDevice *gdevice = event != NULL ? gdk_event_get_device(event) : NULL;
    GdkSeat *gseat = gdevice != NULL ? gdk_device_get_seat(gdevice) : NULL;
    WleSeat *seat = gseat != NULL ? g_object_get_data(G_OBJECT(gseat), WLE_SEAT_KEY) : NULL;

    if (seat == NULL && gseat != NULL) {
        GdkDisplay *gdisplay = gdk_event_get_window(event) != NULL
                                   ? gdk_window_get_display(gdk_event_get_window(event))
                                   : gtk_widget_get_display(GTK_WIDGET(socket));
        GList *gseats = gdk_display_list_seats(gdisplay);
        GList *wleseats = wle_embedded_compositor_list_seats(socket->ec);

        if (g_list_length(gseats) == g_list_length(wleseats)) {
            for (GList *gl = gseats, *wl = wleseats;
                 gl != NULL && wl != NULL;
                 gl = gl->next, wl = wl->next)
            {
                if (GDK_SEAT(gl->data) == gseat) {
                    seat = WLE_SEAT(wl->data);
                    break;
                }
            }
        }

        g_list_free(gseats);

        if (seat != NULL) {
            g_object_set_data(G_OBJECT(gseat), WLE_SEAT_KEY, seat);
            g_object_weak_ref(G_OBJECT(seat), seat_destroyed, g_object_ref(gseat));
        }
    }

    return seat;
}

static void
socket_focus_next_or_previous(WleGtkSocket *socket, GtkDirectionType direction, guint serial) {
    TRACE("entering");
    g_assert(direction == GTK_DIR_TAB_FORWARD || direction == GTK_DIR_TAB_BACKWARD);

    GtkWidget *toplevel = gtk_widget_get_toplevel(GTK_WIDGET(socket));
    if (toplevel != NULL) {
        if (!GTK_IS_WINDOW(toplevel)) {
            gtk_widget_child_focus(toplevel, direction);
        } else {
            GtkWidget *prev_focus_child = gtk_container_get_focus_child(GTK_CONTAINER(toplevel));
            if (prev_focus_child != NULL) {
                if (gtk_widget_child_focus(prev_focus_child, direction)) {
                    // We focused something, so bail
                    return;
                } else {
                    // If we get here, we could be in an infinite loop in the
                    // case that there are no focusable widgets in either the
                    // toplevel or the embedded client.
                    if (serial == socket->last_focus_serial) {
                        DBG("FOCUS LOOP DETECTED");
                        return;
                    }
                }
            }

            GtkWidget *focused = gtk_window_get_focus(GTK_WINDOW(toplevel));
            if (focused != NULL) {
                // Clear focus for toplevel
                GtkWidget *parent = gtk_widget_get_parent(focused);
                while (parent != NULL) {
                    gtk_container_set_focus_child(GTK_CONTAINER(parent), NULL);
                    parent = gtk_widget_get_parent(parent);
                }
            }

            GtkWidget *child = gtk_bin_get_child(GTK_BIN(toplevel));
            if (child != NULL) {
                // Try to focus the first/last widget in the window
                gtk_widget_child_focus(child, direction);
            }
        }
    }
}

static void
embedded_view_focus_next(WleEmbeddedView *eview, guint serial, WleGtkSocket *socket) {
    socket_focus_next_or_previous(socket, GTK_DIR_TAB_FORWARD, serial);
}

static void
embedded_view_focus_previous(WleEmbeddedView *eview, guint serial, WleGtkSocket *socket) {
    socket_focus_next_or_previous(socket, GTK_DIR_TAB_BACKWARD, serial);
}

static void
embedded_view_closed(WleEmbeddedView *view, GError *error, WleGtkSocket *socket) {
    TRACE("entering");
    remove_embedded_view(socket, error);
}

static const struct wl_pointer_listener *
get_gdk_pointer_listener_for_gdk_seat(GdkSeat *gdkseat, struct wl_pointer **wl_pointer_out) {
    GdkDevice *pointer = gdk_seat_get_pointer(gdkseat);
    if (pointer != NULL) {
        struct wl_pointer *wl_pointer = gdk_wayland_device_get_wl_pointer(pointer);
        if (wl_pointer != NULL) {
            *wl_pointer_out = wl_pointer;
            return wl_proxy_get_listener((struct wl_proxy *)wl_pointer);
        }
    }

    return NULL;
}

static const struct wl_pointer_listener *
get_gdk_pointer_listener_for_wle_seat(WleGtkSocket *socket, WleSeat *seat, GdkSeat **gdkseat_out, struct wl_pointer **wl_pointer_out) {
    GdkDisplay *gdisplay = gtk_widget_get_display(GTK_WIDGET(socket));
    GList *gseats = gdk_display_list_seats(gdisplay);
    GList *wleseats = wle_embedded_compositor_list_seats(socket->ec);

    GdkSeat *gdkseat = NULL;
    if (g_list_length(gseats) == g_list_length(wleseats)) {
        for (GList *gl = gseats, *wl = wleseats; gl != NULL && wl != NULL; gl = gl->next, wl = wl->next) {
            if (WLE_SEAT(wl->data) == seat) {
                gdkseat = GDK_SEAT(gl->data);
                break;
            }
        }
    }
    g_list_free(gseats);

    if (gdkseat != NULL) {
        const struct wl_pointer_listener *listener = get_gdk_pointer_listener_for_gdk_seat(gdkseat, wl_pointer_out);
        if (listener != NULL) {
            *gdkseat_out = gdkseat;
            return listener;
        }
    }

    gdkseat = gdk_display_get_default_seat(gdisplay);
    const struct wl_pointer_listener *listener = get_gdk_pointer_listener_for_gdk_seat(gdkseat, wl_pointer_out);
    if (listener != NULL) {
        *gdkseat_out = gdkseat;
        return listener;
    } else {
        return NULL;
    }
}

static void
embedded_view_pointer_enter(WleGtkSocket *socket, WleSeat *seat, uint32_t serial, gdouble x, gdouble y) {
    GdkSeat *gdkseat;
    struct wl_pointer *wl_pointer;
    const struct wl_pointer_listener *listener = get_gdk_pointer_listener_for_wle_seat(socket, seat, &gdkseat, &wl_pointer);
    GdkWindow *window = gtk_widget_get_window(GTK_WIDGET(socket));
    struct wl_surface *surface = gdk_wayland_window_get_wl_surface(window);
    if (listener != NULL && listener->enter != NULL && surface != NULL) {
        DBG("forwarding enter at (%.02f, %.02f) to gdk", x, y);
        if (gdk_seat_grab(gdkseat,
                          window,
                          GDK_SEAT_CAPABILITY_ALL_POINTING,
                          FALSE,
                          NULL,
                          NULL,
                          NULL,
                          NULL)
            != GDK_GRAB_SUCCESS)
        {
            g_message("WleGtkSocket: failed to grab seat on pointer enter");
        }
        listener->enter(gdkseat, wl_pointer, serial, surface, wl_fixed_from_double(x), wl_fixed_from_double(y));
    }
    if (g_list_find(socket->entered_seats, seat) == NULL) {
        socket->entered_seats = g_list_prepend(socket->entered_seats, seat);
    }
    socket->last_pointer_serial = serial;
}

static void
embedded_view_pointer_leave(WleGtkSocket *socket, WleSeat *seat, uint32_t serial) {
    GdkSeat *gdkseat;
    struct wl_pointer *wl_pointer;
    const struct wl_pointer_listener *listener = get_gdk_pointer_listener_for_wle_seat(socket, seat, &gdkseat, &wl_pointer);
    struct wl_surface *surface = gdk_wayland_window_get_wl_surface(gtk_widget_get_window(GTK_WIDGET(socket)));
    if (listener != NULL && listener->leave != NULL && surface != NULL) {
        DBG("forwarding leave to gdk");
        listener->leave(gdkseat, wl_pointer, serial, surface);
        gdk_seat_ungrab(gdkseat);
    }
    socket->entered_seats = g_list_remove(socket->entered_seats, seat);
    socket->last_pointer_serial = serial;
}

static void
embedded_view_pointer_motion(WleGtkSocket *socket, WleSeat *seat, uint32_t time, gdouble x, gdouble y) {
    GdkSeat *gdkseat;
    struct wl_pointer *wl_pointer;
    const struct wl_pointer_listener *listener = get_gdk_pointer_listener_for_wle_seat(socket, seat, &gdkseat, &wl_pointer);
    if (listener != NULL && listener->motion != NULL) {
        // DBG("forwarding motion at (%.02f, %.02f) to gdk", x, y);
        listener->motion(gdkseat, wl_pointer, time, wl_fixed_from_double(x), wl_fixed_from_double(y));
    }
}

static void
embedded_view_pointer_frame(WleGtkSocket *socket, WleSeat *seat) {
    GdkSeat *gdkseat;
    struct wl_pointer *wl_pointer;
    const struct wl_pointer_listener *listener = get_gdk_pointer_listener_for_wle_seat(socket, seat, &gdkseat, &wl_pointer);
    if (listener != NULL && listener->frame != NULL) {
        // DBG("forwarding frame to gdk");
        listener->frame(gdkseat, wl_pointer);
    }
}

static gboolean
embedded_compositor_new_embedded_view(WleEmbeddedCompositor *ec,
                                      const gchar *embedding_token,
                                      WleEmbeddedView *view,
                                      WleGtkSocket *socket) {
    if (socket->embedded_view == NULL && g_strcmp0(socket->embedding_token, embedding_token) == 0) {
        TRACE("entering embedded state");
        socket->embedded_view = view;

        g_signal_connect(view, "size-request",
                         G_CALLBACK(embedded_view_size_request), socket);
        g_signal_connect(view, "focus-in",
                         G_CALLBACK(embedded_view_focused), socket);
        g_signal_connect(view, "focus-out",
                         G_CALLBACK(embedded_view_unfocused), socket);
        g_signal_connect(view, "focus-next",
                         G_CALLBACK(embedded_view_focus_next), socket);
        g_signal_connect(view, "focus-previous",
                         G_CALLBACK(embedded_view_focus_previous), socket);
        g_signal_connect(view, "closed",
                         G_CALLBACK(embedded_view_closed), socket);
        g_signal_connect_swapped(view, "pointer-enter",
                                 G_CALLBACK(embedded_view_pointer_enter), socket);
        g_signal_connect_swapped(view, "pointer-leave",
                                 G_CALLBACK(embedded_view_pointer_leave), socket);
        g_signal_connect_swapped(view, "pointer-motion",
                                 G_CALLBACK(embedded_view_pointer_motion), socket);
        g_signal_connect_swapped(view, "pointer-frame",
                                 G_CALLBACK(embedded_view_pointer_frame), socket);

        update_toplevel_surfaces(socket);
        update_parent_window_widget(socket);

        if (socket->toplevel != NULL && gtk_widget_get_mapped(socket->toplevel)) {
            DBG("have a mapped toplevel; is_active=%s", gtk_window_is_active(GTK_WINDOW(socket->toplevel)) ? "true" : "false");
            toplevel_map(socket->toplevel, socket);
        }

        g_signal_emit(socket, signals[SIG_PLUG_ADDED], 0);
        g_object_notify(G_OBJECT(socket), "embedded-view");

        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * wle_gtk_socket_new: (constructor)
 * @ec: A #WleEmbeddedCompositor.
 *
 * Creates a new socket that can launch an embedded application.
 *
 * Return value: (not nullable) (transfer floating): A new socket as a
 * #GtkWidget.
 **/
GtkWidget *
wle_gtk_socket_new(WleEmbeddedCompositor *ec) {
    g_return_val_if_fail(WLE_IS_EMBEDDED_COMPOSITOR(ec), NULL);
    return g_object_new(WLE_TYPE_GTK_SOCKET,
                        "embedded-compositor", ec,
                        NULL);
}

/**
 * wle_gtk_socket_get_embedded_compositor:
 * @socket: A #WleGtkSocket.
 *
 * Retrieves the #WleEmbeddedCompositor instance used to construct @socket.
 *
 * Return value: (not nullable) (transfer none): A #WleEmbeddedCompositor.
 **/
WleEmbeddedCompositor *
wle_gtk_socket_get_embedded_compositor(WleGtkSocket *socket) {
    g_return_val_if_fail(WLE_IS_GTK_SOCKET(socket), NULL);
    return socket->ec;
}

/**
 * wle_gtk_socket_get_embedding_token:
 * @socket: A #WleGtkSocket.
 *
 * Returns the embedding token associated with @socket.  This embedding token
 * must be passed to the child process that will call @wle_gtk_plug_new() in
 * order to embed its content in the parent.
 *
 * While @socket itself is reusable (that is, you can embed something new in it
 * after a previous embedded application exits), the embedding token is not.
 * You can call this function again to get a new token after the underlying
 * embedded view has been destroyed.
 *
 * Return value: (not nullable) (transfer none): An opaque embedding token,
 * owned by @socket.
 **/
const gchar *
wle_gtk_socket_get_embedding_token(WleGtkSocket *socket) {
    g_return_val_if_fail(WLE_IS_GTK_SOCKET(socket), NULL);
    return socket->embedding_token;
}

/**
 * wle_gtk_socket_set_size_constraints:
 * @socket: A #WleGtkSocket.
 * @minimum_width: The socket's minimum allowed width.
 * @minimum_height: The socket's minimum allowed height.
 * @maximum_width: The socket's maximum allowed width, or -1 for unlimited.
 * @maximum_height: The socket's maximum allowed height, or -1 for unlimited.
 *
 * Constrains the size of @socket between a minimum and maximum size.  As the
 * embedded application sizes its embedded window, @socket will automatically
 * ensure the drawing area remains constrained between the minumum and maximum
 * sizes specified.
 *
 * Pass -1 for @maximum_width or @maximum_height in order to unconstrain the
 * respective maxiumum.
 **/
void
wle_gtk_socket_set_size_constraints(WleGtkSocket *socket,
                                    gint minimum_width,
                                    gint minimum_height,
                                    gint maximum_width,
                                    gint maximum_height) {
    g_return_if_fail(WLE_IS_GTK_SOCKET(socket));
    g_return_if_fail(minimum_width >= 0 && minimum_height >= 0);
    g_return_if_fail(maximum_width == -1 || maximum_width >= minimum_width);
    g_return_if_fail(maximum_height == -1 || maximum_height >= minimum_height);

    socket->minimum_width = minimum_width;
    socket->minimum_height = minimum_height;
    socket->maximum_width = maximum_width;
    socket->maximum_height = maximum_height;

    if (socket->embedded_view != NULL) {
        embedded_view_size_request(socket->embedded_view, socket->requested_width, socket->requested_height, socket);
    }
}

/**
 * wle_gtk_socket_destroy_embedded_view:
 * @socket: A #WleGtkSocket.
 *
 * Destroys @socket's underlying embedded view, if any.
 **/
void
wle_gtk_socket_destroy_embedded_view(WleGtkSocket *socket) {
    g_return_if_fail(WLE_IS_GTK_SOCKET(socket));

    if (socket->embedded_view != NULL) {
        wle_embedded_view_destroy(socket->embedded_view);
    }
}

#define __WLE_GTK_SOCKET_C__
#include <libwlembed-gtk3-visibility.c>
