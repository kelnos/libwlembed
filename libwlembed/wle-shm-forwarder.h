/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_SHM_FORWARDER_H__
#define __WLE_SHM_FORWARDER_H__

#include <glib-object.h>
#include <wayland-client-protocol.h>
#include <wayland-server-core.h>

typedef struct _WleShmForwarder WleShmForwarder;
typedef struct _WleBuffer WleBuffer;

WleShmForwarder *wle_shm_forwarder_new(struct wl_display *remote_display,
                                       struct wl_shm *remote_shm,
                                       struct wl_display *local_display);

GList *wle_shm_forwarder_get_buffers(WleShmForwarder *forwarder);

void wle_shm_forwarder_destroy(WleShmForwarder *forwarder);

#endif /* __WLE_SHM_FORWARDER_H__ */
