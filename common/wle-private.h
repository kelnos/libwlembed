/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_PRIVATE_H__
#define __WLE_PRIVATE_H__

#include <glib-object.h>

// Allow building with glib 2.72 (these appear first in 2.74).
#define WLE_G_DEFINE_ENUM_VALUE(EnumValue, EnumNick) \
    { EnumValue, #EnumValue, EnumNick }
#define WLE_G_DEFINE_ENUM_TYPE(TypeName, type_name, ...) \
    GType \
        type_name##_get_type(void) { \
        static gsize g_define_type__static = 0; \
        if (g_once_init_enter(&g_define_type__static)) { \
            static const GEnumValue enum_values[] = { \
                __VA_ARGS__, \
                { 0, NULL, NULL }, \
            }; \
            GType g_define_type = g_enum_register_static(g_intern_static_string(#TypeName), enum_values); \
            g_once_init_leave(&g_define_type__static, g_define_type); \
        } \
        return g_define_type__static; \
    }

#endif /* __WLE_PRIVATE_H__ */
