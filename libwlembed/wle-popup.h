/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_POPUP_H__
#define __WLE_POPUP_H__

#include <glib-object.h>

#include "wle-positioner.h"
#include "wle-view.h"
#include "wle-xdg-surface.h"

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WlePopup, wle_popup, WLE, POPUP, WleXdgSurface)
#define WLE_TYPE_POPUP (wle_popup_get_type())

WlePopup *_wle_popup_new(WleView *view,
                         struct xdg_surface *remote_xdg_surface,
                         struct wl_resource *local_xdg_surface,
                         struct xdg_popup *remote_xdg_popup,
                         struct wl_resource *local_xdg_popup,
                         WlePositioner *positioner,
                         WleView *parent_view);

G_END_DECLS

#endif /* __WLE_POPUP_H__ */
