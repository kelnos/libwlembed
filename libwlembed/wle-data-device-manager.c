/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <wayland-client.h>
#include <wayland-server-protocol.h>
#include <wayland-server.h>

#include "common/wle-debug.h"
#include "wle-data-device-manager.h"
#include "wle-produces-serials.h"
#include "wle-seat-private.h"
#include "wle-util.h"
#include "wle-view.h"

typedef struct _WleDataOffer WleDataOffer;

struct _WleDataDeviceManager {
    GObject parent;

    struct wl_display *remote_display;
    struct wl_data_device_manager *remote_data_device_manager;

    struct wl_global *local_data_device_manager;
    struct wl_list local_data_device_manager_resources;

    struct wl_list devices;  // WleDataDevice->local_data_device->link
    struct wl_list sources;  // WleDataSource->local_data_source->link
};

typedef struct _WleDataDevice {
    WleDataDeviceManager *manager;
    WleSeat *seat;

    struct wl_data_device *remote_data_device;
    struct wl_resource *local_data_device;

    WleDataOffer *cur_selection;
    struct wl_list offers;  // WleDataOffer->link
    struct wl_list orphan_offer_resources;
} WleDataDevice;


struct _WleDataOffer {
    WleDataDevice *device;
    GPtrArray *mime_types;

    struct wl_data_offer *remote_data_offer;
    struct wl_resource *local_data_offer;

    struct wl_list link;  // <- WleDataDevice->offers
};

typedef struct _WleDataSource {
    struct wl_data_source *remote_data_source;
    struct wl_resource *local_data_source;
} WleDataSource;

static void wle_data_device_manager_finalize(GObject *object);

static void remote_data_offer_offer(
    void *data,
    struct wl_data_offer *remote_data_offer,
    const char *mime_type);
static void remote_data_offer_source_actions(
    void *data,
    struct wl_data_offer *remote_data_offer,
    uint32_t source_actions);
static void remote_data_offer_action(
    void *data,
    struct wl_data_offer *remote_data_offer,
    uint32_t dnd_action);

static void local_data_offer_accept(
    struct wl_client *client,
    struct wl_resource *data_offer_resource,
    uint32_t serial,
    const char *mime_type /* nullable */);
static void local_data_offer_receive(
    struct wl_client *client,
    struct wl_resource *data_offer_resource,
    const char *mime_type,
    int fd);
static void local_data_offer_destroy(
    struct wl_client *client,
    struct wl_resource *data_offer_resource);
static void local_data_offer_finish(
    struct wl_client *client,
    struct wl_resource *data_offer_resource);
static void local_data_offer_set_actions(
    struct wl_client *client,
    struct wl_resource *data_offer_resource,
    uint32_t dnd_actions,
    uint32_t preferred_action);

static void remote_data_source_target(
    void *data,
    struct wl_data_source *remote_data_source,
    const char *mime_type /* nullable */);
static void remote_data_source_send(
    void *data,
    struct wl_data_source *remote_data_source,
    const char *mime_type,
    int fd);
static void remote_data_source_cancelled(
    void *data,
    struct wl_data_source *remote_data_source);
static void remote_data_source_dnd_drop_performed(
    void *data,
    struct wl_data_source *remote_data_source);
static void remote_data_source_dnd_finished(
    void *data,
    struct wl_data_source *remote_data_source);
static void remote_data_source_action(
    void *data,
    struct wl_data_source *remote_data_source,
    uint32_t dnd_action);

static void local_data_source_offer(
    struct wl_client *client,
    struct wl_resource *data_source_resource,
    const char *mime_type);
static void local_data_source_destroy(
    struct wl_client *client,
    struct wl_resource *data_source_resource);
static void local_data_source_set_actions(
    struct wl_client *client,
    struct wl_resource *data_source_resource,
    uint32_t dnd_actions);

static void remote_data_device_data_offer(
    void *data,
    struct wl_data_device *remote_data_device,
    struct wl_data_offer *remote_id);
static void remote_data_device_enter(
    void *data,
    struct wl_data_device *remote_data_device,
    uint32_t serial,
    struct wl_surface *remote_surface,
    wl_fixed_t x,
    wl_fixed_t y,
    struct wl_data_offer *remote_id /* nullable */);
static void remote_data_device_leave(
    void *data,
    struct wl_data_device *remote_data_device);
static void remote_data_device_motion(
    void *data,
    struct wl_data_device *remote_data_device,
    uint32_t time,
    wl_fixed_t x,
    wl_fixed_t y);
static void remote_data_device_drop(
    void *data,
    struct wl_data_device *remote_data_device);
static void remote_data_device_selection(
    void *data,
    struct wl_data_device *remote_data_device,
    struct wl_data_offer *remote_id /* nullable */);

static void local_data_device_start_drag(
    struct wl_client *client,
    struct wl_resource *data_device_resource,
    struct wl_resource *source_resource /* nullable */,
    struct wl_resource *origin_resource,
    struct wl_resource *icon_resource /* nullable */,
    uint32_t serial);
static void local_data_device_set_selection(
    struct wl_client *client,
    struct wl_resource *data_device_resource,
    struct wl_resource *source_resource /* nullable */,
    uint32_t serial);
static void local_data_device_release(
    struct wl_client *client,
    struct wl_resource *data_device_resource);


static void local_data_device_manager_create_data_source(
    struct wl_client *client,
    struct wl_resource *data_device_manager_resource,
    uint32_t id_id);
static void local_data_device_manager_get_data_device(
    struct wl_client *client,
    struct wl_resource *data_device_manager_resource,
    uint32_t id_id,
    struct wl_resource *seat_resource);

static const struct wl_data_offer_listener remote_data_offer_listener = {
    .offer = remote_data_offer_offer,
    .source_actions = remote_data_offer_source_actions,
    .action = remote_data_offer_action,
};

static const struct wl_data_offer_interface local_data_offer_impl = {
    .accept = local_data_offer_accept,
    .receive = local_data_offer_receive,
    .destroy = local_data_offer_destroy,
    .finish = local_data_offer_finish,
    .set_actions = local_data_offer_set_actions,
};

static const struct wl_data_source_listener remote_data_source_listener = {
    .target = remote_data_source_target,
    .send = remote_data_source_send,
    .cancelled = remote_data_source_cancelled,
    .dnd_drop_performed = remote_data_source_dnd_drop_performed,
    .dnd_finished = remote_data_source_dnd_finished,
    .action = remote_data_source_action,
};

static const struct wl_data_source_interface local_data_source_impl = {
    .offer = local_data_source_offer,
    .destroy = local_data_source_destroy,
    .set_actions = local_data_source_set_actions,
};

static const struct wl_data_device_listener remote_data_device_listener = {
    .data_offer = remote_data_device_data_offer,
    .enter = remote_data_device_enter,
    .leave = remote_data_device_leave,
    .motion = remote_data_device_motion,
    .drop = remote_data_device_drop,
    .selection = remote_data_device_selection,
};

static const struct wl_data_device_interface local_data_device_impl = {
    .start_drag = local_data_device_start_drag,
    .set_selection = local_data_device_set_selection,
    .release = local_data_device_release,
};

static const struct wl_data_device_manager_interface local_data_device_manager_impl = {
    .create_data_source = local_data_device_manager_create_data_source,
    .get_data_device = local_data_device_manager_get_data_device,
};


G_DEFINE_FINAL_TYPE_WITH_CODE(WleDataDeviceManager,
                              wle_data_device_manager,
                              G_TYPE_OBJECT,
                              G_IMPLEMENT_INTERFACE(WLE_TYPE_PRODUCES_SERIALS, NULL))


static void
wle_data_device_manager_class_init(WleDataDeviceManagerClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_data_device_manager_finalize;
}

static void
wle_data_device_manager_init(WleDataDeviceManager *manager) {
    wl_list_init(&manager->local_data_device_manager_resources);
    wl_list_init(&manager->devices);
    wl_list_init(&manager->sources);
}

static void
wle_data_device_manager_finalize(GObject *object) {
    WleDataDeviceManager *manager = WLE_DATA_DEVICE_MANAGER(object);
    struct wl_resource *resource, *tmp_resource;

    wl_resource_for_each_safe(resource, tmp_resource, &manager->devices) {
        wl_resource_destroy(resource);
    }

    wl_resource_for_each_safe(resource, tmp_resource, &manager->sources) {
        wl_resource_destroy(resource);
    }

    wl_resource_for_each_safe(resource, tmp_resource, &manager->local_data_device_manager_resources) {
        wl_resource_destroy(resource);
    }

    wl_global_destroy(manager->local_data_device_manager);

    wl_data_device_manager_destroy(manager->remote_data_device_manager);

    G_OBJECT_CLASS(wle_data_device_manager_parent_class)->finalize(object);
}

static void
remote_data_offer_offer(
    void *data,
    struct wl_data_offer *remote_data_offer,
    const char *mime_type) {
    WleDataOffer *offer = data;
    g_ptr_array_add(offer->mime_types, g_strdup(mime_type));
    wl_data_offer_send_offer(offer->local_data_offer, mime_type);
}

static void
remote_data_offer_source_actions(
    void *data,
    struct wl_data_offer *remote_data_offer,
    uint32_t source_actions) {
    WleDataOffer *offer = data;
    if (wl_resource_get_version(offer->local_data_offer) >= WL_DATA_OFFER_SOURCE_ACTIONS_SINCE_VERSION) {
        wl_data_offer_send_source_actions(offer->local_data_offer, source_actions);
    }
}

static void
remote_data_offer_action(
    void *data,
    struct wl_data_offer *remote_data_offer,
    uint32_t dnd_action) {
    WleDataOffer *offer = data;
    if (wl_resource_get_version(offer->local_data_offer) >= WL_DATA_OFFER_ACTION_SINCE_VERSION) {
        wl_data_offer_send_action(offer->local_data_offer, dnd_action);
    }
}

static void
local_data_offer_accept(
    struct wl_client *client,
    struct wl_resource *data_offer_resource,
    uint32_t serial,
    const char *mime_type /* nullable */
) {
    WleDataOffer *offer = wl_resource_get_user_data(data_offer_resource);
    if (offer != NULL) {
        wl_data_offer_accept(offer->remote_data_offer, serial, mime_type);
    }
}

static void
local_data_offer_receive(
    struct wl_client *client,
    struct wl_resource *data_offer_resource,
    const char *mime_type,
    int fd) {
    WleDataOffer *offer = wl_resource_get_user_data(data_offer_resource);
    if (offer != NULL) {
        wl_data_offer_receive(offer->remote_data_offer, mime_type, fd);
    }
    close(fd);
}

static void
local_data_offer_destroy(
    struct wl_client *client,
    struct wl_resource *data_offer_resource) {
    wl_resource_destroy(data_offer_resource);
}

static void
local_data_offer_finish(
    struct wl_client *client,
    struct wl_resource *data_offer_resource) {
    WleDataOffer *offer = wl_resource_get_user_data(data_offer_resource);
    if (offer != NULL) {
        wl_data_offer_finish(offer->remote_data_offer);
    }
}

static void
local_data_offer_set_actions(
    struct wl_client *client,
    struct wl_resource *data_offer_resource,
    uint32_t dnd_actions,
    uint32_t preferred_action) {
    WleDataOffer *offer = wl_resource_get_user_data(data_offer_resource);
    if (offer != NULL) {
        wl_data_offer_set_actions(offer->remote_data_offer, dnd_actions, preferred_action);
    }
}

static void
local_data_offer_destroy_impl(struct wl_resource *data_offer_resource) {
    WleDataOffer *offer = wl_resource_get_user_data(data_offer_resource);
    TRACE("entering %p", offer);

    if (offer != NULL) {
        if (offer->device->cur_selection == offer) {
            offer->device->cur_selection = NULL;
        }
        wl_data_offer_destroy(offer->remote_data_offer);
        g_ptr_array_free(offer->mime_types, TRUE);
        wl_list_remove(&offer->link);
        g_free(offer);
    } else {
        // Orphan resource, remove from device's list
        wl_list_remove(wl_resource_get_link(data_offer_resource));
    }
}

static void
remote_data_source_target(
    void *data,
    struct wl_data_source *remote_data_source,
    const char *mime_type /* nullable */
) {
    WleDataSource *source = data;
    wl_data_source_send_target(source->local_data_source, mime_type);
}

static void
remote_data_source_send(
    void *data,
    struct wl_data_source *remote_data_source,
    const char *mime_type,
    int fd) {
    WleDataSource *source = data;
    wl_data_source_send_send(source->local_data_source, mime_type, fd);
    close(fd);
}

static void
remote_data_source_cancelled(
    void *data,
    struct wl_data_source *remote_data_source) {
    WleDataSource *source = data;
    wl_data_source_send_cancelled(source->local_data_source);
}

static void
remote_data_source_dnd_drop_performed(
    void *data,
    struct wl_data_source *remote_data_source) {
    WleDataSource *source = data;
    if (wl_resource_get_version(source->local_data_source) >= WL_DATA_SOURCE_DND_DROP_PERFORMED_SINCE_VERSION) {
        wl_data_source_send_dnd_drop_performed(source->local_data_source);
    }
}

static void
remote_data_source_dnd_finished(
    void *data,
    struct wl_data_source *remote_data_source) {
    WleDataSource *source = data;
    if (wl_resource_get_version(source->local_data_source) >= WL_DATA_SOURCE_DND_FINISHED_SINCE_VERSION) {
        wl_data_source_send_dnd_finished(source->local_data_source);
    }
}

static void
remote_data_source_action(
    void *data,
    struct wl_data_source *remote_data_source,
    uint32_t dnd_action) {
    WleDataSource *source = data;
    if (wl_resource_get_version(source->local_data_source) >= WL_DATA_SOURCE_ACTION_SINCE_VERSION) {
        wl_data_source_send_action(source->local_data_source, dnd_action);
    }
}

static void
local_data_source_offer(
    struct wl_client *client,
    struct wl_resource *data_source_resource,
    const char *mime_type) {
    WleDataSource *source = wl_resource_get_user_data(data_source_resource);
    wl_data_source_offer(source->remote_data_source, mime_type);
}

static void
local_data_source_destroy(
    struct wl_client *client,
    struct wl_resource *data_source_resource) {
    WleDataSource *source = wl_resource_get_user_data(data_source_resource);
    wl_data_source_destroy(source->remote_data_source);
    source->remote_data_source = NULL;
    wl_resource_destroy(data_source_resource);
}

static void
local_data_source_set_actions(
    struct wl_client *client,
    struct wl_resource *data_source_resource,
    uint32_t dnd_actions) {
    WleDataSource *source = wl_resource_get_user_data(data_source_resource);
    wl_data_source_set_actions(source->remote_data_source, dnd_actions);
}

static void
local_data_source_destroy_impl(struct wl_resource *data_source_resource) {
    WleDataSource *source = wl_resource_get_user_data(data_source_resource);
    if (source->remote_data_source != NULL) {
        wl_data_source_destroy(source->remote_data_source);
    }
    wl_list_remove(wl_resource_get_link(data_source_resource));
    g_free(source);
}

static void
remote_data_device_data_offer(
    void *data,
    struct wl_data_device *remote_data_device,
    struct wl_data_offer *remote_offer) {
    WleDataDevice *device = data;

    struct wl_resource *local_offer = wl_resource_create(wl_resource_get_client(device->local_data_device),
                                                         &wl_data_offer_interface,
                                                         wl_proxy_get_version((struct wl_proxy *)remote_data_device),
                                                         0);
    if (local_offer == NULL) {
        wl_data_offer_destroy(remote_offer);
        // XXX: should we do something else here?
    } else {
        WleDataOffer *offer = g_new0(WleDataOffer, 1);
        offer->device = device;
        offer->mime_types = g_ptr_array_new_with_free_func(g_free);
        offer->remote_data_offer = _wle_proxy_set_ours((struct wl_proxy *)remote_offer);
        offer->local_data_offer = local_offer;
        wl_list_insert(&device->offers, &offer->link);

        wl_resource_set_implementation(local_offer,
                                       &local_data_offer_impl,
                                       offer,
                                       local_data_offer_destroy_impl);
        wl_data_offer_add_listener(remote_offer, &remote_data_offer_listener, offer);

        wl_data_device_send_data_offer(device->local_data_device, local_offer);
    }
}

static void
remote_data_device_enter(
    void *data,
    struct wl_data_device *remote_data_device,
    uint32_t serial,
    struct wl_surface *remote_surface,
    wl_fixed_t x,
    wl_fixed_t y,
    struct wl_data_offer *remote_offer /* nullable */
) {
    WleDataDevice *device = data;
    wle_produces_serials_new_serial(WLE_PRODUCES_SERIALS(device->manager), serial);

    WleView *view = _wle_proxy_get_user_data((struct wl_proxy *)remote_surface);

    if (!WLE_IS_VIEW(view)) {
        DBG("got wl_data_device.enter() with null/unknown surface");
    } else {
        WleDataOffer *offer = _wle_proxy_get_user_data((struct wl_proxy *)remote_offer);

        wl_data_device_send_enter(device->local_data_device,
                                  serial,
                                  wle_view_get_local_surface(view),
                                  x,
                                  y,
                                  offer != NULL ? offer->local_data_offer : NULL);
    }
}

static void
remote_data_device_leave(
    void *data,
    struct wl_data_device *remote_data_device) {
    WleDataDevice *device = data;
    wl_data_device_send_leave(device->local_data_device);
}

static void
remote_data_device_motion(
    void *data,
    struct wl_data_device *remote_data_device,
    uint32_t time,
    wl_fixed_t x,
    wl_fixed_t y) {
    WleDataDevice *device = data;
    wl_data_device_send_motion(device->local_data_device, time, x, y);
}

static void
remote_data_device_drop(
    void *data,
    struct wl_data_device *remote_data_device) {
    WleDataDevice *device = data;
    wl_data_device_send_drop(device->local_data_device);
}

static void
remote_data_device_selection(
    void *data,
    struct wl_data_device *remote_data_device,
    struct wl_data_offer *remote_data_offer /* nullable */
) {
    WleDataDevice *device = data;
    WleDataOffer *offer = _wle_proxy_get_user_data((struct wl_proxy *)remote_data_offer);

    device->cur_selection = offer;

    // TODO: only send if our local client has focus?
    wl_data_device_send_selection(device->local_data_device, offer != NULL ? offer->local_data_offer : NULL);
}

static void
local_data_device_start_drag(
    struct wl_client *client,
    struct wl_resource *data_device_resource,
    struct wl_resource *source_resource /* nullable */,
    struct wl_resource *origin_resource,
    struct wl_resource *icon_resource /* nullable */,
    uint32_t serial) {
    WleView *origin = origin_resource != NULL ? wl_resource_get_user_data(origin_resource) : NULL;

    if (!WLE_IS_VIEW(origin)) {
        wl_client_post_implementation_error(client, "null or unknown origin surface");
    } else {
        WleDataDevice *device = wl_resource_get_user_data(data_device_resource);
        WleDataSource *source = source_resource != NULL ? wl_resource_get_user_data(source_resource) : NULL;
        WleView *icon = icon_resource != NULL ? wl_resource_get_user_data(icon_resource) : NULL;

        wl_data_device_start_drag(device->remote_data_device,
                                  source != NULL ? source->remote_data_source : NULL,
                                  wle_view_get_remote_surface(origin),
                                  WLE_IS_VIEW(icon) ? wle_view_get_remote_surface(icon) : NULL,
                                  serial);
    }
}

static void
local_data_device_set_selection(
    struct wl_client *client,
    struct wl_resource *data_device_resource,
    struct wl_resource *source_resource /* nullable */,
    uint32_t serial) {
    WleDataDevice *device = wl_resource_get_user_data(data_device_resource);
    WleDataSource *source = source_resource != NULL ? wl_resource_get_user_data(source_resource) : NULL;
    wl_data_device_set_selection(device->remote_data_device,
                                 source != NULL ? source->remote_data_source : NULL,
                                 serial);
}

static void
local_data_device_release(
    struct wl_client *client,
    struct wl_resource *data_device_resource) {
    TRACE("entering %p", data_device_resource);
    WleDataDevice *device = wl_resource_get_user_data(data_device_resource);
    wl_data_device_release(device->remote_data_device);
    device->remote_data_device = NULL;
    wl_resource_destroy(data_device_resource);
}

static void
local_data_device_destroy_impl(struct wl_resource *data_device_resource) {
    TRACE("entering %p", data_device_resource);
    WleDataDevice *device = wl_resource_get_user_data(data_device_resource);

    if (device->seat != NULL) {
        g_signal_handlers_disconnect_by_data(device->seat, device);
    }

    struct wl_resource *resource, *tmp_resource;
    wl_resource_for_each_safe(resource, tmp_resource, &device->orphan_offer_resources) {
        wl_resource_destroy(resource);
    }

    WleDataOffer *offer, *tmp_offer;
    wl_list_for_each_safe(offer, tmp_offer, &device->offers, link) {
        wl_resource_destroy(offer->local_data_offer);
    }

    if (device->remote_data_device != NULL) {
        wl_data_device_destroy(device->remote_data_device);
    }

    wl_list_remove(wl_resource_get_link(data_device_resource));
    g_free(device);
}

static void
seat_focus_changed(WleSeat *seat, WleView *focused_view, uint32_t serial, WleDataDevice *device) {
    TRACE("entering");
    if (focused_view != NULL && wl_resource_get_client(device->local_data_device) == wle_view_get_client(focused_view))
    {
        DBG("forwarding current selection %p", device->cur_selection);
        if (device->cur_selection != NULL) {
            WleDataOffer *offer = device->cur_selection;

            struct wl_resource *old_local_offer = offer->local_data_offer;
            offer->local_data_offer = NULL;
            wl_resource_set_user_data(old_local_offer, NULL);
            wl_list_insert(&device->orphan_offer_resources, wl_resource_get_link(old_local_offer));

            struct wl_resource *local_offer = wl_resource_create(wl_resource_get_client(device->local_data_device),
                                                                 &wl_data_offer_interface,
                                                                 wl_resource_get_version(device->local_data_device),
                                                                 0);
            if (local_offer == NULL) {
                g_warning("Failed to create new local primary offer resource");
                wl_data_device_send_selection(device->local_data_device, NULL);
            } else {
                wl_resource_set_implementation(local_offer,
                                               &local_data_offer_impl,
                                               offer,
                                               local_data_offer_destroy_impl);
                offer->local_data_offer = local_offer;

                wl_data_device_send_data_offer(device->local_data_device, local_offer);
                for (guint i = 0; i < offer->mime_types->len; ++i) {
                    wl_data_offer_send_offer(local_offer, g_ptr_array_index(offer->mime_types, i));
                }
                wl_data_device_send_selection(device->local_data_device, local_offer);
            }
        } else {
            wl_data_device_send_selection(device->local_data_device, NULL);
        }
    }
}

static void
seat_destroyed(WleSeat *seat, WleDataDevice *device) {
    TRACE("entering");
    g_signal_handlers_disconnect_by_data(seat, device);
    device->seat = NULL;
    wl_resource_destroy(device->local_data_device);
}


static void
local_data_device_manager_create_data_source(
    struct wl_client *client,
    struct wl_resource *data_device_manager_resource,
    uint32_t id) {
    WleDataDeviceManager *manager = wl_resource_get_user_data(data_device_manager_resource);

    struct wl_data_source *remote_source = wl_data_device_manager_create_data_source(manager->remote_data_device_manager);
    if (remote_source == NULL) {
        wle_propagate_remote_error(manager->remote_display, client, data_device_manager_resource);
    } else {
        struct wl_resource *local_source = wl_resource_create(client,
                                                              &wl_data_source_interface,
                                                              wl_resource_get_version(data_device_manager_resource),
                                                              id);
        if (local_source == NULL) {
            wl_client_post_no_memory(client);
            wl_data_source_destroy(remote_source);
        } else {
            WleDataSource *source = g_new0(WleDataSource, 1);
            source->remote_data_source = _wle_proxy_set_ours((struct wl_proxy *)remote_source);
            source->local_data_source = local_source;

            wl_resource_set_implementation(local_source,
                                           &local_data_source_impl,
                                           source,
                                           local_data_source_destroy_impl);
            wl_data_source_add_listener(remote_source, &remote_data_source_listener, source);

            wl_list_insert(&manager->sources, wl_resource_get_link(local_source));
        }
    }
}

static void
local_data_device_manager_get_data_device(
    struct wl_client *client,
    struct wl_resource *data_device_manager_resource,
    uint32_t id,
    struct wl_resource *seat_resource) {
    WleDataDeviceManager *manager = wl_resource_get_user_data(data_device_manager_resource);
    WleSeat *seat = seat_resource != NULL ? wl_resource_get_user_data(seat_resource) : NULL;

    if (!WLE_IS_SEAT(seat)) {
        wl_client_post_implementation_error(client, "null or unknown seat");
    } else {
        struct wl_data_device *remote_device = wl_data_device_manager_get_data_device(manager->remote_data_device_manager,
                                                                                      _wle_seat_get_remote_seat(seat));
        if (remote_device == NULL) {
            wle_propagate_remote_error(manager->remote_display, client, data_device_manager_resource);
        } else {
            struct wl_resource *local_device = wl_resource_create(client,
                                                                  &wl_data_device_interface,
                                                                  wl_resource_get_version(data_device_manager_resource),
                                                                  id);
            if (local_device == NULL) {
                wl_client_post_no_memory(client);
                wl_data_device_destroy(remote_device);
            } else {
                WleDataDevice *device = g_new0(WleDataDevice, 1);
                device->manager = manager;
                device->seat = seat;
                device->remote_data_device = _wle_proxy_set_ours((struct wl_proxy *)remote_device);
                device->local_data_device = local_device;
                wl_list_init(&device->offers);
                wl_list_init(&device->orphan_offer_resources);

                wl_resource_set_implementation(local_device,
                                               &local_data_device_impl,
                                               device,
                                               local_data_device_destroy_impl);
                wl_data_device_add_listener(remote_device, &remote_data_device_listener, device);

                g_signal_connect(seat, "focus-changed",
                                 G_CALLBACK(seat_focus_changed), device);
                g_signal_connect(seat, "destroy",
                                 G_CALLBACK(seat_destroyed), device);

                wl_list_insert(&manager->devices, wl_resource_get_link(local_device));
            }
        }
    }
}

static void
local_data_device_manager_destroy_impl(struct wl_resource *data_device_manager_resource) {
    wl_list_remove(wl_resource_get_link(data_device_manager_resource));
}

static void
local_data_device_manager_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleDataDeviceManager *manager = WLE_DATA_DEVICE_MANAGER(data);

    if (version > wl_global_get_version(manager->local_data_device_manager)) {
        wl_client_post_implementation_error(client, "unsupported wl_data_device_manager version");
    } else {
        struct wl_resource *local_manager = wl_resource_create(client,
                                                               &wl_data_device_manager_interface,
                                                               version,
                                                               id);
        if (local_manager == NULL) {
            wl_client_post_no_memory(client);
        } else {
            wl_resource_set_implementation(local_manager,
                                           &local_data_device_manager_impl,
                                           manager,
                                           local_data_device_manager_destroy_impl);
            wl_list_insert(&manager->local_data_device_manager_resources, wl_resource_get_link(local_manager));
        }
    }
}

WleDataDeviceManager *
wle_data_device_manager_new(struct wl_display *remote_display,
                            struct wl_data_device_manager *remote_manager,
                            struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_manager != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleDataDeviceManager *manager = g_object_new(WLE_TYPE_DATA_DEVICE_MANAGER, NULL);
    manager->local_data_device_manager = wl_global_create(local_display,
                                                          &wl_data_device_manager_interface,
                                                          wl_proxy_get_version((struct wl_proxy *)remote_manager),
                                                          manager,
                                                          local_data_device_manager_bind);
    if (manager->local_data_device_manager == NULL) {
        g_object_unref(manager);
        wl_data_device_manager_destroy(remote_manager);
        return NULL;
    } else {
        manager->remote_display = remote_display;
        manager->remote_data_device_manager = remote_manager;
        return manager;
    }
}
