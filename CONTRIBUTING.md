# Implementation Notes

These notes are an attempt to make it easier to dive into libwlembed in
order to make contributions.  They are likely far from complete, but
hopefully should give an idea of how things work.

## Ownership

The topic of ownership is a little tricky.  The library uses `GObject`,
which is a reference-counted object system for C.  But who owns the
reference for various objects?

The traditional view is that whoever creates an instance owns the
reference to it.  Other bits of code that want ownership should take
extra references.

For example, `WleCompositor` is responsible for implementing the Wayland
`wl_compositor` interface.  When a client wants to create a new
`wl_surface`, `WleCompositor` creates a `WleView` to hold both the
embedded compositor's `wl_resource`, and the parent compositor's
`wl_surface`.  So, traditionally, `WleCompositor` should own that
reference.

But what really controls the lifecycle of that `WleView`?  Well, I
suppose you could say it's two things:

1. In one way, the Wayland client owns the reference (meaning that the
   `wl_resource` owns it).  The Wayland client can destroy the surface
   whenever it wants, so that implies some level of ownership.
2. `WleCompositor` does own it as well, though: if, instead of the
   client destroying the surface, the entire embedded compositor were to
   be shut down, `WleCompositor` would be responsible for destroying and
   freeing all resources.

One approach would be for both to have ownership: the normal situation
is that `WleView` will have a reference count of two.  If the client
destroys the surface, we'll get notified, and we can reduce the
reference count to one, and then emit a signal saying the surface has
been destroyed, which can cause `WleCompositor` to do any housekeeping
it needs to do, and then reduce the reference count to zero, which frees
the instance.

We could also do either alternative with a single owner: `WleCompositor`
owns the instance, reacts to the `destroy` signal, and frees the
instance when the client destroys the surface.  Or the Wayland client
can own the reference, and destroy it itself, with `WleCompositor`
still able to react to the `destroy` signal to do its own housekeeping.

All of these approaches have pluses and minuses.  Dual ownership means
that we can't make a mistake and have the instance be freed while either
`WleCompositor` or the `wl_resource` thinks the instance is still alive.
But it also means that destroying the instance from our code (say, in
`WleCompositor`'s destructor) means more steps: instead of just reducing
the reference count, `WleCompositor` has to also actively tell the
`WleView` to destroy the `wl_resource`, so that the second reference
will also be removed.

Single ownership is simpler from a reasoning perspective, but means that
one or the other of the "conceptual owners" could think the instance is
still alive when it isn't.

For `WleView` this isn't such a big deal, as there's only one resource
involved (the `wle_surface`).  But unfortunately there are cases where
this matters.

Wayland's xdg-shell protocol involves "stacking" or "inheriting" surface
roles.  First you create an `xdg_surface`, and then, on top of that, you
can create either an `xdg_toplevel` or `xdg_popup`.  If we have a role
class, that means we have *three* entities with possible ownership
interest: `WleXdgShell` (which created the thing in the first place),
and two references for the `WleToplevel` (or `WlePopup`): one for the
`xdg_toplevel` (or `xdg_popup`), and another for the `xdg_surface`.

If we were guaranteed to know the order in which these would be
destroyed (say, first the `xdg_toplevel` and then the `xdg_surface`),
it could still be fine to have a single-ownership model.  Unfortunately,
when the Wayland client exits, `libwayland-server` will just iterate
over a map of all the objects the client had, in whatever order it feels
like, destroying them one by one.  This can result in the `xdg_surface`
getting destroyed before the `xdg_toplevel`, which means we can't rely
on any particular order to determine how we release the final reference.

So, to make a long story slightly less long, I've opted to go for a
multi-owner situation, which gives us the following properties:

1. Theses sorts of instances all have a `destroy` signal, which is
   emitted whenever something happens to make the instance no longer
   valid and complete.
2. The creator of an instance (`WleCompositor` or `WleXdgShell` in the
   example above) will own a reference, and connect to the `destroy`
   signal.  On destruction, it should do any housekeeping it needs to,
   and drop its reference.
3. Every `wl_resource` (the handle for the client's reference to the
   object) will take a reference as well.  When the client (or
   `libwayland-server`, in the case where a client just quits) destroys
   the resource, we'll again do any housekeeping necessary, and drop the
   reference.
4. The classes themselves will have to expose a destructor method (e.g.
   `wle_view_destroy()`), which pro-actively destroys client resources.
   This is to handle the case where the client is still running, but the
   embedded compositor itself is being shut down.
5. As an added possible annoyance, the creator-owner, in the case where
   they need to call `_destroy()`, they need to first disconnect from
   the `destroy` signal so they don't try to double-release their
   reference.

In order to make it more obvious and understandable, such classes will
inherit from the `WleLocalResource` base class.  (This really should be
an interface, but interface method chaining is a pain in `GObject`, so
oh well.)  Note that there are some entities that also own local
resources, but it didn't really make sense to use `GObject` for them, so
they don't entirely follow this pattern.
