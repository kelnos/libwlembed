/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <signal.h>

#include "wle-child-process-manager.h"

struct _WleChildProcessManager {
    GChildWatchFunc callback;
    gpointer callback_user_data;
    GList *processes;
};

typedef struct {
    GPid pid;
    guint child_watch_id;
} ChildProcess;

static void
child_exited(GPid pid, gint wait_status, gpointer user_data) {
    WleChildProcessManager *manager = user_data;
    for (GList *l = manager->processes; l != NULL; l = l->next) {
        ChildProcess *process = l->data;
        if (process->pid == pid) {
            manager->processes = g_list_remove_link(manager->processes, l);
            g_free(process);

            if (manager->callback != NULL) {
                manager->callback(pid, wait_status, manager->callback_user_data);
            }

            g_spawn_close_pid(pid);

            break;
        }
    }
}

static void
orphaned_child_exited(GPid pid, gint wait_status, gpointer user_data) {
    g_spawn_close_pid(pid);
}

static void
forget_processes(WleChildProcessManager *manager, gboolean terminate) {
    GList *processes = manager->processes;
    manager->processes = NULL;

    for (GList *l = processes; l != NULL; l = l->next) {
        ChildProcess *process = l->data;

        g_source_remove(process->child_watch_id);
        process->child_watch_id = 0;
        g_child_watch_add(process->pid, orphaned_child_exited, NULL);

        if (terminate) {
            kill(process->pid, SIGTERM);
        }
    }

    g_list_free_full(processes, g_free);
}

WleChildProcessManager *
wle_child_process_manager_new(GChildWatchFunc callback, gpointer callback_user_data) {
    WleChildProcessManager *manager = g_new0(WleChildProcessManager, 1);
    manager->callback = callback;
    manager->callback_user_data = callback_user_data;
    return manager;
}

void
wle_child_process_manager_add_pid(WleChildProcessManager *manager, GPid pid) {
    ChildProcess *process = g_new0(ChildProcess, 1);
    process->pid = pid;
    manager->processes = g_list_prepend(manager->processes, process);
    process->child_watch_id = g_child_watch_add(pid, child_exited, manager);
}

void
wle_child_process_manager_terminate_all(WleChildProcessManager *manager) {
    forget_processes(manager, TRUE);
}

void
wle_child_process_manager_destroy(WleChildProcessManager *manager) {
    forget_processes(manager, FALSE);
    // forget_processes() will free the process list
    g_free(manager);
}
