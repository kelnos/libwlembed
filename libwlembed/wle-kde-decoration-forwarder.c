/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server.h>

#include "common/wle-debug.h"
#include "protocol/server-decoration-client-protocol.h"
#include "protocol/server-decoration-protocol.h"
#include "wle-kde-decoration-forwarder.h"
#include "wle-util.h"
#include "wle-view.h"

struct _WleKdeDecorationForwarder {
    struct wl_display *remote_display;
    struct org_kde_kwin_server_decoration_manager *remote_decoration_manager;

    struct wl_global *local_decoration_manager;
    struct wl_list local_decoration_manager_resources;

    struct wl_list decorations;
    uint32_t mgr_default_mode;
};

typedef struct {
    WleView *view;

    struct org_kde_kwin_server_decoration *remote_decoration;
    struct wl_resource *local_decoration;

    struct wl_list link;
} KdeDecoration;

static void remote_decoration_manager_default_mode(void *data,
                                                   struct org_kde_kwin_server_decoration_manager *manager,
                                                   enum org_kde_kwin_server_decoration_mode mode);
static void remote_decoration_mode(void *data,
                                   struct org_kde_kwin_server_decoration *decoration,
                                   enum org_kde_kwin_server_decoration_mode mode);

static void local_decoration_request_mode(struct wl_client *client,
                                          struct wl_resource *decoration_resource,
                                          uint32_t mode);
static void local_decoration_release(struct wl_client *client,
                                     struct wl_resource *decoration_resource);

static void local_decoration_manager_create(struct wl_client *client,
                                            struct wl_resource *decoration_manager_resource,
                                            uint32_t decoration_id,
                                            struct wl_resource *surface_resource);


static const struct org_kde_kwin_server_decoration_listener remote_decoration_listener = {
    .mode = remote_decoration_mode,
};

static const struct org_kde_kwin_server_decoration_interface local_decoration_impl = {
    .request_mode = local_decoration_request_mode,
    .release = local_decoration_release,
};

static const struct org_kde_kwin_server_decoration_manager_listener remote_mgr_listener = {
    .default_mode = remote_decoration_manager_default_mode,
};

static const struct org_kde_kwin_server_decoration_manager_interface local_mgr_impl = {
    .create = local_decoration_manager_create,
};


static void
remote_decoration_manager_default_mode(void *data,
                                       struct org_kde_kwin_server_decoration_manager *manager,
                                       enum org_kde_kwin_server_decoration_mode mode) {
    DBG("Got default KDE decoration mode %d from remote", mode);
    WleKdeDecorationForwarder *forwarder = data;
    forwarder->mgr_default_mode = mode;

    struct wl_resource *resource;
    wl_resource_for_each(resource, &forwarder->local_decoration_manager_resources) {
        org_kde_kwin_server_decoration_manager_send_default_mode(resource, mode);
    }
}

static void
remote_decoration_mode(void *data,
                       struct org_kde_kwin_server_decoration *decoration,
                       enum org_kde_kwin_server_decoration_mode mode) {
    DBG("Got KDE decoration mode %d from remote", mode);
    KdeDecoration *decor = data;
    org_kde_kwin_server_decoration_send_mode(decor->local_decoration, mode);
}

static void
local_decoration_request_mode(struct wl_client *client,
                              struct wl_resource *decoration_resource,
                              uint32_t mode) {
    KdeDecoration *decor = wl_resource_get_user_data(decoration_resource);

    if (decor->remote_decoration != NULL) {
        DBG("Telling forwarded surface to have KDE decoration %d", mode);
        org_kde_kwin_server_decoration_request_mode(decor->remote_decoration, mode);
    } else {
        DBG("Telling embedded surface to have no KDE decoration");
        org_kde_kwin_server_decoration_send_mode(decoration_resource, ORG_KDE_KWIN_SERVER_DECORATION_MODE_NONE);
    }
}

static void
local_decoration_release(struct wl_client *client, struct wl_resource *decoration_resource) {
    KdeDecoration *decor = wl_resource_get_user_data(decoration_resource);
    if (decor->remote_decoration != NULL) {
        org_kde_kwin_server_decoration_release(decor->remote_decoration);
    }
}

static void
local_decoration_destroy_impl(struct wl_resource *resource) {
    KdeDecoration *decor = wl_resource_get_user_data(resource);
    if (decor->view != NULL) {
        g_signal_handlers_disconnect_by_data(decor->view, decor);
    }
    if (decor->remote_decoration != NULL) {
        org_kde_kwin_server_decoration_destroy(decor->remote_decoration);
    }
    wl_list_remove(&decor->link);
    g_free(decor);
}

static void
view_mode_changed(WleView *view, KdeDecoration *decor) {
    if (_wle_view_is_embedded(view) && decor->remote_decoration != NULL) {
        org_kde_kwin_server_decoration_destroy(decor->remote_decoration);
        decor->remote_decoration = NULL;
    }
}

static void
view_destroyed(WleView *view, KdeDecoration *decor) {
    decor->remote_decoration = NULL;  // remote compositor will have destroyed this for us
    wl_resource_destroy(decor->local_decoration);
}

static void
local_decoration_manager_create(struct wl_client *client,
                                struct wl_resource *decoration_manager_resource,
                                uint32_t decoration_id,
                                struct wl_resource *surface_resource) {
    WleKdeDecorationForwarder *forwarder = wl_resource_get_user_data(decoration_manager_resource);
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    DBG("new KDE decoration");

    gboolean ok = TRUE;
    struct org_kde_kwin_server_decoration *remote_decoration = NULL;
    if (_wle_view_is_forwarded(view)) {
        remote_decoration = org_kde_kwin_server_decoration_manager_create(forwarder->remote_decoration_manager,
                                                                          wle_view_get_remote_surface(view));
        if (remote_decoration == NULL) {
            ok = FALSE;
            wle_propagate_remote_error(forwarder->remote_display, client, decoration_manager_resource);
        }
    }

    if (ok) {
        struct wl_resource *resource = wl_resource_create(client,
                                                          &org_kde_kwin_server_decoration_interface,
                                                          wl_resource_get_version(decoration_manager_resource),
                                                          decoration_id);
        if (resource == NULL) {
            if (remote_decoration != NULL) {
                org_kde_kwin_server_decoration_destroy(remote_decoration);
            }
            wl_client_post_no_memory(client);
        } else {
            KdeDecoration *decor = g_new0(KdeDecoration, 1);

            decor->view = view;
            g_signal_connect(view, "mode-changed",
                             G_CALLBACK(view_mode_changed), decor);
            g_signal_connect(view, "destroy",
                             G_CALLBACK(view_destroyed), decor);

            decor->local_decoration = resource;
            wl_resource_set_implementation(resource, &local_decoration_impl, decor, local_decoration_destroy_impl);

            if (remote_decoration != NULL) {
                decor->remote_decoration = _wle_proxy_set_ours((struct wl_proxy *)remote_decoration);
                org_kde_kwin_server_decoration_add_listener(remote_decoration, &remote_decoration_listener, decor);
            }

            wl_list_insert(&forwarder->decorations, &decor->link);
        }
    }
}

static void
local_mgr_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_decoration_manager_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleKdeDecorationForwarder *forwarder = data;
    struct wl_resource *resource = wl_resource_create(client,
                                                      &org_kde_kwin_server_decoration_manager_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource, &local_mgr_impl, forwarder, local_mgr_destroy_impl);
        if (forwarder == NULL) {
            wl_list_init(wl_resource_get_link(resource));
        } else {
            wl_list_insert(&forwarder->local_decoration_manager_resources, wl_resource_get_link(resource));
            org_kde_kwin_server_decoration_manager_send_default_mode(resource, forwarder->mgr_default_mode);
        }
    }
}

WleKdeDecorationForwarder *
wle_kde_decoration_forwarder_create(struct wl_display *remote_display,
                                    struct org_kde_kwin_server_decoration_manager *remote_manager,
                                    struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_manager != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleKdeDecorationForwarder *forwarder = g_new0(WleKdeDecorationForwarder, 1);
    forwarder->local_decoration_manager = wl_global_create(local_display,
                                                           &org_kde_kwin_server_decoration_manager_interface,
                                                           wl_proxy_get_version((struct wl_proxy *)remote_manager),
                                                           forwarder,
                                                           local_decoration_manager_bind);

    if (forwarder->local_decoration_manager != NULL) {
        forwarder->remote_display = remote_display;
        forwarder->remote_decoration_manager = remote_manager;
        wl_list_init(&forwarder->local_decoration_manager_resources);
        wl_list_init(&forwarder->decorations);

        org_kde_kwin_server_decoration_manager_add_listener(forwarder->remote_decoration_manager,
                                                            &remote_mgr_listener,
                                                            forwarder);
        wl_display_roundtrip(remote_display);

        return forwarder;
    } else {
        org_kde_kwin_server_decoration_manager_destroy(remote_manager);
        g_free(forwarder);
        return NULL;
    }
}

void
wle_kde_decoration_forwarder_destroy(WleKdeDecorationForwarder *forwarder) {
    if (forwarder != NULL) {
        KdeDecoration *decor, *tmp_decor;
        wl_list_for_each_safe(decor, tmp_decor, &forwarder->decorations, link) {
            wl_resource_destroy(decor->local_decoration);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &forwarder->local_decoration_manager_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(forwarder->local_decoration_manager);

        org_kde_kwin_server_decoration_manager_destroy(forwarder->remote_decoration_manager);

        g_free(forwarder);
    }
}
