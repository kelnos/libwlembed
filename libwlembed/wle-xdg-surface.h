/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_XDG_SURFACE_H__
#define __WLE_XDG_SURFACE_H__

#include <glib-object.h>

#include "protocol/xdg-shell-client-protocol.h"
#include "wle-view-role.h"
#include "wle-view.h"

G_BEGIN_DECLS

G_DECLARE_DERIVABLE_TYPE(WleXdgSurface, wle_xdg_surface, WLE, XDG_SURFACE, WleViewRole)
#define WLE_TYPE_XDG_SURFACE (wle_xdg_surface_get_type())

struct _WleXdgSurfaceClass {
    WleViewRoleClass parent_class;

    void (*commit_state)(WleXdgSurface *xdg_surface,
                         uint32_t serial);
};

WleView *wle_xdg_surface_get_view(WleXdgSurface *xdg_surface);

struct xdg_surface *wle_xdg_surface_get_remote_xdg_surface(WleXdgSurface *xdg_surface);
struct xdg_surface *wle_xdg_surface_steal_remote_xdg_surface(WleXdgSurface *xdg_surface);

struct wl_resource *wle_xdg_surface_steal_local_xdg_surface(WleXdgSurface *xdg_surface);

void wle_xdg_surface_update_size(WleXdgSurface *xdg_surface,
                                 int32_t width,
                                 int32_t height);

void wle_xdg_surface_get_size(WleXdgSurface *xdg_surface,
                              int32_t *width,
                              int32_t *height);

void wle_xdg_surface_set_window_geometry(WleXdgSurface *xdg_surface,
                                         int32_t x,
                                         int32_t y,
                                         int32_t width,
                                         int32_t height);
void wle_xdg_surface_get_window_geometry(WleXdgSurface *xdg_surface,
                                         int32_t *x,
                                         int32_t *y,
                                         int32_t *width,
                                         int32_t *height);

void wle_xdg_surface_ack_configure(WleXdgSurface *xdg_surface,
                                   uint32_t serial);

void wle_xdg_surface_send_configure(WleXdgSurface *xdg_surface,
                                    uint32_t serial);

G_END_DECLS

#endif /* __WLE_XDG_SURFACE_H__ */
