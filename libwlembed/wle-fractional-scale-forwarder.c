/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <wayland-server-core.h>
#include <wayland-server.h>

#include "fractional-scale-v1-client-protocol.h"
#include "protocol/fractional-scale-v1-protocol.h"
#include "wle-fractional-scale-forwarder.h"
#include "wle-util.h"
#include "wle-view.h"

#define FRACTIONAL_SCALE_VERSION 1

struct _WleFractionalScaleForwarder {
    struct wl_display *remote_display;
    struct wp_fractional_scale_manager_v1 *remote_manager;

    struct wl_global *local_manager;
    struct wl_list local_manager_resources;

    struct wl_list fractional_scales;
};

typedef struct {
    WleView *view;
    struct wp_fractional_scale_v1 *remote_fractional_scale;
    struct wl_resource *local_fractional_scale;

    struct wl_list link;
} WleFractionalScale;

static void remote_preferred_scale(void *data,
                                   struct wp_fractional_scale_v1 *fractional_scale,
                                   uint32_t scale);

static void local_fractional_scale_destroy(struct wl_client *client,
                                           struct wl_resource *fractional_scale);


static void local_manager_get_fractional_scale(struct wl_client *client,
                                               struct wl_resource *manager,
                                               uint32_t id,
                                               struct wl_resource *surface);
static void local_manager_destroy(struct wl_client *client,
                                  struct wl_resource *manager);

static const struct wp_fractional_scale_v1_listener remote_fractional_scale_listener = {
    .preferred_scale = remote_preferred_scale,
};

static const struct wp_fractional_scale_v1_interface local_fractional_scale_impl = {
    .destroy = local_fractional_scale_destroy,
};

static const struct wp_fractional_scale_manager_v1_interface local_manager_impl = {
    .get_fractional_scale = local_manager_get_fractional_scale,
    .destroy = local_manager_destroy,
};

static void
remote_preferred_scale(void *data, struct wp_fractional_scale_v1 *wp_fractional_scale, uint32_t scale) {
    WleFractionalScale *fractional_scale = data;
    wp_fractional_scale_v1_send_preferred_scale(fractional_scale->local_fractional_scale, scale);
}

static void
local_fractional_scale_destroy(struct wl_client *client, struct wl_resource *fractional_scale) {
    wl_resource_destroy(fractional_scale);
}

static void
local_fractional_scale_destroy_impl(struct wl_resource *resource) {
    WleFractionalScale *fractional_scale = wl_resource_get_user_data(resource);
    wp_fractional_scale_v1_destroy(fractional_scale->remote_fractional_scale);
    wl_list_remove(&fractional_scale->link);
    g_free(fractional_scale);
}

static void
local_manager_get_fractional_scale(struct wl_client *client,
                                   struct wl_resource *manager,
                                   uint32_t id,
                                   struct wl_resource *surface_resource) {
    WleFractionalScaleForwarder *forwarder = wl_resource_get_user_data(manager);
    WleView *view = WLE_VIEW(wl_resource_get_user_data(surface_resource));

    WleFractionalScale *fractional_scale = NULL;
    wl_list_for_each(fractional_scale, &forwarder->fractional_scales, link) {
        if (fractional_scale->view == view) {
            wl_resource_post_error(manager,
                                   WP_FRACTIONAL_SCALE_MANAGER_V1_ERROR_FRACTIONAL_SCALE_EXISTS,
                                   "fractional scale already exists for this surface");
            return;
        }
    }

    struct wl_resource *resource = wl_resource_create(client,
                                                      &wp_fractional_scale_v1_interface,
                                                      wl_resource_get_version(manager),
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        struct wp_fractional_scale_v1 *remote_fractional_scale = wp_fractional_scale_manager_v1_get_fractional_scale(forwarder->remote_manager,
                                                                                                                     wle_view_get_remote_surface(view));
        if (remote_fractional_scale == NULL) {
            wle_propagate_remote_error(forwarder->remote_display, client, manager);
            wl_resource_destroy(resource);
        } else {
            fractional_scale = g_new0(WleFractionalScale, 1);
            fractional_scale->view = view;
            fractional_scale->remote_fractional_scale = _wle_proxy_set_ours((struct wl_proxy *)remote_fractional_scale);
            fractional_scale->local_fractional_scale = resource;

            wp_fractional_scale_v1_add_listener(remote_fractional_scale,
                                                &remote_fractional_scale_listener,
                                                fractional_scale);
            wl_resource_set_implementation(resource,
                                           &local_fractional_scale_impl,
                                           fractional_scale,
                                           local_fractional_scale_destroy_impl);
            wl_list_insert(&forwarder->fractional_scales, &fractional_scale->link);
        }
    }
}

static void
local_manager_destroy(struct wl_client *client, struct wl_resource *manager) {
    wl_resource_destroy(manager);
}

static void
local_manager_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_server_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleFractionalScaleForwarder *forwarder = data;

    struct wl_resource *resource = wl_resource_create(client, &wp_fractional_scale_manager_v1_interface, version, id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource, &local_manager_impl, forwarder, local_manager_destroy_impl);

        if (forwarder == NULL) {
            wl_list_init(wl_resource_get_link(resource));
            wl_resource_set_user_data(resource, NULL);
        } else {
            wl_list_insert(&forwarder->local_manager_resources, wl_resource_get_link(resource));
        }
    }
}

WleFractionalScaleForwarder *
wle_fractional_scale_forwarder_create(struct wl_display *remote_display,
                                      struct wp_fractional_scale_manager_v1 *remote_manager,
                                      struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_manager != NULL, NULL);
    ;
    g_return_val_if_fail(local_display != NULL, NULL);

    WleFractionalScaleForwarder *forwarder = g_new0(WleFractionalScaleForwarder, 1);
    forwarder->local_manager = wl_global_create(local_display,
                                                &wp_fractional_scale_manager_v1_interface,
                                                FRACTIONAL_SCALE_VERSION,
                                                forwarder,
                                                local_server_bind);
    if (forwarder->local_manager != NULL) {
        forwarder->remote_display = remote_display;
        forwarder->remote_manager = remote_manager;
        wl_list_init(&forwarder->local_manager_resources);
        wl_list_init(&forwarder->fractional_scales);
        return forwarder;
    } else {
        wp_fractional_scale_manager_v1_destroy(remote_manager);
        g_free(forwarder);
        return NULL;
    }
}

void
wle_fractional_scale_forwarder_destroy(WleFractionalScaleForwarder *forwarder) {
    if (forwarder != NULL) {
        WleFractionalScale *fractional_scale, *tmp_fractional_scale;
        wl_list_for_each_safe(fractional_scale, tmp_fractional_scale, &forwarder->fractional_scales, link) {
            wl_resource_destroy(fractional_scale->local_fractional_scale);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &forwarder->local_manager_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(forwarder->local_manager);

        wp_fractional_scale_manager_v1_destroy(forwarder->remote_manager);

        g_free(forwarder);
    }
}
