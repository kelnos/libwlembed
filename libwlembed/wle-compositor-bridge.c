/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>
#include <glib-object.h>
#include <wayland-client-protocol.h>

#include "common/wle-debug.h"
#include "protocol/ext-foreign-toplevel-list-v1-client-protocol.h"
#include "protocol/fractional-scale-v1-client-protocol.h"
#include "protocol/presentation-time-client-protocol.h"
#include "protocol/primary-selection-unstable-v1-client-protocol.h"
#include "protocol/server-decoration-client-protocol.h"
#include "protocol/wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"
#include "protocol/xdg-activation-v1-client-protocol.h"
#include "protocol/xdg-decoration-unstable-v1-client-protocol.h"
#include "protocol/xdg-output-unstable-v1-client-protocol.h"
#include "wle-compositor-bridge.h"
#include "wle-compositor.h"
#include "wle-data-device-manager.h"
#include "wle-error.h"
#include "wle-ext-foreign-toplevel-list.h"
#include "wle-foreign-toplevel-manager.h"
#include "wle-fractional-scale-forwarder.h"
#include "wle-kde-decoration-forwarder.h"
#include "wle-linux-dmabuf.h"
#include "wle-output.h"
#include "wle-presentation-time.h"
#include "wle-primary-selection.h"
#include "wle-protocol-versions.h"
#include "wle-seat-private.h"
#include "wle-shm-forwarder.h"
#include "wle-util.h"
#include "wle-viewporter.h"
#include "wle-xdg-activation.h"
#include "wle-xdg-decoration-forwarder.h"
#include "wle-xdg-output-manager.h"
#include "wle-xdg-shell.h"

typedef struct {
    uint32_t name;
    uint32_t version;
} DeferredGlobal;

struct _WleCompositorBridge {
    GObject parent;

    struct {
        struct wl_display *display;
        struct wl_registry *registry;

        struct wl_compositor *compositor;
        struct wl_subcompositor *subcompositor;
    } remote;

    struct {
        struct wl_display *display;
    } local;

    WleCompositor *compositor;
    WleShmForwarder *shm_forwarder;
    WleLinuxDmabuf *linux_dmabuf;
    WleDataDeviceManager *data_device_manager;
    WlePrimarySelection *primary_selection;
    WleViewporter *viewporter;
    WlePresentationTime *presentation_time;
    WleFractionalScaleForwarder *fractional_scale_forwarder;
    WleForeignToplevelManager *foreign_toplevel_manager;
    WleXdgActivation *xdg_activation;
    WleKdeDecorationForwarder *kde_decoration_forwarder;
    WleXdgDecorationForwarder *xdg_decoration_forwarder;
    WleXdgOutputManager *xdg_output_manager;
    WleXdgShell *xdg_shell;

#ifdef HAVE_EXT_FOREIGN_TOPLEVEL_LIST
    WleExtForeignToplevelList *ext_foreign_toplevel_list;
#endif

    GList *deferred_outputs;  // DeferredGlobal

    GList *seats;  // WleSeat
    GList *outputs;  // WleOutput
};

enum {
    SIG_NEW_SEAT,
    SIG_SEAT_REMOVED,
    SIG_OUTPUT_REMOVED,

    N_SIGNALS,
};

static void wle_compositor_bridge_finalize(GObject *object);

static void remote_registry_global(void *data,
                                   struct wl_registry *wl_registry,
                                   uint32_t name,
                                   const char *interface,
                                   uint32_t version);
static void remote_registry_global_remove(void *data,
                                          struct wl_registry *wl_registry,
                                          uint32_t name);


G_DEFINE_FINAL_TYPE(WleCompositorBridge, wle_compositor_bridge, G_TYPE_OBJECT)


static guint signals[N_SIGNALS] = { 0 };

static const struct wl_registry_listener remote_registry_listener = {
    .global = remote_registry_global,
    .global_remove = remote_registry_global_remove,
};

static void
wle_compositor_bridge_class_init(WleCompositorBridgeClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->finalize = wle_compositor_bridge_finalize;

    signals[SIG_NEW_SEAT] = g_signal_new("new-seat",
                                         WLE_TYPE_COMPOSITOR_BRIDGE,
                                         G_SIGNAL_RUN_LAST,
                                         0,
                                         NULL, NULL,
                                         g_cclosure_marshal_VOID__OBJECT,
                                         G_TYPE_NONE, 1,
                                         WLE_TYPE_SEAT);

    signals[SIG_SEAT_REMOVED] = g_signal_new("seat-removed",
                                             WLE_TYPE_COMPOSITOR_BRIDGE,
                                             G_SIGNAL_RUN_LAST,
                                             0,
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__OBJECT,
                                             G_TYPE_NONE, 1,
                                             WLE_TYPE_SEAT);

    signals[SIG_OUTPUT_REMOVED] = g_signal_new("output-removed",
                                               WLE_TYPE_COMPOSITOR_BRIDGE,
                                               G_SIGNAL_RUN_LAST,
                                               0,
                                               NULL, NULL,
                                               g_cclosure_marshal_VOID__OBJECT,
                                               G_TYPE_NONE, 1,
                                               WLE_TYPE_OUTPUT);
}

static void
wle_compositor_bridge_init(WleCompositorBridge *bridge) {}

static void
wle_compositor_bridge_finalize(GObject *object) {
    WleCompositorBridge *bridge = WLE_COMPOSITOR_BRIDGE(object);

    // The order of destroying things here is unfortunately somewhat important.
    //
    // WleShmForwarder needs to live longer than WleCompositor, because
    // WleCompositor will free WleViews, which can hold WleBuffers, which are
    // owned by the WleShmForwarder.
    //
    // WleCompositor needs to live longer than the WleSeat instances, because
    // The WleSeat instances hold onto WLeView instances sometimes, and those
    // references need to be released before WleCompositor frees the list of
    // WleViews.
    //
    // This is all an unforunate side-effect of the normal pattern of using
    // wl_list where resources "remove themselves" when they are freed.  If the
    // head of the list gets freed before the resource does, then you crash.
    // Eventually I will probably want to rethink how this works.

    g_list_free_full(bridge->deferred_outputs, g_free);
    g_list_free_full(bridge->seats, g_object_unref);
    g_list_free_full(bridge->outputs, g_object_unref);

#ifdef HAVE_EXT_FOREIGN_TOPLEVEL_LIST
    wle_ext_foreign_toplevel_list_destroy(bridge->ext_foreign_toplevel_list);
#endif

    if (bridge->xdg_shell != NULL) {
        g_object_unref(bridge->xdg_shell);
    }

    if (bridge->data_device_manager != NULL) {
        g_object_unref(bridge->data_device_manager);
    }

    wle_viewporter_destroy(bridge->viewporter);
    wle_primary_selection_destroy(bridge->primary_selection);
    wle_linux_dmabuf_destroy(bridge->linux_dmabuf);
    wle_presentation_time_destroy(bridge->presentation_time);
    wle_fractional_scale_forwarder_destroy(bridge->fractional_scale_forwarder);
    wle_foreign_toplevel_manager_destroy(bridge->foreign_toplevel_manager);
    wle_xdg_activation_destroy(bridge->xdg_activation);
    wle_xdg_output_manager_destroy(bridge->xdg_output_manager);
    wle_xdg_decoration_forwarder_destroy(bridge->xdg_decoration_forwarder);
    wle_kde_decoration_forwarder_destroy(bridge->kde_decoration_forwarder);

    if (bridge->compositor != NULL) {
        g_object_unref(bridge->compositor);
    }
    if (bridge->remote.subcompositor != NULL) {
        wl_subcompositor_destroy(bridge->remote.subcompositor);
    }
    if (bridge->remote.compositor != NULL) {
        wl_compositor_destroy(bridge->remote.compositor);
    }

    wle_shm_forwarder_destroy(bridge->shm_forwarder);

    if (bridge->remote.registry != NULL) {
        wl_registry_destroy(bridge->remote.registry);
    }

    G_OBJECT_CLASS(wle_compositor_bridge_parent_class)->finalize(object);
}

static void
new_remote_serial(WleCompositorBridge *bridge, uint32_t serial) {
    // TRACE("got new remote serial %u; local is at %u", serial, wl_display_get_serial(bridge->local.display));
    while (wl_display_get_serial(bridge->local.display) < serial) {
        wl_display_next_serial(bridge->local.display);
    }
}

static void
remote_registry_global(void *data,
                       struct wl_registry *registry,
                       uint32_t name,
                       const char *interface,
                       uint32_t version) {
    WleCompositorBridge *bridge = data;

    if (g_strcmp0(interface, wl_compositor_interface.name) == 0) {
        bridge->remote.compositor = _wle_registry_bind(registry,
                                                       name,
                                                       &wl_compositor_interface,
                                                       MIN(version, WL_COMPOSITOR_VERSION));
    } else if (g_strcmp0(interface, wl_subcompositor_interface.name) == 0) {
        bridge->remote.subcompositor = _wle_registry_bind(registry,
                                                          name,
                                                          &wl_subcompositor_interface,
                                                          MIN(version, WL_SUBCOMPOSITOR_VERSION));
    } else if (g_strcmp0(interface, wl_data_device_manager_interface.name) == 0) {
        struct wl_data_device_manager *remote_data_device_manager = _wle_registry_bind(registry,
                                                                                       name,
                                                                                       &wl_data_device_manager_interface,
                                                                                       MIN(version, WL_DATA_DEVICE_MANAGER_VERSION));
        bridge->data_device_manager = wle_data_device_manager_new(bridge->remote.display,
                                                                  remote_data_device_manager,
                                                                  bridge->local.display);
        g_signal_connect_swapped(bridge->data_device_manager, "new-serial",
                                 G_CALLBACK(new_remote_serial), bridge);
    } else if (g_strcmp0(interface, wl_seat_interface.name) == 0) {
        WleSeat *seat = _wle_seat_new(bridge->remote.display,
                                      bridge->remote.registry,
                                      name,
                                      MIN(version, WL_SEAT_VERSION),
                                      bridge->local.display);
        bridge->seats = g_list_append(bridge->seats, seat);
        g_signal_connect_swapped(seat, "new-serial",
                                 G_CALLBACK(new_remote_serial), bridge);
        g_signal_emit(bridge, signals[SIG_NEW_SEAT], 0, seat);
    } else if (g_strcmp0(interface, wl_output_interface.name) == 0) {
        DeferredGlobal *dg = g_new0(DeferredGlobal, 1);
        dg->name = name;
        dg->version = MIN(version, WL_OUTPUT_VERSION);
        bridge->deferred_outputs = g_list_append(bridge->deferred_outputs, dg);
    } else if (g_strcmp0(interface, wp_viewporter_interface.name) == 0) {
        struct wp_viewporter *remote_viewporter = _wle_registry_bind(registry,
                                                                     name,
                                                                     &wp_viewporter_interface,
                                                                     MIN(version, WP_VIEWPORTER_VERSION));
        bridge->viewporter = wle_viewporter_new(bridge->remote.display,
                                                remote_viewporter,
                                                bridge->local.display);
    } else if (g_strcmp0(interface, xdg_wm_base_interface.name) == 0) {
        struct xdg_wm_base *remote_xdg_wm_base = _wle_registry_bind(registry,
                                                                    name,
                                                                    &xdg_wm_base_interface,
                                                                    MIN(version, XDG_SHELL_VERSION));
        bridge->xdg_shell = wle_xdg_shell_new(bridge->remote.display,
                                              bridge->remote.compositor,
                                              remote_xdg_wm_base,
                                              bridge->local.display);
        g_signal_connect_swapped(bridge->xdg_shell, "new-serial",
                                 G_CALLBACK(new_remote_serial), bridge);
    } else if (g_strcmp0(interface, zxdg_decoration_manager_v1_interface.name) == 0) {
        struct zxdg_decoration_manager_v1 *remote_decoration_manager = _wle_registry_bind(registry,
                                                                                          name,
                                                                                          &zxdg_decoration_manager_v1_interface,
                                                                                          MIN(version, XDG_DECORATION_VERSION));
        bridge->xdg_decoration_forwarder = wle_xdg_decoration_forwarder_create(bridge->remote.display,
                                                                               remote_decoration_manager,
                                                                               bridge->local.display);
    } else if (g_strcmp0(interface, org_kde_kwin_server_decoration_manager_interface.name) == 0) {
        struct org_kde_kwin_server_decoration_manager *remote_decoration_manager = _wle_registry_bind(registry,
                                                                                                      name,
                                                                                                      &org_kde_kwin_server_decoration_manager_interface,
                                                                                                      MIN(version, KDE_SERVER_DECORATION_VERSION));
        bridge->kde_decoration_forwarder = wle_kde_decoration_forwarder_create(bridge->remote.display,
                                                                               remote_decoration_manager,
                                                                               bridge->local.display);
    } else if (g_strcmp0(interface, wp_fractional_scale_manager_v1_interface.name) == 0) {
        struct wp_fractional_scale_manager_v1 *remote_manager = _wle_registry_bind(registry,
                                                                                   name,
                                                                                   &wp_fractional_scale_manager_v1_interface,
                                                                                   MIN(version, WP_FRACTIONAL_SCALE_VERSION));
        bridge->fractional_scale_forwarder = wle_fractional_scale_forwarder_create(bridge->remote.display,
                                                                                   remote_manager,
                                                                                   bridge->local.display);
    } else if (g_strcmp0(interface, wl_shm_interface.name) == 0) {
        struct wl_shm *remote_shm = _wle_registry_bind(registry,
                                                       name,
                                                       &wl_shm_interface,
                                                       MIN(version, WL_SHM_VERSION));
        bridge->shm_forwarder = wle_shm_forwarder_new(bridge->remote.display,
                                                      remote_shm,
                                                      bridge->local.display);
    } else if (g_strcmp0(interface, zwp_linux_dmabuf_v1_interface.name) == 0) {
        struct zwp_linux_dmabuf_v1 *remote_dmabuf = _wle_registry_bind(registry,
                                                                       name,
                                                                       &zwp_linux_dmabuf_v1_interface,
                                                                       MIN(version, WP_LINUX_DMABUF_VERSION));
        bridge->linux_dmabuf = wle_linux_dmabuf_new(bridge->remote.display,
                                                    remote_dmabuf,
                                                    bridge->local.display);
    } else if (g_strcmp0(interface, zwlr_foreign_toplevel_manager_v1_interface.name) == 0) {
        struct zwlr_foreign_toplevel_manager_v1 *remote_foreign_toplevel_manager = _wle_registry_bind(registry,
                                                                                                      name,
                                                                                                      &zwlr_foreign_toplevel_manager_v1_interface,
                                                                                                      MIN(version, WLR_FOREIGN_TOPLEVEL_MANAGEMENT_VERSION));
        bridge->foreign_toplevel_manager = wle_foreign_toplevel_manager_new(bridge->remote.display,
                                                                            remote_foreign_toplevel_manager,
                                                                            bridge->local.display);
    } else if (g_strcmp0(interface, wp_presentation_interface.name) == 0) {
        struct wp_presentation *remote_presentation_time = _wle_registry_bind(registry,
                                                                              name,
                                                                              &wp_presentation_interface,
                                                                              MIN(version, WP_PRESENTATION_TIME_VERSION));
        bridge->presentation_time = wle_presentation_time_new(bridge->remote.display,
                                                              remote_presentation_time,
                                                              bridge->local.display);
    } else if (g_strcmp0(interface, zxdg_output_manager_v1_interface.name) == 0) {
        struct zxdg_output_manager_v1 *remote_xdg_output_manager = _wle_registry_bind(registry,
                                                                                      name,
                                                                                      &zxdg_output_manager_v1_interface,
                                                                                      MIN(version, XDG_OUTPUT_VERSION));
        bridge->xdg_output_manager = wle_xdg_output_manager_new(bridge->remote.display,
                                                                remote_xdg_output_manager,
                                                                bridge->local.display);
    } else if (g_strcmp0(interface, zwp_primary_selection_device_manager_v1_interface.name) == 0) {
        struct zwp_primary_selection_device_manager_v1 *remote_manager = _wle_registry_bind(registry,
                                                                                            name,
                                                                                            &zwp_primary_selection_device_manager_v1_interface,
                                                                                            MIN(version, WP_PRIMARY_SELECTION_VERSION));
        bridge->primary_selection = wle_primary_selection_new(bridge->remote.display,
                                                              remote_manager,
                                                              bridge->local.display);
    } else if (g_strcmp0(interface, xdg_activation_v1_interface.name) == 0) {
        struct xdg_activation_v1 *remote_xdg_activation = _wle_registry_bind(registry,
                                                                             name,
                                                                             &xdg_activation_v1_interface,
                                                                             MIN(version, XDG_ACTIVATION_VERSION));
        bridge->xdg_activation = wle_xdg_activation_create(bridge->remote.display,
                                                           remote_xdg_activation,
                                                           bridge->local.display);
    } else {
#ifdef HAVE_EXT_FOREIGN_TOPLEVEL_LIST
        if (g_strcmp0(interface, ext_foreign_toplevel_list_v1_interface.name) == 0) {
            struct ext_foreign_toplevel_list_v1 *remote_toplevel_list = _wle_registry_bind(registry,
                                                                                           name,
                                                                                           &ext_foreign_toplevel_list_v1_interface,
                                                                                           MIN(version, EXT_FOREIGN_TOPLEVEL_LIST_VERSION));
            bridge->ext_foreign_toplevel_list = wle_ext_foreign_toplevel_list_new(bridge->remote.display,
                                                                                  remote_toplevel_list,
                                                                                  bridge->local.display);
        }
#endif
    }
}

static void
remote_registry_global_remove(void *data, struct wl_registry *wl_registry, uint32_t name) {
    WleCompositorBridge *bridge = data;

    for (GList *l = bridge->seats; l != NULL; l = l->next) {
        WleSeat *seat = WLE_SEAT(l->data);
        if (name == wl_proxy_get_id((struct wl_proxy *)_wle_seat_get_remote_seat(seat))) {
            bridge->seats = g_list_remove_link(bridge->seats, l);
            g_signal_emit(bridge, signals[SIG_SEAT_REMOVED], 0, seat);
            g_object_unref(seat);
            break;
        }
    }

    for (GList *l = bridge->outputs; l != NULL; l = l->next) {
        WleOutput *output = WLE_OUTPUT(l->data);
        if (name == wl_proxy_get_id((struct wl_proxy *)wle_output_get_remote_output(output))) {
            bridge->outputs = g_list_remove_link(bridge->outputs, l);
            g_signal_emit(bridge, signals[SIG_OUTPUT_REMOVED], 0, output);
            g_object_unref(output);
            break;
        }
    }
}

static void
undefer_outputs(WleCompositorBridge *bridge) {
    for (GList *l = bridge->deferred_outputs; l != NULL; l = l->next) {
        DeferredGlobal *dg = l->data;
        struct wl_output *remote_output = _wle_registry_bind(bridge->remote.registry,
                                                             dg->name,
                                                             &wl_output_interface,
                                                             dg->version);
        WleOutput *output = wle_output_new(remote_output, bridge->local.display);
        bridge->outputs = g_list_append(bridge->outputs, output);
    }
    g_list_free_full(bridge->deferred_outputs, g_free);
    bridge->deferred_outputs = NULL;
    wl_display_roundtrip(bridge->remote.display);
}

WleCompositorBridge *
wle_compositor_bridge_new(struct wl_display *remote_display,
                          struct wl_display *local_display,
                          GError **error) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);
    g_return_val_if_fail(error == NULL || *error == NULL, NULL);

    WleCompositorBridge *bridge = g_object_new(WLE_TYPE_COMPOSITOR_BRIDGE, NULL);
    bridge->remote.display = remote_display;
    bridge->local.display = local_display;

    bridge->remote.registry = wl_display_get_registry(bridge->remote.display);
    wl_registry_add_listener(bridge->remote.registry, &remote_registry_listener, bridge);
    wl_display_roundtrip(remote_display);

    if (bridge->remote.compositor == NULL
        || bridge->remote.subcompositor == NULL
        || bridge->data_device_manager == NULL
        || bridge->shm_forwarder == NULL
        || bridge->xdg_shell == NULL)
    {
        if (error != NULL) {
            GStrvBuilder *missing = g_strv_builder_new();
            if (bridge->remote.compositor == NULL) {
                g_strv_builder_add(missing, "wl_compositor");
            }
            if (bridge->remote.subcompositor == NULL) {
                g_strv_builder_add(missing, "wl_subcompositor");
            }
            if (bridge->data_device_manager == NULL) {
                g_strv_builder_add(missing, "wl_data_device_manager");
            }
            if (bridge->shm_forwarder == NULL) {
                g_strv_builder_add(missing, "wl_shm");
            }
            if (bridge->xdg_shell == NULL) {
                g_strv_builder_add(missing, "xdg_shell");
            }

            gchar **missing_strs = g_strv_builder_end(missing);
            gchar *missing_str = g_strjoinv(", ", missing_strs);
            *error = g_error_new(WLE_ERROR,
                                 WLE_ERROR_MISSING_PROTOCOL,
                                 "Remote compositor is missing reqired protocols: %s",
                                 missing_str);

            g_free(missing_str);
            g_strfreev(missing_strs);
            g_strv_builder_unref(missing);
        }
        g_object_unref(bridge);
        return NULL;
    } else if (bridge->seats == NULL) {
        g_set_error_literal(error, WLE_ERROR, WLE_ERROR_REMOTE_ERROR, "Remote compositor reported no seats");
        g_object_unref(bridge);
        return NULL;
    } else {
        undefer_outputs(bridge);

        bridge->compositor = wle_compositor_new(bridge->remote.display,
                                                bridge->remote.compositor,
                                                bridge->remote.subcompositor,
                                                bridge->local.display);
        if (bridge->compositor == NULL) {
            g_set_error_literal(error, WLE_ERROR, WLE_ERROR_FAILED, "Failed to create compositor");
            g_object_unref(bridge);
            return NULL;
        } else {
            return bridge;
        }
    }
}

GList *
wle_compositor_bridge_get_seats(WleCompositorBridge *bridge) {
    g_return_val_if_fail(WLE_IS_COMPOSITOR_BRIDGE(bridge), NULL);
    return bridge->seats;
}

GList *
wle_compositor_bridge_get_outputs(WleCompositorBridge *bridge) {
    g_return_val_if_fail(WLE_IS_COMPOSITOR_BRIDGE(bridge), NULL);
    return bridge->outputs;
}

WleCompositor *
wle_compositor_bridge_get_compositor(WleCompositorBridge *bridge) {
    g_return_val_if_fail(WLE_IS_COMPOSITOR_BRIDGE(bridge), NULL);
    return bridge->compositor;
}

struct wl_display *
wle_compositor_bridge_get_remote_display(WleCompositorBridge *bridge) {
    g_return_val_if_fail(WLE_IS_COMPOSITOR_BRIDGE(bridge), NULL);
    return bridge->remote.display;
}

struct wl_registry *
wle_compositor_bridge_get_remote_registry(WleCompositorBridge *bridge) {
    g_return_val_if_fail(WLE_IS_COMPOSITOR_BRIDGE(bridge), NULL);
    return bridge->remote.registry;
}

struct wl_display *
wle_compositor_bridge_get_local_display(WleCompositorBridge *bridge) {
    g_return_val_if_fail(WLE_IS_COMPOSITOR_BRIDGE(bridge), NULL);
    return bridge->local.display;
}
