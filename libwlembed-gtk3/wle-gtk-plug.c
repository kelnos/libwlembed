/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * SECTION:wle-gtk-plug
 * @title: WleGtkPlug
 * @short_description: a GTK widget container for an embedded
 *                     application (client-side)
 * @stability: Unstable
 * @include: libwlembed-gtk/libwlembed-gtk3.h
 *
 * #WleGtkPlug is a container that holds widgets that are to be embedded
 * in a parent application.  It can be used somewhat analogously to
 * #GtkPlug, and hides many of the defaults around sizing and event
 * handling for you.
 **/

#include <gdk/gdkwayland.h>
#include <libwlembed/libwlembed.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "protocol/wle-embedding-v1-client-protocol.h"
#include "wle-gtk-plug.h"
#include "libwlembed-gtk3-visibility.h"

#define WLE_EMBEDDING_MANAGER_VERSION 1
#define WLE_GTK_PLUG_GET_PRIVATE(plug) ((WleGtkPlugPrivate *)wle_gtk_plug_get_instance_private(WLE_GTK_PLUG(plug)))

typedef struct _WleGtkPlugPrivate {
    gchar *embedding_token;
    struct wle_embedded_surface_v1 *embedded_surface;

    uint32_t last_focus_serial;
} WleGtkPlugPrivate;

typedef struct {
    struct wl_display *display;
    struct wl_registry *registry;
    struct wle_embedding_manager_v1 *manager;
} EmbeddingManager;

enum {
    PROP0,
    PROP_EMBEDDING_TOKEN,
};

enum {
    SIG_EMBEDDED,
    SIG_FAILED,
    SIG_REMOVED,

    N_SIGNALS,
};

static void wle_gtk_plug_finalize(GObject *object);
static void wle_gtk_plug_set_property(GObject *object,
                                      guint prop_id,
                                      const GValue *value,
                                      GParamSpec *pspec);
static void wle_gtk_plug_get_property(GObject *object,
                                      guint prop_id,
                                      GValue *value,
                                      GParamSpec *pspec);

static void wle_gtk_plug_map(GtkWidget *widget);
static void wle_gtk_plug_unmap(GtkWidget *widget);
static gboolean wle_gtk_plug_focus(GtkWidget *widget,
                                   GtkDirectionType direction);

static void wle_gtk_plug_set_focus(GtkWindow *window,
                                   GtkWidget *focus);

static void embedding_registry_global(void *data,
                                      struct wl_registry *registry,
                                      uint32_t name,
                                      const char *interface,
                                      uint32_t version);
static void embedding_registry_global_remove(void *data,
                                             struct wl_registry *wl_registry,
                                             uint32_t name);

static void embedded_surface_embedded(void *data,
                                      struct wle_embedded_surface_v1 *embedded_surface);
static void embedded_surface_failed(void *data,
                                    struct wle_embedded_surface_v1 *embedded_surface);
static void embedded_surface_removed(void *data,
                                     struct wle_embedded_surface_v1 *embedded_surface);
static void embedded_surface_focus_first(void *data,
                                         struct wle_embedded_surface_v1 *embedded_surface,
                                         uint32_t serial);
static void embedded_surface_focus_last(void *data,
                                        struct wle_embedded_surface_v1 *embedded_surface,
                                        uint32_t serial);


G_DEFINE_TYPE_WITH_PRIVATE(WleGtkPlug, wle_gtk_plug, GTK_TYPE_WINDOW)


static GOnce embedding_manager_guard = G_ONCE_INIT;
static EmbeddingManager *embedding_manager = NULL;

static guint signals[N_SIGNALS] = { 0 };

static const struct wl_registry_listener embedding_registry_listener = {
    .global = embedding_registry_global,
    .global_remove = embedding_registry_global_remove,
};

static const struct wle_embedded_surface_v1_listener embedded_surface_listener = {
    .embedded = embedded_surface_embedded,
    .failed = embedded_surface_failed,
    .removed = embedded_surface_removed,
    .focus_first = embedded_surface_focus_first,
    .focus_last = embedded_surface_focus_last,
};

static gpointer
embedding_manager_init(gpointer data) {
    EmbeddingManager *mgr = g_new0(EmbeddingManager, 1);
    mgr->display = data;

    mgr->registry = wl_display_get_registry(mgr->display);
    wl_registry_add_listener(mgr->registry, &embedding_registry_listener, mgr);
    wl_display_roundtrip(mgr->display);

    if (mgr->manager == NULL) {
        g_warning("The compositor does not support the wle-embedding-manager-v1 protocol");
    }

    embedding_manager = mgr;

    return NULL;
}

static void
wle_gtk_plug_class_init(WleGtkPlugClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_gtk_plug_finalize;
    gobject_class->set_property = wle_gtk_plug_set_property;
    gobject_class->get_property = wle_gtk_plug_get_property;

    g_object_class_install_property(gobject_class,
                                    PROP_EMBEDDING_TOKEN,
                                    g_param_spec_string("embedding-token",
                                                        "embedding-token",
                                                        "embedding-token",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
    widget_class->map = wle_gtk_plug_map;
    widget_class->unmap = wle_gtk_plug_unmap;
    widget_class->focus = wle_gtk_plug_focus;

    GtkWindowClass *window_class = GTK_WINDOW_CLASS(klass);
    window_class->set_focus = wle_gtk_plug_set_focus;

    /**
     * WleGtkPlug::embedded:
     * @plug: The #WleGtkPlug that received the signal.
     *
     * Received when @plug has been successfully embedded in the parent.
     *
     * Note that the parent controls display of the contents of any embedded
     * views, so the emission of this signal does not necessarily mean @plug's
     * contents are visible to the user.
     **/
    signals[SIG_EMBEDDED] = g_signal_new("embedded",
                                         WLE_TYPE_GTK_PLUG,
                                         G_SIGNAL_RUN_LAST,
                                         0,
                                         NULL,
                                         NULL,
                                         g_cclosure_marshal_VOID__VOID,
                                         G_TYPE_NONE,
                                         0);

    /**
     * WleGtkPlug::failed:
     * @plug: The #WleGtkPlug that received the signal.
     *
     * Received when @plug has failed to be embedded in the parent.
     *
     * This could occur for a variety of reasons.  The embedding token may not
     * have been valid, or the parent application could have changed its mind
     * and rejected the embedding attempt.
     **/
    signals[SIG_FAILED] = g_signal_new("failed",
                                       WLE_TYPE_GTK_PLUG,
                                       G_SIGNAL_RUN_LAST,
                                       0,
                                       NULL,
                                       NULL,
                                       g_cclosure_marshal_VOID__VOID,
                                       G_TYPE_NONE,
                                       0);

    /**
     * WleGtkPlug::removed:
     * @plug: The #WleGtkPlug that received the signal.
     *
     * Received when @plug has been removed from the parent, usually because
     * the parent has destroyed the embedding area or is shutting down.
     **/
    signals[SIG_REMOVED] = g_signal_new("removed",
                                        WLE_TYPE_GTK_PLUG,
                                        G_SIGNAL_RUN_LAST,
                                        0,
                                        NULL,
                                        NULL,
                                        g_cclosure_marshal_VOID__VOID,
                                        G_TYPE_NONE,
                                        0);
}

static void
wle_gtk_plug_init(WleGtkPlug *plug) {
    g_once(&embedding_manager_guard,
           embedding_manager_init,
           gdk_wayland_display_get_wl_display(gdk_display_get_default()));

    gtk_window_set_decorated(GTK_WINDOW(plug), FALSE);
}

static void
wle_gtk_plug_finalize(GObject *object) {
    WleGtkPlugPrivate *priv = WLE_GTK_PLUG_GET_PRIVATE(object);

    g_free(priv->embedding_token);

    G_OBJECT_CLASS(wle_gtk_plug_parent_class)->finalize(object);
}

static void
wle_gtk_plug_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec) {
    WleGtkPlugPrivate *priv = WLE_GTK_PLUG_GET_PRIVATE(object);

    switch (prop_id) {
        case PROP_EMBEDDING_TOKEN:
            priv->embedding_token = g_value_dup_string(value);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_gtk_plug_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec) {
    WleGtkPlugPrivate *priv = WLE_GTK_PLUG_GET_PRIVATE(object);

    switch (prop_id) {
        case PROP_EMBEDDING_TOKEN:
            g_value_set_string(value, priv->embedding_token);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_gtk_plug_map(GtkWidget *widget) {
    GTK_WIDGET_CLASS(wle_gtk_plug_parent_class)->map(widget);

    if (embedding_manager == NULL || embedding_manager->manager == NULL) {
        g_message("No embedding manager; behaving like regular GtkWindow");
    } else {
        WleGtkPlugPrivate *priv = WLE_GTK_PLUG_GET_PRIVATE(widget);
        struct wl_surface *surface = gdk_wayland_window_get_wl_surface(gtk_widget_get_window(widget));
        priv->embedded_surface = wle_embedding_manager_v1_create_embedded_surface(embedding_manager->manager,
                                                                                  priv->embedding_token,
                                                                                  surface);
        if (priv->embedded_surface == NULL) {
            g_warning("Failed to embed surface");
        } else {
            wle_embedded_surface_v1_add_listener(priv->embedded_surface, &embedded_surface_listener, widget);
        }
    }
}

static void
wle_gtk_plug_unmap(GtkWidget *widget) {
    WleGtkPlugPrivate *priv = WLE_GTK_PLUG_GET_PRIVATE(widget);
    if (priv->embedded_surface != NULL) {
        wle_embedded_surface_v1_destroy(priv->embedded_surface);
        priv->embedded_surface = NULL;
    }

    GTK_WIDGET_CLASS(wle_gtk_plug_parent_class)->unmap(widget);
}

static gboolean
wle_gtk_plug_focus(GtkWidget *widget, GtkDirectionType direction) {
    WleGtkPlugPrivate *priv = WLE_GTK_PLUG_GET_PRIVATE(widget);

    if (priv->embedded_surface == NULL) {
        return GTK_WIDGET_CLASS(wle_gtk_plug_parent_class)->focus(widget, direction);
    } else {
        GtkWidget *prev_focus_child = gtk_container_get_focus_child(GTK_CONTAINER(widget));
        if (prev_focus_child != NULL) {
            if (gtk_widget_child_focus(prev_focus_child, direction)) {
                // child has something next in the focus chain, so we're good
                return TRUE;
            } else {
                GtkWidget *focused = gtk_window_get_focus(GTK_WINDOW(widget));
                if (focused != NULL) {
                    // either got to the end or begining, clear focus on the entire
                    // hierarchy so we can send focus back to the compositor
                    GtkWidget *parent = gtk_widget_get_parent(focused);
                    while (parent != NULL) {
                        gtk_container_set_focus_child(GTK_CONTAINER(parent), NULL);
                        parent = gtk_widget_get_parent(parent);
                    }
                    gtk_window_set_focus(GTK_WINDOW(widget), NULL);
                }
            }
        } else {
            // try to focus first widget
            GtkWidget *first = gtk_bin_get_child(GTK_BIN(widget));
            if (first != NULL) {
                if (gtk_widget_child_focus(first, direction)) {
                    return TRUE;
                }
            }
        }

        if (gtk_container_get_focus_child(GTK_CONTAINER(widget)) == NULL) {
            // if we get here, we weren't able to focus anything, so
            // ask the compositor to focus outside the embed
            switch (direction) {
                case GTK_DIR_UP:
                case GTK_DIR_LEFT:
                case GTK_DIR_TAB_BACKWARD:
                    wle_embedded_surface_v1_focus_previous(priv->embedded_surface, priv->last_focus_serial);
                    break;

                case GTK_DIR_DOWN:
                case GTK_DIR_RIGHT:
                case GTK_DIR_TAB_FORWARD:
                    wle_embedded_surface_v1_focus_next(priv->embedded_surface, priv->last_focus_serial);
                    break;
            }
        }

        return FALSE;
    }
}

static void
wle_gtk_plug_set_focus(GtkWindow *window, GtkWidget *focus) {
    GTK_WINDOW_CLASS(wle_gtk_plug_parent_class)->set_focus(window, focus);

    WleGtkPlugPrivate *priv = WLE_GTK_PLUG_GET_PRIVATE(window);
    if (priv->embedded_surface != NULL && focus != NULL && !gtk_window_has_toplevel_focus(window)) {
        wle_embedded_surface_v1_request_focus(priv->embedded_surface, 0);
    }
}

static void
embedding_registry_global(void *data,
                          struct wl_registry *registry,
                          uint32_t name,
                          const char *interface,
                          uint32_t version) {
    if (g_strcmp0(interface, wle_embedding_manager_v1_interface.name) == 0) {
        EmbeddingManager *mgr = data;
        if (mgr->manager == NULL) {
            mgr->manager = wl_registry_bind(registry,
                                            name,
                                            &wle_embedding_manager_v1_interface,
                                            MIN(version, WLE_EMBEDDING_MANAGER_VERSION));
        }
    }
}

static void
embedding_registry_global_remove(void *data, struct wl_registry *wl_registry, uint32_t name) {
    EmbeddingManager *mgr = data;

    if (mgr->manager != NULL && wl_proxy_get_id((struct wl_proxy *)mgr->manager) == name) {
        g_message("Embedding manager shut down by compositor");
        wle_embedding_manager_v1_destroy(mgr->manager);
        mgr->manager = NULL;
    }
}

static void
embedded_surface_embedded(void *data, struct wle_embedded_surface_v1 *embedded_surface) {
    g_signal_emit(data, signals[SIG_EMBEDDED], 0);
}

static void
embedded_surface_failed(void *data, struct wle_embedded_surface_v1 *embedded_surface) {
    g_message("Failed to embed surface");
    wle_embedded_surface_v1_destroy(embedded_surface);
    WLE_GTK_PLUG_GET_PRIVATE(data)->embedded_surface = NULL;
    g_signal_emit(data, signals[SIG_FAILED], 0);
}

static void
embedded_surface_removed(void *data, struct wle_embedded_surface_v1 *embedded_surface) {
    wle_embedded_surface_v1_destroy(embedded_surface);
    WLE_GTK_PLUG_GET_PRIVATE(data)->embedded_surface = NULL;
    g_signal_emit(data, signals[SIG_REMOVED], 0);
}

static void
focus_first_or_last(WleGtkPlug *plug, GtkDirectionType direction) {
    GtkWidget *focused = gtk_window_get_focus(GTK_WINDOW(plug));
    if (focused != NULL) {
        GtkWidget *parent = gtk_widget_get_parent(focused);
        while (parent != NULL) {
            gtk_container_set_focus_child(GTK_CONTAINER(parent), NULL);
            parent = gtk_widget_get_parent(parent);
        }
        gtk_window_set_focus(GTK_WINDOW(plug), NULL);
    }

    gtk_widget_child_focus(GTK_WIDGET(plug), direction);
}

static void
embedded_surface_focus_first(void *data, struct wle_embedded_surface_v1 *embedded_surface, uint32_t serial) {
    WleGtkPlug *plug = WLE_GTK_PLUG(data);
    WLE_GTK_PLUG_GET_PRIVATE(plug)->last_focus_serial = serial;
    focus_first_or_last(plug, GTK_DIR_TAB_FORWARD);
}

static void
embedded_surface_focus_last(void *data, struct wle_embedded_surface_v1 *embedded_surface, uint32_t serial) {
    WleGtkPlug *plug = WLE_GTK_PLUG(data);
    WLE_GTK_PLUG_GET_PRIVATE(plug)->last_focus_serial = serial;
    focus_first_or_last(plug, GTK_DIR_TAB_BACKWARD);
}

/**
 * wle_gtk_plug_new: (constructor)
 * @embedding_token: An opaque token string.
 *
 * Creates a new #WleGtkPlug.  @embedding_token must have been passed to
 * this process by the process hosting the embedded compositor.  See
 * @wle_gtk_socket_get_embedding_token() for details.
 *
 * Return value: (not nullable) (transfer floating): A new plug as a
 * #GtkWidget.
 **/
GtkWidget *
wle_gtk_plug_new(const gchar *embedding_token) {
    return g_object_new(WLE_TYPE_GTK_PLUG,
                        "embedding-token", embedding_token,
                        NULL);
}

#define __WLE_GTK_PLUG_C__
#include <libwlembed-gtk3-visibility.c>
