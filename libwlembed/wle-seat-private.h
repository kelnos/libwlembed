/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_SEAT_PRIVATE_H__
#define __WLE_SEAT_PRIVATE_H__

#include "wle-seat.h"
#include "wle-view.h"

G_BEGIN_DECLS

WleSeat *_wle_seat_new(struct wl_display *remote_display,
                       struct wl_registry *remote_regsitry,
                       uint32_t remote_seat_name,
                       uint32_t remote_seat_version,
                       struct wl_display *local_display);

struct wl_seat *_wle_seat_get_remote_seat(WleSeat *seat);
struct wl_resource *_wle_seat_get_local_seat(WleSeat *seat);

WleView *_wle_seat_get_focused_view(WleSeat *seat);
void _wle_seat_set_focused_view(WleSeat *seat,
                                WleView *view,
                                uint32_t serial);

void _wle_seat_synthesize_keyboard_enter(WleSeat *seat,
                                         WleView *view,
                                         uint32_t serial);
void _wle_seat_synthesize_keyboard_leave(WleSeat *seat,
                                         WleView *view,
                                         uint32_t serial);

G_END_DECLS

#endif /* __WLE_SEAT_PRIVATE_H__ */
