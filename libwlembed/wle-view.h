/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_VIEW_H__
#define __WLE_VIEW_H__

#include <glib-object.h>
#include <wayland-server-core.h>

#include "protocol/wlr-layer-shell-unstable-v1-client-protocol.h"
#include "protocol/xdg-shell-client-protocol.h"
#include "wle-local-resource.h"
#include "wle-seat.h"
#include "wle-view-role.h"

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleView, wle_view, WLE, VIEW, WleLocalResource)
#define WLE_TYPE_VIEW (wle_view_get_type())
#define WLE_TYPE_MODE (wle_mode_get_type())

typedef struct {
    struct wl_surface *remote_parent_surface;

    struct wl_subsurface *remote_subsurface;
    int32_t offset_x;
    int32_t offset_y;

    struct xdg_surface *remote_toplevel_xdg_surface;
    struct xdg_toplevel *remote_toplevel_xdg_toplevel;
    struct zwlr_layer_surface_v1 *remote_toplevel_layer_surface;
} WleViewEmbedData;

typedef enum {
    WLE_MODE_FORWARDED = 0,
    WLE_MODE_EMBEDDED,
} WleMode;

GType wle_mode_get_type(void) G_GNUC_CONST;

WleView *wle_view_new(struct wl_display *remote_display,
                      struct wl_compositor *remote_compositor,
                      struct wl_subcompositor *remote_subcompositor,
                      struct wl_surface *remote_surface,
                      struct wl_resource *local_surface);

gboolean _wle_view_is_embedded(WleView *view);
gboolean _wle_view_is_forwarded(WleView *view);

void wle_view_set_role(WleView *view,
                       WleViewRole *view_role);
WleViewRole *wle_view_get_role(WleView *view);

gboolean _wle_view_set_embedded(WleView *view);

void _wle_view_embedder_shown(WleView *view,
                              struct wl_surface *new_remote_parent_surface);
void _wle_view_embedder_hidden(WleView *view);

struct wl_client *wle_view_get_client(WleView *view);

void _wle_view_min_size_changed(WleView *view,
                                gint width,
                                gint height);
void _wle_view_max_size_changed(WleView *view,
                                gint width,
                                gint height);
void _wle_view_configured(WleView *view,
                          uint32_t serial);
void _wle_view_map(WleView *view);
void _wle_view_unmap(WleView *view);
void _wle_view_activated(WleView *view,
                         uint32_t serial);

void _wle_view_set_size(WleView *view,
                        uint32_t serial,
                        int32_t width,
                        int32_t height);

struct wl_surface *wle_view_get_remote_surface(WleView *view);
struct wl_resource *wle_view_get_local_surface(WleView *view);

GList *wle_view_get_outputs(WleView *view);

WleViewEmbedData *wle_view_get_embed_data(WleView *view);

void wle_view_set_embedded_offset(WleView *view,
                                  int32_t x,
                                  int32_t y);
void wle_view_set_embedded_xdg_toplevel(WleView *view,
                                        struct xdg_surface *xdg_surface,
                                        struct xdg_toplevel *xdg_toplevel);
void wle_view_set_embedded_layer_toplevel(WleView *view,
                                          struct zwlr_layer_surface_v1 *layer_surface);

void wle_view_pointer_enter(WleView *view,
                            WleSeat *seat,
                            uint32_t serial,
                            gdouble x,
                            gdouble y);
void wle_view_pointer_leave(WleView *view,
                            WleSeat *seat,
                            uint32_t serial);
void wle_view_pointer_motion(WleView *view,
                             WleSeat *seat,
                             uint32_t time,
                             gdouble x,
                             gdouble y);
void wle_view_pointer_frame(WleView *view,
                            WleSeat *seat);

G_END_DECLS

#endif /* __WLE_VIEW_H__ */
