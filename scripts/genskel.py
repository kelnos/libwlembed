#!/usr/bin/env python3

"""
Generates a skeleton .c protocol forwarding file given a Wayland protocol
XML file on stdin.  Outputs to stdout.

Possible command-line options:

    --replace STRING/REPLACEMENT  Replaces STRING with REPLACEMENT in struct
                                  names (separated with a slash).
    --strip STRING                Strips STRING from function and variable
                                  names.
    --interface IFACE_NAME        Interface to generate code for (can be
                                  specified multiple times). Default is to
                                  generate for all interfaces in file.
    --reverse                     Reverses the order of interfaces in the
                                  output file.

Both can be repeated.  String replacing will be done first, then stripping.
"""

import regex
from xml.etree import ElementTree as ET
import sys

LEADING_PREFIX_RE = regex.compile(r'^z?(wl|wp|wlr|xdg|org_kde_kwin)_')
TRAILING_VERSION_RE = regex.compile(r'_v[0-9]+$')
SNAKE_TO_PASCAL_RE = regex.compile(r'(^[a-z]|_[a-z])')

STATE_NONE = 0
STATE_PROTOCOL = 1
STATE_INTERFACE = 2
STATE_REQUEST = 3
STATE_EVENT = 4
STATE_ARG = 5
STATE_COPYRIGHT = 6
STATE_DESCRIPTION = 7
STATE_ENUM = 8
STATE_ENTRY = 9

TYPE_NEW_ID = 0
TYPE_OBJECT = 1
TYPE_UINT = 2
TYPE_INT = 3
TYPE_FIXED = 4
TYPE_STRING = 5
TYPE_FD = 6
TYPE_ARRAY = 7
TYPES = {
    'new_id': TYPE_NEW_ID,
    'object': TYPE_OBJECT,
    'uint': TYPE_UINT,
    'int': TYPE_INT,
    'fixed': TYPE_FIXED,
    'string': TYPE_STRING,
    'fd': TYPE_FD,
    'array': TYPE_ARRAY,
}

state = [STATE_NONE]
interfaces = []
cur_interface = None
cur_request = None
cur_event = None
cur_arg = None

replace_pairs = []
strip_strs = []
do_interfaces = None
reverse = False

def cur_state():
    global state
    return state[-1]


def strip_leading_prefix(s):
    return LEADING_PREFIX_RE.sub('', s)


def strip_trailing_version(s):
    return TRAILING_VERSION_RE.sub('', s)


def snake_to_pascal(s):
    callback = lambda pat: pat.group(1).upper().replace('_', '')
    return SNAKE_TO_PASCAL_RE.sub(callback, s)


def interface_struct_name(interface_name):
    global replace_pairs
    global strip_strs

    pascal = snake_to_pascal(strip_leading_prefix(strip_trailing_version(interface_name)))
    for s, repl in replace_pairs:
        pascal = pascal.replace(s, repl)
    for s in strip_strs:
        pascal = pascal.replace(s, '')

    return 'Wle' + pascal


def interface_func_name(interface_name):
    global replace_pairs
    global strip_strs

    stripped = strip_leading_prefix(strip_trailing_version(interface_name))
    for s, repl in replace_pairs:
        stripped = stripped.replace(s, repl)
    for s in strip_strs:
        stripped = stripped.replace(s, '')

    return stripped


def parse_type(tstr):
    return TYPES[tstr]


def earg_to_str(arg):
    typ = arg['type']
    arg['nullable_annot'] = ' /* nullable */' if arg.get('allow-null', False) else ''
    if typ in [TYPE_NEW_ID, TYPE_OBJECT]:
        if 'interface' not in arg:
            arg['interface'] = 'void'
        return 'struct {interface} *remote_{name}{nullable_annot}'.format(**arg)
    elif typ == TYPE_UINT:
        return 'uint32_t {name}'.format(**arg)
    elif typ == TYPE_INT:
        return 'int32_t {name}'.format(**arg)
    elif typ == TYPE_FIXED:
        return 'wl_fixed_t {name}'.format(**arg)
    elif typ == TYPE_STRING:
        return 'const char *{name}{nullable_annot}'.format(**arg)
    elif typ == TYPE_FD:
        return 'int {name}'.format(**arg)
    elif typ == TYPE_ARRAY:
        return 'struct wl_array *{name}{nullable_annot}'.format(**arg)
    else:
        raise "bad type " + str(typ)


def rarg_to_str(arg):
    typ = arg['type']
    arg['nullable_annot'] = ' /* nullable */' if arg.get('allow-null', False) else ''
    if typ == TYPE_NEW_ID:
        return 'uint32_t {name}_id'.format(**arg)
    elif typ == TYPE_OBJECT:
        return 'struct wl_resource *{name}_resource{nullable_annot}'.format(**arg)
    elif typ == TYPE_UINT:
        return 'uint32_t {name}'.format(**arg)
    elif typ == TYPE_INT:
        return 'int32_t {name}'.format(**arg)
    elif typ == TYPE_FIXED:
        return 'wl_fixed_t {name}'.format(**arg)
    elif typ == TYPE_STRING:
        return 'const char *{name}{nullable_annot}'.format(**arg)
    elif typ == TYPE_FD:
        return 'int {name}'.format(**arg)
    elif typ == TYPE_ARRAY:
        return 'struct wl_array *{name}{nullable_annot}'.format(**arg)
    else:
        raise "bad type " + str(typ)


argv = list(reversed(sys.argv[1:]))
while len(argv) > 0:
    arg = argv.pop()
    if arg == '--replace':
        pair = argv.pop().split('/', 2)
        if len(pair) == 2:
            replace_pairs.append(pair)
        else:
            raise 'invalid --replace argument'
    elif arg == '--strip':
        strip_strs.append(argv.pop())
    elif arg == '--interface':
        if do_interfaces is None:
            do_interfaces = []
        do_interfaces.append(argv.pop())
    elif arg == '--reverse':
        reverse = True

parser = ET.XMLPullParser(['start', 'end'])
parser.feed(sys.stdin.read())
for event, elem in parser.read_events():
    attrs = elem.attrib
    if event == 'start':
        if elem.tag == 'protocol':
            assert cur_state() == STATE_NONE
            state.append(STATE_PROTOCOL)
        elif elem.tag == 'copyright':
            assert cur_state() == STATE_PROTOCOL
            state.append(STATE_COPYRIGHT)
        elif elem.tag == 'description':
            state.append(STATE_DESCRIPTION)
        elif elem.tag == 'interface':
            assert cur_state() == STATE_PROTOCOL
            state.append(STATE_INTERFACE)
            cur_interface = {
                'name': attrs['name'],
                'struct_name': interface_struct_name(attrs['name']),
                'func_name': interface_func_name(attrs['name']),
                'version': int(attrs.get('version', 1)),
                'requests': [],
                'events': [],
            }
        elif elem.tag == 'request':
            assert cur_state() == STATE_INTERFACE
            assert cur_interface is not None
            state.append(STATE_REQUEST)
            cur_request = {
                'interface': cur_interface['name'],
                'interface_struct': cur_interface['struct_name'],
                'interface_func': cur_interface['func_name'],
                'name': attrs['name'],
                'args': [],
            }
        elif elem.tag == 'event':
            assert cur_state() == STATE_INTERFACE
            assert cur_interface is not None
            state.append(STATE_EVENT)
            cur_event = {
                'interface': cur_interface['name'],
                'interface_struct': cur_interface['struct_name'],
                'interface_func': cur_interface['func_name'],
                'name': attrs['name'],
                'args': [],
            }
        elif elem.tag == 'arg':
            assert cur_state() in [STATE_REQUEST, STATE_EVENT]
            state.append(STATE_ARG)
            cur_arg = {
                'name': attrs['name'],
                'type': parse_type(attrs['type']),
                'interface_struct': cur_interface['struct_name'],
                'interface_func': cur_interface['func_name'],
                'interface': attrs.get('interface'),
                'allow-null': attrs.get('allow-null', 'false') == 'true',
            }
        elif elem.tag == 'enum':
            assert cur_state() == STATE_INTERFACE
            state.append(STATE_ENUM)
        elif elem.tag == 'entry':
            assert cur_state() == STATE_ENUM
            state.append(STATE_ENTRY)
        else:
            raise 'bad start elem ' + elem.tag
    elif event == 'end':
        if elem.tag == 'protocol':
            assert state.pop() == STATE_PROTOCOL
        elif elem.tag == 'copyright':
            assert state.pop() == STATE_COPYRIGHT
        elif elem.tag == 'description':
            assert state.pop() == STATE_DESCRIPTION
        elif elem.tag == 'interface':
            assert state.pop() == STATE_INTERFACE
            assert cur_interface is not None
            interfaces.append(cur_interface)
            cur_interface = None
        elif elem.tag == 'request':
            assert state.pop() == STATE_REQUEST
            assert cur_request is not None
            assert cur_interface is not None
            cur_interface['requests'].append(cur_request)
            cur_request = None
        elif elem.tag == 'event':
            assert state.pop() == STATE_EVENT
            assert cur_event is not None
            assert cur_interface is not None
            cur_interface['events'].append(cur_event)
            cur_event = None
        elif elem.tag == 'arg':
            assert state.pop() == STATE_ARG
            assert cur_request is not None or cur_event is not None
            if cur_request is not None:
                cur_request['args'].append(cur_arg)
            elif cur_event is not None:
                cur_event['args'].append(cur_arg)
            else:
                raise 'notreached'
            cur_arg = None
        elif elem.tag == 'enum':
            assert state.pop() == STATE_ENUM
        elif elem.tag == 'entry':
            assert state.pop() == STATE_ENTRY
        else:
            raise 'bad end elem ' + elem.tag

assert state.pop() == STATE_NONE

if do_interfaces is not None:
    interfaces = list(filter(lambda interface: interface['name'] in do_interfaces, interfaces))

for interface in interfaces:
    print(
"""typedef struct _{struct_name} {{
    struct {name} *remote_{func_name};
    struct wl_resource *local_{func_name};
    struct wl_list link;
}} {struct_name};
""".format(**interface)
    )

if reverse:
    interfaces = list(reversed(interfaces))

for interface in interfaces:
    for event in interface['events']:
        event['after_iface_arg'] = ',' if len(event['args']) > 0 else ');'
        print(
"""static void remote_{interface_func}_{name}(
    void *data,
    struct {interface} *remote_{interface_func}{after_iface_arg}""".format(**event)
        )
        argstrs = list(map(earg_to_str, event['args']))
        if len(argstrs) > 0:
            print('    ' + ',\n    '.join(argstrs) + ');')
    print('')

    for request in interface['requests']:
        request['after_iface_arg'] = ',' if len(request['args']) > 0 else ');'
        print(
"""static void local_{interface_func}_{name}(
    struct wl_client *client,
    struct wl_resource *{interface_func}_resource{after_iface_arg}""".format(**request)
        )
        argstrs = list(map(rarg_to_str, request['args']))
        if len(argstrs) > 0:
            print('    ' + ',\n    '.join(argstrs) + ');')
    print('')

for interface in interfaces:
    if len(interface['events']) > 0:
        print("static const struct {name}_listener remote_{func_name}_listener = {{".format(**interface))
        for event in interface['events']:
            print("    .{name} = remote_{interface_func}_{name},".format(**event))
        print('};\n')

    if len(interface['requests']) > 0:
        print("static const struct {name}_interface local_{func_name}_impl = {{".format(**interface))
        for request in interface['requests']:
            print("    .{name} = local_{interface_func}_{name},".format(**request))
        print('};\n')


for interface in interfaces:
    for event in interface['events']:
        event['after_iface_arg'] = ',' if len(event['args']) > 0 else ''
        print(
"""static void
remote_{interface_func}_{name}(
    void *data,
    struct {interface} *remote_{interface_func}{after_iface_arg}""".format(**event)
        )
        argstrs = list(map(earg_to_str, event['args']))
        if len(argstrs) > 0:
            print('    ' + ',\n    '.join(argstrs))
        print(
""") {{
    {interface_struct} *{interface_func} = data;
}}
""".format(**event)
        )

    for request in interface['requests']:
        request['after_iface_arg'] = ',' if len(request['args']) > 0 else ''
        print(
"""static void
local_{interface_func}_{name}(
    struct wl_client *client,
    struct wl_resource *{interface_func}_resource{after_iface_arg}""".format(**request)
        )
        argstrs = list(map(rarg_to_str, request['args']))
        if len(argstrs) > 0:
            print('    ' + ',\n    '.join(argstrs))
        print(
""") {{
    {interface_struct} *{interface_func} = wl_resource_get_user_data({interface_func}_resource);
}}
""".format(**request)
        )

    print(
"""static void
local_{func_name}_destroy_impl(struct wl_resource *{func_name}_resource) {{
    {struct_name} *{func_name} = wl_resource_get_user_data({func_name}_resource);
    wl_list_remove(wl_resource_get_link({func_name}_resource));
    g_free({func_name});
}}
""".format(**interface)
    )
