/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "wle-util.h"

static const char *proxy_tag = "libwlembed-proxy-tag";

void
wle_propagate_remote_error(struct wl_display *remote_display, struct wl_client *client, struct wl_resource *resource) {
    int err = wl_display_get_error(remote_display);
    if (err != 0) {
        const struct wl_interface *error_interface = NULL;
        uint32_t error_id = 0;
        uint32_t proto_err = wl_display_get_protocol_error(remote_display, &error_interface, &error_id);
        if (proto_err != 0 && resource != NULL) {
            wl_resource_post_error(resource,
                                   proto_err,
                                   "protocol error from remote compositor: %s@%u",
                                   error_interface != NULL ? error_interface->name : "(unknown)",
                                   error_id);
        } else if (client != NULL) {
            wl_client_post_implementation_error(client, "error from remote compositor: %s", strerror(err));
        }
    }
}

void *
_wle_proxy_set_ours(struct wl_proxy *proxy) {
    wl_proxy_set_tag(proxy, &proxy_tag);
    return proxy;
}

void *
_wle_proxy_get_user_data(struct wl_proxy *proxy) {
    if (proxy != NULL && wl_proxy_get_tag(proxy) == &proxy_tag) {
        return wl_proxy_get_user_data(proxy);
    } else {
        return NULL;
    }
}

void *
_wle_registry_bind(struct wl_registry *registry,
                   uint32_t name,
                   const struct wl_interface *interface,
                   uint32_t version) {
    struct wl_proxy *proxy = wl_registry_bind(registry, name, interface, version);
    if (proxy != NULL) {
        _wle_proxy_set_ours(proxy);
    }
    return proxy;
}

static gboolean
_wle_open_shm(gsize len, int *rw_fd, int *ro_fd) {
    g_return_val_if_fail(len > 0, FALSE);
    g_return_val_if_fail(rw_fd != NULL, FALSE);
    g_return_val_if_fail(ro_fd != NULL, FALSE);

    GRand *rand = g_rand_new();
    gchar *name = g_strdup_printf("/libwlembed-%08x%08x", g_rand_int(rand), g_rand_int(rand));
    g_rand_free(rand);

    *rw_fd = shm_open(name, O_CREAT | O_EXCL | O_RDWR, 0600);
    if (*rw_fd >= 0) {
        *ro_fd = shm_open(name, O_RDONLY, 0);
    } else {
        *ro_fd = -1;
    }
    shm_unlink(name);
    g_free(name);

    int ret = -1;
    if (*rw_fd >= 0 && *ro_fd >= 0) {
        fchmod(*rw_fd, 0);

        do {
            ret = ftruncate(*rw_fd, len);
        } while (ret < 0 && errno == EINTR);
    }

    if (ret == -1) {
        if (*rw_fd >= 0) {
            close(*rw_fd);
            *rw_fd = -1;
        }
        if (*ro_fd >= 0) {
            close(*ro_fd);
            *ro_fd = -1;
        }
        return FALSE;
    } else {
        return TRUE;
    }
}

int
_wle_shm_share_data(void *data, gsize len) {
    g_return_val_if_fail(data != NULL, -1);
    g_return_val_if_fail(len > 0, -1);

    int rw_fd, ro_fd;
    if (_wle_open_shm(len, &rw_fd, &ro_fd)) {
        void *mapping = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, rw_fd, 0);
        if (mapping != MAP_FAILED) {
            memcpy(mapping, data, len);
            munmap(mapping, len);
        } else {
            close(ro_fd);
            ro_fd = -1;
        }
        close(rw_fd);
        return ro_fd;
    } else {
        return -1;
    }
}
