<SECTION>
<FILE>libwlembed-gtk</FILE>
wle_gtk_create_embedded_compositor
wle_gtk_create_embedded_compositor_full
<SUBSECTION Private>
xdg_surface
xdg_toplevel
</SECTION>

<SECTION>
<FILE>wle-gtk-socket</FILE>
WleGtkSocket
wle_gtk_socket_new
wle_gtk_socket_get_embedded_compositor
wle_gtk_socket_get_embedding_token
wle_gtk_socket_set_size_constraints
wle_gtk_socket_destroy_embedded_view
<SUBSECTION Standard>
WLE_TYPE_GTK_SOCKET
WleGtkSocketClass
wle_gtk_socket_get_type
</SECTION>

<SECTION>
<FILE>wle-gtk-plug</FILE>
WleGtkPlug
wle_gtk_plug_new
<SUBSECTION Standard>
WLE_TYPE_GTK_PLUG
WleGtkPlugClass
wle_gtk_plug_get_type
</SECTION>
