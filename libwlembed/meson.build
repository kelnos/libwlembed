libwlembed_generated_sources = []

libwlembed_generated_sources += gnome.genmarshal(
  'wle-marshal',
  internal: true,
  prefix: '_wle_marshal',
  sources: ['wle-marshal.list'],
)

protos_needed = [
  'fractional-scale',
  'layer-shell',
  'linux-dmabuf-unstable-v1',
  'primary-selection',
  'presentation-time',
  'server-decoration',
  'viewporter',
  'wle-embedding',
  'wlr-foreign-toplevel-management',
  'xdg-activation-v1',
  'xdg-decoration-v1',
  'xdg-foreign-v2',
  'xdg-output-v1',
  'xdg-shell',
]

if have_ext_toplevel_list
  protos_needed += 'ext-foreign-toplevel-list'
endif

foreach name: protos_needed
  libwlembed_generated_sources += protocol_sources[name]
endforeach

libwlembed_generated_sources += custom_target(
  'libwlembed-visibility.h',
  input: 'libwlembed.symbols',
  output: 'libwlembed-visibility.h',
  command: [xdt_gen_visibility, '--kind=header', '@INPUT@', '@OUTPUT@'],
)
libwlembed_generated_sources += custom_target(
  'libwlembed-visibility.c',
  input: 'libwlembed.symbols',
  output: 'libwlembed-visibility.c',
  command: [xdt_gen_visibility, '--kind=source', '@INPUT@', '@OUTPUT@'],
)

libwlembed_apiname = 'wlembed-@0@'.format(lib_so_version)

libwlembed_headers = [
  'libwlembed.h',
  'wle-embedded-compositor.h',
  'wle-embedded-view.h',
  'wle-error.h',
  'wle-seat.h',
]

libwlembed_public_sources = [
  'wle-embedded-compositor.c',
  'wle-embedded-view.c',
  'wle-error.c',
  'wle-seat.c',
]

libwlembed_sources = [
  'wle-child-process-manager.c',
  'wle-compositor-bridge.c',
  'wle-compositor.c',
  'wle-data-device-manager.c',
  'wle-embedding-manager.c',
  'wle-foreign-toplevel-manager.c',
  'wle-fractional-scale-forwarder.c',
  'wle-kde-decoration-forwarder.c',
  'wle-linux-dmabuf.c',
  'wle-local-resource.c',
  'wle-output.c',
  'wle-popup.c',
  'wle-positioner.c',
  'wle-presentation-time.c',
  'wle-primary-selection.c',
  'wle-private-types.c',
  'wle-produces-serials.c',
  'wle-shm-forwarder.c',
  'wle-subsurface.c',
  'wle-view-role.c',
  'wle-toplevel.c',
  'wle-util.c',
  'wle-view.c',
  'wle-viewporter.c',
  'wle-xdg-activation.c',
  'wle-xdg-decoration-forwarder.c',
  'wle-xdg-output-manager.c',
  'wle-xdg-shell.c',
  'wle-xdg-surface.c',
]

if have_ext_toplevel_list
  libwlembed_sources += 'wle-ext-foreign-toplevel-list.c'
endif

libwlembed_deps = [
  dl,
  glib,
  gobject,
  gio,
  libm,
  wl_client,
  wl_server,
  xkbcommon,
]

libwlembed = library(
  libwlembed_apiname,
  sources: libwlembed_public_sources + libwlembed_sources + libwlembed_generated_sources,
  version: lib_version,
  soversion: lib_so_version,
  include_directories: [
    include_directories('..'),
  ],
  gnu_symbol_visibility: gnu_symbol_visibility,
  dependencies: libwlembed_deps,
  install: true,
)

install_headers(
  libwlembed_headers,
  subdir: join_paths(root_libname, 'libwlembed'),
)

libwlembed_pkgname = 'lib@0@'.format(libwlembed_apiname)
pc.generate(
  libraries: [libwlembed],
  version: meson.project_version(),
  name: libwlembed_pkgname,
  filebase: libwlembed_pkgname,
  description: 'Library for creating an embedded/nested Wayland compositor',
  requires: ['glib-2.0', 'gobject-2.0'],
  subdirs: [root_libname],
  install_dir: get_option('prefix') / get_option('libdir') / 'pkgconfig',
)

if get_option('introspection')
  libwlembed_gir_sources = libwlembed_public_sources + libwlembed_headers

  libwlembed_gir = gnome.generate_gir(
    libwlembed,
    sources: libwlembed_gir_sources,
    namespace: 'Libwlembed',
    nsversion: '0.0',
    identifier_prefix: [
      'Libwlembed',
      'Wle',
    ],
    symbol_prefix: [
      'libwlembed',
      'wle',
    ],
    export_packages: libwlembed_pkgname,
    includes: [
      'GLib-2.0',
      'GObject-2.0',
      'Gio-2.0',
    ],
    header: 'libwlembed/libwlembed.h',
    install: true,
    extra_args: [
      '-DLIBWLEMBED_COMPILATION=1',
    ],
  )

  libwlembed_gir_dep = declare_dependency(sources: libwlembed_gir)
endif

libwlembed_dep = declare_dependency(
  dependencies: libwlembed_deps,
  link_with: libwlembed
)
