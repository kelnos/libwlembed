/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_EXT_FOREIGN_TOPLEVEL_LIST_H__
#define __WLE_EXT_FOREIGN_TOPLEVEL_LIST_H__

#include <glib.h>
#include <wayland-client-core.h>

#include "protocol/ext-foreign-toplevel-list-v1-client-protocol.h"

G_BEGIN_DECLS

typedef struct _WleExtForeignToplevelList WleExtForeignToplevelList;

WleExtForeignToplevelList *wle_ext_foreign_toplevel_list_new(struct wl_display *remote_display,
                                                             struct ext_foreign_toplevel_list_v1 *remote_toplevel_list,
                                                             struct wl_display *local_display);
void wle_ext_foreign_toplevel_list_destroy(WleExtForeignToplevelList *toplevel_list);

G_END_DECLS

#endif /* __WLE_EXT_FOREIGN_TOPLEVEL_LIST_H__ */
