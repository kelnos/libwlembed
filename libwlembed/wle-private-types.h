/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_PRIVATE_TYPES_H__
#define __WLE_PRIVATE_TYPES_H__

#include <glib.h>
#include <wayland-client-protocol.h>
#include <wayland-server-core.h>

#define wle_buffer_for_each_safe(buffer, tmp, list) \
    for (buffer = 0, tmp = 0, \
        buffer = wle_buffer_from_link((list)->next), \
        tmp = wle_buffer_from_link((list)->next->next); \
         wle_buffer_get_link(buffer) != (list); \
         buffer = tmp, \
        tmp = wle_buffer_from_link(wle_buffer_get_link(buffer)->next))

#define wle_region_for_each_safe(region, tmp, list) \
    for (region = 0, tmp = 0, \
        region = wle_region_from_link((list)->next), \
        tmp = wle_region_from_link((list)->next->next); \
         wle_region_get_link(region) != (list); \
         region = tmp, \
        tmp = wle_region_from_link(wle_region_get_link(region)->next))

typedef struct _WleBuffer WleBuffer;
typedef struct _WleRegion WleRegion;

WleBuffer *wle_buffer_new(struct wl_buffer *remote_buffer,
                          struct wl_resource *local_buffer);
struct wl_buffer *wle_buffer_get_remote_buffer(WleBuffer *buffer);
struct wl_resource *wle_buffer_get_local_buffer(WleBuffer *buffer);
void wle_buffer_set_forward_release(WleBuffer *buffer,
                                    gboolean forward_release);
struct wl_list *wle_buffer_get_link(WleBuffer *buffer);
WleBuffer *wle_buffer_from_link(struct wl_list *link);
WleBuffer *wle_buffer_ref(WleBuffer *buffer);
void wle_buffer_unref(WleBuffer *buffer);
void wle_buffer_destroy(WleBuffer *buffer);

WleRegion *wle_region_new(struct wl_region *remote_region,
                          struct wl_resource *local_region);
struct wl_region *wle_region_get_remote_region(WleRegion *region);
struct wl_resource *wle_region_get_local_region(WleRegion *region);
struct wl_list *wle_region_get_link(WleRegion *region);
WleRegion *wle_region_from_link(struct wl_list *link);
WleRegion *wle_region_ref(WleRegion *region);
void wle_region_unref(WleRegion *region);
void wle_region_destroy_local_region(WleRegion *region);

#endif /* __WLE_PRIVATE_TYPES_H__ */
