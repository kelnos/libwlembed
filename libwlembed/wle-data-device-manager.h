/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __WLE_DATA_DEVICE_MANAGER_H__
#define __WLE_DATA_DEVICE_MANAGER_H__

#include <glib-object.h>
#include <wayland-server-protocol.h>

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE(WleDataDeviceManager, wle_data_device_manager, WLE, DATA_DEVICE_MANAGER, GObject)
#define WLE_TYPE_DATA_DEVICE_MANAGER (wle_data_device_manager_get_type())

WleDataDeviceManager *wle_data_device_manager_new(struct wl_display *remote_display,
                                                  struct wl_data_device_manager *remote_manager,
                                                  struct wl_display *local_display);

G_END_DECLS

#endif /* __WLE_DATA_DEVICE_MANAGER_H__ */
