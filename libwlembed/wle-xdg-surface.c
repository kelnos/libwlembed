/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common/wle-debug.h"
#include "protocol/xdg-shell-protocol.h"
#include "wle-xdg-surface.h"

#define WLE_XDG_SURFACE_GET_PRIVATE(obj) ((WleXdgSurfacePrivate *)wle_xdg_surface_get_instance_private(WLE_XDG_SURFACE(obj)))

typedef struct _WleXdgSurfacePrivate {
    WleViewRole parent;

    WleView *view;
    struct xdg_surface *remote_xdg_surface;
    struct wl_resource *local_xdg_surface;

    struct {
        int32_t x;
        int32_t y;
        int32_t width;
        int32_t height;
    } geometry;

    int32_t width;
    int32_t height;
} WleXdgSurfacePrivate;

enum {
    PROP0,
    PROP_VIEW,
    PROP_REMOTE_XDG_SURFACE,
    PROP_LOCAL_XDG_SURFACE,
};

enum {
    SIG_GEOMETRY_CHANGED,

    N_SIGNALS,
};

static void wle_xdg_surface_constructed(GObject *object);
static void wle_xdg_surface_set_property(GObject *object,
                                         guint prop_id,
                                         const GValue *value,
                                         GParamSpec *pspec);
static void wle_xdg_surface_get_property(GObject *object,
                                         guint prop_id,
                                         GValue *value,
                                         GParamSpec *pspec);
static void wle_xdg_surface_finalize(GObject *object);

static gboolean wle_xdg_surface_destroy_remote_role(WleViewRole *view_role);

static void wle_xdg_surface_destroy(WleLocalResource *local_resource);


G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE(WleXdgSurface, wle_xdg_surface, WLE_TYPE_VIEW_ROLE)


static guint signals[N_SIGNALS] = { 0 };

static void
wle_xdg_surface_class_init(WleXdgSurfaceClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->constructed = wle_xdg_surface_constructed;
    gobject_class->set_property = wle_xdg_surface_set_property;
    gobject_class->get_property = wle_xdg_surface_get_property;
    gobject_class->finalize = wle_xdg_surface_finalize;

    g_object_class_install_property(gobject_class,
                                    PROP_VIEW,
                                    g_param_spec_object("view",
                                                        "view",
                                                        "view",
                                                        WLE_TYPE_VIEW,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gobject_class,
                                    PROP_REMOTE_XDG_SURFACE,
                                    g_param_spec_pointer("remote-xdg-surface",
                                                         "remote-xdg-surface",
                                                         "remote-xdg-surface",
                                                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gobject_class,
                                    PROP_LOCAL_XDG_SURFACE,
                                    g_param_spec_pointer("local-xdg-surface",
                                                         "local-xdg-surface",
                                                         "local-xdg-surface",
                                                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    WleViewRoleClass *view_role_class = WLE_VIEW_ROLE_CLASS(klass);
    view_role_class->destroy_remote_role = wle_xdg_surface_destroy_remote_role;

    WleLocalResourceClass *local_resource_class = WLE_LOCAL_RESOURCE_CLASS(klass);
    local_resource_class->destroy = wle_xdg_surface_destroy;

    signals[SIG_GEOMETRY_CHANGED] = g_signal_new("geometry-changed",
                                                 WLE_TYPE_XDG_SURFACE,
                                                 G_SIGNAL_RUN_LAST,
                                                 0,
                                                 NULL, NULL,
                                                 g_cclosure_marshal_VOID__VOID,
                                                 G_TYPE_NONE, 0);
}

static void
wle_xdg_surface_init(WleXdgSurface *xdg_surface) {}

static void
wle_xdg_surface_constructed(GObject *object) {
    G_OBJECT_CLASS(wle_xdg_surface_parent_class)->constructed(object);

    // local_xdg_surface takes a reference
    g_object_ref(object);
}

static void
wle_xdg_surface_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec) {
    WleXdgSurfacePrivate *xdg_surface = WLE_XDG_SURFACE_GET_PRIVATE(object);

    switch (prop_id) {
        case PROP_VIEW:
            xdg_surface->view = g_value_get_object(value);
            break;

        case PROP_REMOTE_XDG_SURFACE:
            xdg_surface->remote_xdg_surface = g_value_get_pointer(value);
            break;

        case PROP_LOCAL_XDG_SURFACE:
            xdg_surface->local_xdg_surface = g_value_get_pointer(value);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_xdg_surface_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec) {
    WleXdgSurfacePrivate *xdg_surface = WLE_XDG_SURFACE_GET_PRIVATE(object);

    switch (prop_id) {
        case PROP_VIEW:
            g_value_set_object(value, xdg_surface->view);
            break;

        case PROP_REMOTE_XDG_SURFACE:
            g_value_set_pointer(value, xdg_surface->remote_xdg_surface);
            break;

        case PROP_LOCAL_XDG_SURFACE:
            g_value_set_pointer(value, xdg_surface->local_xdg_surface);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
wle_xdg_surface_finalize(GObject *object) {
    // We don't free remote_xdg_surface and local_xdg_surface here, because,
    // depending on how we are being destroyed, ownership may revert to
    // WleXdgSurfaceHolder (in WleXdgShell).

    G_OBJECT_CLASS(wle_xdg_surface_parent_class)->finalize(object);
}

static gboolean
wle_xdg_surface_destroy_remote_role(WleViewRole *view_role) {
    WleXdgSurfacePrivate *xdg_surface = WLE_XDG_SURFACE_GET_PRIVATE(view_role);
    if (xdg_surface->remote_xdg_surface != NULL) {
        DBG("destroying remote xdg surface");
        xdg_surface_destroy(xdg_surface->remote_xdg_surface);
        xdg_surface->remote_xdg_surface = NULL;
    }
    return TRUE;
}

static void
wle_xdg_surface_destroy(WleLocalResource *local_resource) {
    WleXdgSurfacePrivate *xdg_surface = WLE_XDG_SURFACE_GET_PRIVATE(local_resource);

    if (xdg_surface->remote_xdg_surface != NULL) {
        xdg_surface_destroy(xdg_surface->remote_xdg_surface);
        xdg_surface->remote_xdg_surface = NULL;
    }

    if (xdg_surface->local_xdg_surface != NULL) {
        wl_resource_destroy(xdg_surface->local_xdg_surface);
        xdg_surface->local_xdg_surface = NULL;
    }

    WLE_LOCAL_RESOURCE_CLASS(wle_xdg_surface_parent_class)->destroy(local_resource);
}

WleView *
wle_xdg_surface_get_view(WleXdgSurface *xdg_surface) {
    g_return_val_if_fail(WLE_IS_XDG_SURFACE(xdg_surface), NULL);
    return WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface)->view;
}

struct xdg_surface *
wle_xdg_surface_get_remote_xdg_surface(WleXdgSurface *xdg_surface) {
    g_return_val_if_fail(WLE_IS_XDG_SURFACE(xdg_surface), NULL);
    return WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface)->remote_xdg_surface;
}

struct xdg_surface *
wle_xdg_surface_steal_remote_xdg_surface(WleXdgSurface *xdg_surface) {
    g_return_val_if_fail(WLE_IS_XDG_SURFACE(xdg_surface), NULL);
    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    struct xdg_surface *remote_xdg_surface = priv->remote_xdg_surface;
    priv->remote_xdg_surface = NULL;
    return remote_xdg_surface;
}


struct wl_resource *
wle_xdg_surface_steal_local_xdg_surface(WleXdgSurface *xdg_surface) {
    g_return_val_if_fail(WLE_IS_XDG_SURFACE(xdg_surface), NULL);
    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    struct wl_resource *local_xdg_surface = priv->local_xdg_surface;
    priv->local_xdg_surface = NULL;
    return local_xdg_surface;
}

void
wle_xdg_surface_update_size(WleXdgSurface *xdg_surface, int32_t width, int32_t height) {
    g_return_if_fail(WLE_IS_XDG_SURFACE(xdg_surface));
    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    priv->width = width;
    priv->height = height;
}

void
wle_xdg_surface_get_size(WleXdgSurface *xdg_surface, int32_t *width, int32_t *height) {
    g_return_if_fail(WLE_IS_XDG_SURFACE(xdg_surface));
    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    *width = priv->width;
    *height = priv->height;
}

void
wle_xdg_surface_set_window_geometry(WleXdgSurface *xdg_surface, int32_t x, int32_t y, int32_t width, int32_t height) {
    g_return_if_fail(WLE_IS_XDG_SURFACE(xdg_surface));

    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    if (priv->geometry.x != x
        || priv->geometry.y != y
        || priv->geometry.width != width
        || priv->geometry.height != height)
    {
        priv->geometry.x = x;
        priv->geometry.y = y;
        priv->geometry.width = width;
        priv->geometry.height = height;
        if (priv->remote_xdg_surface != NULL && width > 0 && height > 0) {
            xdg_surface_set_window_geometry(priv->remote_xdg_surface, x, y, width, height);
        }
        g_signal_emit(xdg_surface, signals[SIG_GEOMETRY_CHANGED], 0);
    }
}

void
wle_xdg_surface_get_window_geometry(WleXdgSurface *xdg_surface,
                                    int32_t *x,
                                    int32_t *y,
                                    int32_t *width,
                                    int32_t *height) {
    g_return_if_fail(WLE_IS_XDG_SURFACE(xdg_surface));

    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    if (x != NULL) {
        *x = priv->geometry.x;
    }
    if (y != NULL) {
        *y = priv->geometry.y;
    }
    if (width != NULL) {
        *width = priv->geometry.width;
    }
    if (height != NULL) {
        *height = priv->geometry.height;
    }
}


void
wle_xdg_surface_ack_configure(WleXdgSurface *xdg_surface, uint32_t serial) {
    g_return_if_fail(WLE_IS_XDG_SURFACE(xdg_surface));
    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    if (priv->remote_xdg_surface != NULL) {
        xdg_surface_ack_configure(priv->remote_xdg_surface, serial);
    }
    _wle_view_configured(priv->view, serial);
}

void
wle_xdg_surface_send_configure(WleXdgSurface *xdg_surface, uint32_t serial) {
    g_return_if_fail(WLE_IS_XDG_SURFACE(xdg_surface));
    WleXdgSurfacePrivate *priv = WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface);
    WleXdgSurfaceClass *klass = WLE_XDG_SURFACE_GET_CLASS(xdg_surface);
    if (priv->remote_xdg_surface == NULL) {
        if (klass->commit_state != NULL) {
            klass->commit_state(xdg_surface, serial);
        }
    }
    xdg_surface_send_configure(WLE_XDG_SURFACE_GET_PRIVATE(xdg_surface)->local_xdg_surface, serial);
}
