/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "protocol/xdg-shell-protocol.h"
#include "wle-positioner.h"

struct _WlePositioner {
    WleLocalResource parent;

    struct xdg_positioner *remote_positioner;
    struct wl_resource *local_positioner;

    struct {
        int32_t x;
        int32_t y;
        int32_t width;
        int32_t height;
    } local_anchor_rect;

    int32_t parent_offset_x;
    int32_t parent_offset_y;

    struct {
        int32_t x;
        int32_t y;
        int32_t width;
        int32_t height;
    } parent_geometry;
};

static void wle_positioner_finalize(GObject *object);

static void wle_positioner_destroy(WleLocalResource *local_resource);

static void local_positioner_set_anchor(struct wl_client *client,
                                        struct wl_resource *positioner_resource,
                                        uint32_t anchor);
static void local_positioner_set_anchor_rect(struct wl_client *client,
                                             struct wl_resource *positioner_resource,
                                             int32_t x,
                                             int32_t y,
                                             int32_t width,
                                             int32_t height);
static void local_positioner_set_gravity(struct wl_client *client,
                                         struct wl_resource *positioner_resource,
                                         uint32_t gravity);
static void local_positioner_set_offset(struct wl_client *client,
                                        struct wl_resource *positioner_resource,
                                        int32_t x,
                                        int32_t y);
static void local_positioner_set_size(struct wl_client *client,
                                      struct wl_resource *positioner_resource,
                                      int32_t width,
                                      int32_t height);
static void local_positioner_set_constraint_adjustment(struct wl_client *client,
                                                       struct wl_resource *positioner_resource,
                                                       uint32_t constraint_adjustment);
static void local_positioner_set_reactive(struct wl_client *client,
                                          struct wl_resource *positioner_resource);
static void local_positioner_set_parent_size(struct wl_client *client,
                                             struct wl_resource *positioner_resource,
                                             int32_t width,
                                             int32_t height);
static void local_positioner_set_parent_configure(struct wl_client *client,
                                                  struct wl_resource *positioner_resource,
                                                  uint32_t serial);
static void local_positioner_destroy(struct wl_client *client,
                                     struct wl_resource *positioner_resource);


G_DEFINE_FINAL_TYPE(WlePositioner, wle_positioner, WLE_TYPE_LOCAL_RESOURCE)


static const struct xdg_positioner_interface local_positioner_impl = {
    .set_anchor = local_positioner_set_anchor,
    .set_anchor_rect = local_positioner_set_anchor_rect,
    .set_gravity = local_positioner_set_gravity,
    .set_offset = local_positioner_set_offset,
    .set_size = local_positioner_set_size,
    .set_constraint_adjustment = local_positioner_set_constraint_adjustment,
    .set_reactive = local_positioner_set_reactive,
    .set_parent_size = local_positioner_set_parent_size,
    .set_parent_configure = local_positioner_set_parent_configure,
    .destroy = local_positioner_destroy,
};

static void
wle_positioner_class_init(WlePositionerClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    gobject_class->finalize = wle_positioner_finalize;

    WleLocalResourceClass *local_resource_class = WLE_LOCAL_RESOURCE_CLASS(klass);
    local_resource_class->destroy = wle_positioner_destroy;
}

static void
wle_positioner_init(WlePositioner *positioner) {}

static void
wle_positioner_finalize(GObject *object) {
    WlePositioner *positioner = WLE_POSITIONER(object);

    if (positioner->remote_positioner != NULL) {
        xdg_positioner_destroy(positioner->remote_positioner);
    }

    if (positioner->local_positioner != NULL) {
        wl_resource_destroy(positioner->local_positioner);
    }

    G_OBJECT_CLASS(wle_positioner_parent_class)->finalize(object);
}

static void
wle_positioner_destroy(WleLocalResource *local_resource) {
    WlePositioner *positioner = WLE_POSITIONER(local_resource);

    if (positioner->remote_positioner != NULL) {
        xdg_positioner_destroy(positioner->remote_positioner);
        positioner->remote_positioner = NULL;
    }

    if (positioner->local_positioner != NULL) {
        wl_resource_destroy(positioner->local_positioner);
        positioner->local_positioner = NULL;
    }

    WLE_LOCAL_RESOURCE_CLASS(wle_positioner_parent_class)->destroy(local_resource);
}

static void
local_positioner_set_anchor(struct wl_client *client, struct wl_resource *positioner_resource, uint32_t anchor) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    xdg_positioner_set_anchor(positioner->remote_positioner, anchor);
}

static void
local_positioner_set_anchor_rect(struct wl_client *client,
                                 struct wl_resource *positioner_resource,
                                 int32_t x,
                                 int32_t y,
                                 int32_t width,
                                 int32_t height) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    positioner->local_anchor_rect.x = x;
    positioner->local_anchor_rect.y = y;
    positioner->local_anchor_rect.width = width;
    positioner->local_anchor_rect.height = height;
    xdg_positioner_set_anchor_rect(positioner->remote_positioner,
                                   x + positioner->parent_offset_x,
                                   y + positioner->parent_offset_y,
                                   width,
                                   height);
}

static void
local_positioner_set_gravity(struct wl_client *client, struct wl_resource *positioner_resource, uint32_t gravity) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    xdg_positioner_set_gravity(positioner->remote_positioner, gravity);
}

static void
local_positioner_set_offset(struct wl_client *client, struct wl_resource *positioner_resource, int32_t x, int32_t y) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    xdg_positioner_set_offset(positioner->remote_positioner, x, y);
}

static void
local_positioner_set_size(struct wl_client *client,
                          struct wl_resource *positioner_resource,
                          int32_t width,
                          int32_t height) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    xdg_positioner_set_size(positioner->remote_positioner, width, height);
}

static void
local_positioner_set_constraint_adjustment(struct wl_client *client,
                                           struct wl_resource *positioner_resource,
                                           uint32_t constraint_adjustment) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    xdg_positioner_set_constraint_adjustment(positioner->remote_positioner, constraint_adjustment);
}

static void
local_positioner_set_reactive(struct wl_client *client, struct wl_resource *positioner_resource) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    xdg_positioner_set_reactive(positioner->remote_positioner);
}

static void
local_positioner_set_parent_size(struct wl_client *client,
                                 struct wl_resource *positioner_resource,
                                 int32_t width,
                                 int32_t height) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    if (wl_proxy_get_version((struct wl_proxy *)positioner->remote_positioner) >= XDG_POSITIONER_SET_PARENT_SIZE_SINCE_VERSION) {
        xdg_positioner_set_parent_size(positioner->remote_positioner,
                                       positioner->parent_geometry.width > 0 ? positioner->parent_geometry.width : width,
                                       positioner->parent_geometry.height > 0 ? positioner->parent_geometry.height : height);
    }
}

static void
local_positioner_set_parent_configure(struct wl_client *client,
                                      struct wl_resource *positioner_resource,
                                      uint32_t serial) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(positioner_resource));
    xdg_positioner_set_parent_configure(positioner->remote_positioner, serial);
}

static void
local_positioner_destroy(struct wl_client *client, struct wl_resource *positioner_resource) {
    wl_resource_destroy(positioner_resource);
}

static void
local_positioner_destroy_impl(struct wl_resource *resource) {
    WlePositioner *positioner = WLE_POSITIONER(wl_resource_get_user_data(resource));

    if (positioner->remote_positioner != NULL) {
        xdg_positioner_destroy(positioner->remote_positioner);
        positioner->remote_positioner = NULL;
    }

    positioner->local_positioner = NULL;
    wle_local_resource_destroyed(WLE_LOCAL_RESOURCE(positioner));
    g_object_unref(positioner);
}

static void
update_anchor_rect(WlePositioner *positioner) {
    if (positioner->remote_positioner != NULL
        && positioner->local_anchor_rect.width > 0
        && positioner->local_anchor_rect.height > 0)
    {
        xdg_positioner_set_anchor_rect(positioner->remote_positioner,
                                       positioner->local_anchor_rect.x + positioner->parent_offset_x - positioner->parent_geometry.x,
                                       positioner->local_anchor_rect.y + positioner->parent_offset_y - positioner->parent_geometry.y,
                                       positioner->local_anchor_rect.width,
                                       positioner->local_anchor_rect.height);
    }
}

WlePositioner *
wle_positioner_new(struct xdg_positioner *remote_positioner, struct wl_resource *local_positioner) {
    g_return_val_if_fail(remote_positioner != NULL, NULL);
    g_return_val_if_fail(local_positioner != NULL, NULL);

    WlePositioner *positioner = g_object_new(WLE_TYPE_POSITIONER, NULL);
    positioner->remote_positioner = remote_positioner;
    positioner->local_positioner = local_positioner;

    wl_resource_set_implementation(local_positioner,
                                   &local_positioner_impl,
                                   positioner,
                                   local_positioner_destroy_impl);

    // local_psitioner takes a reference
    g_object_ref(positioner);

    return positioner;
}

struct xdg_positioner *
wle_positioner_get_remote_positioner(WlePositioner *positioner) {
    g_return_val_if_fail(WLE_IS_POSITIONER(positioner), NULL);
    return positioner->remote_positioner;
}

void
wle_positioner_set_parent_offset(WlePositioner *positioner, int32_t x, int32_t y) {
    g_return_if_fail(WLE_IS_POSITIONER(positioner));

    if (positioner->parent_offset_x != x || positioner->parent_offset_y != y) {
        positioner->parent_offset_x = x;
        positioner->parent_offset_y = y;
        update_anchor_rect(positioner);
    }
}

void
wle_positioner_get_parent_offset(WlePositioner *positioner, int32_t *x, int32_t *y) {
    g_return_if_fail(WLE_IS_POSITIONER(positioner));
    if (x != NULL) {
        *x = positioner->parent_offset_x;
    }
    if (y != NULL) {
        *y = positioner->parent_offset_y;
    }
}

void
wle_positioner_set_parent_geometry(WlePositioner *positioner, int32_t x, int32_t y, int32_t width, int32_t height) {
    g_return_if_fail(WLE_IS_POSITIONER(positioner));
    g_return_if_fail(width >= 0);
    g_return_if_fail(height >= 0);

    if (positioner->parent_geometry.x != x
        || positioner->parent_geometry.y != y
        || positioner->parent_geometry.width != width
        || positioner->parent_geometry.height != height)
    {
        positioner->parent_geometry.x = x;
        positioner->parent_geometry.y = y;
        positioner->parent_geometry.width = width;
        positioner->parent_geometry.height = height;

        update_anchor_rect(positioner);

        if (positioner->parent_geometry.width > 0
            && positioner->parent_geometry.height > 0
            && positioner->remote_positioner != NULL
            && wl_proxy_get_version((struct wl_proxy *)positioner->remote_positioner) >= XDG_POSITIONER_SET_PARENT_SIZE_SINCE_VERSION)
        {
            xdg_positioner_set_parent_size(positioner->remote_positioner,
                                           positioner->parent_geometry.width,
                                           positioner->parent_geometry.height);
        }
    }
}

void
wle_positioner_get_parent_geometry(WlePositioner *positioner, int32_t *x, int32_t *y, int32_t *width, int32_t *height) {
    g_return_if_fail(WLE_IS_POSITIONER(positioner));
    if (x != NULL) {
        *x = positioner->parent_geometry.x;
    }
    if (y != NULL) {
        *y = positioner->parent_geometry.y;
    }
    if (width != NULL) {
        *width = positioner->parent_geometry.width;
    }
    if (height != NULL) {
        *height = positioner->parent_geometry.height;
    }
}
