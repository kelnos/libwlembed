/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <wayland-client-protocol.h>
#include <wayland-server-protocol.h>

#include "wle-private-types.h"
#include "wle-shm-forwarder.h"
#include "wle-util.h"

struct _WleShmForwarder {
    struct wl_display *remote_display;
    struct wl_shm *remote_shm;
    GArray *remote_formats;

    struct wl_global *local_shm;
    struct wl_list local_shm_resources;
    struct wl_list local_shm_pool_resources;

    struct wl_list buffers;  // WleBuffer
};

typedef struct {
    WleShmForwarder *forwarder;
    struct wl_shm_pool *remote_pool;
    struct wl_resource *local_pool;
} WleShmPool;

static void remote_shm_format(void *data,
                              struct wl_shm *wl_shm,
                              uint32_t format);

static void local_shm_create_pool(struct wl_client *client,
                                  struct wl_resource *shm_resource,
                                  uint32_t id,
                                  int fd,
                                  int32_t size);

#if defined(WL_SHM_RELEASE_SINCE_VERSION)
static void local_shm_release(struct wl_client *client,
                              struct wl_resource *shm_resource);
#endif

static void local_shm_pool_create_buffer(struct wl_client *client,
                                         struct wl_resource *shm_pool_resource,
                                         uint32_t id,
                                         int32_t offset,
                                         int32_t width,
                                         int32_t height,
                                         int32_t stride,
                                         uint32_t format);
static void local_shm_pool_resize(struct wl_client *client,
                                  struct wl_resource *shm_pool_resource,
                                  int32_t size);
static void local_shm_pool_destroy(struct wl_client *client,
                                   struct wl_resource *shm_pool_resource);

static const struct wl_shm_listener remote_shm_listener = {
    .format = remote_shm_format,
};

static const struct wl_shm_interface local_shm_impl = {
    .create_pool = local_shm_create_pool,
#if defined(WL_SHM_RELEASE_SINCE_VERSION)
    .release = local_shm_release,
#endif
};

static const struct wl_shm_pool_interface local_shm_pool_impl = {
    .create_buffer = local_shm_pool_create_buffer,
    .resize = local_shm_pool_resize,
    .destroy = local_shm_pool_destroy,
};

static void
remote_shm_format(void *data, struct wl_shm *shm, uint32_t format) {
    WleShmForwarder *forwarder = data;
    g_array_append_val(forwarder->remote_formats, format);

    struct wl_resource *local_shm;
    wl_resource_for_each(local_shm, &forwarder->local_shm_resources) {
        wl_shm_send_format(local_shm, format);
    }
}

static void
local_shm_pool_create_buffer(struct wl_client *client,
                             struct wl_resource *shm_pool_resource,
                             uint32_t buffer_id,
                             int32_t offset,
                             int32_t width,
                             int32_t height,
                             int32_t stride,
                             uint32_t format) {
    WleShmPool *pool = wl_resource_get_user_data(shm_pool_resource);

    struct wl_buffer *remote_buffer = wl_shm_pool_create_buffer(pool->remote_pool,
                                                                offset,
                                                                width,
                                                                height,
                                                                stride,
                                                                format);
    if (remote_buffer == NULL) {
        wle_propagate_remote_error(pool->forwarder->remote_display, client, shm_pool_resource);
    } else {
        _wle_proxy_set_ours((struct wl_proxy *)remote_buffer);

        struct wl_resource *resource = wl_resource_create(client,
                                                          &wl_buffer_interface,
                                                          wl_resource_get_version(shm_pool_resource),
                                                          buffer_id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
            wl_buffer_destroy(remote_buffer);
        } else {
            WleBuffer *buffer = wle_buffer_new(remote_buffer, resource);
            wl_list_insert(&pool->forwarder->buffers, wle_buffer_get_link(buffer));
        }
    }
}

static void
local_shm_pool_resize(struct wl_client *client, struct wl_resource *shm_pool_resource, int32_t size) {
    WleShmPool *pool = wl_resource_get_user_data(shm_pool_resource);
    wl_shm_pool_resize(pool->remote_pool, size);
}

static void
local_shm_pool_destroy(struct wl_client *client, struct wl_resource *shm_pool_resource) {
    wl_resource_destroy(shm_pool_resource);
}

static void
local_shm_pool_destroy_impl(struct wl_resource *resource) {
    WleShmPool *pool = wl_resource_get_user_data(resource);

    if (pool->remote_pool != NULL) {
        wl_shm_pool_destroy(pool->remote_pool);
    }

    wl_list_remove(wl_resource_get_link(resource));
    g_free(pool);
}

static void
local_shm_create_pool(struct wl_client *client,
                      struct wl_resource *shm_resource,
                      uint32_t id,
                      int fd,
                      int32_t size) {
    WleShmForwarder *forwarder = wl_resource_get_user_data(shm_resource);
    struct wl_shm_pool *remote_pool = wl_shm_create_pool(forwarder->remote_shm, fd, size);
    if (remote_pool == NULL) {
        wle_propagate_remote_error(forwarder->remote_display, client, shm_resource);
    } else {
        _wle_proxy_set_ours((struct wl_proxy *)remote_pool);

        struct wl_resource *resource = wl_resource_create(client,
                                                          &wl_shm_pool_interface,
                                                          wl_resource_get_version(shm_resource),
                                                          id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
            wl_shm_pool_destroy(remote_pool);
        } else {
            WleShmPool *pool = g_new0(WleShmPool, 1);
            pool->forwarder = forwarder;
            pool->remote_pool = remote_pool;
            pool->local_pool = resource;
            wl_resource_set_implementation(resource, &local_shm_pool_impl, pool, local_shm_pool_destroy_impl);
            wl_list_insert(&forwarder->local_shm_pool_resources, wl_resource_get_link(resource));
        }
    }

    close(fd);
}

#if defined(WL_SHM_RELEASE_SINCE_VERSION)
static void
local_shm_release(struct wl_client *client, struct wl_resource *shm_resource) {
    wl_resource_destroy(shm_resource);
}
#endif

static void
local_shm_destroy_impl(struct wl_resource *resource) {
    wl_list_remove(wl_resource_get_link(resource));
}

static void
local_shm_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WleShmForwarder *forwarder = data;
    struct wl_resource *resource = wl_resource_create(client, &wl_shm_interface, version, id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource, &local_shm_impl, forwarder, local_shm_destroy_impl);
        if (forwarder == NULL) {
            wl_list_init(wl_resource_get_link(resource));
        } else {
            wl_list_insert(&forwarder->local_shm_resources, wl_resource_get_link(resource));
        }

        for (guint i = 0; i < forwarder->remote_formats->len; ++i) {
            wl_shm_send_format(resource, g_array_index(forwarder->remote_formats, uint32_t, i));
        }
    }
}

WleShmForwarder *
wle_shm_forwarder_new(struct wl_display *remote_display,
                      struct wl_shm *remote_shm,
                      struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_shm != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WleShmForwarder *forwarder = g_new0(WleShmForwarder, 1);
    int version = wl_proxy_get_version((struct wl_proxy *)remote_shm);
    ;
    forwarder->local_shm = wl_global_create(local_display,
                                            &wl_shm_interface,
                                            version,
                                            forwarder,
                                            local_shm_bind);
    if (forwarder->local_shm == NULL) {
        wl_shm_destroy(remote_shm);
        g_free(forwarder);
        return NULL;
    } else {
        forwarder->remote_display = remote_display;
        forwarder->remote_shm = remote_shm;
        forwarder->remote_formats = g_array_new(FALSE, TRUE, sizeof(uint32_t));
        wl_list_init(&forwarder->local_shm_resources);
        wl_list_init(&forwarder->local_shm_pool_resources);
        wl_list_init(&forwarder->buffers);

        wl_shm_add_listener(remote_shm, &remote_shm_listener, forwarder);
        wl_display_roundtrip(remote_display);

        return forwarder;
    }
}

void
wle_shm_forwarder_destroy(WleShmForwarder *forwarder) {
    if (forwarder != NULL) {
        WleBuffer *buffer, *tmp_buffer;
        wle_buffer_for_each_safe(buffer, tmp_buffer, &forwarder->buffers) {
            wle_buffer_destroy(buffer);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &forwarder->local_shm_pool_resources) {
            wl_resource_destroy(resource);
        }
        wl_resource_for_each_safe(resource, tmp_resource, &forwarder->local_shm_resources) {
            wl_resource_destroy(resource);
        }

        wl_global_destroy(forwarder->local_shm);

        wl_shm_destroy(forwarder->remote_shm);
        g_array_free(forwarder->remote_formats, TRUE);

        g_free(forwarder);
    }
}
