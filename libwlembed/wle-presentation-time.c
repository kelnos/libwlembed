/*
 * libwlembed - a Wayland embedded compositor library
 *
 * Copyright (C) 2023-2024 Brian J. Tarricone <brian@tarricone.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "protocol/presentation-time-protocol.h"
#include "wle-output.h"
#include "wle-presentation-time.h"
#include "wle-util.h"
#include "wle-view.h"

struct _WlePresentationTime {
    struct wl_display *remote_display;
    struct wp_presentation *remote_presentation;

    struct wl_global *local_presentation;
    struct wl_list local_presentation_resources;

    uint32_t remote_clock_id;

    struct wl_list feedbacks;  // WlePresentationFeedback
};

typedef struct {
    struct wp_presentation_feedback *remote_feedback;
    struct wl_resource *local_feedback;
    struct wl_list link;
} WlePresentationFeedback;

static void remote_feedback_sync_output(void *data,
                                        struct wp_presentation_feedback *remote_feedback,
                                        struct wl_output *remote_output);
static void remote_feedback_presented(void *data,
                                      struct wp_presentation_feedback *remote_feedback,
                                      uint32_t tv_sec_hi,
                                      uint32_t tv_sec_lo,
                                      uint32_t tv_nsec,
                                      uint32_t refresh,
                                      uint32_t seq_hi,
                                      uint32_t seq_lo,
                                      uint32_t flags);
static void remote_feedback_discarded(void *data,
                                      struct wp_presentation_feedback *remote_feedback);

static void remote_presentation_clock_id(void *data,
                                         struct wp_presentation *remote_presentation,
                                         uint32_t clock_id);

static void local_presentation_feedback(struct wl_client *client,
                                        struct wl_resource *presentation_resource,
                                        struct wl_resource *surface_resource,
                                        uint32_t feedback_id);
static void local_presentation_destroy(struct wl_client *client,
                                       struct wl_resource *presentation_resource);

static const struct wp_presentation_feedback_listener remote_feedback_listener = {
    .sync_output = remote_feedback_sync_output,
    .presented = remote_feedback_presented,
    .discarded = remote_feedback_discarded,
};

static const struct wp_presentation_listener remote_presentation_listener = {
    .clock_id = remote_presentation_clock_id,
};

static const struct wp_presentation_interface local_presentation_impl = {
    .feedback = local_presentation_feedback,
    .destroy = local_presentation_destroy,
};

static void
remote_feedback_sync_output(void *data,
                            struct wp_presentation_feedback *remote_feedback,
                            struct wl_output *remote_output) {
    WlePresentationFeedback *feedback = data;
    WleOutput *output = _wle_proxy_get_user_data((struct wl_proxy *)remote_feedback);

    if (WLE_IS_OUTPUT(output)) {
        GList *local_outputs = wle_output_get_local_outputs(output, wl_resource_get_client(feedback->local_feedback));
        for (GList *l = local_outputs; l != NULL; l = l->next) {
            struct wl_resource *output_resource = l->data;
            wp_presentation_feedback_send_sync_output(feedback->local_feedback, output_resource);
        }
        g_list_free(local_outputs);
    }
}

static void
remote_feedback_presented(void *data,
                          struct wp_presentation_feedback *remote_feedback,
                          uint32_t tv_sec_hi,
                          uint32_t tv_sec_lo,
                          uint32_t tv_nsec,
                          uint32_t refresh,
                          uint32_t seq_hi,
                          uint32_t seq_lo,
                          uint32_t flags) {
    WlePresentationFeedback *feedback = data;
    wp_presentation_feedback_send_presented(feedback->local_feedback,
                                            tv_sec_hi,
                                            tv_sec_lo,
                                            tv_nsec,
                                            refresh,
                                            seq_hi,
                                            seq_lo,
                                            flags);
}

static void
remote_feedback_discarded(void *data, struct wp_presentation_feedback *remote_feedback) {
    WlePresentationFeedback *feedback = data;
    wp_presentation_feedback_send_discarded(feedback->local_feedback);
}

static void
local_feedback_destroy_impl(struct wl_resource *feedback_resource) {
    WlePresentationFeedback *feedback = wl_resource_get_user_data(feedback_resource);
    wp_presentation_feedback_destroy(feedback->remote_feedback);
    wl_list_remove(&feedback->link);
    g_free(feedback);
}

static void
remote_presentation_clock_id(void *data, struct wp_presentation *remote_presentation, uint32_t clock_id) {
    WlePresentationTime *presentation = data;
    presentation->remote_clock_id = clock_id;

    struct wl_resource *resource;
    wl_resource_for_each(resource, &presentation->local_presentation_resources) {
        wp_presentation_send_clock_id(resource, clock_id);
    }
}

static void
local_presentation_feedback(struct wl_client *client,
                            struct wl_resource *presentation_resource,
                            struct wl_resource *surface_resource,
                            uint32_t feedback_id) {
    WlePresentationTime *presentation = wl_resource_get_user_data(presentation_resource);
    WleView *view = surface_resource != NULL ? WLE_VIEW(wl_resource_get_user_data(surface_resource)) : NULL;

    if (view == NULL) {
        wl_resource_post_error(presentation_resource,
                               WL_DISPLAY_ERROR_INVALID_OBJECT,
                               "passed null or unknown surface");
    } else {
        struct wl_resource *resource = wl_resource_create(client,
                                                          &wp_presentation_feedback_interface,
                                                          wl_resource_get_version(presentation_resource),
                                                          feedback_id);
        if (resource == NULL) {
            wl_client_post_no_memory(client);
        } else {
            struct wp_presentation_feedback *remote_feedback = wp_presentation_feedback(presentation->remote_presentation,
                                                                                        wle_view_get_remote_surface(view));
            if (remote_feedback == NULL) {
                wle_propagate_remote_error(presentation->remote_display, client, presentation_resource);
            } else {
                WlePresentationFeedback *feedback = g_new0(WlePresentationFeedback, 1);
                feedback->remote_feedback = _wle_proxy_set_ours((struct wl_proxy *)remote_feedback);
                feedback->local_feedback = resource;
                wl_list_insert(&presentation->feedbacks, &feedback->link);
                wl_resource_set_implementation(resource, NULL, feedback, local_feedback_destroy_impl);
                wp_presentation_feedback_add_listener(remote_feedback, &remote_feedback_listener, feedback);
            }
        }
    }
}

static void
local_presentation_destroy(struct wl_client *client, struct wl_resource *presentation_resource) {
    wl_resource_destroy(presentation_resource);
}

static void
local_presentation_destroy_impl(struct wl_resource *presentation_resource) {
    wl_list_remove(wl_resource_get_link(presentation_resource));
}

static void
local_presentation_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id) {
    WlePresentationTime *presentation = data;
    struct wl_resource *resource = wl_resource_create(client,
                                                      &wp_presentation_interface,
                                                      version,
                                                      id);
    if (resource == NULL) {
        wl_client_post_no_memory(client);
    } else {
        wl_resource_set_implementation(resource,
                                       &local_presentation_impl,
                                       presentation,
                                       local_presentation_destroy_impl);
        if (presentation == NULL) {
            wl_list_init(wl_resource_get_link(resource));
        } else {
            wl_list_insert(&presentation->local_presentation_resources, wl_resource_get_link(resource));
            wp_presentation_send_clock_id(resource, presentation->remote_clock_id);
        }
    }
}

WlePresentationTime *
wle_presentation_time_new(struct wl_display *remote_display,
                          struct wp_presentation *remote_presentation,
                          struct wl_display *local_display) {
    g_return_val_if_fail(remote_display != NULL, NULL);
    g_return_val_if_fail(remote_presentation != NULL, NULL);
    g_return_val_if_fail(local_display != NULL, NULL);

    WlePresentationTime *presentation = g_new0(WlePresentationTime, 1);
    presentation->local_presentation = wl_global_create(local_display,
                                                        &wp_presentation_interface,
                                                        wl_proxy_get_version((struct wl_proxy *)remote_presentation),
                                                        presentation,
                                                        local_presentation_bind);
    if (presentation->local_presentation == NULL) {
        wp_presentation_destroy(remote_presentation);
        g_free(presentation);
        return NULL;
    } else {
        presentation->remote_display = remote_display;
        presentation->remote_presentation = remote_presentation;
        wl_list_init(&presentation->local_presentation_resources);
        wl_list_init(&presentation->feedbacks);
        wp_presentation_add_listener(remote_presentation, &remote_presentation_listener, presentation);
        return presentation;
    }
}

void
wle_presentation_time_destroy(WlePresentationTime *presentation_time) {
    if (presentation_time != NULL) {
        WlePresentationFeedback *feedback, *tmp_feedback;
        wl_list_for_each_safe(feedback, tmp_feedback, &presentation_time->feedbacks, link) {
            wl_resource_destroy(feedback->local_feedback);
        }

        struct wl_resource *resource, *tmp_resource;
        wl_resource_for_each_safe(resource, tmp_resource, &presentation_time->local_presentation_resources) {
            wl_resource_destroy(resource);
        }
        wl_global_destroy(presentation_time->local_presentation);

        wp_presentation_destroy(presentation_time->remote_presentation);

        g_free(presentation_time);
    }
}
